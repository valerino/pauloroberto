#!/usr/bin/env bash
# build the agent and plugins using cmake

# print usage
function usage() {
    echo "usage: $1 <debug|release|clean> [--rebuild]"
}

# perform build
# $1=rebuild(0/1)
# $2=32/64
# $3=build configuration
function build() {
    _BUILDFOLDER=./cmake-build-$3
    if [ $2 = "32" ]; then
        _BUILDFOLDER=./cmake-build-$332
    fi
    if [ $1 -eq 1 ]; then
        # rebuild, delete build folder
        rm -rf $_BUILDFOLDER
    fi

    # run cmake and generate the makefile if needed
    mkdir -p $_BUILDFOLDER
    _PWD=$PWD
    cd $_BUILDFOLDER
    if [ $2 = "32" ]; then
        cmake -DCMAKE_BUILD_TYPE=$3 -DBUILD_32BIT=1 ..
    else
        cmake -DCMAKE_BUILD_TYPE=$3 ..
    fi

    if [ $? -ne 0 ]; then
        echo "** ERROR in cmake ($2,$3) **"
        cd $_PWD
        exit 1
    fi

    # perform the actual make
    make
    if [ $? -ne 0 ]; then
        echo "** ERROR in make ($2,$3) **"
        cd $_PWD
        exit 1
    fi

    cd $_PWD
}

# check commandline
if [ $# -lt 1 ]; then
    usage $0
    exit 1
fi

# check for clean
if [ $1 = "clean" ]; then
    # delete all build folders
    rm -rf out
    rm -rf cmake-build-*
    echo "cleanup done!"
    exit 0
fi

# check configuration to build
if [ "$1" = "debug" ]; then
    _BUILDCONFIG=debug
elif [ "$1" = "release" ]; then
    _BUILDCONFIG=minsizerel
else
    usage $0
    exit 1
fi

# check for rebuild
_REBUILD=0
if [ $# -eq 2 ]; then
    if [ "$2" = "--rebuild" ]; then
        # rebuild
        _REBUILD=1
    fi
fi

# patch arduinojson cmakelist to disable testing
sed -i 's/enable_testing()/#/' libs/ArduinoJson/CMakeLists.txt
sed -i 's/add_subdirectory(test)/#/' libs/ArduinoJson/CMakeLists.txt

# build 64 and 32bit
build $_REBUILD 64 $_BUILDCONFIG
build $_REBUILD 32 $_BUILDCONFIG

# build the documentation
doxygen ./rcslinux.doxy

