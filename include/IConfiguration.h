//
// Created by jrf/ht on 14/02/19.
//

#ifndef RCSLINUX_ICONFIGURATION_H
#define RCSLINUX_ICONFIGURATION_H

#include <stdint.h>
namespace BdUtils {
    class CJson;
    class CMutex;
}

/**
 * AgentCore groups all public agent interfaces
 */
namespace AgentCore {
    /**
     * @interface IConfiguration
     * @brief exposes the configuration manager @ref AgentCorePrivate::CConfigManager to the plugins
     */
    class IConfiguration {
    public:
        /**
         * get the configuration file path
         * @see @ref AgentCorePrivate::CBinaryPatch::cfgFileName
         * @return const char*
         */
        virtual const char* cfgPath() = 0;
        /**
         * lock to access the configuration
         * @return CMutex
         */
        virtual BdUtils::CMutex& configLock() = 0;

        /**
         * get the configuration node for the given plugin
         * @param name the plugin name
         * @return a JsonObject, check with JsonObject::success() for correctness
         */
        virtual JsonObject &plugin(const char *name) = 0;

        /**
         * get the configuration core node
         * @return JsonObject&
         */
        virtual JsonObject &core() = 0;

        /**
         * get the plugins node
         * @return JsonObject&
         */
        virtual JsonObject &plugins() = 0;

        /**
         * updates the configuration on-disk, and notify (reinitializes) all loaded plugins
         * @note the configuration is decrypted first with the configuration(fallback) key, the JSON is parsed for structure correctness only, and fails if invalid
         * @param blob the configuration encrypted buffer
         * @param size blob size
         * @return 0 on success, or errno
         */
        virtual int update(const uint8_t *blob, size_t size) = 0;

        /**
         * updates the configuration on-disk, and notify (reinitializes) all loaded plugins
         * @note the JSON is parsed for structure correctness only, and fails if invalid
         * @param json the json configuration buffer
         * @return 0 on success, or errno
         */
        virtual int update(const char* json) = 0;
    };
}
#endif //RCSLINUX_ICONFIGURATION_H
