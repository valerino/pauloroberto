"""
utilities used by backend tools
jrf, 2k19
"""

import binascii
import zlib

import Crypto
from Crypto.Cipher import AES, PKCS1_v1_5
from Crypto.PublicKey import RSA
from Crypto.Util import Counter
import uuid


def intFromBytes(data, big_endian=False):
    """
    emulates python 3.x int.from_bytes
    :param data: a byte array
    :param big_endian: True for big endian
    :return:
    """
    if isinstance(data, str):
        data = bytearray(data)
    if big_endian:
        data = reversed(data)

    num = 0
    for offset, byte in enumerate(data):
        num += byte << (offset * 8)
    return num


def buildRandomIvHexString():
    """
    build a random 16 bytes IV hex string (32 characters)
    :return:
    """
    return uuid.uuid4().hex


def buildRandomKeyHexString():
    """
    build a random 32 bytes hex string (64 characters)
    :return:
    """
    part1 = uuid.uuid4().hex
    part2 = uuid.uuid4().hex
    return part1 + part2


def fileFromBuffer(path, data):
    """
    creates/overwrite a file with the given data
    :param path: path to the file to create/overwrite
    :param data: data to write, i.e. a bytes array
    :return:
    """
    f = open(path, 'wb')
    f.write(data)
    f.close()


def fileToBuffer(path):
    """
    read whole file to buffer
    :param path: path to the file to create/overwrite
    :return: bytearray
    """
    f = open(path, 'rb')
    data = f.read()
    f.close()
    return data


def encryptGeneric(data, key, iv=None):
    """
    encrypts a buffer from/to agent
    :param data the data to be encrypted
    :param key aes 32 byte key as hex string
    :paramn iv optional 16 bytes iv, default to generate randomly
    :return: bytearray, 16bytes IV + AES256/ctr/nopadding(uint32_t_uncompressed_size + deflate(buffer))
    """

    # generate random iv
    if iv is None:
        iv = buildRandomIvHexString()

    # compress
    originalSize = len(data)
    compressed = zlib.compress(data, 9)
    print('\t[-] compression, originalSize=%d, compressedSize=%d' % (originalSize, len(compressed)))

    # generates the buffer to be encrypted(originalsize+compressed)
    tmp = bytearray()
    tmp.extend(originalSize.to_bytes(4, byteorder="little"))
    tmp.extend(compressed)

    # encrypt
    print('\t[-] encryption, key=%s, iv=%s' % (key, iv))
    initial = int.from_bytes(binascii.unhexlify(iv), "big")
    nonce = Counter.new(128, initial_value=initial)
    crypter = AES.new(binascii.unhexlify(key), AES.MODE_CTR, counter=nonce)
    encrypted = crypter.encrypt(bytes(tmp))

    # generates the output buffer (iv+aes(deflate(data)))
    out=bytearray()
    out.extend(binascii.unhexlify(iv))
    out.extend(encrypted)
    print('\t[-] generated encrypted asset buffer, size=%d' % len(out))
    return out


def encryptGenericFromFile(path, key, iv=None):
    """
    encrypts a buffer from/to agent
    :param path input file
    :param key aes 32 byte key as hex string
    :paramn iv optional 16 bytes iv, default to generate randomly
    :return: bytearray, 16bytes IV + AES256/ctr/nopadding(uint32_t_uncompressed_size + deflate(buffer))
    """

    # read file
    buf = fileToBuffer(path)

    # encrypt
    b = encryptGeneric(buf, key, iv)
    return b


def decryptGeneric(data, key, iv=None):
    """
    decrypts a buffer from/to agent
    :param data the data to decrypt, 16bytes IV + AES256/ctr/nopadding(uint32_t_uncompressed_size + deflate(buffer))
    :param key aes 32 byte key as hex string
    :param iv if not None, it's taken from the first 16 bytes of data
    :return: bytearray
    """

    if iv is None:
        # first 16 bytes is the iv
        iv = data[:16]
        encrypted = data[16:]
    else:
        # iv provided
        encrypted = data

    ivString = binascii.hexlify(iv)

    # decrypt
    print('\t[-] decryption, key=%s, iv=%s' % (key, ivString))
    initial = int.from_bytes(iv, "big")
    nonce = Counter.new(128, initial_value=initial)
    crypter = AES.new(binascii.unhexlify(key), AES.MODE_CTR, counter=nonce)
    decrypted = crypter.decrypt(encrypted)
    originalSize = int.from_bytes(decrypted[:4], "little")

    # skip the originalsize (4 bytes)
    decrypted = decrypted[4:]

    # decompress
    out = zlib.decompress(decrypted)
    print('\t[-] decompression, originalSize=%d' % (len(decrypted)))
    print('\t[-] generated decrypted buffer, size=%d' % len(out))
    return out


def decryptGenericFromFile(path, key, iv=None):
    """
    decrypts a buffer from/to agent
    :param path path to the file to be decrypted, must contain 16bytes IV + AES256/ctr/nopadding(uint32_t_uncompressed_size + deflate(buffer))
    :param key aes 32 byte key as hex string
    :param iv if not None, it's taken from the first 16 bytes of data
    :return: bytearray
    """

    # read file to buffer
    buf = fileToBuffer(path)
    return decryptGeneric(buf, key, iv)


def decryptEvidenceFromFile(path, rsaPath):
    """
    decrypt evidence as sent by the agent to the backend
    :param data the data to decrypt, rsa(key+iv) + AES256/ctr/nopadding(uint32_t_uncompressed_size + deflate(buffer))
    :param rsaPath path to the backend RSA key
    :return: bytearray containing json file
    """

    # read file to buffer
    buf = fileToBuffer(path)

    # read rsa key
    with open(rsaPath, 'r') as f:
        rsakBuf = f.read();
        rsak = RSA.import_key(rsakBuf)
        print('\t[-] read RSA key=%s, bits=%d' % (rsaPath, rsak.size_in_bits()))

    # decrypt aes key and iv
    # with PKCS#v1.5 padding, if the result is = sentinel if decryption fails.
    # we do not check, since the next code will fail anyway if the RSA decryption fails.
    sentinel = Crypto.Random.get_random_bytes(32+16)
    sizeAesIv = int(rsak.size_in_bits() / 8)
    aesIvRsaBuffer = buf[:sizeAesIv]
    pkcs =PKCS1_v1_5.new(rsak)
    aesIvBuf = pkcs.decrypt(aesIvRsaBuffer, sentinel)

    # decrypt data
    k = aesIvBuf[:32]
    iv = aesIvBuf[32:]
    data = buf[sizeAesIv:]
    ks = binascii.hexlify(k)
    decrypted = decryptGeneric(data, ks, iv)
    return decrypted


def encryptEvidence(path, rsaPath):
    """
    encrypt evidence as the agent would send to the backend
    :param path path to a json file
    :param rsaPath path to the backend RSA key
    :return: bytearray, rsa(key+iv) + AES256/ctr/nopadding(uint32_t_uncompressed_size + deflate(buffer))
    """

    # read rsa key
    with open(rsaPath, 'r') as f:
        rsakBuf = f.read();
        rsak = RSA.import_key(rsakBuf)
        print('\t[-] read RSA key, bits=%d' % rsak.size_in_bits())

    # generate random key and iv
    aesKeyIv = Crypto.Random.get_random_bytes(32+16)
    k = aesKeyIv[:32]
    iv = aesKeyIv[32:]

    # rsa encrypt key + iv
    pkcs =PKCS1_v1_5.new(rsak)
    aesIvBuf = pkcs.encrypt(aesKeyIv)

    # encrypt data
    ks = binascii.hexlify(k)
    encrypted = encryptGenericFromFile(path, ks, iv)

    # return full buffer (rsa + encrypted data)
    b = aesKeyIv + encrypted
    return b
