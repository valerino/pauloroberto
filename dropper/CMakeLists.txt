# dropper is always built 32bit
project(dropper32)

add_executable(${PROJECT_NAME}
        dropper.cpp
        )

set (PRJ ${PROJECT_NAME})

# needs 32bit flags
set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -m32")

target_link_libraries(${PROJECT_NAME}
        bdutils
        )

# copy built binary to ./out directory
add_custom_command(TARGET ${PRJ} POST_BUILD
        COMMAND
        mkdir -p ${OUT_DIR}
        COMMAND
        cp $<TARGET_FILE:${PRJ}> ${OUT_DIR}/${PRJ}
        )
