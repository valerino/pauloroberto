//
// Created by jrf/ht on 21/02/19.
//

#include <bdutils.h>
#include "CCoreUtils.h"
#include "CAppContext.h"
using namespace BdUtils;
using namespace AgentCorePrivate;

int AgentCorePrivate::CCoreUtils::writeEncryptedWithDeviceKey(const char *path, uint8_t *buf, size_t size) {
    if (!path || !buf || !size) {
        return EINVAL;
    }

    // generate a random iv
    uint8_t iv[16];
    CAppContext::instance()->rng()->randBytes(iv,sizeof(iv));

    int res = 0;
    uint8_t* compressed = nullptr;
    size_t compressedSize = 0;
    do {
        // open file for overwriting
        CFile f(path);
        res = f.open("wb");
        if (res != 0) {
            break;
        }

        // compress buffer, the output buffer is already prepended with uint32_t size
        res = CZLib::compress(buf,size,&compressed,&compressedSize);
        if (res != 0) {
            break;
        }

        // encrypt with origsize+compressed with the device key
#ifndef NDEBUG
        char* aIv;
        char* aDk;
        CString::toHexString(iv,sizeof(iv),&aIv);
        CString::toHexString(CAppContext::instance()->deviceKey(), DEVICE_ID_SIZE, &aDk);
        DBGLOGV("encrypting buffer size=%d, devicekey=%s, iv=%s", compressedSize, aDk, aIv);
        CMemory::free(aIv);
        CMemory::free(aDk);
#endif
        res = CAES::encryptCtr(CAppContext::instance()->deviceKey(), DEVICE_ID_SIZE * 8, iv, compressed, compressedSize, &compressed);
        if (res != 0) {
            break;
        }

        // write iv
        res = f.write(iv,sizeof(iv));
        if (res != 0) {
            break;
        }

        // write data
        res = f.write(compressed, compressedSize);
        if (res != 0) {
            break;
        }
        DBGLOGV("%s rewritten encrypted with the device key", path);
    } while (0);

    if (res != 0) {
        DBGLOGE("can't rewrite %s, errno=%d (%s)", path, res, CString::fromErrno(res));
    }
    CMemory::free(compressed);
    return res;
}

/**
 * try to decrypt/decompress the given buffer and check if it returns an executable type
 * @param buf the encrypted(aes256/ctr/nopadding)/compressed(deflate) buffer
 * @param size size of the encrypted buffer
 * @param out on successful return, the decrypted/decompressed buffer to be freed with CMemory::free()
 * @param outSize on successful return, size of the decrypted/decompressed buffer
 * @param key the aes key
 * @param keyBits bits of the key (128/192/256)
 * @param iv the iv (16 bytes)
 * @param flags may specify READ_ENCRYPTED_CHECK_EXECUTABLE or READ_ENCRYPTED_CHECK_JSON, to check decrypted output
 * @note decryption happens in place (so it changes the input buffer!), decompression allocates a new buffer
 * @return 0 if it decrypts successfully
 */
int AgentCorePrivate::CCoreUtils::tryDecryptInternal(uint8_t *buf, size_t size, uint8_t** out, size_t* outSize, const uint8_t *key, int keyBits, uint8_t *iv, int flags) {
    *outSize = 0;
    *out = nullptr;
    uint8_t* o = nullptr;
    int res = 0;
    do {
        // decrypt origsize+compressed with the given key and iv
#ifndef NDEBUG
        char* aIv;
        char* aDk;
        CString::toHexString(iv,16,&aIv);
        CString::toHexString(key, keyBits/8, &aDk);
        DBGLOGV("decrypting buffer compressedsize=%d, devicekey=%s, iv=%s", size, aDk, aIv);
        CMemory::free(aIv);
        CMemory::free(aDk);
#endif
        // decrypt in  place, result is uint32_t+compressed
        uint8_t* decrypted = buf;
        res = CAES::decryptCtr((uint8_t*)key, keyBits, iv, buf, size, &decrypted);
        if (res != 0) {
            break;
        }

        // decompress
        size_t oSize = 0;
        res = CZLib::decompress(decrypted,size,&o, &oSize);
        if (res != 0) {
            break;
        }

        // check
        if (flags & READ_ENCRYPTED_CHECK_EXECUTABLE) {
            // check for executable header
            if (!CExecutableFormat::isMatchingArchitecture(o, oSize)) {
                res = EBADMSG;
                break;
            }
        }
        else if (flags & READ_ENCRYPTED_CHECK_JSON) {
            // check if the json is at least parsable
            if (!CJson::isValid((char*)o)) {
                res = EBADMSG;
                break;
            }
        }

        // decrypted/decompressed ok
        *out = o;
        *outSize = oSize;
        DBGLOGV("decrypted/decompressed buffer size=%d, originalSize=%d", size, oSize);
    } while(0);
    if (res != 0) {
        // free eventually decompressed buffer on error
        CMemory::free(o);
    }
    return res;
}

int AgentCorePrivate::CCoreUtils::readEncrypted(const char *path, uint8_t **out, size_t *outSize, int flags, int* flagMatch) {
    int res = 0;
    uint8_t *fileBuf=nullptr;
    uint8_t *copy =nullptr;
    uint8_t* decompressed = nullptr;
    size_t decompressedSize = 0;
    size_t fileSize = 0;
    uint8_t iv[16] = {};
    int successFlag = 0;
    *out = nullptr;
    *outSize = 0;

    do {
        // read to buffer
        res = CFile::toBuffer(path, &fileBuf, &fileSize);
        if (res != 0) {
            break;
        }

        // make a copy to try different decryptions
        copy = (uint8_t *) CMemory::dup(fileBuf, fileSize);
        if (!copy) {
            res = ENOMEM;
            break;
        }

        // buffer is 16 bytes iv + data compressed/encrypted with one of the 32 bytes AES keys
        uint8_t *data = nullptr;
        size_t sizeData = 0;
        res = -1;
        if (flags & READ_ENCRYPTED_USE_DEVICE_KEY) {
            // try to decrypt with the device key (32 bytes sha256 used as key)
            DBGLOGV("attempting decryption of %s with the device key", path);
            memcpy(iv, fileBuf, sizeof(iv));
            data = fileBuf + sizeof(iv);
            sizeData = fileSize - sizeof(iv);
            res = tryDecryptInternal(data, sizeData, &decompressed, &decompressedSize, CAppContext::instance()->deviceKey(), DEVICE_ID_SIZE * 8, iv, flags);
            if (res == 0) {
                successFlag = READ_ENCRYPTED_USE_DEVICE_KEY;
                DBGLOGV("%s decrypted with the device key !", path);
            }
        }
        if (res != 0) {
            if (flags & READ_ENCRYPTED_USE_DISPOSABLE_KEY) {
                // try with the disposable key
                DBGLOGV("attempting decryption of %s with the disposable key", path);

                // check if the disposable key is available
                if (CBinaryPatch::instance()->disposableKey() == nullptr) {
                    DBGLOGW("disposable key is missing!");
                }
                else {
                    // try to decrypt with the disposable key
                    DBGLOGV("attempting decryption of %s with the disposable key", path);
                    const uint8_t* key = CBinaryPatch::instance()->disposableKey();
                    memcpy(fileBuf, copy, fileSize);
                    memcpy(iv, fileBuf, sizeof(iv));
                    data = fileBuf + sizeof(iv);
                    sizeData = fileSize - sizeof(iv);
                    decompressed = nullptr;
                    decompressedSize = 0;
                    res = tryDecryptInternal(data, sizeData, &decompressed, &decompressedSize, key, AGENT_AES_KEY_BITS, iv, flags);
                    if (res == 0) {
                        successFlag = READ_ENCRYPTED_USE_DISPOSABLE_KEY;
                        DBGLOGV("%s decrypted with the disposable key !", path);
                    }
                }
            }
        }
        if (res != 0) {
            if (flags & READ_ENCRYPTED_USE_FALLBACK_KEY) {
                // try to decrypt with the fallback key
                DBGLOGV("attempting decryption of %s with the fallback key", path);
                const uint8_t* key = CBinaryPatch::instance()->fallbackKey();
                memcpy(fileBuf, copy, fileSize);
                memcpy(iv, fileBuf, sizeof(iv));
                data = fileBuf + sizeof(iv);
                sizeData = fileSize - sizeof(iv);
                decompressed = nullptr;
                decompressedSize = 0;
                res = tryDecryptInternal(data, sizeData, &decompressed, &decompressedSize, key, AGENT_AES_KEY_BITS, iv, flags);
                if (res == 0) {
                    successFlag = READ_ENCRYPTED_USE_FALLBACK_KEY;
                    DBGLOGV("%s decrypted with the fallback key !", path);
                }
            }
        }
        if (res != 0) {
            // decryption error!
            DBGLOGE("can't decrypt %s !", path);
            break;
        }

        // decrypted successfully
        if (flagMatch) {
            *flagMatch = successFlag;
        }

        // copy decompressed data to another buffer, ensuring it's 0 terminated (may be a string)
        uint8_t *o = (uint8_t *) CMemory::alloc(decompressedSize + 1);
        if (!o) {
            res = ENOMEM;
            break;
        }
        memcpy(o, decompressed, decompressedSize);
        *out = o;
        *outSize = decompressedSize;
    } while (0);

    CMemory::free(decompressed);
    CMemory::free(copy);
    CMemory::free(fileBuf);
    return res;
}
