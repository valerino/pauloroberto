//
// Created by jrf/ht on 12/02/19.
//

#ifndef RCSLINUX_CCONFIGMANAGER_H
#define RCSLINUX_CCONFIGMANAGER_H

#include <cstdint>
#include <CJson.h>
#include <CMutex.h>
#include <IConfiguration.h>

/**
 * AgentCorePrivate groups private core stuff
 */
namespace AgentCorePrivate {
    /**
     * @addtogroup AGENT_ARCHITECTURE Agent architecture
     * @{
     * @class CConfigManager
     * @brief manages agent configuration
     *
     * the configuration manager allows each module to access the configuration different parts (essentially, the "core" and "plugins" node) through the @ref AgentCore::IConfiguration interface.
     *
     * the configuration manager is accessible by each plugin via the @ref AgentCore::ICore::cfgMgr() method
     *
     * @note each plugin can use the @ref AgentPluginPrivate::CPluginBase::ownConfiguration() method, which is just a shortcut to call into the configuration manager and access the plugin's own configuration node
     * @note the configuration manager processes directly the _CONFIG_ command (@see REMOTECMD_PLUGINS)
     * @}
     */
    class CConfigManager : public AgentCore::IConfiguration {
    public:
        /**
         * singleton
         * @return CConfigMgr*
         */
        static CConfigManager* instance();

        JsonObject &plugin(const char *name) override;

        JsonObject &core() override;

        JsonObject &plugins() override;

        ~CConfigManager();

        /**
         * read the configuration from the encrypted blob
         * @param update optional, if true all the loaded plugins are reinitialized calling their @ref AgentPlugin::IPlugin::init() entrypoint
         * @return 0 on success, or errno
         */
        int read(bool update=false);

        int update(const uint8_t *blob, size_t size) override;

        int update(const char* json) override;

        BdUtils::CMutex &configLock() override;

        const char *cfgPath() override;

    protected:
        CConfigManager();

    private:
        static CConfigManager* _instance;
        BdUtils::CMutex _lock;
        BdUtils::CJson* _js = nullptr;
        char* _cfgPath = nullptr;
    };
}

#endif //RCSLINUX_CCONFIGMANAGER_H
