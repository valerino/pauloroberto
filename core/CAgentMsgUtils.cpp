//
// Created by jrf/ht on 05/05/19.
//

#include "CAgentMsgUtils.h"
#include "bdutils.h"
using namespace BdUtils;

JsonObject &AgentCorePrivate::CAgentMsgUtils::body(JsonObject &js) {
    return js[vxENCRYPT("body")];
}

JsonObject &AgentCorePrivate::CAgentMsgUtils::header(JsonObject &js) {
    return js[vxENCRYPT("header")];
}

uint32_t AgentCorePrivate::CAgentMsgUtils::cmdId(JsonObject &js) {
    return js[vxENCRYPT("cmdid")];
}

uint32_t AgentCorePrivate::CAgentMsgUtils::batchId(JsonObject &js) {
    return js[vxENCRYPT("batchid")];
}

char* AgentCorePrivate::CAgentMsgUtils::agentId(JsonObject &js, const char** out) {
    char* s = CJson::getStringValue(js, vxENCRYPT("agentid"), out);
    return s;
}
