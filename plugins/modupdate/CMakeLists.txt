project(modupdate)

# 32bit filename ends with '32'
if (DEFINED BUILD_32BIT)
    set (PRJ ${PROJECT_NAME}32)
else()
    set (PRJ ${PROJECT_NAME})
endif()

add_library (${PRJ} MODULE
        ../../include/IPlugin.h CModUpdate.cpp CModUpdate.h
        ../CPluginBase.cpp
        ../CPluginBase.h ../modremotecmd/CAgentRemoteCmdUtils.cpp ../modremotecmd/CAgentRemoteCmdUtils.h)

target_include_directories (${PRJ} PUBLIC
        ${PROJECT_SOURCE_DIR}
        ../../include
        ../../plugins
        )

target_link_libraries(${PRJ}
        bdutils
        )

# copy built binary to ./out/plg directory
add_custom_command(TARGET ${PRJ} POST_BUILD
        COMMAND
        mkdir -p ${RCS_PLUGINS_DIR}
        COMMAND
        # encrypt plugin with the default disposable key
        ../../../backendtools/agentcrypt.py --mode encrypt --infile $<TARGET_FILE:${PRJ}> --outfile ${RCS_PLUGINS_DIR}/lib${PRJ}.so --key ${DEFAULT_DISPOSABLE_KEY} --asset
        )

