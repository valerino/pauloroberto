//
// Created by jrf/ht on 26/02/19.
//

#ifndef RCSLINUX_CMODEXFILTRATE_H
#define RCSLINUX_CMODEXFILTRATE_H

#include <CPluginBase.h>

namespace BdUtils {
    class CRNG;
}

/**
 * AgentPluginPrivate groups private plugins stuff
 */
namespace AgentPluginPrivate {
    /**
     * @class CModExfiltrate
     * @brief implements default evidence exfiltration
     * @note exfiltration always runs in a separate thread, handled by the evidence manager
     * @see AgentCorePrivate::CEvidenceManager::recurrentExfiltratorThread()
     * @see AgentCorePrivate::CEvidenceManager::exfiltrateEvidence()
     * @see @ref EXFILTRATOR_PLUGINS
     * @see @ref DEFAULT_COMMUNICATION_PROTOCOL
     */
    class CModExfiltrate: public CPluginBase {
    public:
        CModExfiltrate();

        ~CModExfiltrate() final;

        int start(void *params=nullptr) override;

        int init(AgentCore::ICore *core, AgentPlugin::PluginInitStruct *params) override;

    private:
        int exfiltrateSingleInternal(const char *path, uint8_t *data, int size);
        char _exfiltratingCurrent[1024] = {0};
        BdUtils::CMutex _lock;
    };
}

#endif //RCSLINUX_CMODEXFILTRATE_H
