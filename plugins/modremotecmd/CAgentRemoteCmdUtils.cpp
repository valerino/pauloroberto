//
// Created by jrf/ht on 02/05/19.
//

#include "CAgentRemoteCmdUtils.h"
#include <bdutils.h>
#include "errno.h"

using namespace AgentPluginPrivate;
using namespace BdUtils;

/**
 * internal decryption/decompression code
 * @param k 32 bytes AES256 key
 * @param iv 16 bytes iv
 * @param buffer buffer to be decrypted
 * @param size buffer size
 * @param out on successful return, the decrypted/decompressed message (a json string) to be freed with @ref BdUtils::CMemory::free()
 * @param outSize on successful return, size of the decrypted/decompressed buffer
 * @return 0 on success, or errno
 */
int CAgentRemoteCmdUtils::decryptRemoteCmdInternal(uint8_t * k, uint8_t* iv, uint8_t *buffer, uint32_t size, uint8_t **out, uint32_t *outSize) {
    int res = 0;
    uint8_t* decompressed = nullptr;
    size_t decompressedSize = 0;
    do {
#ifndef NDEBUG
        // print the key and iv
        char* aK;
        char* aIv;
        CString::toHexString(k,sizeof(k),&aK);
        CString::toHexString(iv,sizeof(iv),&aIv);
        DBGLOGV("trying to decrypt using AES key: %s, iv=%s", aK, aIv);
#endif
        // decrypt buffer, inplace
        uint8_t* data = buffer;
        res = CAES::decryptCtr(k, 256, iv, buffer, size, &data);
        if (res != 0) {
            DBGLOGE("failed to AES decrypt buffer!");
            res = EBADMSG;
            break;
        }

        // decompress
        res = CZLib::decompress(data, size, &decompressed, &decompressedSize);
        if (res != 0) {
            DBGLOGE("failed to decompress decrypted buffer! (wrong key ?)");
            res = EBADMSG;
            break;
        }

        // done
        *out = decompressed;
        *outSize = decompressedSize;
        DBGLOGI("buffer decrypted successfully: %s, decompressed size=%d", decompressed, decompressedSize);
    }while (0);
    if (res != 0) {
        CMemory::free(decompressed);
    }
    return res;
}

int CAgentRemoteCmdUtils::decryptRemoteCmd(uint8_t* deviceKey, uint8_t* fallbackKey, uint8_t *cmdBuffer, uint32_t size, uint8_t **out,
                                     uint32_t *outSize) {
    if (!cmdBuffer || !size || !out || !outSize || !deviceKey || !fallbackKey) {
        return EINVAL;
    }
    *out = 0;
    *outSize = 0;

    // get iv (first 16 bytes)
    uint8_t iv[16]={0};
    memcpy(iv,cmdBuffer,16);
    uint8_t* data = cmdBuffer + 16;
    uint32_t dataSize = size - 16;

    uint8_t* decompressed = nullptr;
    uint32_t decompressedSize = 0;
    DBGLOGI("trying decryption with the device key");
    int res = decryptRemoteCmdInternal(deviceKey, iv, data, dataSize, &decompressed, &decompressedSize);
    if (res != 0) {
        // failed, retrying with the fallback key
        DBGLOGW("retrying decryption with the fallback key");
        res = decryptRemoteCmdInternal(fallbackKey, iv, data, dataSize, &decompressed, &decompressedSize);
        if (res != 0) {
            DBGLOGE("neither the device key nor the fallback key works, can't decrypt command !!!");
        }
    }
    return res;
}