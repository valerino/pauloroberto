//
// Created by jrf/ht on 20/02/19.
//

#include "CPluginBase.h"
#include <ICore.h>
#include <IConfiguration.h>
#include <bdutils.h>
#include <gitbuild.h>
using namespace BdUtils;
using namespace AgentPlugin;
using namespace AgentPluginPrivate;
using namespace AgentCore;

// the plugin instance
IPlugin* _instance = nullptr;

int CPluginBase::start(void *params) {
    if (_started) {
        //DBGLOGW("plugin %s(%s) ALREADY STARTED, modhandle=%p", _internalName, _path, _modHandle);
        return EINPROGRESS;
    }

    _started=true;
    DBGLOGV("STARTING plugin %s(%s), modhandle=%p", _internalName, _path, _modHandle);
    return 0;
}

int CPluginBase::stop(void *params) {
    if (!_started) {
        //DBGLOGW("plugin %s(%s) NOT STARTED, modhandle=%p", _internalName, _path, _modHandle);
        return ECANCELED;
    }
    _started= false;
    DBGLOGV("STOPPING plugin %s(%s), modhandle=%p", _internalName, _path, _modHandle);
    return 0;
}

int CPluginBase::uninstall(void *params) {
    DBGLOGV("UNINSTALLING plugin %s(%s), modhandle=%p", _internalName, _path, _modHandle);
    return 0;
}

int CPluginBase::init(ICore *core, PluginInitStruct *params) {
    if (params && core) {
        // new initialization
        _path = CString::dup(params->path);
        _modHandle = params->handle;
        _core = core;
        DBGLOGV("INITIALIZING plugin %s(%s), modhandle=%p, type=%d", _internalName, _path, _modHandle, _type);
    }
    return 0;
}

const char *CPluginBase::internalName() {
    return _internalName;
}

CPluginBase::~CPluginBase() {
    DBGLOGV("DESTROYING plugin %s(%s), modhandle=%p", _internalName, _path, _modHandle);

    // complete cleanup
    CMemory::free(_internalName);
    CMemory::free(_path);
    delete _rng;
}

void CPluginBase::dispose() {
    DBGLOGV("DISPOSING plugin %s(%s), modhandle=%p", _internalName, _path, _modHandle);
    setClosing();
    if (_started) {
        // stop the plugin first
        stop();
    }
    delete this;
}

CPluginBase::CPluginBase(const char *name) {
    //DBGLOGV(nullptr);
    _internalName = CString::dup(name);
    _instance = this;
    _rng = new CRNG();
}

void *CPluginBase::moduleHandle() {
    return _modHandle;
}

const char *CPluginBase::path() {
    return _path;
}

void CPluginBase::setClosing() {
    _evClosing.set();
}

bool CPluginBase::isClosing() {
    return _evClosing.signaled();
}

bool CPluginBase::waitClosingSignaled(int msec) {
    if (_evClosing.wait(msec) == 0) {
        return true;
    }
    return false;
}

bool CPluginBase::started() {
    return _started;
}

ArduinoJson::JsonObject &CPluginBase::ownConfiguration() {
    JsonObject& o = _core->cfgMgr()->plugin(_internalName);
    return o;
}

int CPluginBase::buildVersion(char *gitVersion, int size) {
    if (!gitVersion | !size) {
        return EINVAL;
    }
    int len = strlen(GIT_BUILD_VERSION);
    if (len + 1 > size) {
        return EOVERFLOW;
    }
    strcpy(gitVersion, GIT_BUILD_VERSION);
    return 0;
}

int CPluginBase::type() {
    return _type;
}

bool CPluginBase::isRemote() {
    return _remote;
}

BdUtils::CRNG *CPluginBase::rng() {
    return _rng;
}

bool CPluginBase::isInitialized() {
    return _initializedCorrectly;
}
