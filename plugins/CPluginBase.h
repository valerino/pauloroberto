//
// Created by jrf/ht on 20/02/19.
//

#ifndef RCSLINUX_CPLUGINBASE_H
#define RCSLINUX_CPLUGINBASE_H

namespace AgentCore {
    class ICore;
}
#include "IPlugin.h"
#include <CDynLoad.h>
#include <CEvent.h>
#include <CJson.h>

namespace BdUtils {
    class CRNG;
}

/**
 * AgentPluginPrivate groups private plugins stuff
 */
namespace AgentPluginPrivate {

    /**
     * @class CPluginBase
     * @brief base class for all plugins, implements @ref AgentPlugin::IPlugin
     */
    class CPluginBase : public AgentPlugin::IPlugin {
    public:
        int start(void *params=nullptr) override;

        int stop(void *params=nullptr) override;

        int uninstall(void *params=nullptr) override;

        int init(AgentCore::ICore *core=nullptr, AgentPlugin::PluginInitStruct *params=nullptr) override;

        const char *internalName() override;

        virtual ~CPluginBase();

        int buildVersion(char *gitVersion, int size) override;

        const char *path() override;

        void *moduleHandle() override;

        void dispose() override;

        bool started() override;

        int type() override;

        bool isClosing() override;

        bool isRemote() override;

    protected:
        /**
         * constructor
         * @param name the plugin internal name defined in each plugin
         */
        explicit CPluginBase(const char* name);

        /**
         * set the plugin closing status
         */
        void setClosing();

        /**
         * a random number generator initialized by the plugin
         * @return BdUtils::CRNG*
         */
        BdUtils::CRNG* rng();

        /**
         * waits for the plugin own closing event(plugin is terminating) to be set
         * @param msec wait for at least these many milliseconds
         * @return true if the wait is satisfied, or false on timeout
         */
         bool waitClosingSignaled(int msec);

        /**
         * returns plugin's own configuration node
         * @return JsonObject&
         */
        ArduinoJson::JsonObject& ownConfiguration();

    public:
        bool isInitialized() override;

    protected:
        int _type = PLUGIN_TYPE_GENERIC;

        AgentCore::ICore* _core = nullptr;

        bool _initializedCorrectly = true;
    private:
        BdUtils::CEvent _evClosing;
        char* _internalName = nullptr;
        char* _path = nullptr;
        bool _started = false;
        bool _remote = false;
        BdUtils::CRNG* _rng = nullptr;
        BdUtils::LibraryHandle * _modHandle = nullptr;
    };
}

/**
 * this is returned by the C interface instance(), exported by the plugin shared library
 */
extern AgentPlugin::IPlugin* _instance;

#endif //RCSLINUX_CPLUGINBASE_H
