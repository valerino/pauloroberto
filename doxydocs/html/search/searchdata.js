var indexSectionsWithContent =
{
  0: "_abcdefghijlmnoprstuvwxz~",
  1: "_civ",
  2: "ab",
  3: "abcdgimorw",
  4: "abcdefghijlmnoprstuvwx~",
  5: "_abcdefhimoprsuv",
  6: "bcdefilprswz",
  7: "v",
  8: "adfghilmprstv",
  9: "acdeimprt",
  10: "t"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enumvalues",
  8: "defines",
  9: "groups",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerator",
  8: "Macros",
  9: "Modules",
  10: "Pages"
};

