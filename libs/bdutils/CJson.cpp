//
// Created by jrf/ht on 13/02/19.
//

#include "CJson.h"
#include "CFile.h"
#include "CMemory.h"
#include "CLog.h"
#include "CString.h"
#include <errno.h>

BdUtils::CJson::CJson(const char *txt) {
    JsonObject& r = _jsBuf.parse(txt);
    if (!r.success()) {
        throw std::exception();
    }
    _node = r;
}

BdUtils::CJson::~CJson() {
    _jsBuf.clear();
}

int BdUtils::CJson::write(const char *path, bool prettyPrint) {
    if (!path) {
        return EINVAL;
    }

    // print to string
    const char* j = toString(prettyPrint);
    if (!j) {
        return ENOMEM;
    }

    // write to file
    int res = CFile::fromBuffer(path, (uint8_t*)j, strlen(j));
    CMemory::free((void*)j);
    return res;
}

char *BdUtils::CJson::toString(bool prettyPrint) {
    JsonObject& n = node();
    return CJson::toString(n, prettyPrint);
}

char *BdUtils::CJson::toString(JsonObject& js, bool prettyPrint) {
    // allocate memory
    int len;
    if (prettyPrint) {
        len = js.measurePrettyLength();
    }
    else {
        len = js.measureLength();
    }
    char* j = (char*)CMemory::alloc(len + 1);
    if (!j) {
        return nullptr;
    }

    // print
    if (prettyPrint) {
        js.prettyPrintTo(j, len + 1);
    }
    else {
        js.printTo(j, len + 1);
    }

    return j;
}

BdUtils::CJson *BdUtils::CJson::loadFile(const char *path) {
    // read to file
    char* str = nullptr;
    int res = CFile::toBuffer(path, (uint8_t**)&str);
    if (res != 0) {
        return nullptr;
    }

    // initialize json
    CJson* c = new CJson(str);
    CMemory::free(str);
    return c;
}

BdUtils::CJson::CJson() {
    _node = _jsBuf.createObject();
}

JsonObject &BdUtils::CJson::node() {
    return _node;
}

bool BdUtils::CJson::isValid(const char *json) {
    bool res = true;
    CJson* tmp;
    try {
        tmp = new CJson(json);
        delete tmp;
    }
    catch(...) {
        res = false;
    }
    return res;
}

char * BdUtils::CJson::getStringValue(JsonObject &n, const char *name, const char** out) {
    if (!name) {
        return nullptr;
    }
    if (out) {
        *out = nullptr;
    }
    const char* a = n[name].as<char*>();
    if (!a) {
        return nullptr;
    }
    if (out) {
        // do not allocate
        *out = a;
        return (char*)a;
    }

    // allocate
    return CString::dup(a);
}

char * BdUtils::CJson::getStringValue(const char *name, const char** out) {
    JsonObject& n = node();
    return CJson::getStringValue(n, name, out);
}

JsonObject &BdUtils::CJson::getNodeValue(JsonObject& n, const char* name) {
    if (!name) {
        return JsonObject::invalid();
    }
    JsonObject& o = n[name].as<JsonObject&>();
    return o;
}

JsonObject &BdUtils::CJson::getNodeValue(const char* name) {
    JsonObject& n = node();
    return CJson::getNodeValue(n,name);
}

JsonArray &BdUtils::CJson::getArrayValue(JsonObject& n, const char* name) {
    if (!name) {
        return JsonArray::invalid();
    }
    JsonArray& o = n[name].as<JsonArray&>();
    return o;
}

JsonArray &BdUtils::CJson::getArrayValue(const char* name) {
    JsonObject& n = node();
    return CJson::getArrayValue(n,name);
}

int BdUtils::CJson::size() {
    return _node.size();
}

int BdUtils::CJson::memorySize() {
    return _jsBuf.size();
}

int BdUtils::CJson::test() {
#ifndef NDEBUG
    CJson* js = new CJson();
    CJson* js2 = new CJson();
    char* s = nullptr;
    int res = 0;
    do {
        // fill some nodes
        js->node()["test"]=1234;
        js2->node()["value"] ="thesubnode";
        js->node()["subnode"]= js2->node();
        s = js->toString();
        if (!s) {
            res = ECANCELED;
            break;
        }
        char compare[] = "{\"test\":1234,\"subnode\":{\"value\":\"thesubnode\"}";
        if (memcmp(s,compare,strlen(compare)) != 0) {
            // not match!
            res = ECANCELED;
            break;
        }
        // check sizes
        int sz = js->memorySize();
        int sz2 = js2->memorySize();
        if (sz == 0 || sz2 == 0) {
            res = ECANCELED;
            break;
        }

        // free jsons
        delete js;
        delete js2;

        // recheck sizes (must be 0 now)
        sz = js->memorySize();
        sz2 = js2->memorySize();
        if (sz != 0 || sz2 != 0) {
            res = ECANCELED;
            break;
        }
        js = nullptr;
        js2 = nullptr;
    } while(0);
    delete js;
    delete js2;
    CMemory::free(s);
#endif
    return res;
}

BdUtils::CJson* BdUtils::CJson::fromJsonObject(JsonObject &js) {
    // allocate memory
    int len = js.measureLength();
    char* j = (char*)CMemory::alloc(len + 1);
    if (!j) {
        throw std::exception();
    }

    // to string
    js.printTo(j, len);

    // create new cjson object
    CJson* n = new CJson(j);
    CMemory::free(j);
    return n;
}
