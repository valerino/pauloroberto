//
// Created by jrf/ht on 19/02/19.
//

#include <thread>
#include "CMutex.h"
#include "CLog.h"

using namespace BdUtils;

BdUtils::CMutex::CMutex() {
}

BdUtils::CMutex::~CMutex() {
}

int BdUtils::CMutex::lock(int msec) {
    if (msec == -1) {
        // infinite
        try {
            // lock indefinitely
            _mtx.lock();
        }
        catch (std::system_error& e) {
            // some exception happened, the mutex is not acquired
            return EACCES;
        }

        // lock acquired
        _locked = true;
        return 0;
    }

    // try to acquire with a timeout
    bool acquired = false;
    try {
        acquired = _mtx.try_lock_for(std::chrono::milliseconds(msec));
    }
    catch (...) {

    }
    if (!acquired) {
        return ETIMEDOUT;
    }

    // acquired
    _locked = true;
    return 0;
}

int BdUtils::CMutex::tryLock() {
    return lock(0);
}

void BdUtils::CMutex::unlock() {
    if (_locked) {
        _locked = false;
        _mtx.unlock();
    }
}

std::recursive_timed_mutex& BdUtils::CMutex::stdMutex() {
    return _mtx;
}

static void testThread1(void* ctx, bool useGuard) {
#ifndef NDEBUG
    DBGLOGV("t1 starts");
    BdUtils::CMutex* mtx = (BdUtils::CMutex*)ctx;

    // lock for 10 seconds
    if (useGuard) {
        MUTEX_GUARD(mtx->stdMutex());
        DBGLOGV("t1 owned the guarded lock and sleeps for 10 seconds");
        std::this_thread::sleep_for(std::chrono::milliseconds(10*1000));
        DBGLOGV("t1 exiting");
    }
    else {
        mtx->lock();
        DBGLOGV("t1 owned the lock and sleeps for 10 seconds");
        std::this_thread::sleep_for(std::chrono::milliseconds(10*1000));
        mtx->unlock();
        DBGLOGV("t1 exiting");
    }
#else
    return;
#endif
}

static void testThread2(void* ctx) {
#ifndef NDEBUG
    DBGLOGV("t2 starts and waits for owning the lock");
    BdUtils::CMutex* mtx = (BdUtils::CMutex*)ctx;
    mtx->lock();
    DBGLOGV("t2 owned the lock and immediately unlocks");
    mtx->unlock();
    DBGLOGV("t2 exiting");
#else
    return;
#endif
}

int BdUtils::CMutex::test() {
#ifndef NDEBUG
    CMutex mtx;

    DBGLOGV("test with lock()/unlock()");
    auto t1 = std::thread(testThread1, &mtx, false);
    auto t2 = std::thread(testThread2, &mtx);
    t1.join();
    t2.join();

    DBGLOGV("test with MUTEX_GUARD");
    t1 = std::thread(testThread1, &mtx, true);
    t2 = std::thread(testThread2, &mtx);
    t1.join();
    t2.join();
    return 0;
#else
    return 0;
#endif
}
