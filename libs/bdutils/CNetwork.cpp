//
// Created by jrf/ht on 28/02/19.
//

#include "CNetwork.h"
#include "CHttp.h"
#include "CMemory.h"

#ifdef _WIN32
#include <winsock.h>
#else
#include <sys/socket.h>
#include <arpa/inet.h>
#endif
#include <stdio.h>
#include <memory.h>

bool BdUtils::CNetwork::initialized() {
#ifdef _WIN32
    return CHttp::initialized();
#else
    // no need on unix
    return true;
#endif
}

int BdUtils::CNetwork::initNetworkLib() {
#ifdef _WIN32
    return CHttp::initHttpLib();
#else
    // no need on unix
    return 0;
#endif
}

void BdUtils::CNetwork::deinitNetworkLib() {
#ifdef _WIN32
    CHttp::deinitHttpLib();
#else
    // no need on unix
#endif
}

int BdUtils::CNetwork::ipFromString(const char *ipStr, void *ip, size_t size) {
    if (!ipStr || !ip) {
        return EINVAL;
    }
    if (!initialized()) {
        return ECANCELED;
    }

    int res = 0;
    do {
        // check for ipv6
        bool isV6 = (strchr(ipStr, ':') != nullptr);
        if (isV6) {
            if (size != 16) {
                // wrong size
                return EINVAL;
            }
            struct in6_addr addr = {};
            res = inet_pton(AF_INET6, ipStr, &addr);
            if (res == 1) {
                // done, copy
                memcpy((uint8_t*)ip,&addr,16);
                res = 0;
                break;
            }
        }
        else {
            // ipv4
            if (size != sizeof(uint32_t)) {
                // wrong size
                return EINVAL;
            }
            struct in_addr addr = {};
            res = inet_pton(AF_INET, ipStr, &addr);
            if (res == 1) {
                *((uint32_t*)ip)=addr.s_addr;
                res = 0;
                break;
            }
        }
    } while (0);

    if (res != 0) {
        // invalid input
        res = EINVAL;
    }

    return res;
}

int BdUtils::CNetwork::ipToString(void *ip, size_t size, char *out, size_t outSize) {
    if (!ip || !size || !out || !outSize) {
        return EINVAL;
    }

    char* o = nullptr;
    int res = 0;
    do {
        // call the alloc version
        res = ipToString(ip,size,&o);
        if (res != 0) {
            break;
        }
        if (outSize < (strlen(o) + 1)) {
            res = EOVERFLOW;
            break;
        }

        // done, copy
        strcpy(out,o);
    } while(0);

    CMemory::free(o);
    return res;
}

int BdUtils::CNetwork::ipToString(void *ip, size_t size, char **out) {
    if (!ip || !size || !out) {
        return EINVAL;
    }
    *out=nullptr;

    int res = 0;
    char* o = nullptr;
    do {
        // allocate memory big enough for both ip sizes
        o = (char*)CMemory::alloc(INET6_ADDRSTRLEN);
        if (!o) {
            res = ENOMEM;
            break;
        }

        const char* p;
        if (size == 16) {
            // ipv6
            p = inet_ntop(AF_INET6, ip, o, INET6_ADDRSTRLEN);
        }
        else {
            // ipv4
            p = inet_ntop(AF_INET, ip, o, INET_ADDRSTRLEN);
        }
        if (!p) {
            res = errno;
            break;
        }

        // ok, return string
        *out = o;
    }while (0);

    if (res != 0) {
        CMemory::free(o);
    }
    return res;
}

int BdUtils::CNetwork::test() {
#ifndef NDEBUG
    int res = 1;
    initNetworkLib();

    // convert string to ip
    uint32_t ip;
    char ipStr[64];
    ipFromString("127.0.0.1",&ip,sizeof(uint32_t));

    // and back to string
    ipToString(&ip,sizeof(uint32_t),ipStr,sizeof(ipStr));
    if (strcmp(ipStr, "127.0.0.1") == 0) {
        // success
        res = 0;
    }
    deinitNetworkLib();
    return res;
#else
    return 0;
#endif
}
