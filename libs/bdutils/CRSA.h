//
// Created by jrf/ht on 29/04/19.
//

#ifndef RCSLINUX_CRSA_H
#define RCSLINUX_CRSA_H

#include <stdint.h>
#include <mbedtls/pk.h>
#include "CRNG.h"

/**
 * BdUtils groups backdoor utilities framework
 */
namespace BdUtils {

    /**
     * @class CRSA
     * @brief implements basic RSA encryption/decryption, using mbedtls
     */
    class CRSA {
    public:
        /**
         * encrypt data with RSA, PKCS#1.5 padding
         * @param in buffer to be encrypted
         * @param inSize size of the input buffer
         * @note maximum size which can be encrypted with RSA is keysize (in bytes) - 11 (PKCS v1.5 padding max size)
         * @param out on successful return, the encrypted buffer to be freed with @ref CMemory::free()
         * @param outSize on successful return, size of the encrypted buffer
         * @return 0 on success, or errno
         */
        int encrypt(uint8_t* in, uint32_t inSize, uint8_t** out, size_t* outSize);

        /**
         * decrypt data with RSA, assuming PKCS#1.5 padding
         * @param in buffer to be decrypted
         * @param inSize size of the input buffer
         * @param out on successful return, the decrypted buffer to be freed with @ref CMemory::free()
         * @param outSize on successful return, size of the decrypted buffer
         * @warning decryption is allowed only with private key!
         * @see @ref BdUtils::CRSA::encrypt()
         * @return 0 on success, or errno
         */
        int decrypt(uint8_t* in, uint32_t inSize, uint8_t** out, size_t* outSize);

        /**
         * initializes an RSA context for encryption or decryption
         * @param key pointer to a public or private key, DER format
         * @param derSize size of the DER key buffer, in bytes
         * @param keyBits key size in bits, i.e. 2048 (256 bytes)
         * @param rng a @ref BdUtils::CRNG instance
         * @param isPrivate if true, key represents a private key. either, it's a public key
         * @throws on invalid key
         */
        CRSA(const uint8_t* key, int derSize, int keyBits, CRNG* rng, bool isPrivate);
        virtual ~CRSA();

        /**
         * test code
         * @return 0 on success;
         */
        static int test();

    private:
        mbedtls_pk_context _ctx = {0};
        int _kSize = 0;
        CRNG* _rng = nullptr;
        bool _priv = true;
    };
}

#endif //RCSLINUX_CRSA_H
