//
// Created by jrf/ht on 15/04/19.
//

#include "CZlib.h"
#include "CMemory.h"
#include <errno.h>

int BdUtils::CZLib::compress(uint8_t *in, size_t size, uint8_t **out, size_t *outSize, int level) {
    if (!in || !size || !out || !outSize) {
        return EINVAL;
    }
    uint8_t* o = nullptr;
    size_t oSize = 0;
    int res = 0;

    do {
        // get the needed size for compression (including the initial ZLibHdr holding the original size)
        size_t needed = compressBound(size) + sizeof(ZLibHdr);

        // check if input parameters are provided
        if (*out && *outSize) {
            o = *out;
            oSize = *outSize;

            // check if the provided size is big enough
            if (oSize < needed) {
                return EOVERFLOW;
            }
        } else {
            *out = nullptr;
            *outSize = 0;

            // allocate the output buffer
            // always allocate +1 to account for a 0 terminator in case the decompressed buffer is a string, to allow string functions to work flawlessy
            o = (uint8_t *) CMemory::alloc(needed + 1);
            if (!o) {
                res = ENOMEM;
                break;
            }

        }

        // compress data, prepending the header
        ZLibHdr hdr = {(uint32_t)size};
        memcpy(o,&hdr,sizeof(ZLibHdr));
        uint8_t* oData = o + sizeof(ZLibHdr);

        size_t compressedSize = needed - sizeof(ZLibHdr);
        res = compress2(oData, &compressedSize, in, size, level);
        if (res != Z_OK) {
            res = ECANCELED;
            break;
        }

        // done
        *out = o;
        *outSize = compressedSize + sizeof(ZLibHdr);
    }
    while (0);

    if (res != 0) {
        // free any allocated buffer
        if (o && o != *out) {
            CMemory::free(o);
        }
    }
    return res;
}

int BdUtils::CZLib::decompress(uint8_t *in, size_t size, uint8_t **out, size_t *outSize) {
    if (!in || !size || !out || !outSize) {
        return EINVAL;
    }
    uint8_t* o = nullptr;
    size_t oSize = 0;
    int res = 0;

    do {
        // get the needed size for decompression
        uint32_t needed = INFLATE_ORIGINAL_SIZE(in);

        // check if input parameters are provided
        if (*out && *outSize) {
            o = *out;
            oSize = *outSize;

            // check if the provided size is big enough
            if (oSize < needed) {
                return EOVERFLOW;
            }
        } else {
            *out = nullptr;
            *outSize = 0;

            // allocate the output buffer
            o = (uint8_t *) CMemory::alloc(needed);
            if (!o) {
                res = ENOMEM;
                break;
            }

        }

        // decompress data
        size_t decompressedSize = needed;
        res = uncompress(o, &decompressedSize, INFLATE_COMPRESSED_DATAPTR(in), INFLATE_COMPRESSED_SIZE(size));
        if (res != Z_OK) {
            res = ECANCELED;
            break;
        }

        // done
        *out = o;
        *outSize = decompressedSize;
    }
    while (0);

    if (res != 0) {
        // free any allocated buffer
        if (o && o != *out) {
            CMemory::free(o);
        }
    }
    return res;
}

int BdUtils::CZLib::test() {
#ifndef NDEBUG
    char in[]="\"blablablablablablublibloblablablablabla!!";
    uint8_t* outCompr = nullptr;
    size_t outComprSize = 0;
    uint8_t* outDec = nullptr;
    size_t outDecSize = 0;
    int res = 0;
    do {
        // compression
        res = CZLib::compress((uint8_t*)in,strlen(in) + 1, &outCompr, &outComprSize);
        if (res != 0) {
            break;
        }

        // decompression
        res = CZLib::decompress((uint8_t*)outCompr, outComprSize, &outDec, &outDecSize);
        if (res != 0) {
            break;
        }

        // check
        if (memcmp(outDec,in,outDecSize) != 0) {
            res = ECANCELED;
        }
    } while(0);
    CMemory::free(outDec);
    CMemory::free(outCompr);
    return res;
#else
    return 0;
#endif
}

