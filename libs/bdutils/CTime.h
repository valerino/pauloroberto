//
// Created by jrf/ht on 12/02/19.
//

#ifndef RCSLINUX_CTIME_H
#define RCSLINUX_CTIME_H

#include <time.h>
#include <chrono>

/**
 * BdUtils groups backdoor utilities framework
 */
namespace BdUtils {
    /**
     * @class CTime
     * @brief portable time utilities
     */
    class CTime {
    public:
        /**
         * timestamp in milliseconds since unix epoch
         * @return uint64_t
         */
        static uint64_t now();

        /**
         * timestamp in seconds since unix epoch
         * @return int
         */
        static int nowSeconds();

        /**
         * returns time string
         * @param out on successful return, filled with the time string
         * @param sizeOut size of the out buffer
         * @return 0 on success, or errno
         */
        static int nowString(char* out, size_t sizeOut);
        /**
         * test code
         * @return 0 on success
         */
        static int test();
    };
}



#endif //RCSLINUX_CTIME_H
