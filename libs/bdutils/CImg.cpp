//
// Created by jrf/ht on 21/03/19.
//

#include "CImg.h"
#include "CMemory.h"
#include "CFile.h"
#include "CWindow.h"
#include <errno.h>
#if defined (__linux__) && !defined(__ANDROID__)
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#endif
#include <jpeglib.h>
#include "CFileMap.h"

int BdUtils::CImg::getScreenshot(uint8_t **out, size_t *size, int screenNum, bool activeWindow, int quality, int x, int y, int h, int w) {
    if (!out || !size) {
        return EINVAL;
    }
    *size = 0;
    *out = nullptr;
#if defined(__linux__) && !defined(__ANDROID__)
    // x11 specific
    return x11GetScreenshot(out, size, screenNum, activeWindow, quality, x, y, h, w);
#endif
    return 0;
}

int BdUtils::CImg::getScreenshotToFile(const char *path, int screenNum, bool activeWindow, int quality, int x, int y, int h, int w) {
    // get screenshot to buffer
    uint8_t* buf;
    size_t size;
    int res = getScreenshot(&buf, &size, screenNum, activeWindow, quality, x, y, h, w);
    if (res != 0) {
        return res;
    }

    // to file
    CFile f(path);
    bool error=false;
    do {
        // create file
        res = f.open("wb");
        if (res != 0) {
            break;
        }

        // write screenshot
        res = f.write(buf,size);
        if (res != 0) {
            error=true;
        }
    }while(0);
    if (error) {
        // remove file on error
        f.remove();
    }
    CMemory::free(buf);
    return res;
}

#if defined(__linux__) && !defined(__ANDROID__)
/**
 * get a screenshot, x11 specific
 * @param out on successful return, the screenshot buffer in JPG format
 * @param size on successful return, the buffer size
 * @param screenNum optional, the screen number (default is -1=all screens)
 * @param bool activeWindow if true, captures active window (screenNum is ignored)
 * @param quality optional, JPG quality (0=default, 100=max)
 * @param x optional, x coordinate of the rect to capture (default= top left corner x)
 * @param y optional, y coordinate of the rect to capture (default=top left corner y)
 * @param h optional, height of the rect to capture (default=-1, use screen/rect height)
 * @param w optional, width of the rect to capture (default=-1, use screen/rect width)
 * @return 0 on success, or errno
 */
int BdUtils::CImg::x11GetScreenshot(uint8_t **out, size_t *size, int screenNum, bool activeWindow, int quality, int x, int y, int h, int w) {
    XImage *image = nullptr;
    uint8_t *buf = nullptr;
    uint8_t *compressed = nullptr;
    size_t compressedSize = 0;
    DisplayHandle* display = nullptr;
    WindowHandle wnd = 0;
    int res = ECANCELED;

    do {
        // open the display
        display = XOpenDisplay(nullptr);
        if (!display) {
            break;
        }

        // get the requested screen
        int screen;
        if (screenNum == -1) {
            // or the whole screen
            screen = DefaultScreen(display);
        }
        else {
            screen = screenNum;
        }

        // get the requested window
        if (activeWindow) {
            CWindow::getFocusWindow(display,&wnd);
        }
        else {
            // or the whole display
            wnd = RootWindow(display, screen);
        }

        // get window info
        XWindowAttributes wndAttrs = {};
        Status status = XGetWindowAttributes(display, wnd, &wndAttrs);
        if (status == 0) {
            break;
        }

        // get dimensions/position
        int wX = (x == -1 ? 0 : x);
        int wY = (y == -1 ? 0 : y);
        int wH = (h == -1 ? wndAttrs.height : h);
        int wW = (w == -1 ? wndAttrs.width : w);

        // get the image bitmap
        image = XGetImage(display, wnd, wX, wY, wW, wH, AllPlanes, ZPixmap);
        if (!image) {
            break;
        }

        // allocate memory for the image
        int bytesPerPixel = image->depth / 8;
        buf = (uint8_t*)CMemory::alloc(image->width * bytesPerPixel * image->height);
        if (!buf) {
            break;

        }

        // copy the image to buffer, pixel per pixel

        for(y = 0; y < image->height; y++) {
            for(x = 0; x < image->width; x++) {
                uint32_t pixel = XGetPixel(image, x, y);
                if (bytesPerPixel > 3) {
                    buf[y * image->width * bytesPerPixel + x * bytesPerPixel + 0] = (unsigned char)((pixel>>24) & 0xff);
                    buf[y * image->width * bytesPerPixel + x * bytesPerPixel + 1] = (unsigned char)((pixel>>16) & 0xff);
                    buf[y * image->width * bytesPerPixel + x * bytesPerPixel + 2] = (unsigned char)((pixel>>8) & 0xff);
                    buf[y * image->width * bytesPerPixel + x * bytesPerPixel + 3] = (unsigned char)(pixel & 0xff);
                }
                else if (bytesPerPixel > 2) {
                    buf[y * image->width * bytesPerPixel + x * bytesPerPixel + 0] = (unsigned char)((pixel>>16) & 0xff);
                    buf[y * image->width * bytesPerPixel + x * bytesPerPixel + 1] = (unsigned char)((pixel>>8) & 0xff);
                    buf[y * image->width * bytesPerPixel + x * bytesPerPixel + 2] = (unsigned char)((pixel) & 0xff);
                }
                else if (bytesPerPixel > 1) {
                    buf[y * image->width * bytesPerPixel + x * bytesPerPixel + 0] = (unsigned char)((pixel>>8) & 0xff);
                    buf[y * image->width * bytesPerPixel + x * bytesPerPixel + 1] = (unsigned char)((pixel) & 0xff);
                }
                else {
                    buf[y * image->width * bytesPerPixel + x * bytesPerPixel] = (unsigned char)(pixel & 0xff);
                }
           }
        }

        // compress image
        res = jpegEncodeImage(buf, image->width, image->height, quality, bytesPerPixel, &compressed, &compressedSize);
        if (res == 0) {
            *out = compressed;
            *size = compressedSize;
        }
    }
    while (0);
    CMemory::free(buf);
    if (res != 0) {
        CMemory::free(compressed);
    }
    if (image) {
        XDestroyImage(image);
    }
    if (display) {
        XCloseDisplay(display);
    }
    return 0;
}

int BdUtils::CImg::jpegEncodeImage(const uint8_t *in, int w, int h, int quality, int bytesPerPixel, uint8_t **out,
                                   size_t *outSize) {
    if (!in || !w || !h || !out || !outSize || !bytesPerPixel) {
        return EINVAL;
    }
    *out = nullptr;
    *outSize = 0;
    CFileMap f;
    int res = 0;
    uint8_t* buf = nullptr;
    struct jpeg_error_mgr err = {};
    struct jpeg_compress_struct cs = {};
    do {
        // open a filemap to write to
        res = f.open("wb");
        if (res != 0) {
            break;
        }

        // setup libjpeg to use the underlying stream in the filemap
        cs.err = jpeg_std_error(&err);
        jpeg_create_compress(&cs);
        jpeg_stdio_dest(&cs, f.ptr());

        // set encoding parameters
        cs.image_width = w;
        cs.image_height = h;
        cs.input_components = bytesPerPixel;
        cs.in_color_space = JCS_RGB;
        jpeg_set_defaults(&cs);
        int q;
        if (quality == 0) {
            // default compression quality
            q = 60;
        }
        else {
            q = quality;
        }
        jpeg_set_quality(&cs, q, TRUE);

        // compress
        jpeg_start_compress(&cs, TRUE);
        JSAMPROW row;
        while(cs.next_scanline < cs.image_height) {
            row = (JSAMPROW)&in[cs.next_scanline * bytesPerPixel * w];
            jpeg_write_scanlines(&cs, &row, 1);
        }
        jpeg_finish_compress(&cs);
        f.close();

        // get the buffer
        uint8_t* b;
        size_t s;
        f.mem(&b,&s);
        buf = (uint8_t*)CMemory::dup(b,s);
        if (!buf) {
            res = ENOMEM;
            break;
        }
        *out = buf;
        *outSize = s;
    } while(0);
    return res;
}

int BdUtils::CImg::test() {
#ifndef NDEBUG
    int res = CImg::getScreenshotToFile("~/Downloads/screen.jpg");
    if (res != 0) {
        return res;
    }
    res = CImg::getScreenshotToFile("~/Downloads/screen-active.jpg",-1,true);
    return res;
#else
    return 0;
#endif
}

#endif

