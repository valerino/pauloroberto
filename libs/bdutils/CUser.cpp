//
// Created by jrf/ht on 13/02/19.
//

#include "CUser.h"
#include "CMemory.h"
#include "CString.h"
#include <stdlib.h>
#include <errno.h>
#include <pwd.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <climits>
#include "obfuscate.h"
#include "CProcess.h"
#include <sys/param.h>

/**
 * @brief default temp directory on unix
 */
#define TMP_DEFAULT_UNIX "/tmp"

int BdUtils::CUser::home(char **homePath) {
    if (!homePath) {
        return EINVAL;
    }
    *homePath=nullptr;

    int res = 0;
    int allocSize = 0x4000;
    char *mem = (char *) CMemory::alloc(allocSize);
    if (!mem) {
        return ENOMEM;
    }
    do {
        // get through env
        char *hd = getenv("HOME");
        if (hd) {
            strcpy(mem, hd);
        }
        else {
            // try with getpwuid()
            struct passwd pwd;
            struct passwd *result;
            res = getpwuid_r(getuid(), &pwd, mem, allocSize, &result);
            if (res != 0) {
                break;
            }
            if (result == nullptr) {
                res = ENOENT;
                break;
            }

            // found
            char* mm = CString::dup(result->pw_dir);
            if (!mm) {
                res = ENOMEM;
                break;
            }
            CMemory::free(mem);
            mem = mm;
        }
    }
    while (0);
    if (res != 0) {
        CMemory::free(mem);
    }
    else {
        *homePath = mem;
    }

    return res;
}

int BdUtils::CUser::userName(char **user){
    if (!user) {
        return EINVAL;
    }

    int res = 0;
    int allocSize = 0x4000;
    char *mem = (char *) CMemory::alloc(allocSize);
    if (!mem) {
        return ENOMEM;
    }
    do {
        struct passwd pwd;
        struct passwd *result;
        res = getpwuid_r(getuid(), &pwd, mem, allocSize, &result);
        if (res != 0) {
            break;
        }
        if (result == nullptr) {
            res = ENOENT;
            break;
        }

        // found
        char* mm = CString::dup(result->pw_name);
        if (!mm) {
            res = ENOMEM;
            break;
        }
        CMemory::free(mem);
        mem = mm;
    }
    while (0);
    if (res != 0) {
        CMemory::free(mem);
    }
    else {
        *user = mem;
    }

    return res;
}

int BdUtils::CUser::hostName(char **host) {
    if (!host) {
        return EINVAL;
    }
    *host = nullptr;
    int sizeAlloc = 1024;
    char* mem = (char*)CMemory::alloc(sizeAlloc);
    if (!mem) {
        return ENOMEM;
    }
    int res = gethostname(mem, sizeAlloc);
    if (res != 0) {
        res = errno;
        CMemory::free(mem);
        return res;
    }
    *host = mem;
    return 0;
}

int BdUtils::CUser::tmp(char **tmpPath) {
    if (!tmpPath) {
        return EINVAL;
    }
    *tmpPath = nullptr;

    // get from env
    char* tt = getenv(vxENCRYPT("TMPDIR"));
    if (!tt) {
        tt = getenv(vxENCRYPT("TMP"));
        if (!tt) {
            tt = getenv(vxENCRYPT("TEMP"));
            if (!tt) {
                tt = getenv(vxENCRYPT("TEMPDIR"));
            }
        }
    }

    // allocate
    int allocLen = 0;
    if (tt == nullptr) {
        // use /tmp
        allocLen = strlen(vxENCRYPT(TMP_DEFAULT_UNIX)) + 1;
    }
    else {
        allocLen = strlen(tt) + 1;
    }
    char* t = (char*)CMemory::alloc(allocLen);
    if (!t) {
        return ENOMEM;
    }

    // copy
    if (tt == nullptr) {
        // use /tmp
        strcpy(t,vxENCRYPT(TMP_DEFAULT_UNIX));
    }
    else {
        strcpy(t,tt);
    }
    *tmpPath = t;
    return 0;
}

void BdUtils::CUser::logoff() {
#ifdef _WIN32
    // this will cause logoff
    ExitWindowsEx(EWX_LOGOFF,0);
#else
    // kill all user processes, this will cause logoff
    char* user;
    CUser::userName(&user);
    char str[260];
    snprintf(str,sizeof(str),vxENCRYPT("-KILL -u %s"), user);
    CProcess::run(vxENCRYPT("pkill"),str);
    CMemory::free(user);
#endif
}
