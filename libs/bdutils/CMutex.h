//
// Created by jrf/ht on 19/02/19.
//

#ifndef RCSLINUX_CMUTEX_H
#define RCSLINUX_CMUTEX_H

/**
 * BdUtils groups backdoor utilities framework
 */
#include <mutex>

namespace BdUtils {

    /**
     * @brief mutex wrapper, wraps the inner mutex with std::lock_guard to provide RAII behaviour:\n
     *  attempts to grab the lock (blocks indefinitely until acquired), unlocks automatically on leaving the scope\n
     * @param m pass C::Mutex->stdMutex() here
     */
    #define MUTEX_GUARD(m) std::lock_guard<std::recursive_timed_mutex> __m__(m)

    /**
     * @brief mutex wrapper, wraps the inner mutex with std::unique_lock
     * @param m pass C::Mutex->stdMutex() here
     * @param mm the std::unique_lock variable name
     * @note provides the same RAII functionalities as MUTEX_GUARD, but allows the object to be used with lock/unlock/wait/wait_for
     */
    #define MUTEX_UNIQUE_LOCK(m, mm) std::unique_lock<std::recursive_timed_mutex> mm(m)

    /**
     * @class CMutex
     * @brief portable mutex implementation
     * @todo std based, if not supported add platform support
     */
    class CMutex {
    public:
        /**
         * creates a mutex
         */
        CMutex();
        ~CMutex();

        /**
         * blocks until the mutex is acquired (or error occurs)
         * @param msec optional, blocks for as many milliseconds (either, blocks until unlocked)
         * @note the mutex can be locked multiple times by the same thread, as soon as the same number of unlocks are performed.
         * @return 0 if the mutex is acquired, or errno
         */
        int lock(int msec=-1);

        /**
         * try to acquire the mutex and immediately exit
         * @return 0 if the mutex is acquired, or errno
         */
        int tryLock();

        /**
         * release the mutex, called on an already released mutex has no effect
         * @return 0 on success, or errno
         */
        void unlock();

        /**
         * returns the underlying std::recursive_timed_mutex
         * @return std::recursive_timed_mutex&
         */
        std::recursive_timed_mutex& stdMutex();

        /**
         * debug test code placeholder
         * @todo: partially implemented, write proper unit test !
         * @return 0 on success
         */
        static int test();

    private:
        std::recursive_timed_mutex _mtx;
        bool _locked=false;

    };
}
#endif //RCSLINUX_CMUTEX_H
