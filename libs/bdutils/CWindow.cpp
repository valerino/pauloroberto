//
// Created by jrf/ht on 14/03/19.
//

#include "CWindow.h"
#include <errno.h>
#include "obfuscate.h"
#include "CString.h"
#include "CMemory.h"
#include "CFile.h"
#include <X11/Xatom.h>
#include <X11/Xutil.h>
#include <cstring>

/**
 * get the given X11 property
 * @param display an opened X11 display
 * @param w the root window
 * @param name name of the property to retrieve
 * @return the property to be freed with XFree(), or null
 * @note X11 only
 */
void *BdUtils::CWindow::x11GetWindowProperty(DisplayHandle* display, WindowHandle w, const char *name) {
#if defined (__linux__) && !defined (__ANDROID__)
    Atom actualType = 0;
    int actualFormat = 0;
    unsigned long items = 0;
    unsigned long bytesAfter = 0;
    unsigned char* property = nullptr;
    Atom activeWindow = XInternAtom( display, name, true );
    int res = XGetWindowProperty(display, w, activeWindow, 0, -1, false, AnyPropertyType, &actualType, &actualFormat, &items, &bytesAfter, &property);
    if (res == Success) {
        // return found property
        return property;
    }
    return nullptr;
#endif
}

int BdUtils::CWindow::getFocusWindow(DisplayHandle *display, WindowHandle *wnd) {
#if defined (_WIN32)
    HWND hwnd = GetActiveWindow();
    *wnd = hwnd;
    return 0;
#elif defined (TARGET_OS_MAC) && !defined(TARGET_OS_IPHONE)
    // @todo macos
#elif defined (__linux__) && !defined (__ANDROID__)
    // linux
    if (!display) {
        return EINVAL;
    }
    *wnd = 0;
    Window root = DefaultRootWindow(display);
    unsigned char* data = (unsigned char*)x11GetWindowProperty(display, root, vxENCRYPT("_NET_ACTIVE_WINDOW"));
    if (!data) {
        return ENOENT;
    }

    // got the active window id
    Window w = *(Window*)data;
    *wnd = w;
    XFree(data);
    return 0;
#endif
}

int BdUtils::CWindow::getWindowTitle(DisplayHandle *display, WindowHandle wnd, char **title, char** process) {
    if (!title || !wnd) {
        return EINVAL;
    }
    *title = nullptr;
    char* t = nullptr;
    if (process) {
        *process = nullptr;
    }

#if defined (_WIN32)
    // @todo windows
#elif defined (TARGET_OS_MAC) && !defined(TARGET_OS_IPHONE)
    // @todo macos
#else
    // linux
    if (!display) {
        return EINVAL;
    }

    // get title
    unsigned char* data = (unsigned char*)x11GetWindowProperty(display, wnd, vxENCRYPT("_NET_WM_NAME"));
    if (!data) {
        // try alternative (old) name
        data = (unsigned char *) x11GetWindowProperty(display, wnd, vxENCRYPT("WM_NAME"));
    }
    if (data) {
        // dup the title
        t = CString::dup((char*)data);
        XFree(data);
    }
    else {
        // empty
        t = CString::dup("");
    }

    if (process) {
        // get pid
        pid_t pid = 0;
        data = (unsigned char*)x11GetWindowProperty(display, wnd, vxENCRYPT("_NET_WM_PID"));
        if (data) {
            pid = (*(pid_t *)data);
            XFree(data);
        }

        // get class name
        char* p = nullptr;
        data = (unsigned char*)x11GetWindowProperty(display, wnd, vxENCRYPT("WM_CLASS"));
        if (data) {
            // according to x11 docs, the process name is the 2nd string in the returned buffer
            char* processName = (char*)data + strlen((char*)data) + 1;

            // build process string
            p = (char*)CMemory::alloc(strlen(processName) + 32);
            sprintf(p,"%s (%u)", processName, pid);
            XFree(data);
        }
        else {
            // unknown
            p = (char*)CMemory::alloc(32);
            sprintf(p, vxENCRYPT("Unknown (%u)"), pid);
        }
        *process = p;
    }
    *title = t;
    return 0;
#endif
}

int BdUtils::CWindow::getCurrentWindowTitle(DisplayHandle *display, char **title, char **process) {
    Window wnd;
    int res = getFocusWindow(display ,&wnd);
    if (res != 0) {
        return res;
    }
    return getWindowTitle(display,wnd,title, process);
}

BdUtils::DisplayHandle *BdUtils::CWindow::openDisplay() {
#if defined (__linux__) && !defined(__ANDROID__)
    // open the X11 display
    Display* display = XOpenDisplay(nullptr);
    if (!display) {
        return nullptr;
    }
    return display;
#endif
}

void BdUtils::CWindow::closeDisplay(DisplayHandle* d) {
#if defined (__linux__) && !defined(__ANDROID__)
    if (d) {
        // close the x11 display
        XCloseDisplay(d);
    }
#endif
}
