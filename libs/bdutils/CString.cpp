//
// Created by jrf/ht on 13/02/19.
//

#include "CString.h"
#include "CMemory.h"
#include "bdutils.h"
#include <errno.h>
#include <string.h>
#include <locale>
#include <string>
#include <codecvt>

char *BdUtils::CString::dup(const char *s) {
    if (!s) {
        return nullptr;
    }
    int l = strlen(s);
    char* ss = (char*)CMemory::dup((void*)s, l + 1);
    return ss;
}

int BdUtils::CString::joinPath(char *path1, size_t path1Size, const char *path2) {
    if (!path2 || !path1 || !path1Size) {
        return EINVAL;
    }
    if (path1Size < strlen(path1) + strlen(path2) + 2) {
        return EOVERFLOW;
    }

    // call the alloc overload
    char* r;
    int res = joinPath(path1,path2,&r);
    if (res == 0) {
        strcpy(path1,r);
        CMemory::free(r);
    }
    return res;
}

int BdUtils::CString::joinPath(const char *path1, const char *path2, char **out) {
    if (!path2 || !path2 || !out) {
        return EINVAL;
    }
    *out = nullptr;
    int l2 = strlen(path2);
    int l1 = strlen(path1);
    int allocLen = l2 + l1 + 2;
    char* r=(char*)CMemory::alloc(allocLen);
    if (!r) {
        return ENOMEM;
    }

    // copy & strcat
    strcpy(r,path1);
    stripLastSlash(r);
    strcat(r,"/");
    strcat(r,path2);
    *out = r;
    return 0;
}

void BdUtils::CString::stripLastSlash(char *s) {
    stripLastChar(s,'/');
}

void BdUtils::CString::stripLastChar(char *s, char c) {
    if (!s) {
        return;
    }
    int l = strlen(s);
    if (l != 0) {
        if (s[l - 1] == c) {
            s[l - 1] = '\0';
        }
    }
}

int BdUtils::CString::fromErrno(int errnoVal, char *err, size_t size) {
    if (!err || !size) {
        return EINVAL;
    }

    // hack to sorta convert gnu to posix
    char* res = strerror_r(errnoVal, err, size);
    if (res && res != err) {
        if (size < strlen(res) + 1) {
            return EOVERFLOW;
        }
        // copy back the result
        strcpy(err,res);
    }

    // either, strerror_r returns in the passed buffer directly
    return 0;
}

const char *BdUtils::CString::fromErrno(int errnoVal) {
    return strerror(errnoVal);
}


int BdUtils::CString::toHexString(const uint8_t *in, size_t inSize, char *hexStr, size_t size) {
    if (!in || !inSize || !hexStr || !size) {
        return EINVAL;
    }
    if (size < ((inSize * 2) + 1)) {
        return EOVERFLOW;
    }

    // call the alloc version
    char* h;
    int res = toHexString(in, inSize, &h);
    if (res != 0) {
        return res;
    }
    strcpy(hexStr,h);
    CMemory::free(h);
    return 0;
}

int BdUtils::CString::toHexString(const uint8_t *in, size_t inSize, char **hexStr) {
    if (!in || !inSize || !hexStr) {
        return EINVAL;
    }
    *hexStr = nullptr;

    // allocate
    char* out = (char*)CMemory::alloc((inSize*2) + 1);
    if (!out) {
        return ENOMEM;
    }

    for (int i=0; i < inSize; i++) {
        char b[3] = {0};
        sprintf(b,"%02x",in[i]);
        strcat(out,b);
    }
    *hexStr = out;
    return 0;
}

int BdUtils::CString::fromHexString(const char *hexStr, uint8_t *out, size_t outSize) {
    if (!hexStr || !out || !outSize) {
        return EINVAL;
    }
    int len = strlen(hexStr);
    if (outSize < (len / 2)) {
        return EOVERFLOW;
    }

    // call the alloc version
    uint8_t* o;
    size_t oSize;
    int res = fromHexString(hexStr,&o,&oSize);
    if (res != 0) {
        return res;
    }

    // copy result
    memcpy(out,o,oSize);
    CMemory::free(o);
    return 0;

}

int BdUtils::CString::fromHexString(const char *hexStr, uint8_t **out, size_t *outSize) {
    if (!hexStr || !out) {
        return EINVAL;
    }
    *out = nullptr;
    if (outSize) {
        *outSize = 0;
    }

    int l = strlen(hexStr);
    if ((l % 2) != 0) {
        // not a proper hex string
        return EINVAL;
    }

    // allocate
    uint8_t* b = (uint8_t*)CMemory::alloc(l / 2);
    if (!b) {
        return ENOMEM;
    }

    int k = 0;
    for (int i = 0; i < l; i += 2) {
        char tmp[3] = {0};
        tmp[0] = hexStr[i];
        tmp[1] = hexStr[i + 1];
        b[k] = strtol(tmp, nullptr,16);
        k++;
    }
    if (outSize) {
        *outSize = l / 2;
    }
    *out=b;

    return 0;
}

int BdUtils::CString::toUtf16(const char *utf8Str, char16_t **utf16Str, size_t *size) {
    if (!utf16Str || !utf8Str || !size) {
        return EINVAL;
    }
    char16_t* out = nullptr;
    size_t outSize = 0;
    if (*utf16Str && *size) {
        // provided
        out = *utf16Str;
        if (*size == 0) {
            return EINVAL;
        }
        outSize = *size;
        memset(out,0,outSize);
    }
    else {
        // will alloc
        *utf16Str = nullptr;
        *size = 0;
    }

    // perform the conversion
    std::wstring_convert<std::codecvt_utf8_utf16<char16_t>,char16_t> convert;
    std::u16string wcs = convert.from_bytes(utf8Str);

    // calculate size (needed size is size of string in bytes + size of the 0 terminator + size of the initial length)
    int wcsLen = wcs.length() * sizeof(char16_t);
    int neededSize = wcsLen + sizeof(uint32_t) + sizeof(char16_t);
    if (outSize && (outSize < neededSize)) {
        // if size provided, and it's not enough
        return EOVERFLOW;
    }

    if (!outSize) {
        // alloc
        out = (char16_t*)CMemory::alloc(neededSize);
        if (!out) {
            return ENOMEM;
        }
    }

    // write size in bytes, including null terminator (2 bytes)
    int l = (wcs.length() + 1) * 2;
    memcpy((char*)out, &l, sizeof(int));

    // write string
    memcpy((char*)out + sizeof(int), wcs.c_str(), wcsLen);

    *utf16Str = out;
    *size = wcsLen;
    return 0;
}

int BdUtils::CString::fromUtf16(const char16_t *utf16Str, char **utf8Str, size_t* utf16Size) {
    if (!utf16Str || !utf8Str) {
        return EINVAL;
    }
    if (utf16Size) {
        *utf16Size = 0;
    }
    *utf8Str = nullptr;

    // skip the size, the underlying library will perform the conversion without
    uint32_t uSize = *((uint32_t*)utf16Str);
    char* start = (char*)utf16Str + sizeof(uint32_t);

    // perform the conversion
    std::wstring_convert<std::codecvt_utf8_utf16<char16_t>,char16_t> convert;
    std::string mbs = convert.to_bytes((const char16_t*)start);
    char* p = CString::dup(mbs.c_str());
    if (!p) {
        return ENOMEM;
    }
    *utf8Str = p;
    if (utf16Size) {
        *utf16Size = uSize;
    }
    return 0;
}

int BdUtils::CString::test() {
#ifndef NDEBUG
    /*
    char str[] = "%USERPROFILE%/bla/$dir$/blu/${HOME}/bla.exe";
    char* out;
    CString::expandEnvVars(str,&out);
    CMemory::free(out);
    */
    char16_t * ucs2 = nullptr;
    size_t size = 0;
    CString::toUtf16("valerio", &ucs2, &size);
    char* utf8;
    CString::fromUtf16(ucs2, &utf8);
    if (strcmp(utf8,"valerio") == 0) {
        return 0;
    }
    return 1;
#else
    return 0;
#endif
}

/**
 * internally used by @ref expandEnvVars()
 * @param src on input, pointer to the input buffer where to check for an environment variable (%something%,$something$,...). on output, the pointer advanced of the token size or by a char if no token found
 * @param out on input, the output buffer for the expanded variable. on output, it may be the same or a reallocated buffer (no need to free the original)
 * @param allocSize on input, allocated size of the output buffer. on output, if the buffer is reallocated, the new reallocated size
 * @param consumed on input, current offset into the output buffer. on output, the new offset after adding the expanded string
 * @return 0 on success, or errno
 */
int BdUtils::CString::expandEnvVarsInternal(char **src, char **out, int *allocSize, int *consumed) {
    if (!src || !allocSize || !consumed || !out) {
        return EINVAL;
    }
    bool found = false;
    char* pStr = *src;
    // check if it's an env var
    char* v = nullptr;
    bool vAllocated = false;
    if (strncasecmp(pStr, vxENCRYPT("%userprofile%"), 13) == 0) {
#ifndef _WIN32
        // this will work also when root
        CUser::home(&v);
        vAllocated = true;
#else
        // @todo use GetEnvironmentVariable()
#endif

        // advance source pointer
        *src += 13;
        found = true;
    } else if (strncasecmp(pStr, vxENCRYPT("$dir$"), 5) == 0) {
        char cwd[1024]={0};
#ifndef _WIN32
        getcwd(cwd,sizeof(cwd));
#else
        // @todo use GetCurrentDirectory()
#endif
        v = cwd;

        // advance source pointer
        *src += 5;
        found = true;
    }
    else if (*pStr == '$' || *pStr == '{') {
        // environment variable in the format ${var}
        char vv[64]={0};
        char* p = pStr + 2;
        int vLen = 0;
        while(*p != '}' && *p != '\0' && vLen < sizeof(vv)) {
            vv[vLen] = *p;
            vLen++;
            p++;
        }
#ifndef _WIN32
        v = getenv(vv);
#else
        // @todo use GetEnvironmentVariable()
#endif
        // advance source pointer (size + $ { })
        *src += 3 + vLen;
        found = true;
    }

    int n;
    if (found) {
        // size of the expanded environment variable value
        if (v) {
            n = strlen(v);
        }
        else {
            // env variable was not set ?
            n = 0;
        }
    }
    else {
        // only 1 char will be copied from src to out, then src advanced by 1 char
        n = 1;
        v = *src;
        *src += 1;
    }
    char* o = *out;
    if (*consumed + n > *allocSize) {
        // if it exceeds the allocated size, reallocate buffer
        *allocSize += 1024 + n;
        o = (char *) BdUtils::CMemory::realloc(*out, *allocSize);
        if (!o) {
            if (vAllocated) {
                CMemory::free(v);
            }
            return ENOMEM;
        }

        // replace output buffer with the new reallocated
        *out = o;
    }

    // copy the string to the buffer at offset
    memcpy(o + *consumed, v, n);

    // and update the consumed offset
    *consumed += n;
    if (vAllocated) {
        CMemory::free(v);
    }
    return 0;
}

int BdUtils::CString::expandEnvVars(const char *str, char **out) {
    if (!str || !out) {
        return EINVAL;
    }
    int res = 0;

    // preallocate a buffer for the output string
    int consumed = 0;
    int allocSize = 1024;
    char* o = (char*)CMemory::alloc(allocSize);
    if (!o) {
        return ENOMEM;
    }
    do {
        // walk the input string char by char
        char* p = (char*)str;
        while (*p) {
            res = expandEnvVarsInternal(&p, &o, &allocSize, &consumed);
            if (res != 0) {
                break;
            }
        }
    } while (0);
    if (res != 0) {
        CMemory::free(o);
        o=nullptr;
    }
    *out = o;
    return res;
}

int BdUtils::CString::basePath(const char *path, char **out) {
    if (!path || !out) {
        return EINVAL;
    }
    *out = nullptr;

    // get filename ptr
    const char* fn = filename(path);
    int lenAlloc = strlen(path) - strlen(fn);
    char* mem = (char*)CMemory::alloc(lenAlloc);
    if (!mem) {
        return ENOMEM;
    }

    // copy the path only (no last slash)
    memcpy(mem, path, lenAlloc - 1);
    *out = mem;
    return 0;
}

const char* BdUtils::CString::filename(const char *path) {
    if (!path) {
        return nullptr;
    }

    if (strcmp(path,"/")) {
        // special case
        return path;
    }

    const char* p = strrchr(path,'/');
    if (!p) {
#ifdef _WIN32
        // retry with windows only separator
        p = strrchr(path,'\\');
        if (!p) {
            return path;
        }
#else
        return path;
#endif
    }
    p++;
    return p;
}

int BdUtils::CString::randomString(int rndBytes, char *str, int size) {
    if (!str || !size || !rndBytes) {
        return EINVAL;
    }

    // check size
    int neededSize = (rndBytes * 2) + 1;
    if (size < neededSize) {
       return EOVERFLOW;
    }
    memset(str,0,size);

    // generate the random string
    uint8_t* rnd = (uint8_t*)CMemory::alloc(rndBytes);
    if (!rnd) {
        return ENOMEM;
    }
    CRNG rng;
    rng.randBytes(rnd, rndBytes);

    // stringify
    toHexString(rnd, rndBytes, str, size);
    CMemory::free(rnd);
    return 0;
}
