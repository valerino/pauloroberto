//
// Created by jrf/ht on 13/02/19.
//

#ifndef RCSLINUX_CAES_H
#define RCSLINUX_CAES_H

#include <stdint.h>
#include <stdio.h>
#include <mbedtls/aes.h>

/**
 * BdUtils groups backdoor utilities framework
 */
namespace BdUtils {
    /**
     * @brief pass to constructor to perform AES encryption (used in CBC mode only)
     */
    #define AES_MODE_ENCRYPT MBEDTLS_AES_ENCRYPT
    /**
     * @brief pass to constructor to perform AES decryption (used in CBC mode only)
     */
    #define AES_MODE_DECRYPT MBEDTLS_AES_DECRYPT

    /**
     * @brief no padding (buffers must be supplied already padded) (used in CBC mode only)
     */
    #define AES_NO_PADDING 0

    /**
     * @brief pad with zeroes to the next 16 bytes (used in CBC mode only)
     */
    #define AES_ZERO_PADDING 1

    /**
     * add PKCS style padding (to the next 16 bytes), allows to retrieve the real size on decryption (used in CBC mode only)
     */
    #define AES_PKCS7_PADDING 2

    /**
     * @class CAES
     * @brief implements AES encryption (128,192,256), CTR/nopadding, CBC/padding supported, based on mbedtls
     * @todo CBC mode may be removed ......
     */
    class CAES {
    public:
        /**
         * test code
         * @return 0 on success
         */
        static int test();

        /**
         * encrypt data using AES CBC with the given key and iv, oneshot
         * @param key the key buffer
         * @param keyBits the key bits (128/192/256, 16-24-32 bytes)
         * @param paddingMode padding to 16 bytes mode. AES_NO_PADDING (expects already padded input), AES_ZERO_PADDING(pad with 0), AES_PKCS7_PADDING
         * @param iv the iv (16 bytes), if NULL an empty iv is used (discouraged!)
         * @param in input buffer
         * @param size size of the input buffer
         * @param out on input, may point to an already supplied buffer\n
         *  if it points to NULL, the buffer is allocated according to paddingMode and must be freed using CMemory::free()
         * @param outSize on input, only if out is supplied, must contain the 16-bytes PADDED size of the output buffer\n
         *  on successful return, size of the processed buffer
         * @return 0 on success, or errno
         */
        static int encryptCbc(uint8_t *key, int keyBits, int paddingMode, uint8_t *iv, void *in, size_t size,
                              uint8_t **out, size_t *outSize);

        /**
         * decrypt data encrypted using AES CBC with the given key and iv, oneshot
         * @param key the key buffer
         * @param keyBits the key bits (128/192/256, 16-24-32 bytes)
         * @param paddingMode padding to 16 bytes mode. AES_NO_PADDING (expects already padded input), AES_ZERO_PADDING(pad with 0), AES_PKCS7_PADDING
         * @param iv the iv (16 bytes), if NULL an empty iv is used (discouraged!)
         * @param in input buffer
         * @param size size of the input buffer
         * @param out on input, may point to an already supplied buffer\n
         *  if it points to NULL, the buffer is allocated according to paddingMode and must be freed using CMemory::free()
         * @param outSize on input, only if out is supplied, must contain the 16-bytes PADDED size of the output buffer\n
         *  on successful return, size of the processed buffer
         * @return 0 on success, or errno
         */
        static int decryptCbc(uint8_t *key, int keyBits, int paddingMode, uint8_t *iv, void *in, size_t size,
                              uint8_t **out, size_t *outSize);

        /**
         * encrypt data using AES CTR with the given key and iv, oneshot
         * @param key the key buffer
         * @param keyBits the key bits (128/192/256, 16-24-32 bytes)
         * @param nonce a 16 bytes nonce, if NULL an empty buffer is used (discouraged!)
         * @param in input buffer
         * @param size size of the input buffer
         * @param out on input, may point to an already supplied buffer\n
         *  if it points to NULL, the buffer is allocated and must be freed using CMemory::free()
         * @return 0 on success, or errno
         */
        static int encryptCtr(uint8_t *key, int keyBits, uint8_t *nonce, void *in, size_t size,
                              uint8_t **out);

        /**
         * decrypt data encrypted using AES CTR with the given key and iv, oneshot
         * @param key the key buffer
         * @param keyBits the key bits (128/192/256, 16-24-32 bytes)
         * @param nonce a 16 bytes nonce, if NULL an empty buffer is used (discouraged!)
         * @param in input buffer
         * @param size size of the input buffer
         * @param out on input, may point to an already supplied buffer\n
         *  if it points to NULL, the buffer is allocated and must be freed using CMemory::free()
         * @return 0 on success, or errno
         */
        static int decryptCtr(uint8_t *key, int keyBits, uint8_t *nonce, void *in, size_t size,
                              uint8_t **out);

        /**
         * initializes an empty AES/CTR encrypter/decrypter, on which update() can be called subsequently
         * @param key the key buffer
         * @param keyBits the key bits (128/192/256, 16-24-32 bytes)
         * @param nonce a 16 bytes nonce, if NULL an empty buffer is used (discouraged!)
         * @throws on wrong parameters
         */
        CAES(uint8_t* key, int keyBits, uint8_t* nonce= nullptr);

        /**
         * initializes an empty AES CBC encrypter/decrypter, on which update() can be called subsequently
         * @param mode AES_MODE_ENCRYPT, AES_MODE_DECRYPT, AES_MODE_DECRYPT_PKCS7. AES is used in CBC mode
         * @param key the key buffer
         * @param keyBits the key bits (128/192/256, 16-24-32 bytes)
         * @param paddingMode padding to 16 bytes mode. AES_NO_PADDING (expects already padded input), AES_ZERO_PADDING(pad with 0), AES_PKCS7_PADDING
         * @param iv the iv (16 bytes). if NULL (default) an empty iv is used (discouraged!)
         * @throws on wrong parameters
         */
        CAES(int mode, uint8_t* key, int keyBits, int paddingMode, uint8_t* iv= nullptr);
        virtual ~CAES();

        /**
         * encrypt or decrypt, updating the internal context (cbc mode only)
         * @param in buffer to be encrypted/decrypted
         * @param size size of the input buffer
         * @param out on input, may point to an already supplied buffer, padded if needed\n
         *  if it points to NULL, the buffer is allocated according to paddingMode and must be freed using CMemory::free()
         * @param outSize on input, if out is supplied, must contain the size (padded if needed) of the out buffer\n
         *  on successful return, size of the processed buffer
         * @returns 0 on success, or ENOMEM or MBEDTLS_ERR_AES_INVALID_INPUT_LENGTH
         */
        int update(void *in, size_t size, uint8_t **out, size_t *outSize);

        /**
         * encrypt or decrypt, updating the internal context (ctr mode only)
         * @param in buffer to be encrypted/decrypted
         * @param size size of the input/output buffer
         * @param out on input, may point to an already supplied buffer\n
         *  if it points to NULL, the buffer is allocated and must be freed using CMemory::free()
         * @returns 0 on success, or ENOMEM or MBEDTLS_ERR_AES_INVALID_INPUT_LENGTH
         */
        int updateCtr(void *in, size_t size, uint8_t **out);

    private:
        int _mode;
        int _paddingMode;
        bool _ctr = false;
        mbedtls_aes_context _ctx = {0};
        uint8_t _iv[16] = {0};
        size_t _ctrOffset = 0;
        uint8_t _ctrStreamBlock[16] = {};
    };
}

#endif //RCSLINUX_CAES_H
