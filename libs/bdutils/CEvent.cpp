//
// Created by jrf/ht on 18/02/19.
//

#include "CEvent.h"
#include "CLog.h"
#include <errno.h>
#include "CThread.h"
using namespace BdUtils;

BdUtils::CEvent::CEvent(bool autoReset) {
    _autoreset = autoReset;
}

BdUtils::CEvent::~CEvent() {
}

void BdUtils::CEvent::set() {
    // signal the event to all waiting threads
    _mtx.lock();
    _signaled = true;
    _cond.notify_all();
    _mtx.unlock();
}

/**
 * as per c++11 docs, using a predicate function avoid checking for spurious awakes
 * @return true if the condition variable is effectively signaled (set() has been called)
 */
bool BdUtils::CEvent::predicate() {
    return _signaled;
}

bool CEvent::signaled() {
    // 0msec wait
    if (wait(0) == 0) {
        return true;
    }
    return false;
}

int BdUtils::CEvent::wait(int msec) {
    MUTEX_UNIQUE_LOCK(_mtx.stdMutex(), lock);
    _numWaiting++;
    int res = 0;
    if (msec == -1) {
        // infinite wait (using a lambda to easily use a member function as predicate)
        _cond.wait(lock, [this] { return this->predicate(); });
    }
    else {
        // timed wait (using a lambda to easily use a member function as predicate)
        bool b = _cond.wait_for(lock, std::chrono::milliseconds(msec), [this] { return this->predicate(); });
        if (!b) {
            res = ETIMEDOUT;
        }
    }
    if (res == 0) {
        // wait satisfied, decrement the number of waiting threads
        _numWaiting--;
    }

    if (_autoreset && _numWaiting == 0) {
        // reset the event if there's no more waiting threads and autoreset is active
        reset();
    }
    return res;
}

void CEvent::reset() {
    MUTEX_GUARD(_mtx.stdMutex());
    _signaled = false;
}

void CEvent::testThread1(void *params) {
#ifndef NDEBUG
    DBGLOGV("t1 starts");
    CEvent* ev = (CEvent*) params;
    DBGLOGV("t1 sleeps for 10 seconds");
    std::this_thread::sleep_for(std::chrono::milliseconds(10*1000));
    DBGLOGV("t1 sets the event");
    ev->set();
    DBGLOGV("t1 exiting");
#else
    return;
#endif
}

void CEvent::testThread2(void *params) {
#ifndef NDEBUG
    DBGLOGV("t2 starts and waits for the condition to be set");
    CEvent* ev = (CEvent*) params;
    ev->wait();
    DBGLOGV("t2 exiting");
#else
    return;
#endif
}

void CEvent::testThread3(void *params) {
#ifndef NDEBUG
    DBGLOGV("t3 starts and waits for the condition to be set");
    CEvent* ev = (CEvent*) params;
    ev->wait();
    DBGLOGV("t3 exiting");
#else
    return;
#endif
}

void CEvent::testThread4(void *params) {
#ifndef NDEBUG
    DBGLOGV("t4 starts and waits for the condition to be set");
    CEvent* ev = (CEvent*) params;
    ev->wait();
    DBGLOGV("t4 exiting");
#else
    return;
#endif
}

int BdUtils::CEvent::test() {
#ifndef NDEBUG
    CEvent evt;

    DBGLOGV("test condition signaled");
    CThread t1 = CThread(false, &CEvent::testThread1, &evt);
    CThread t2 = CThread(false, &CEvent::testThread2, &evt);
    CThread t3 = CThread(false, &CEvent::testThread3, &evt);
    CThread t4 = CThread(false, &CEvent::testThread4, &evt);
    t1.wait();
    t2.wait();
    t3.wait();
    t4.wait();

    DBGLOGV("test condition signaled again (without reset, immediately unlocks)");
    CThread t5 = CThread(false, &CEvent::testThread1, &evt);
    CThread t6 = CThread(false, &CEvent::testThread2, &evt);
    CThread t7 = CThread(false, &CEvent::testThread3, &evt);
    CThread t8 = CThread(false, &CEvent::testThread4, &evt);
    t5.wait();
    t6.wait();
    t7.wait();
    t8.wait();

    DBGLOGV("test condition signaled again (after being reset)");
    evt.reset();
    CThread t9 = CThread(false, &CEvent::testThread1,  &evt);
    CThread t10 = CThread(false, &CEvent::testThread2, &evt);
    CThread t11 = CThread(false, &CEvent::testThread3, &evt);
    CThread t12 = CThread(false, &CEvent::testThread4, &evt);
    t9.wait();
    t10.wait();
    t11.wait();
    t12.wait();
    return 0;
#else
    return 0;
#endif
}
