//
// Created by jrf/ht on 20/02/19.
//

#include "CSHA256.h"
#include <CFile.h>
#include <CString.h>
#include <CMemory.h>
#include <errno.h>

using namespace BdUtilsPrivate;
BdUtils::CSHA256::CSHA256() : CSHABase(&_ctx, (SHA_UPDATE_FUNC)mbedtls_sha256_update_ret, (SHA_FINISH_FUNC)mbedtls_sha256_finish_ret, HASH_SHA256_SIZE) {
    mbedtls_sha256_init(&_ctx);
    mbedtls_sha256_starts_ret(&_ctx, 0);
}

BdUtils::CSHA256::~CSHA256() {
    mbedtls_sha256_free(&_ctx);
}

int BdUtils::CSHA256::test() {
#ifndef NDEBUG
    uint8_t hash[32];
    hashFile("/tmp/testfile",hash,sizeof(hash) );
    char* hashS;
    CString::toHexString(hash,sizeof(hash),&hashS);
    CMemory::free(hashS);
    return 0;
#else
    return 0;
#endif
}

int BdUtils::CSHA256::hashFile(const char *path, uint8_t *hash, size_t hashSize) {
    CSHA256 c;
    return CSHABase::hashFile(path, hash, hashSize, &c);
}

int BdUtils::CSHA256::hashBuffer(const uint8_t *buf, size_t size, uint8_t *hash, size_t hashSize) {
    CSHA256 c;
    return CSHABase::hashBuffer(buf, size, hash, hashSize, &c);
}
