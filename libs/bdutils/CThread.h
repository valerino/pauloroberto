//
// Created by jrf/ht on 18/02/19.
//

#ifndef RCSLINUX_CTHREAD_H
#define RCSLINUX_CTHREAD_H

#include <thread>

/**
 * BdUtils groups backdoor utilities framework
 */
namespace BdUtils {
    /**
     * @class CThread
     * @brief implements threading utilities, std based
     */
    class CThread {
    public:
        /**
         * constructor, wraps an std::thread
         * @param detached true if the thread must start detached (fire and forget)
         * @note the destructor automatically calls @ref CThread::wait() for non-detached threads
         * @param f the thread start function
         * @param args variable arguments
         */
        template<typename Callable, typename... Args> CThread(bool detached, Callable &&f, Args &&... args) {
            _t = new std::thread(f, std::forward<Args>(args)...);
            if (detached) {
                _t->detach();
                _detached = true;
            }
        }

        /**
         * the destructor automatically waits for tread termination of non detached threads,
         */
        ~CThread();

        /**
         * waits for the thread to terminate (join)
         * @note calling this for detached threads blocks until @ref CThread::setTerminated() is called on the CThread instance
         */
        void wait();

        /**
         * set this thread as terminated, to be used to call @ref CThread::wait() on a detached thread
         */
        void setTerminated();

        /**
         * check if @ref CThread::setTerminated() has been called
         * @return bool
         */
        bool terminated();

        /**
         * get the underlying native thread pointer (i.e. HANDLE on windows)
         * @return void*
         */
        void* ptr();

        /**
         * sleep the current thread for the given time
         * @param msec milliseconds to sleep
         */
        static void sleep(int msec);

        /**
         * get the current thread id
         * @return std::thread::id
         */
        static std::thread::id id();

        /**
         * get the current thread id as string (portable)
         * @return std::string
         */
        static std::string idString();

    private:
        std::thread* _t = nullptr;
        bool _terminated = false;
        bool _detached = false;
    };
}

#endif //RCSLINUX_CTHREAD_H
