//
// Created by jrf/ht on 26/02/19.
//

#ifndef RCSLINUX_CHTTP_H
#define RCSLINUX_CHTTP_H

#include <stdint.h>
#include <vector>
#include <string>
#include <curl/curl.h>

namespace BdUtils {
    class CFile;
}

/**
 * BdUtils groups backdoor utilities framework
 */
namespace BdUtils {
    #if defined (_WIN32)
        /**
     * @brief the default user agent for get/post, windows
     */
    #define HTTP_DEFAULT_USER_AGENT "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36"
    #elif defined (__APPLE__) && defined (TARGET_OS_MAC)
        /**
     * @brief the default user agent for get/post, macos
     */
    #define HTTP_DEFAULT_USER_AGENT "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36"
    #elif defined (linux)
    /**
     * @brief the default user agent for get/post, linux
     */
    #define HTTP_DEFAULT_USER_AGENT "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/71.0.3578.98 Chrome/71.0.3578.98 Safari/537.36"
    #elif defined (__ANDROID_API__)
        /**
     * @brief the default user agent for get/post, android
     */
    #define HTTP_DEFAULT_USER_AGENT "Mozilla/5.0 (Linux; Android 8.1.0; Moto G (5)) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.80 Mobile Safari/537.36"
    #else
    /**
     * @brief the default user agent for get/post, iOS
     */
    #define HTTP_DEFAULT_USER_AGENT "Mozilla/5.0 (iPhone; CPU iPhone OS 12_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/70.0.3538.75 Mobile/15E148 Safari/605.1"
    #endif

    /**
     * @brief used internally to define a GET operation
     */
    #define HTTP_GET 1

    /**
     * @brief used internally to define a POST operation
     */
    #define HTTP_POST 2

    /**
     * @brief used internally by @ref CHttp::get() and @ref CHttp::post()
     */
    typedef struct _curlCtx {
        CURL* curl; /**< the curl private context */
        uint8_t* buf; /**< buffer allocated to transfer data for GET*/
        size_t bufSize; /**< size of the allocated buffer */
        uint8_t* respBuf; /**< buffer allocated for POST response buffer*/
        size_t respBufSize; /**< size of the allocated response buffer */
        size_t offset; /**< current offset into buf (size of transferred data at now) */
        size_t respOffset; /**< current offset into respBuf (size of transferred data at now) */
        BdUtils::CFile* f; /** pointer to a file for read/write operations if needed */
        struct curl_slist *hl; /** used to add headers */
        int mode; /**< HTTP_GET or HTTP_POST */
    } CurlCtx;

    /**
     * @class CHttp
     * @brief http/s client utilities, based on mbedtls
     */
    class CHttp {
    public:
        /**
         * to be called before any other CHttp function, to initialize the underlying curl/mbedtls library
         * @return 0 on success, or an error defined in curl.h
         */
        static int initHttpLib();

        /**
         * to be called on cleanup, to release library resources
         */
        static void deinitHttpLib();

        /**
         * check if the library has been initialized correctly through @ref initHttpLib()
         * @return bool
         */
        static bool initialized();

        /**
         * download a file via http/s
         * @param url the complete url, [http|https]://url[:port]
         * @param out on successful return, a buffer with the downloaded file which must be freed using CMemory::free()
         * @param size on successful return, the buffer size
         * @param additionalHeaders optional, array of strings with headers to append in the request
         * @param userAgent optional, the user agent (default is @ref HTTP_DEFAULT_USER_AGENT)
         * @return 0 on success, EINVAL on invalid parameters, ECANCELED if initNetwork() haven't been called or an error defined in curl.h
         */
        static int get(const char* url, uint8_t** out, uint64_t * size, std::vector<std::string>* additionalHeaders=nullptr, const char* userAgent=nullptr);

        /**
         * download a file via http/s to path
         * @param url the complete url, [http|https]://url[:port]
         * @param path path to download the file to
         * @param size optional, on successful return the downloaded file size
         * @param additionalHeaders optional, array of strings with headers to append in the request
         * @param userAgent optional, the user agent (default is @ref HTTP_DEFAULT_USER_AGENT)
         * @return 0 on success, EINVAL on invalid parameters, ECANCELED if initNetwork() haven't been called or an error defined in curl.h
         */
        static int get(const char* url, const char* path, uint64_t * size, std::vector<std::string>* additionalHeaders=nullptr, const char* userAgent=nullptr);

        /**
         * post a buffer via http/s
         * @param url the complete url, [http|https]://url[:port]
         * @param buf the post buffer
         * @param size size of buffer
         * @param out on successful return, the response buffer to be freed with @ref BdUtils::CMemory::free()
         * @param outSize on successful return, the response buffer size
         * @param additionalHeaders optional, array of strings with headers to append in the request
         * @param userAgent optional, the user agent (default is @ref HTTP_DEFAULT_USER_AGENT)
         * @return 0 on success, EINVAL on invalid parameters, ECANCELED if initNetwork() haven't been called or an error defined in curl.h
         */
        static int post(const char* url, const uint8_t* buf, size_t size, uint8_t** out, size_t* outSize, std::vector<std::string>* additionalHeaders=nullptr, const char* userAgent=nullptr);

        /**
         * post a buffer via http/s, reading from file
         * @param url the complete url, [http|https]://url[:port]
         * @param userAgent optional, default is mozilla
         * @param path path to the file to be posted
         * @param out on successful return, the response buffer to be freed with @ref BdUtils::CMemory::free()
         * @param outSize on successful return, the response buffer size
         * @param additionalHeaders optional, array of strings with headers to append in the request
         * @param userAgent optional, the user agent (default is @ref HTTP_DEFAULT_USER_AGENT)
         * @return 0 on success, EINVAL on invalid parameters, ECANCELED if initNetwork() haven't been called or an error defined in curl.h
         */
        static int post(const char* url, const char* path, uint8_t** out, size_t* outSize, std::vector<std::string>* additionalHeaders=nullptr, const char* userAgent=nullptr);

        /**
         * test code
         * @return 0 on success
         */
        static int test();

    private:
        static bool _initialized;

        static int initOperation(CurlCtx* ctx, int mode, const char* url, std::vector<std::string>* additionalHeaders=nullptr, const char* userAgent=nullptr,
                const uint8_t* postBuf=nullptr, size_t size=0, const char* path= nullptr);

        static void finishOperation(CurlCtx* ctx);
    };
}


#endif //RCSLINUX_CHTTP_H
