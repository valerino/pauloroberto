//
// Created by jrf/ht on 26/02/19.
//

#include "CHttp.h"
#include "CMemory.h"
#include "CString.h"
#include "CFile.h"
#include "CSHA1.h"
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <vector>
#include "obfuscate.h"

bool BdUtils::CHttp::_initialized = false;

int BdUtils::CHttp::initHttpLib() {
    if (_initialized) {
        // already initialized
        return 0;
    }

    // initialize
    int res = curl_global_init(CURL_GLOBAL_ALL);
    if (res == CURLE_OK) {
        // done
        _initialized = true;
    }
    return res;
}

void BdUtils::CHttp::deinitHttpLib() {
    if (_initialized) {
        curl_global_cleanup();
        _initialized = false;
    }
}

size_t curlWriteCallback(char *ptr, size_t size, size_t nmemb, void *userdata) {
    if (size == 0) {
        return 0;
    }

    // nmemb is the actual size, size is always 1
    size_t actualSize = size*nmemb;
    BdUtils::_curlCtx* ctx = (BdUtils::_curlCtx*) userdata;

    size_t written;
    if (ctx->f) {
        // write to file
        ctx->f->write((uint8_t*)ptr, actualSize ,&written);
    }
    else {
        do {
            // write to internal buffer
            size_t neededBufSize = ctx->offset + actualSize;
            if (neededBufSize > ctx->bufSize) {
                // reallocate internal buffer
                ctx->buf = (uint8_t *) BdUtils::CMemory::realloc(ctx->buf, neededBufSize);
                if (!ctx->buf) {
                    // out of memory, setting written to 0 will report error, and transfer will be stopped
                    written = 0;
                    break;
                }
                // update the buffer size
                ctx->bufSize = neededBufSize;
            }

            // copy to offset
            memcpy(ctx->buf + ctx->offset,ptr, actualSize);

            // update offset
            ctx->offset += actualSize;

            // report all written
            written = actualSize;
        } while (0);
    }

    return written;
}

size_t curlWriteResponseCallback(char *ptr, size_t size, size_t nmemb, void *userdata) {
    if (size == 0) {
        return 0;
    }

    // nmemb is the actual size, size is always 1
    size_t actualSize = size*nmemb;
    BdUtils::_curlCtx* ctx = (BdUtils::_curlCtx*) userdata;

    size_t written;
    do {
        // write to internal buffer
        size_t neededBufSize = ctx->respOffset + actualSize;
        if (neededBufSize > ctx->respBufSize) {
            // reallocate internal buffer
            ctx->respBuf = (uint8_t *) BdUtils::CMemory::realloc(ctx->respBuf, neededBufSize);
            if (!ctx->respBuf) {
                // out of memory, setting written to 0 will report error, and transfer will be stopped
                written = 0;
                break;
            }
            // update the buffer size
            ctx->respBufSize = neededBufSize;
        }

        // copy to offset
        memcpy(ctx->respBuf + ctx->respOffset,ptr, actualSize);

        // update offset
        ctx->respOffset += actualSize;

        // report all written
        written = actualSize;
    } while (0);

    return written;
}

int BdUtils::CHttp::get(const char *url, uint8_t **out, uint64_t *size, std::vector<std::string>* additionalHeaders, const char *userAgent) {
    if (!url || !out || !size) {
        return EINVAL;
    }
    if (!_initialized) {
        // lib not initialized
        return ECANCELED;
    }

    *out=nullptr;
    *size=0;
    int res = 0;
    CurlCtx ctx = {};
    do {
        // initialize curl
        res = initOperation(&ctx, HTTP_GET, url, additionalHeaders, userAgent);
        if (res != CURLE_OK) {
            break;
        }

        // go!
        res = curl_easy_perform(ctx.curl);
        if (res != CURLE_OK) {
            break;
        }

        // transfer ownership of the allocated buffer
        *out = ctx.buf;
        *size = ctx.offset;
        ctx.buf = nullptr;
        ctx.bufSize = 0;
    } while (0);

    finishOperation(&ctx);
    return res;
}

int BdUtils::CHttp::get(const char *url, const char *path, uint64_t *size, std::vector<std::string>* additionalHeaders, const char *userAgent) {
    if (!url || !path) {
        return EINVAL;
    }
    if (!_initialized) {
        // lib not initialized
        return ECANCELED;
    }
    if (size) {
        *size = 0;
    }
    int res = 0;
    CurlCtx ctx = {};
    do {
        // initialize curl
        res = initOperation(&ctx, HTTP_GET, url, additionalHeaders, userAgent, nullptr, 0, path);
        if (res != CURLE_OK) {
            break;
        }

        // go!
        res = curl_easy_perform(ctx.curl);
        if (res != CURLE_OK) {
            break;
        }
    } while (0);

    // this also closes the file anyway, flushing the buffers
    finishOperation(&ctx);

    if (res == 0 && size) {
        // get the size
        CFile::size(path, size);
    }
    return res;
}

size_t curlReadCallback(char *buffer, size_t size, size_t nitems, void *userdata) {
    if (size == 0) {
        return 0;
    }
    BdUtils::_curlCtx* ctx = (BdUtils::_curlCtx*) userdata;
    size_t readBytes;
    size_t toRead = size*nitems;
    if (ctx->f) {
        // read from file
        ctx->f->read((uint8_t*)buffer, toRead, &readBytes);
    }
    else {
        // read from internal buffer
        if (ctx->offset + toRead > ctx->bufSize) {
            // read just the remaining
            toRead = ctx->bufSize - ctx->offset;
        }
        memcpy((uint8_t *) buffer, ctx->buf + ctx->offset, toRead);
        readBytes = toRead;
        ctx->offset += toRead;
    }

    return readBytes;
}

int BdUtils::CHttp::post(const char *url, const uint8_t *buf, size_t size, uint8_t** out, size_t* outSize, std::vector<std::string>* additionalHeaders, const char *userAgent) {
    if (!url || !buf || !size || !out || !outSize) {
        return EINVAL;
    }
    *out = nullptr;
    *outSize = 0;
    if (!_initialized) {
        // lib not initialized
        return ECANCELED;
    }

    int res = 0;
    CurlCtx ctx = {};
    do {
        // initialize curl
        res = initOperation(&ctx, HTTP_POST, url, additionalHeaders, userAgent, buf, size);
        if (res != CURLE_OK) {
            break;
        }

        // go!
        res = curl_easy_perform(ctx.curl);
        if (res != CURLE_OK) {
            break;
        }

        // transfer ownership of the allocated response buffer
        *out = ctx.respBuf;
        *outSize = ctx.respOffset;
        ctx.respBuf = nullptr;
        ctx.respBufSize = 0;
    } while (0);

    finishOperation(&ctx);
    return res;
}

int BdUtils::CHttp::post(const char *url, const char *path, uint8_t** out, size_t* outSize,  std::vector<std::string>* additionalHeaders, const char *userAgent) {
    if (!url || !path || !out || !outSize) {
        return EINVAL;
    }
    *out = nullptr;
    *outSize = 0;
    if (!_initialized) {
        // lib not initialized
        return ECANCELED;
    }

    int res = 0;
    CurlCtx ctx = {};
    do {
        // initialize curl
        res = initOperation(&ctx, HTTP_POST, url, additionalHeaders, userAgent, nullptr, 0, path);
        if (res != CURLE_OK) {
            break;
        }

        // go!
        res = curl_easy_perform(ctx.curl);
        if (res != CURLE_OK) {
            break;
        }

        // transfer ownership of the allocated response buffer
        *out = ctx.respBuf;
        *outSize = ctx.respOffset;
        ctx.respBuf = nullptr;
        ctx.respBufSize = 0;
    } while (0);

    finishOperation(&ctx);
    return res;
}

/**
 * called internally to cleanup context by @ref get() and @ref post() once done
 * @param ctx the curl context returned by @ref initOperation()
 * @note after returning, ctx and all its content are invalid!
 */
void BdUtils::CHttp::finishOperation(BdUtils::CurlCtx *ctx) {
    if (ctx) {
        if (ctx->mode == HTTP_GET) {
            // buffer is allocated
            CMemory::free(ctx->buf);
        }
        if (ctx->respBuf && ctx->mode == HTTP_POST) {
            // response buffer is allocated
            CMemory::free(ctx->respBuf);
        }
        if (ctx->hl) {
            curl_slist_free_all(ctx->hl);
        }
        if (ctx->curl) {
            curl_easy_cleanup(ctx->curl);
        }
        delete ctx->f;
    }
}

/**
 * called internally to initialize context for @ref get and @ref post
 * @param ctx on successful return, an initialized context
 * @param mode HTTP_GET or HTTP_POST
 * @param url the complete url, [http|https]://url[:port]
 * @param additionalHeaders optional, array of strings with headers to append in the request
 * @param userAgent optional, the user agent (default is @ref HTTP_DEFAULT_USER_AGENT)
 * @param postBuf optional, post buffer, ignored for HTTP_GET
 * @param size optional, post buffer size, ignored for HTTP_GET
 * @param path optional, path to the file to be posted, ignored for HTTP_GET or if post do not specify a file path
 * @return 0 on success, or errno (ECANCELED if curl initialization fails)
 */
int BdUtils::CHttp::initOperation(BdUtils::CurlCtx *ctx, int mode, const char *url, std::vector<std::string>* additionalHeaders, const char *userAgent,
        const uint8_t* postBuf, size_t size, const char *path) {
    if (!url || !ctx) {
        return EINVAL;
    }
    memset(ctx,0,sizeof(CurlCtx));

    int res = ECANCELED;
    do {
        // init curl
        ctx->curl = curl_easy_init();
        if (!ctx->curl) {
            break;
        }

        if (path) {
            // reading/writing buffers from/to file
            ctx->f = new CFile(path);
            if (mode == HTTP_GET) {
                // open file for writing
                res = ctx->f->open("wb");
                if (res != 0) {
                    // error opening file
                    break;
                }
            }
            else {
                // post, open file for reading
                res = ctx->f->open("rb");
                if (res != 0) {
                    // error opening file
                    break;
                }

                // set post size = file size
                uint64_t fs;
                ctx->f->size(&fs);
                curl_easy_setopt(ctx->curl, CURLOPT_POSTFIELDSIZE_LARGE, fs);
            }
        }
        else {
            // reading/writing from/to buffer in memory
            if (mode == HTTP_GET) {
                // allocate buffer for download
                ctx->buf = (uint8_t*)CMemory::alloc(CURL_MAX_WRITE_SIZE);
                if (!ctx->buf) {
                    res = ENOMEM;
                    break;
                }
                ctx->bufSize = CURL_MAX_WRITE_SIZE;
            }
            else {
                // post, use the provided buffer for upload
                ctx->buf = (uint8_t*)postBuf;
                ctx->bufSize = size;

                // set post size
                curl_easy_setopt(ctx->curl, CURLOPT_POSTFIELDSIZE_LARGE, size);
            }
        }

        // set additional headers if any
        if (additionalHeaders) {
            for (std::string& s : *additionalHeaders) {
                ctx->hl = curl_slist_append(ctx->hl, s.c_str());
                if (!ctx->hl) {
                    // out of memory
                    res = ENOMEM;
                    break;
                }
            }
            if (res == ENOMEM) {
                break;
            }
            curl_easy_setopt(ctx->curl, CURLOPT_HTTPHEADER, ctx->hl);
        }

        // set url
        curl_easy_setopt(ctx->curl, CURLOPT_URL, url);

        // for ssl, we disable verification
        curl_easy_setopt(ctx->curl, CURLOPT_SSL_VERIFYPEER, 0);

        // set mode
        ctx->mode = mode;

        if (userAgent) {
            // set agent
            curl_easy_setopt(ctx->curl, CURLOPT_USERAGENT, userAgent);
        }
        else {
            // set default
            curl_easy_setopt(ctx->curl, CURLOPT_USERAGENT, vxENCRYPT(HTTP_DEFAULT_USER_AGENT));
        }

        // set callbacks (mandatory for win32 compatibility, as per curl docs)
        if (mode == HTTP_GET) {
            curl_easy_setopt(ctx->curl, CURLOPT_WRITEFUNCTION, curlWriteCallback);
            curl_easy_setopt(ctx->curl, CURLOPT_WRITEDATA, ctx);
        }
        else {
            // post
            curl_easy_setopt(ctx, CURLOPT_POST,1);
            // handled by the read callback by setting NULL postfields
            curl_easy_setopt(ctx->curl, CURLOPT_POSTFIELDS, nullptr);

            curl_easy_setopt(ctx->curl, CURLOPT_READFUNCTION, curlReadCallback);
            curl_easy_setopt(ctx->curl, CURLOPT_READDATA, ctx);

            // allocate buffer for response
            ctx->respBuf = (uint8_t*)CMemory::alloc(CURL_MAX_WRITE_SIZE);
            if (!ctx->respBuf) {
                res = ENOMEM;
                break;
            }
            ctx->respBufSize = CURL_MAX_WRITE_SIZE;
            curl_easy_setopt(ctx->curl, CURLOPT_WRITEFUNCTION, curlWriteResponseCallback);
            curl_easy_setopt(ctx->curl, CURLOPT_WRITEDATA, ctx);
        }

        // done
        res=0;
    }while (0);

    if (res != 0) {
        // cleanup on error
        finishOperation(ctx);
    }
    return res;
}

bool BdUtils::CHttp::initialized() {
    return _initialized;
}

int BdUtils::CHttp::test() {
#ifndef NDEBUG
    int res = 0;
    uint8_t* response = nullptr;
    res = initHttpLib();
    if (res != 0) {
        return res;
    }

    do {
        // get in memory
        uint8_t hashMem[HASH_SHA1_SIZE] = {0};
        uint8_t* buf;
        uint64_t size;
        res = get("https://www.7-zip.org/a/7z1900-x64.exe", &buf, &size);
        if (res != 0) {
            break;
        }
        CSHA1::hashBuffer(buf,size,hashMem,HASH_SHA1_SIZE);
        size = 0;

        // get to file
        uint8_t hashFile[HASH_SHA1_SIZE] = {0};
        res = get("https://www.7-zip.org/a/7z1900-x64.exe", "/tmp/downloaded.exe", &size);
        if (res != 0) {
            break;
        }
        CSHA1::hashFile("/tmp/downloaded.exe",hashFile,HASH_SHA1_SIZE);
        if (memcmp(hashFile,hashMem,HASH_SHA1_SIZE) != 0) {
            res = ECANCELED;
            break;
        }

        // test post
        char simpleBuf[] = "12345678";
        std::vector<std::string> v = {"Content-Type: application/octet-stream"};
        size_t responseSize;
        res = post("https://enxruxaaim15.x.pipedream.net",(uint8_t*)buf,strlen(simpleBuf),&response,&responseSize,&v);
        if (res != 0) {
            break;
        }
        if (!strstr((char*)response,"success")) {
            res = ECANCELED;
        }
        /*
        // testing post with nc -kdl localhost 8000
        // post from memory
        res = post("http://localhost:8000", buf, size, &v);
        if (res != 0) {
            break;
        }

        // post from file
        res = post("http://localhost:8000", "/tmp/downloaded.exe", &v);
        if (res != 0) {
            break;
        }
        */
    } while (0);
    CMemory::free(response);
    return res;
#else
    return 0;
#endif
}
