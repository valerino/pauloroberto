//
// Created by jrf/ht on 20/02/19.
//

#ifndef RCSLINUX_CSHABASE_H
#define RCSLINUX_CSHABASE_H

#include <stdio.h>
#include <stdint.h>

/**
 * BdUtilsPrivate groups stuff private to the BdUtils framework
 */
namespace BdUtilsPrivate {
    /**
     * @brief to pass the SHA finish function to the base class
     */
    typedef int (*SHA_FINISH_FUNC)(void* ctx, unsigned char* output);
    /**
     * @brief to pass the SHA update function to the base class
     */
    typedef int (*SHA_UPDATE_FUNC)(void* ctx, const unsigned char* input, size_t len);

    /**
     * @class CSHABase
     * @brief implements base class for hashing, based on mbedtls
     * @see CSHA1
     * @see CSHA256
     */
    class CSHABase {
    protected:
        /**
         * initializes the base class with the function pointers for the SHA implementation
         * @param ctx the hash context
         * @param updateFunc the update function
         * @param finishFunc the finish function
         * @param shaSize size of SHA hash (20 bytes for sha1, 32 bytes for sha256)
         */
        CSHABase(void* ctx, SHA_UPDATE_FUNC updateFunc, SHA_FINISH_FUNC finishFunc, int shaSize);

        /**
         * hash the given file, oneshot
         * @param path path to the given file
         * @param hash on succesful return, the hash buffer
         * @param hashSize size of the hash buffer (must be at least 20 bytes for sha1, 32 bytes for sha256)
         * @param impl a CSHABase implementation (CSHA1, CSHA256)
         * @return 0 on success, or errno
         */
        static int hashFile(const char* path, uint8_t* hash, size_t hashSize, CSHABase* impl);

        /**
         * hash the given buffer, oneshot
         * @param buf the buffer to hash
         * @param size size of the buffer
         * @param hash on succesful return, the hash buffer
         * @param hashSize size of the hash buffer (must be at least 20 bytes for sha1, 32 bytes for sha256)
         * @param impl a CSHABase implementation (CSHA1, CSHA256)
         * @return 0 on success, or errno
         */
        static int hashBuffer(const uint8_t* buf, size_t size, uint8_t* hash, size_t hashSize, CSHABase* impl);

    public:
        /**
         * initializes an empty SHA1 digest, on which update() can be called subsequently
         */
        virtual ~CSHABase();

        /**
         * update the internal context with the given buffer
         * @param buf the buffer
         * @param size size of buffer
         */
        virtual int update(const uint8_t* buf, size_t size) final;

        /**
         * finish the hash and calculate the digest
         * @param buf on successful return, the digest buffer
         * @param size the buffer size (must be at least 20 bytes for sha1, 32 bytes for sha256)
         * @return 0 on success, or errno
         */
        virtual int digest(uint8_t *buf, size_t size) final;

        /**
         * finish the hash and calculate the digest
         * @param buf on successful return, the digest buffer to be freed with CMemory::free()
         * @param size optional, on successful return the hash size
         * @return 0 on success, or errno
         */
        virtual int digest(uint8_t **buf, size_t *size = nullptr) final;

        /**
         * finish the hash and calculate the digest
         * @param hexStr on successful return, the digest buffer as hexstring
         * @param size the buffer size, must be at least the hash size (20 for sha1, 32 for sha256) * 2 + 1
         * @return 0 on success, or errno
         */
        virtual int digestString(char *hexStr, size_t size) final;

        /**
         * finish the hash and calculate the digest
         * @param hexStr on successful return, the digest buffer as hexstring, to be freed with CMemory::free()
         * @return 0 on success, or errno
         */
        virtual int digestString(char** hexStr) final;
    private:
        int _hashSize = 0;
        SHA_UPDATE_FUNC _updateFunc = nullptr;
        SHA_FINISH_FUNC _finishFunc = nullptr;
        void* _ctx;
    };
}

#endif //RCSLINUX_CSHABASE_H
