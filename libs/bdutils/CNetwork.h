//
// Created by jrf/ht on 28/02/19.
//

#ifndef RCSLINUX_CNETWORK_H
#define RCSLINUX_CNETWORK_H

#include <stdint.h>
#include <unistd.h>

/**
 * BdUtils groups backdoor utilities framework
 */
namespace BdUtils {
    /**
     * @class CNetwork
     * @brief generic network utilities
     */
    class CNetwork {
    public:
        /**
         * convert string to ip
         * @param ipStr the ip string, may be ipv4 or ipv6
         * @param ip on successful return, the ip (cast to uint32_t for ipv4, or uint8_t* for v6)
         * @param size size of the ip buffer, must be sizeof(uint32_t) for ipv4, 16 for int64
         * @return 0 on success, or errno
         */
        static int ipFromString(const char* ipStr, void* ip, size_t size);

        /**
         * convert ip to string
         * @param ip points to a dword for ipv4, or to a byte array for ipv6
         * @param size size of the ip buffer (sizeof(uin32_t) for ipv4 or 16 for ipv6)
         * @param out on successful return, the ip string
         * @param outSize size of the output buffer, must be big enough to contain the output buffer, including the . and : and the \0 terminator
         * @return 0 on success, or errno
         */
        static int ipToString(void* ip, size_t size, char* out, size_t outSize);

        /**
         * convert ip to string
         * @param ip points to a dword for ipv4, or to a byte array for ipv6
         * @param size size of the ip buffer (sizeof(uin32_t) for ipv4 or 16 for ipv6)
         * @param out on successful return, the ip string to be freed with @ref CMemory::free()
         * @return 0 on succes, or errno
         */
        static int ipToString(void* ip, size_t size, char** out);

        /**
         * to be called before any other CNetwork function, to initialize the underlying library
         * @note needed on win32 only, this is the same as calling @ref CHttp::initHttpLib()
         * @return 0 on success, or an error defined in curl.h
         */
        static int initNetworkLib();

        /**
         * to be called on cleanup, to release library resources
         * @note needed on win32 only, this is the same as calling @ref CHttp::deinitHttpLib()
         */
        static void deinitNetworkLib();

        /**
         * check if the library has been initialized correctly through @ref initNetworkLib()
         * @note needed on win32 only, this is the same as calling @ref CHttp::initialized()
         * @return bool
         */
        static bool initialized();

        /**
         * test code
         * @return 0 on success
         */
        static int test();
    };
}

#endif //RCSLINUX_CNETWORK_H
