//
// Created by jrf/ht on 12/02/19.
//

#include "CTime.h"
#include <chrono>
#include <stdio.h>
#include <errno.h>
#include <time.h>
#include <inttypes.h>
#include "obfuscate.h"
#include "CRNG.h"

int BdUtils::CTime::test() {
#ifndef NDEBUG
    uint64_t millis = now();
    printf("now=%" PRIu64,  millis);

    int seconds = nowSeconds();
    printf("nowSeconds=%d", seconds);
    return 0;
#else
    return 0;
#endif
}

uint64_t BdUtils::CTime::now() {
    std::chrono::high_resolution_clock clock;
    uint64_t millis = std::chrono::duration_cast<std::chrono::milliseconds>(clock.now().time_since_epoch()).count();
    return millis;
}

int BdUtils::CTime::nowSeconds(){
    std::chrono::high_resolution_clock clock;
    int seconds = std::chrono::duration_cast<std::chrono::seconds>(clock.now().time_since_epoch()).count();
    return seconds;
}

int BdUtils::CTime::nowString(char *out, size_t sizeOut) {
    if (!out || !sizeOut) {
        return EINVAL;
    }

    // time in millis
    std::chrono::system_clock::time_point now = std::chrono::system_clock::now();

    // calculate milliseconds part
    uint64_t millis = std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch()).count();
    int ms = millis % 1000;

    // to seconds
    std::time_t nowTt = std::chrono::system_clock::to_time_t(now);
    std::tm ttm = *std::localtime(&nowTt);

    // to string
    snprintf(out ,sizeOut , vxENCRYPT("%04d%02d%02d_%02d:%02d:%02d:%03d"), ttm.tm_year +1900, ttm.tm_mon, ttm.tm_mday, ttm.tm_hour, ttm.tm_min, ttm.tm_sec, ms);
    return 0;
}
