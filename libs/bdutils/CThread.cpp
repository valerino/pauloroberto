//
// Created by jrf/ht on 18/02/19.
//

#include <errno.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include "CThread.h"
#include "CLog.h"
#include <time.h>
#include <unistd.h>
#include <sstream>

void BdUtils::CThread::sleep(int msec) {
    std::this_thread::sleep_for(std::chrono::milliseconds(msec));
}

std::thread::id BdUtils::CThread::id() {
    return std::this_thread::get_id();
}

std::string BdUtils::CThread::idString() {
    std::stringstream ss;
    ss << id();
    return std::string(ss.str());
}

void *BdUtils::CThread::ptr() {
    return reinterpret_cast<void *>(_t->native_handle());
}

BdUtils::CThread::~CThread() {
    DBGLOGV("DESTROYING thread %s", idString().c_str());
    if (_detached) {
        // forget about the std::thread
        delete _t;
    }
    else {
        // join() the non detached thread
        wait();
        delete _t;
    }
    DBGLOGV("DONE DESTROYING thread %s", idString().c_str());
}

void BdUtils::CThread::wait() {
    if (_detached) {
        // waits the detached thread
        while (!_terminated) {
            CThread::sleep(1000);
        }
    }
    else {
        // waits for termination
        _t->join();
    }
}

void BdUtils::CThread::setTerminated() {
    _terminated = true;
}

bool BdUtils::CThread::terminated() {
    return _terminated;
}
