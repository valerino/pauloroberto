//
// Created by jrf/ht on 13/02/19.
//

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include "CMemory.h"
#include "CString.h"
#include "CProcess.h"
#include "CRNG.h"
#include "CUser.h"
#include <sys/stat.h>
#include <limits.h>
#include <stddef.h>
#include <time.h>
#include "CFile.h"

int BdUtils::CFile::fromBuffer(const char *path, uint8_t *buf, size_t size) {
    if (!path || !buf) {
        return EINVAL;
    }
    if (size == 0) {
        // legit!
        return 0;
    }
    CFile f(path);
    int res = f.open("wb");
    if (res != 0) {
        return res;
    }
    res = f.write(buf,size);
    return res;
}

int BdUtils::CFile::toBuffer(const char *path, uint8_t **buf, size_t *size) {
    if (!path || !buf) {
        return EINVAL;
    }
    uint8_t* outBuf = nullptr;
    size_t outSize = 0;
    if (*buf != nullptr) {
        // buffer provided
        if (!size || *size == 0) {
            // size must be provided too
            return EINVAL;
        }
        // ok, both buf and size provided
        outBuf = *buf;
        outSize = *size;
    }
    else {
        // not provided
        *buf = nullptr;
        if (size) {
            *size = 0;
        }
    }

    int res = 0;
    do {
        CFile f(path);
        res = f.open("rb");
        if (res != 0) {
            break;
        }

        // get file size
        uint64_t fSize;
        res = f.size(&fSize);
        if (res != 0) {
            break;
        }
        if (fSize == 0) {
            // can't read
            res = EIO;
            break;
        }
        if (outSize != 0 && outSize < fSize) {
            // size provided, but not big enough
            res = EOVERFLOW;
            break;
        }

        if (!outBuf) {
            // allocate memory
            outBuf = (uint8_t*)CMemory::alloc(fSize);
            if (outBuf == nullptr) {
                res = ENOMEM;
                break;
            }
        }

        // read file
        res = f.read(outBuf,fSize);
        if (res != 0) {
            break;
        }

        // done
        if (size) {
            *size = fSize;
        }
        *buf = outBuf;
    } while (0);

    if (res != 0 && *buf == nullptr) {
        // error, free allocated memory
        CMemory::free(outBuf);
    }
    return res;
}

BdUtils::CFile::CFile(const char *path) : CFile() {
    _path = CString::dup(path);
}

int BdUtils::CFile::chmod(const char *path, int attrs) {
    if (!path) {
        return EINVAL;
    }
    int res = 0;
#ifdef _WIN32
    res = _chmod(path, attrs);
#else
    res = ::chmod(path, attrs);
#endif
    return res;
}

BdUtils::CFile::~CFile() {
    this->close();
    CMemory::free(_path);
}

int BdUtils::CFile::size(const char *path, uint64_t *size) {
    CFile f(path);
    int res = f.open("rb");
    if (res != 0) {
        return res;
    }
    res = f.size(size);
    return res;
}

int BdUtils::CFile::size(uint64_t *size) {
    if (!_f || !size) {
        return EINVAL;
    }
    *size = 0;
    int res = seek(0,SEEK_END);
    if (res != 0) {
        return res;
    }
    res = pos(size);
    rewind(_f);
    if (res != 0) {
        return res;
    }
    return 0;
}

int BdUtils::CFile::read(uint8_t *buf, size_t size, size_t* readBytes) {
    if (!_f || !buf) {
        return EINVAL;
    }
    size_t res = fread(buf, 1, size, _f);
    if (readBytes) {
        *readBytes = res;
    }
    if (res != size) {
        if (feof(_f)) {
            // consider EOF as success
            return 0;
        }
        return EIO;
    }
    return 0;
}

int BdUtils::CFile::write(uint8_t *buf, size_t size, size_t* written) {
    if (!_f || !buf) {
        return EINVAL;
    }
    size_t res = fwrite(buf, 1, size, _f);
    if (written) {
        *written = res;
    }
    if (res != size) {
        return EIO;
    }
    return 0;
}

int BdUtils::CFile::seek(uint64_t offset, uint64_t start) {
    if (!_f) {
        return EINVAL;
    }

    int res = fseek(_f, offset, start);
    if (res == -1) {
        return errno;
    }
    return 0;
}

int BdUtils::CFile::pos(uint64_t *offset) {
    if (!offset || !_f) {
        return EINVAL;
    }
    *offset = 0;
    long p = ftell(_f);
    if (p == -1) {
        return errno;
    }
    *offset = p;
    return 0;
}

int BdUtils::CFile::open(const char *path, const char *flags) {
    if (_path) {
        // can be used only with the default constructor
        return EINPROGRESS;
    }
    _path = CString::dup(path);
    int res = open(flags);
    if (res != 0) {
        CMemory::free(_path);
        _path = nullptr;
    }
    return 0;
}

int BdUtils::CFile::open(const char *flags) {
    if (!flags) {
        return EINVAL;
    }
    if (_f) {
        return EEXIST;
    }

    // resolve $HOME in path if needed
    char* m;
    int res = resolvePath(_path, &m);
    if (res != 0) {
        return res;
    }

    // replace path in instance, if any
    CMemory::free(_path);
    _path = m;

    // open file
    _f = fopen(m, flags);
    if (!_f) {
        res = errno;
    }

    return res;
}

int BdUtils::CFile::close() {
    if (!_f) {
        return EINVAL;
    }
    int res = fclose(_f);
    if (res != 0) {
        return errno;
    }
    _f = nullptr;
    return res;
}

int BdUtils::CFile::flush() {
    if (!_f) {
        return EINVAL;
    }

    int res = fflush(_f);
    if (res != 0) {
        return errno;
    }
    return res;
}

FILE *BdUtils::CFile::ptr() {
    return _f;
}

const char *BdUtils::CFile::path() {
    return _path;
}

BdUtils::CFile::CFile(FILE *f) {
    _f = f;
}

BdUtils::CFile::CFile() {
    _f = nullptr;
    _path = nullptr;
}

int BdUtils::CFile::mkpath(const char *path, bool errorIfExist) {
    if (!path) {
        return EINVAL;
    }
    char* savePtr = nullptr;
    char* p = CString::dup(path);
    if (!p) {
        return ENOMEM;
    }
    char* p2 = CString::dup(path);
    if (!p2) {
        CMemory::free(p);
        return ENOMEM;
    }
    p2[0] = '/';
    p2[1] = '\0';

    int res = 0;
    char* t = strtok_r(p,"/",&savePtr);
    if (!t) {
        // direct
        res = mkdir(path, 0777);
    }
    else {
        // walk path
        while(t) {
            strcat(p2, t);
            res = mkdir(p2,0777);
            if (res != 0) {
                if (errorIfExist) {
                    res = errno;
                    break;
                }
                else {
                    if (errno != EEXIST) {
                        res = errno;
                        break;
                    }
                }
            }
            t = strtok_r(nullptr, "/", &savePtr);
            if (t) {
                strcat(p2, "/");
            }
        }
    }

    // done
    if (res != 0) {
        res = errno;
        if (!errorIfExist) {
            if (res == EEXIST) {
                res = 0;
            }
        }
    }

    CMemory::free(p);
    CMemory::free(p2);
    return res;
}

/**
 * DIRCB implementation to count all files size and number
 * @param path the path to analyze
 * @param context pointer to @ref BdUtils::WalkDirSizeContext
 * @param depth
 * @param entry
 * @see CFile::walkDirSize()
 * @return 0
 */
static int sizeCountCallback(const char* path, void* context, int depth, struct dirent* entry) {
    BdUtils::WalkDirSizeContext* ctx = (BdUtils::WalkDirSizeContext*)context;
    if (entry->d_type == DT_REG) {
        ctx->count++;
        uint64_t s = 0;
        BdUtils::CFile::size(path,&s);
        ctx->size+=s;
    }
    return 0;
}

int BdUtils::CFile::walkDirSize(const char* path, uint64_t* size, uint32_t* n, bool recursive, int maxDepth, int currentDepth) {
    if (!path || !size) {
        return EINVAL;
    }
    *size = 0;
    if (n) {
        *n = 0;
    }

    // walk
    WalkDirSizeContext ctx={0};
    int res = walkDir(path, sizeCountCallback, &ctx, recursive, maxDepth, currentDepth);
    if (res != 0) {
        return res;
    }

    // return values
    *size = ctx.size;
    if (n) {
        *n = ctx.count;
    }
    return 0;
}

int BdUtils::CFile::walkDirRemove(const char *path, BdUtils::DIRWALKCB cb, void *context, bool recursive, int maxDepth, int currentDepth) {
    return walkDir(path, cb, context, recursive, maxDepth, currentDepth, true);
}


int BdUtils::CFile::walkDir(const char *path, BdUtils::DIRWALKCB cb, void* context, bool recursive, int maxDepth, int currentDepth, bool removeDir) {
    if (!path || !cb) {
        return EINVAL;
    }
    if (recursive && (maxDepth != -1 && currentDepth > maxDepth)) {
        // gracefully exit
        return 0;
    }

    int res = 0;
    DIR* dir = opendir(path);
    if (!dir) {
        if (recursive && currentDepth == 0 && errno == EACCES) {
            // fail on first call only
            res = errno;
        }
        else {
            res = errno;
        }
        return res;
    }

    while (res == 0) {
        // reset errno before readdir: if null is returned, on end of enumeration errno is unchanged, either it's error
        errno = 0;
        struct dirent* de = readdir(dir);
        if (de == nullptr) {
            if (errno != 0) {
                // error
                res = errno;
            }
            break;
        }

        if (strcmp(de->d_name,".") == 0 || strcmp(de->d_name,"..") == 0) {
            // skip . and ..
            continue;
        }

        // process this entry, allocate memory
        char* p;
        CString::joinPath(path,de->d_name,&p);
        if (de->d_type == DT_DIR) {
            // call the callback (to eventually handle the directory entry)
            cb(p, context, currentDepth, de);
            if (recursive) {
                // recurse
                int depth = currentDepth + 1;
                res = walkDir(p, cb, context, maxDepth, depth, removeDir);
            }
        }
        else {
            // it's a file, call the callback
            res = cb(p, context, currentDepth, de);
        }

        // done
        CMemory::free(p);
        if (res != 0) {
            // exit on error
            break;
        }
    }
    if (removeDir) {
        // remove directory (must be empty, no error is checked)
        CFile::remove(path);
    }
    return res;
}

bool BdUtils::CFile::exists(const char *path) {
#ifdef _WIN32
    int res = _access(path, R_OK);
#else
    int res = access(path, R_OK);
#endif
    if (res == 0) {
        return true;
    }
    return false;
}

int BdUtils::CFile::temporaryFilePath(char **path) {
    if (!path) {
        return EINVAL;
    }
    *path = nullptr;

    int res = 0;
    char *hexStr = nullptr;
    char* tmpPath = nullptr;
    do {
        // get random name between 8 and 16 characters
        CRNG rng;
        uint8_t rndName[16];
        uint32_t rndSize = rng.nextRand(8, 16);
        rng.randBytes(rndName, sizeof(rndName));
        res = CString::toHexString(rndName, rndSize, &hexStr);
        if (res != 0) {
            break;
        }

        // build tmp path string
        char *t;
        res = CUser::tmp(&t);
        if (res != 0) {
            break;
        }
        res = CString::joinPath(t,hexStr,&tmpPath);
        CMemory::free(t);
        if (res != 0) {
            break;
        }
    } while (0);

    CMemory::free(hexStr);
    *path = tmpPath;
    return res;
}

/**
 * wipe a file with a random pattern for 15 times
 * @param f
 * @param buf
 * @param size
 * @param offset
 * @param chunkNum
 * @param totalChunks
 * @param chunkSize
 * @param context a 256k random pattern
 * @see BdUtils::CFile::walkFile()
 * @return 0
 */
static int removeWipeFileCb(BdUtils::CFile* f,uint8_t* buf, size_t size, size_t offset, int chunkNum, int totalChunks, size_t chunkSize, void* context) {
    uint8_t* mem = (uint8_t*) context;
    BdUtils::CRNG rng;
    for (int i=0; i < 15; i++) {
        // be sure it's positioned at offset
        f->seek(offset);

        // and overwrite
        rng.randBytes(mem,FILE_CHUNK_SIZE);
        f->write(mem,size);
    }
    return 0;
}

int BdUtils::CFile::remove(bool wipe) {
    if (!_path) {
        return ENOENT;
    }

    if (wipe) {
        // wipe the file with a random pattern for 15 times
        uint8_t* mem = (uint8_t*)CMemory::alloc(FILE_CHUNK_SIZE);
        if (mem) {
            walkFile(_path, removeWipeFileCb, true, mem);
            CMemory::free(mem);
        }
    }

    // close the underlying file, if it's opened
    this->close();

    // unlink/remove
    int res = ::remove(_path);
    if (res == -1) {
        res = errno;
    }
    return res;
}

int BdUtils::CFile::remove(const char *path, bool wipe) {
    CFile f(path);
    int res = f.remove(wipe);
    return res;
}

int BdUtils::CFile::resolvePath(const char *path, char **out) {
    if (!path || !out) {
        return EINVAL;
    }
    *out = nullptr;
    int res = 0;


    // allocate memory
    char* m;
    if (strlen(path) > 2 && path[0] == '~' && path[1] == '/') {
        // expand tilde
        res = CUser::home(&m);
        if (res != 0) {
            CMemory::free(m);
            return res;
        }

        // add the rest of the path
        strcat(m, path + 1);
    }
    else {
        // just copy
        m = (char*)CMemory::alloc(PATH_MAX);
        if (!m) {
            res = ENOMEM;
            return res;
        }
        strcpy(m,path);
    }

    *out = m;
    return 0;
}

int BdUtils::CFile::walkFile(const char *path, BdUtils::FILEWALKCB cb, bool write, void *context, size_t chunkSize) {
    if (!path || !cb || !chunkSize) {
        return EINVAL;
    }

    // open file
    CFile f(path);
    int res = 0;
    if (write) {
        res = f.open("wb");
    }
    else {
        res = f.open("rb");
    }
    if (res != 0) {
        return res;
    }

    uint64_t fSize;
    res = f.size(&fSize);
    if (res != 0) {
        return res;
    }

    // process file in chunks
    uint8_t* mem = (uint8_t*)CMemory::alloc(chunkSize);
    if (!mem) {
        return ENOMEM;
    }

    uint64_t remaining = fSize;
    int num=0;
    int totalChunks = (fSize / chunkSize) + (fSize % chunkSize != 0 ? 1 : 0);

    size_t thisSize = chunkSize;
    size_t offset = 0;
    while (remaining != 0) {
        if (remaining < chunkSize) {
            thisSize = remaining;
        }

        if (!write) {
            // read file chunk
            f.read(mem,chunkSize);
        }

        // process in callback
        res = cb(&f, mem, thisSize, offset, num, chunkSize, totalChunks, context);
        if (res != 0) {
            // error in callback
            break;
        }
        remaining -= thisSize;
        offset += thisSize;
    }

    // done
    CMemory::free(mem);
    return res;

}

int BdUtils::CFile::timeCreate(const char *path, timespec *t) {
    if (!path || !t) {
        return EINVAL;
    }
    struct stat s = {};
    int res = statInternal(path,&s);
    if (res != 0) {
        return res;
    }
    t->tv_sec = s.st_ctim.tv_sec;
    t->tv_nsec = s.st_ctim.tv_nsec;
    return 0;
}

int BdUtils::CFile::timeModified(const char *path, timespec *t) {
    if (!path || !t) {
        return EINVAL;
    }
    struct stat s = {};
    int res = statInternal(path,&s);
    if (res != 0) {
        return res;
    }
    t->tv_sec = s.st_mtim.tv_sec;
    t->tv_nsec = s.st_mtim.tv_nsec;
    return 0;
}

int BdUtils::CFile::timeLastAccess(const char *path, timespec *t) {
    if (!path || !t) {
        return EINVAL;
    }
    struct stat s = {};
    int res = statInternal(path,&s);
    if (res != 0) {
        return res;
    }
    t->tv_sec = s.st_atim.tv_sec;
    t->tv_nsec = s.st_atim.tv_nsec;
    return 0;
}

int BdUtils::CFile::attributes(const char *path, uint32_t *attrs) {
    if (!path || !attrs) {
        return EINVAL;
    }
    struct stat s = {};
    int res = statInternal(path,&s);
    if (res != 0) {
        return res;
    }
    *attrs = s.st_mode;
    return 0;
}

/**
 * calls stat() to obtain file information
 * @param path path to file/directory
 * @param s on successful return, filled stat buffer
 * @return 0 on success, or errno
 */
int BdUtils::CFile::statInternal(const char *path, struct stat *s) {
    int res = stat(path, s);
    if (res != 0) {
        res = errno;
    }
    return res;
}

int BdUtils::CFile::getFreeSpace(const char *path, uint64_t *space) {
    if (!path || !space) {
        return EINVAL;
    }
    *space = 0;
    struct statvfs st;
    if (statvfs(path, &st) != 0) {
        return errno;
    }

    // the available size is f_bsize * f_bavail
    size_t s = st.f_bsize * st.f_bavail;
    *space = s;
    return 0;
}

/**
 * to be used in derived class, to set the file pointer
 * @param f the file pointer
 */
void BdUtils::CFile::setFilePtr(FILE *f) {
    _f = f;
}

/**
 * callback used by @ref BdUtils::CFile::copy()
 * @param f
 * @param buf
 * @param size
 * @param offset
 * @param chunkNum
 * @param totalChunkNum
 * @param chunkSize
 * @param context
 * @return 0
 */
static int copyFileCb (BdUtils::CFile* f, uint8_t* buf, size_t size, size_t offset, int chunkNum, int totalChunkNum, size_t chunkSize, void* context) {
    BdUtils::CFile* dst = (BdUtils::CFile*)context;
    int res = dst->write(buf,size);
    return res;
}

int BdUtils::CFile::copy(const char *src, const char *dst, bool overwrite) {
    if (!src || !dst) {
        return EINVAL;
    }

    if (!overwrite) {
        // check if the dst file exists
        if (CFile::exists(dst)) {
            return EEXIST;
        }
    }

    // create the destination file
    CFile* dstF = new CFile(dst);
    int res = dstF->open("wb");
    if (res != 0) {
        delete dstF;
        return res;
    }

    // walk
    res = CFile::walkFile(src,copyFileCb,false,dstF);
    if (res != 0) {
        // delete uncompleted write
        dstF->remove();
    }
    delete dstF;
    return res;
}

int BdUtils::CFile::rename(const char *oldPath, const char *newPath) {
    if (!oldPath || !newPath) {
        return EINVAL;
    }
    int res = ::rename(oldPath, newPath);
    if (res != 0) {
        res = errno;
    }
    return res;
}

int BdUtils::CFile::test() {
#ifndef NDEBUG
    uint64_t s;
    uint32_t n;
    int res = CFile::walkDirSize("/tmp/testdir",&s,&n,false);
    return res;
#else
    return 0;
#endif
}

int BdUtils::CFile::removeToTemp(const char *path, char **tmp, bool wipe) {
    if (!path || !tmp) {
        return EINVAL;
    }
    *tmp = nullptr;
    int res = 0;
#ifdef _WIN32
    // generate a temporary path
    char* moveToTemp;
    CFile::temporaryFilePath(&moveToTemp);
    *tmp = moveToTemp;

    // rename
    int res = CFile::rename(path, moveToTemp);

    // this will fail unless they change how windows work .... anyway ... let's try :)
    CFile::remove(moveToTemp, wipe);
#else
    res = CFile::remove(path);
#endif
    return res;
}
