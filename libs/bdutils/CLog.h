//
// Created by jrf/ht on 13/02/19.
//

#ifndef RCSLINUX_CLOG_H
#define RCSLINUX_CLOG_H

#include <stdio.h>
#include "CFile.h"

/**
 * BdUtils groups backdoor utilities framework
 */
namespace BdUtils {
     /**
      * @brief noisiest log level
      */
    #define LOG_LEVEL_VERBOSE 0

    /**
     * @brief masks out LOG_LEVEL_VERBOSE
     */
    #define LOG_LEVEL_ERROR 1

    /**
     * @brief masks out LOG_LEVEL_VERBOSE and LOG_LEVEL_ERROR
     */
    #define LOG_LEVEL_WARNING 2

    /**
     * @brief masks out everything except LOG_LEVEL_INFO
     */
    #define LOG_LEVEL_INFO 3

    /**
     * @brief internal, defined only in debug builds (on release, debug log is always disabled except LOG_FORCE_ENABLED specified)
     */
    #define DO_LOG
    #ifdef NDEBUG
    #undef DO_LOG
    #endif

    /**
     * @brief set to enable debug messages on release builds
     */
    #ifdef LOG_FORCE_ENABLED
    #define DO_LOG
    #endif

    #ifdef DO_LOG
    /**
     * @brief prints INFO debug log
     */
    #define DBGLOGI(...) CLog::info(__FILE__, __FUNCTION__, __LINE__, true, __VA_ARGS__)
    /**
     * @brief prints WARNING debug log
     */
    #define DBGLOGW(...) CLog::warning(__FILE__, __FUNCTION__, __LINE__, true, __VA_ARGS__)
    /**
     * @brief prints ERROR debug log
     */
    #define DBGLOGE(...) CLog::error(__FILE__, __FUNCTION__, __LINE__, true, __VA_ARGS__)
    /**
     * @brief prints VERBOSE debug log
     */
    #define DBGLOGV(...) CLog::verbose(__FILE__, __FUNCTION__, __LINE__, true, __VA_ARGS__)
    #else
    #define DBGLOGI(...)
    #define DBGLOGW(...)
    #define DBGLOGE(...)
    #define DBGLOGV(...)
    #endif

    /**
     * @class CLog
     * @brief debug logging utilities, by default disabled on release builds
     */
    class CLog {
    private:
        static CFile* _out;
        static void printInternal(int level, const char* file, const char* func, int line, bool includeTime, const char* format, va_list ap);
        static int _level;
    public:
        /**
         * sets the debug level (default is LOG_LEVEL_VERBOSE)
         * @param level LOG_LEVEL_VERBOSE, LOG_LEVEL_INFO, LOG_LEVEL_WARNING, LOG_LEVEL_ERROR.
         * higher level masks lower (i.e. INFO masks ERROR, WARNING, VERBOSE. VERBOSE shows all)
         */
        static void setLevel(int level);

        /**
         * redirect output to file
         * @param path path to redirect stdout to
         * @return 0 on success or errno
         */
        static int redirect(const char* path);

        /**
         * print log message followed by newline, defaults to stdout
         * (DO NOT USE DIRECTLY, USE DBGLOGI macro)
         * @param file pass __FILE__ here
         * @param func pass __FUNCTION__ here
         * @param line pass __LINE__ here
         * @param includeTime add a timestamp string ?
         * @param format the format string, may be null to just print src/line info
         * @param ...
         */
        static void info(const char* file, const char* func, int line, bool includeTime, const char* format, ...);

        /**
         * print log message followed by newline, defaults to stdout
         * (DO NOT USE DIRECTLY, USE DBGLOGW macro)
         * @param file pass __FILE__ here
         * @param func pass __FUNCTION__ here
         * @param line pass __LINE__ here
         * @param includeTime add a timestamp string ?
         * @param format the format string, may be null to just print src/line info
         * @param ...
         */
        static void warning(const char* file, const char* func, int line, bool includeTime, const char* format, ...);

        /**
         * print log message followed by newline, defaults to stdout
         * (DO NOT USE DIRECTLY, USE DBGLOGE macro)
         * @param file pass __FILE__ here
         * @param func pass __FUNCTION__ here
         * @param line pass __LINE__ here
         * @param includeTime add a timestamp string ?
         * @param format the format string, may be null to just print src/line info
         * @param ...
         */
        static void error(const char* file, const char* func, int line, bool includeTime, const char* format, ...);

        /**
         * print log message followed by newline, defaults to stdout
         * (DO NOT USE DIRECTLY, USE DBGLOGV macro)
         * @param file pass __FILE__ here
         * @param func pass __FUNCTION__ here
         * @param line pass __LINE__ here
         * @param includeTime add a timestamp string ?
         * @param format the format string, may be null to just print src/line info
         * @param ...
         */
        static void verbose(const char* file, const char* func, int line, bool includeTime, const char* format, ...);
    };
}

#endif //RCSLINUX_CLOG_H
