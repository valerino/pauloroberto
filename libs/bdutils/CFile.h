//
// Created by jrf/ht on 13/02/19.
//

#ifndef RCSLINUX_CFILE_H
#define RCSLINUX_CFILE_H

#include <stdint.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <sys/stat.h>
#ifndef _WIN32
#include <sys/statvfs.h>
#endif

#ifdef _WIN32
// windows-only glob() implementation
#include "win32-glob.h"
#else
#include <glob.h>
#endif

#ifdef _WIN32
// use unix compatible implementation
#include "../dirent/include/dirent.h"
#else
#include <dirent.h>
#endif

/**
 * BdUtils groups backdoor utilities framework
 */
namespace BdUtils {
    #ifndef PATH_MAX
    /**
     * @brief max size for a a path
     */
    #define PATH_MAX 4096
    #endif

    /**
     * @brief the default size for chunking operations
     */
    #define FILE_CHUNK_SIZE 256*1024

    /**
     * @brief callback for @ref CFile::walkDir()
     * @param path the entry path (file or folder path)
     * @param context optional context to be passed to the callback
     * @param depth this entry depth (global, from the top path)
     * @param entry the current dirent referring to the file/folder at path
     * @return must return 0 to continue walking
     */
    typedef int (*DIRWALKCB)(const char* path, void* context, int depth, struct dirent* entry);

    /**
     * @brief callback for @ref CFile::walkFile()
     * @param f the file
     * @param buf a file chunk
     * @param size_t size of buf
     * @param size_t offset offset in the file for this chunk
     * @param chunkNum this chunk number, 0 based
     * @param totalChunkNum total number of chunks for this file
     * @param chunkSize this file has been chunked with this size chunk (=each chunk has this size, or less for the last chunk)
     * @param optional context
     * @return must return 0 to continue walking
     */
     class CFile;
     typedef int (*FILEWALKCB)(BdUtils::CFile* f, uint8_t* buf, size_t size, size_t offset, int chunkNum, int totalChunkNum, size_t chunkSize, void* context);

     /**
      * @brief internally used by @ref BdUtils::CFile::walkDirSize()
      */
     typedef struct _WalkDirSizeContext {
        uint64_t size; /**< files total size */
        uint32_t count; /**< files total count */
     } WalkDirSizeContext;

    /**
     * @class CFile
     * @brief generic file utilities, based on POSIX api mostly
     * @todo support win32 through implementing the missing posix api
     *
     * ~~~
     *  currently implemented:
     *  glob() -> win32_glob.c
     *  dirent() -> ../libs/dirent
     *
     *  missing:
     *  statvfs()
     * ~~~
     * @todo add fprintf() etc.... (currently we use @ref CFile::ptr() to do uninmplemented file operations)
     */
    class CFile {
    public:
        /**
         * initializes a file object
         * @param path path to the file
         * @note the constructor only initializes the path, file must be opened with @ref open() to use all the other methods
         */
        CFile(const char* path);

        /**
         * wraps existing file
         * @param f the file handle to wrap
         * @note @ref path() and @ref remove() can't be used with this constructor, they need a path set
         */
        CFile(FILE* f);

        /**
         * base
         */
        CFile();

        /**
         * the destructor automatically closes the underlying file
         */
        virtual ~CFile();

        /**
         * make path, recursive
         * @param path directory path to be created
         * @param errorIfExist if true, errors if the path (or part of it) exists (default false)
         * @return 0 on success, or errno
         */
        static int mkpath(const char* path, bool errorIfExist=false);

        /**
         * create file from buffer
         * @param path path to the file to be written (will be overwritten if exists)
         * @param buf buffer to write
         * @param size size of buffer
         * @return 0 on success, or errno
         */
        static int fromBuffer(const char* path, uint8_t* buf, size_t size);

        /**
         * read whole file content to buffer
         * @param path path to the file to read
         * @param buf if it points to null, on successful return is an allocated buffer with the file content to be freed with @ref CMemory::free()\n
         *  either, it must point to an already allocated (big enough) buffer which on output will hold the file content
         * @param size optional if buf points to null, on successful return will hold the file size\n
         *  either, on input must contain the size of the the buffer provided in buf and on output will hold the file size\n
         * @return 0 on success, or errno
         */
        static int toBuffer(const char* path, uint8_t** buf, size_t* size = nullptr);

        /**
         * get file size
         * @param path path to the file
         * @param size on successful return, file size
         * @return 0 on success, or errno
         */
        static int size(const char *path, uint64_t *size);

        /**
         * get file size
         * @param size on successful return, file size
         * @return 0 on success, or errno
         */
        int size(uint64_t *size);

        /**
         * get file/directory creation time
         * @param path path to the file/directory
         * @param t on successful return, the time as unix epoch
         * @return 0 on success, or errno
         */
        static int timeCreate(const char* path, timespec* t);

        /**
         * get file/directory last modification time
         * @param path path to the file/directory
         * @param t on successful return, the time as unix epoch
         * @return 0 on success, or errno
         */
        static int timeModified(const char* path, timespec* t);

        /**
         * get file/directory last access time
         * @param path path to the file/directory
         * @param t on successful return, the time as unix epoch
         * @return 0 on success, or errno
         */
        static int timeLastAccess(const char* path, timespec* t);

        /**
         * get file/directory attributes
         * @param path path to the file/directory
         * @param attrs on successful return, the attributes (from stat.st_mode)
         * @return 0 on success, or errno
         */
        static int attributes (const char* path, uint32_t* attrs);

        /**
         * read buffer from file
         * @param buf buffer to read to
         * @param size size to be read
         * @param readBytes optional, bytes effectively read
         * @return 0 on success, or errno
         */
        int read(uint8_t* buf, size_t size, size_t* readBytes=nullptr);

        /**
         * write buffer to file
         * @param buf buffer to be written
         * @param size size to write
         * @param written optional, bytes effectively written
         * @return 0 on success, or errno
         */
        int write(uint8_t* buf, size_t size, size_t* written=nullptr);

        /**
         * go to file position
         * @param offset offset to position the file pointer to
         * @param start start offset, fseek-style, default is file start
         * @return 0 on success, or errno
         */
        int seek(uint64_t offset, uint64_t start=SEEK_SET);

        /**
         * get file position
         * @param offset on successful return, the current file position
         * @return 0 on success, or errno
         */
        int pos(uint64_t* offset);

        /**
         * open file (support $HOME expansion)
         * @param flags fopen() style flags (i.e. "wb", ...)
         * @see resolvePath
         * @return 0 on success, or errno
         */
        virtual int open(const char* flags);

        /**
         * set path and open file (support $HOME expansion)
         * @note fails if path is already set (so, this can be used only with the empty default constructor @ref CFile())
         * @param path the path
         * @param flags fopen() style flags (i.e. "wb", ...)
         * @see resolvePath
         * @return 0 on success, or errno
         */
        int open(const char* path, const char* flags);

        /**
         * close file
         * @note after close() returns success the internal file pointer becomes invalid.
         *  to do further operation reusing the same CFile instance you need to call @ref CFile::open() again
         * @return 0 on success, or errno
         */
        int close();

        /**
         * delete the file
         * @param wipe true to wipe file content
         * @note internally call @ref CFile::close() before deletion
         * @todo support path expansion as open() ?
         */
        int remove(bool wipe=false);

        /**
         * delete file or directory (if empty)
         * @param path path to the file
         * @param wipe true to wipe file content
         * @todo support path expansion as open() ?
         * @return 0 on success, or errno
         */
        static int remove(const char *path, bool wipe=false);

        /**
         * flush file buffers
         * @return 0 on success, or errno
         */
        int flush();

        /**
         * get the underlying FILE pointer
         * @return FILE*
         */
        FILE* ptr();

        /**
         * get file path
         * @return const char*
         */
        const char* path();

        /**
         * walks path and calls a callback on each file/directory found
         * @param path the path to walk
         * @param cb the callback defined as @ref DIRWALKCB, called for each file/dir found
         * @param context optional context to be passed to the callback
         * @param recursive optional, default is true
         * @param maxDepth max depth (from initial path) to recurse, default is 10, -1 to recurse indefinitely, ignored if recursive is false
         * @param currentDepth internal, must be 0, ignored if recursive is false
         * @param removeDir if true, the function tries to remove each directory, if empty, once processed (this implies the callback deletes each files inside)
         * @note if the cb returns != 0, enumeration is stopped.
         * @note by design, if recursive is true, this function doesn't fail on EACCES error unless at first call (currentDepth=0)
         * @return 0 on success, or errno
         */
        static int walkDir(const char* path, DIRWALKCB cb, void* context = nullptr, bool recursive=true, int maxDepth=10, int currentDepth=0, bool removeDir=false);

        /**
         * walks path and remove each dir, just a shortcut for @ref CFile::walkDir() with removeDir=true
         * @param path the path to walk
         * @param cb the callback defined as @ref DIRWALKCB, called for each file/dir found
         * @note the callback must delete each file!
         * @param context optional context to be passed to the callback
         * @param recursive optional, default is true
         * @param maxDepth max depth (from initial path) to recurse, default is 10, -1 to recurse indefinitely, ignored if recursive is false
         * @param currentDepth internal, must be 0, ignored if recursive is false
         * @note if the cb returns != 0, enumeration is stopped.
         * @note by design, if recursive is true, this function doesn't fail on EACCES error unless at first call (currentDepth=0)
         * @return 0 on success, or errno
         */
        static int walkDirRemove(const char* path, DIRWALKCB cb, void* context = nullptr, bool recursive=true, int maxDepth=10, int currentDepth=0);

        /**
         * walks path and count size and number of files, just a shortcut for @ref CFile::walkDir()
         * @param path the path to walk
         * @param size on return, sum of all filesizes in the examined path and, possibly, descendents
         * @param n optional, if not null, on return, is the file count
         * @param recursive optional, default is true
         * @param maxDepth optional, max depth (from initial path) to recurse, default is 10, -1 to recurse indefinitely, ignored if recursive is false
         * @param currentDepth internal, must be 0, ignored if recursive is false
         * @note if the cb returns != 0, enumeration is stopped.
         * @note by design, if recursive is true, this function doesn't fail on EACCES error unless at first call (currentDepth=0)
         * @return 0 on success, or errno
         */
        static int walkDirSize(const char* path, uint64_t* size, uint32_t* n=nullptr, bool recursive=true, int maxDepth=10, int currentDepth=0);

        /**
         * walk a file in chunks
         * @param path path to the file to be processed
         * @param cb the callback defined as @ref BdUtils::FILEWALKCB, called for each file chunk
         * @param write optional, if true the file is walked in write mode, just calling the callback at each chunk (no read is performed)
         * @param context optional context to be passed to the callback
         * @param chunkSize optional size of each chunk, default 256kb
         * @note if write is specified, assumes the file exists (this is meant mostly to overwrite existing file content by the callback)
         * @return 0 on success, or errno
         */
        static int walkFile(const char* path, FILEWALKCB cb, bool write=false, void* context=nullptr, size_t chunkSize=FILE_CHUNK_SIZE);

        /**
         * check if the file/dir at path exists and is readable
         * @param path the path to check
         * @return true if the file/dir exists and is readable
         */
        static bool exists(const char* path);

        /**
         * get a random temporary file path
         * @param path on successful return, the path to be freed with @ref CMemory::free()
         * @return 0 on success, or errno
         */
        static int temporaryFilePath(char** path);

        /**
         * resolve $HOME in path, turning to absolute path
         * @todo add resolving other env vars
         * @param path the input path to be resolved
         * @param out on successful return, the path to be freed with @ref CMemory::free()
         * @return 0 on success, or errno
         */
        static int resolvePath(const char* path, char** out);

        /**
         * get the (user) free space on a volume
         * @param path path to a directory on the volume
         * @param space on successful return, the free space in bytes
         * @return 0 on success, or errno
         */
        static int getFreeSpace(const char* path, uint64_t* space);

        /**
         * copy file
         * @param src source path
         * @param dst destination path
         * @param overwrite optional, if true the file is overwritten, either the function fails
         * @return 0 on success, or errno
         */
        static int copy(const char* src, const char* dst, bool overwrite=true);

        /**
         * rename file
         * @param oldPath the old path
         * @param newPath the new path
         * @note renaming on different volumes is not supported
         * @return 0 on success, or errno
         */
        static int rename(const char* oldPath, const char* newPath);

        /**
         * set file attributes
         * @param path path to the file
         * @param attrs attributes, unix style
         * @return 0 on success, or errno
         */
        static int chmod(const char* path, int attrs);

        /**
         * move file to the temporary folder with a random path, then attempt real delete
         * @param path path to the file to be deleted
         * @param tmp on successful return, the temporary path to be freed with @ref BdUtils::CMemory::free()
         * @param wipe optional, true to wipe
         * @return 0 on success, or errno
         * @note this is used to i.e. replace files on windows without rebooting, does a rename instead of delete
         * @note on non windows, it performs real delete and returns null in the tmp parameter
         */
        static int removeToTemp(const char* path, char** tmp, bool wipe=false);

        /**
         * test code
         * @return 0 on success
         */
        static int test();

    protected:
        void setFilePtr(FILE* f);

    private:
        FILE* _f;
        char* _path;
        static int statInternal(const char* path, struct stat* s);
    };
}

#endif //RCSLINUX_CFILE_H
