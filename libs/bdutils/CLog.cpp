//
// Created by jrf/ht on 12/02/19.
//

#include "CLog.h"
#include <stdio.h>
#include <errno.h>
#include <stdarg.h>
#include <string.h>
#include "CTime.h"
#include "CFile.h"
#include "CThread.h"

BdUtils::CFile* BdUtils::CLog::_out = new CFile(stdout);
int BdUtils::CLog::_level = LOG_LEVEL_VERBOSE;

/**
 * prints a debug message
 * @param level higher level masks lower (i.e. INFO masks ERROR, WARNING, VERBOSE. VERBOSE shows all)
 * @param file pass __FILE__ here
 * @param func pass __FUNCTION__ here
 * @param line pass __LINE__ here
 * @param includeTime add a timestamp string ?
 * @param format the format string, may be null to just print src/line info
 * @param ap internal
 */
void BdUtils::CLog::printInternal(int level, const char* file, const char* func, int line, bool includeTime, const char *format, va_list ap) {
    if (level < _level) {
        // skip
        return;
    }
    std::string s;

    char timeStr[64] = {};
    if (includeTime) {
        // current time
        CTime::nowString(timeStr, sizeof(timeStr));
    }

    char levelStr[4]={};
    switch (level) {
        case LOG_LEVEL_VERBOSE:
            strcpy(levelStr,"DBG");
            break;
        case LOG_LEVEL_ERROR:
            strcpy(levelStr,"ERR");
            break;
        case LOG_LEVEL_INFO:
            strcpy(levelStr,"INF");
            break;
        case LOG_LEVEL_WARNING:
            strcpy(levelStr,"WRN");
            break;
        default:
            break;
    }

    // print info
    char internalBuffer[1024];
    sprintf(internalBuffer, "[%s-%s-TID=%s,%s,%s,%d] ", includeTime ? timeStr : "/", levelStr, CThread::idString().c_str(), file, func,  line);
    s += internalBuffer;
    if (format != nullptr) {
        // print the variable part
        vsprintf(internalBuffer, format, ap);
        s+=internalBuffer;
    }
    // add linefeed and ensure output is flushed
    s+="\n";

    // finally print to stream
    _out->write((uint8_t*)s.c_str(), s.length());
    _out->flush();
}

void BdUtils::CLog::setLevel(int level) {
    _level = level;
}

int BdUtils::CLog::redirect(const char *path) {
    delete _out;
    _out = new CFile(path);
    int res = _out->open("w+b");
    return res;
}

void BdUtils::CLog::info(const char* file, const char* func, int line, bool includeTime, const char *format, ...) {
    va_list ap;
    va_start(ap, format);
    printInternal(LOG_LEVEL_INFO, file, func, line, includeTime, format, ap);
    va_end(ap);
}

void BdUtils::CLog::warning(const char* file, const char* func, int line, bool includeTime, const char *format, ...) {
    va_list ap;
    va_start(ap, format);
    printInternal(LOG_LEVEL_WARNING, file, func, line, includeTime, format, ap);
    va_end(ap);
}

void BdUtils::CLog::error(const char* file, const char* func, int line, bool includeTime, const char *format, ...) {
    va_list ap;
    va_start(ap, format);
    printInternal(LOG_LEVEL_ERROR, file, func, line, includeTime, format, ap);
    va_end(ap);
}

void BdUtils::CLog::verbose(const char* file, const char* func, int line, bool includeTime, const char *format, ...) {
    va_list ap;
    va_start(ap, format);
    printInternal(LOG_LEVEL_VERBOSE, file, func, line, includeTime, format, ap);
    va_end(ap);
}
