//
// Created by jrf/ht on 14/02/19.
//

#include "CProcess.h"
#include "CFile.h"
#include "CMemory.h"
#include <errno.h>
#include "obfuscate.h"
int BdUtils::CProcess::currentProcessPath(char *path, size_t size) {
    if (!path || !size) {
        return EINVAL;
    }

    // read from procfs
    int res = readlink(vxENCRYPT("/proc/self/exe"),path, size);
    return res;
}

BdUtils::ProcessId BdUtils::CProcess::pid() {
    return getpid();
}

int BdUtils::CProcess::run(const char *path, const char *params, bool background) {
    if (!path) {
        return EINVAL;
    }

    char* cmd = (char*)path;
    if (params) {
        // allocate full commandline
        cmd = (char*)CMemory::alloc(strlen(path) + 1 + strlen(params) + 1);
        if (!cmd) {
            return ENOMEM;
        }
        if (background) {
            sprintf(cmd, "%s %s &", path, params);
        }
        else {
            sprintf(cmd, "%s %s", path, params);
        }
    }

    // execute (ensure executable)
    CFile::chmod(cmd, 755);
    int res = system(cmd);
    CMemory::free(cmd);
    return res;
}
