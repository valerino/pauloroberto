//
// Created by jrf/ht on 19/02/19.
//

#include "CSHA1.h"
#include "CString.h"
#include "CMemory.h"
#include <CFile.h>
#include <errno.h>

using namespace BdUtilsPrivate;

BdUtils::CSHA1::CSHA1() : CSHABase(&_ctx, (SHA_UPDATE_FUNC)mbedtls_sha1_update_ret, (SHA_FINISH_FUNC)mbedtls_sha1_finish_ret, HASH_SHA1_SIZE) {
    mbedtls_sha1_init(&_ctx);
    mbedtls_sha1_starts_ret(&_ctx);
}

BdUtils::CSHA1::~CSHA1() {
    mbedtls_sha1_free(&_ctx);
}

int BdUtils::CSHA1::test() {
#ifndef NDEBUG
    uint8_t hash[20];
    hashFile("/tmp/testfile",hash,sizeof(hash) );
    char* hashS;
    CString::toHexString(hash,sizeof(hash),&hashS);
    CMemory::free(hashS);
    return 0;
#else
    return 0;
#endif
}

int BdUtils::CSHA1::hashFile(const char *path, uint8_t *hash, size_t hashSize) {
    CSHA1 c;
    return CSHABase::hashFile(path, hash, hashSize, &c);
}

int BdUtils::CSHA1::hashBuffer(const uint8_t *buf, size_t size, uint8_t *hash, size_t hashSize) {
    CSHA1 c;
    return CSHABase::hashBuffer(buf, size, hash, hashSize, &c);
}
