//
// Created by jrf/ht on 21/03/19.
//

#ifndef RCSLINUX_CAUDIO_H
#define RCSLINUX_CAUDIO_H

#include <speex/speex.h>
/**
 * BdUtils groups backdoor utilities framework
 */
namespace BdUtils {
    /**
     * @class CAudio
     * @brief audio encoding utilities, based on speex
     * @todo implement!
     */
    class CAudio {
    public:
        /**
         * debug test code placeholder
         * @todo: partially implemented, write proper unit test !
         * @return 0 on success
         */
        static int test();

    };
}


#endif //RCSLINUX_CAUDIO_H
