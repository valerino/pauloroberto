//
// Created by jrf/ht on 14/04/19.
//

#include "CBase64.h"
#include "CMemory.h"
#include <mbedtls/base64.h>
#include <errno.h>
#include <string.h>
int BdUtils::CBase64::fromBase64(const char *b64, uint8_t **out, size_t *outSize) {
    if (!b64 || !out || !outSize) {
        return EINVAL;
    }
    *out = nullptr;
    *outSize = 0;

    size_t required = 0;
    uint8_t* o = nullptr;
    int res = 0;
    do {
        // get required size
        int inLen = strlen(b64);
        res = mbedtls_base64_decode(nullptr, 0, &required, (const unsigned char*)b64, inLen);
        if (res != MBEDTLS_ERR_BASE64_BUFFER_TOO_SMALL) {
            // something wrong
            res = EBADMSG;
            break;
        }

        // allocate buffer
        // always allocate +1 to account for a 0 terminator in case the decompressed buffer is a string, to allow string functions to work flawlessy
        o = (uint8_t*)CMemory::alloc(required + 1);
        if (!o) {
            res = ENOMEM;
            break;
        }

        // decode
        res = mbedtls_base64_decode(o, required, &required, (const unsigned char*)b64, inLen);
        if (res != 0) {
            // something wrong
            res = EBADMSG;
            break;
        }

        // done
        *out = o;
        *outSize = required;
    } while(0);
    return res;
}

int BdUtils::CBase64::toBase64(void *in, size_t size, char **out) {
    if (!in || !out || !size) {
        return EINVAL;
    }
    *out = nullptr;

    size_t required = 0;
    uint8_t* o = nullptr;
    int res = 0;
    do {
        // get required size
        res = mbedtls_base64_encode(nullptr, 0, &required, (unsigned char*)in, size);
        if (res != MBEDTLS_ERR_BASE64_BUFFER_TOO_SMALL) {
            // something wrong
            res = EBADMSG;
            break;
        }

        // allocate buffer
        // always allocate +1 to account for a 0 terminator in case the decompressed buffer is a string, to allow string functions to work flawlessy
        o = (uint8_t*)CMemory::alloc(required + 1);
        if (!o) {
            res = ENOMEM;
            break;
        }

        // encode
        res = mbedtls_base64_encode(o, required, &required, (unsigned char*)in, size);
        if (res != 0) {
            // something wrong
            res = EBADMSG;
            break;
        }

        // done
        *out = (char*)o;
    } while(0);
    return res;
}

int BdUtils::CBase64::test() {
#ifndef NDEBUG
    char testBuf[]="blablablablablablublibloblablablablabla!!";
    char* b64=nullptr;
    uint8_t* dec=nullptr;
    size_t decSize = 0;
    int res = EBADMSG;

    // encode
    toBase64(testBuf,strlen(testBuf) + 1,&b64);

    // decode
    fromBase64(b64,&dec,&decSize);
    if (memcmp(dec,testBuf,strlen(testBuf)+1) == -0) {
        // ok!
        res = 0;
    }
    return res;
#else
    return 0;
#endif
}
