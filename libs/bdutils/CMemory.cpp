//
// Created by jrf/ht on 13/02/19.
//

#include "CMemory.h"
#include <stdint.h>
#include <errno.h>
void *BdUtils::CMemory::alloc(size_t size) {
    if (size) {
        return calloc(1, size);
    }
    return nullptr;
}

void BdUtils::CMemory::free(void *ptr) {
    if (ptr) {
        std::free(ptr);
    }
}

void *BdUtils::CMemory::realloc(void *ptr, size_t newSize) {
    if (ptr == nullptr) {
        // alloc
        return alloc(newSize);
    }
    if (newSize) {
        return std::realloc(ptr, newSize);
    }
    return nullptr;
}

void *BdUtils::CMemory::allocPadded(size_t size, size_t *outSize, bool pkcs7) {
    if (!size || !outSize) {
        return nullptr;
    }
    // allocate output memory
    *outSize = 0;
    uint8_t pad;
    size_t allocSize = sizeToPaddedSize(size, &pad, pkcs7);
    uint8_t* o = (uint8_t*)CMemory::alloc(allocSize);
    if (!o) {
        return nullptr;
    }

    if (pkcs7 && pad) {
        // add pkcs#7 padding
        for (int i=0; i < pad; i++) {
            o[size + i] = pad;
        }
    }

    *outSize = allocSize;
    return o;
}

size_t BdUtils::CMemory::sizeToPaddedSize(size_t size, uint8_t *padByte, bool pkcs7) {
    size_t allocSize = size;
    uint8_t pad=0;
    while (allocSize % 16) {
        allocSize++;
        // increment how many bytes are added as pad
        pad++;
    }
    if (pad == 0) {
        if (pkcs7) {
            // pkcs#7: add a full block
            pad = 16;
            allocSize += 16;
        }
    }
    if (padByte) {
        *padByte = pad;
    }
    return allocSize;
}

void *BdUtils::CMemory::dup(void *ptr, size_t size) {
    void* p = nullptr;
    if (ptr && size) {
        p = alloc(size);
        if (p) {
            memcpy(p,ptr,size);
        }
    }
    return p;
}
