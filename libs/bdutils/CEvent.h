//
// Created by jrf/ht on 18/02/19.
//

#ifndef RCSLINUX_CEVENT_H
#define RCSLINUX_CEVENT_H

/**
 * BdUtils groups backdoor utilities framework
 */
#include "CMutex.h"
#include <condition_variable>

namespace BdUtils {
    /**
     * @class CEvent
     * @brief implements a synchronization event, similar to win32 events
     */
    class CEvent {
    public:
        /**
         * creates a waitable event object
         * @param autoReset optional, true to auto-reset after being signaled
         */
        CEvent(bool autoReset=false);
        ~CEvent();

        /**
         * sets the event to signaled state and signal all the waiting threads
         */
        void set();

        /**
         * resets the event to non-signaled state
         */
        void reset();

        /**
         * wait for the event to be signaled
         * @param msec optional, blocks for as many milliseconds (either, blocks until unlocked)
         * @note a 0msec wait is the same as calling @ref signaled() to test if the event is signaled without blocking
         * @return 0 if the event is signaled, or errno
         */
        int wait(int msec=-1);

        /**
         * check if the event is in the signaled state
         * @return true if the event is signaled
         */
        bool signaled();

        /**
         * debug test code placeholder
         * @todo: partially implemented, write proper unit test !
         * @return 0 on success
         */
        static int test();

        /**
         * debug test thread
         * @param params CEvent*
         */
        static void testThread1(void* params);

        /**
         * debug test thread
         * @param params CEvent*
         */
        static void testThread2(void* params);

        /**
         * debug test thread
         * @param params CEvent*
         */
        static void testThread3(void* params);

        /**
         * debug test thread
         * @param params CEvent*
         */
        static void testThread4(void* params);

    private:
        bool predicate();
        CMutex _mtx;
        std::condition_variable_any _cond;
        bool _signaled = false;
        bool _autoreset =false;
        int _numWaiting=0;
    };

}
#endif //RCSLINUX_CEVENT_H
