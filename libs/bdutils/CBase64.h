//
// Created by jrf/ht on 14/04/19.
//

#ifndef RCSLINUX_CBASE64_H
#define RCSLINUX_CBASE64_H

#include <stdint.h>
#include <stdio.h>

/**
 * BdUtils groups backdoor utilities framework
 */
namespace BdUtils {
    /**
     * @class CBase64
     * @brief base64 encoding/decoding utilities, based on mbedtls
     */
    class CBase64 {
    public:
        /**
         * decodes base64 buffer
         * @param b64 the base64 string
         * @param out on successful return, the decoded buffer which must be freed with BdUtils::CMemory::free()
         * @param outSize on successful return, size of the decoded buffer
         * @return 0 on success, or errno
         */
        static int fromBase64(const char *b64, uint8_t **out, size_t *outSize);

        /**
         * encodes buffer to base64
         * @param in the input buffer
         * @param size input buffer size
         * @param out on successful return, the base64 encoded string, 0 terminated
         * @return 0 on success, or errno
         */
        static int toBase64(void *in, size_t size, char **out);

        /**
         * test code
         * @return 0 on success, or errno
         */
        static int test();
    };
}
#endif //RCSLINUX_CBASE64_H
