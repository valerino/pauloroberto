//
// Created by jrf/ht on 19/02/19.
//

#ifndef RCSLINUX_CSHA1_H
#define RCSLINUX_CSHA1_H

#include "CSHABase.h"
#include <mbedtls/sha1.h>

/**
 * BdUtils groups backdoor utilities framework
 */
namespace BdUtils {
    #define HASH_SHA1_SIZE 20

    /**
     * @class CSHA1
     * @brief implements sha1 hash, based on mbedtls
     */
    class CSHA1 : public BdUtilsPrivate::CSHABase {
    public:
        /**
         * initializes an empty SHA1 digest, on which update() can be called subsequently
         */
        CSHA1();
        ~CSHA1();

        /**
         * hash the given file, oneshot
         * @param path path to the given file
         * @param hash on succesful return, the hash buffer
         * @param hashSize size of the hash buffer (must be at least SHA1_HASH_SIZE)
         * @return 0 on success, or errno
         */
        static int hashFile(const char* path, uint8_t* hash, size_t hashSize);

        /**
         * hash the given buffer, oneshot
         * @param buf the buffer to hash
         * @param size size of the buffer
         * @param hash on succesful return, the hash buffer
         * @param hashSize size of the hash buffer (must be at least SHA1_HASH_SIZE)
         * @return 0 on success, or errno
         */
        static int hashBuffer(const uint8_t* buf, size_t size, uint8_t* hash, size_t hashSize);

        /**
         * debug test code placeholder
         * @todo: partially implemented, write proper unit test !
         * @return 0 on success
         */
        static int test();
    private:
        mbedtls_sha1_context _ctx = {0};
    };
}

#endif //RCSLINUX_CSHA1_H
