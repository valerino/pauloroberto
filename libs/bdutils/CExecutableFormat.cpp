//
// Created by jrf/ht on 20/02/19.
//

#include "CExecutableFormat.h"
#include "CMemory.h"

bool BdUtils::CExecutableFormat::isArch32(const char *path) {
    // load
    uint8_t* buf = nullptr;
    size_t size;
    int res = CFile::toBuffer(path,&buf,&size);
    if (res != 0) {
        // error occurred reading the file
        return false;
    }

    // check arch
    bool b = isArch32(buf,size);
    CMemory::free(buf);
    return b;
}

bool BdUtils::CExecutableFormat::isArch64(const char *path) {
    // load
    uint8_t* buf = nullptr;
    size_t size;
    int res = CFile::toBuffer(path,&buf,&size);
    if (res != 0) {
        // error occurred reading the file
        return false;
    }

    // check arch
    bool b = isArch64(buf,size);
    CMemory::free(buf);
    return b;
}

/**
 * check if the given buffer starts with the ELF header
 * @param buf the buffer
 * @param size size of buffer
 * @param arch ARCH_32 or ARCH_64
 * @return true if buf matches the given arch
 */
bool BdUtils::CExecutableFormat::isELF(const uint8_t *buf, size_t size, int arch) {
    if (!buf || size < 5) {
        return false;
    }

    // \x7fELF\x1 = elf32, \x7fELF\x2 = elf64
    if (buf[0] == 0x7f && buf[1] == 0x45 && buf[2] == 0x4c && buf[3] == 0x46 && buf[4] == (arch == ARCH_32 ? 0x01 : 0x02)) {
        return true;
    }
    return false;
}

bool BdUtils::CExecutableFormat::isArch32(const uint8_t *buf, size_t size) {
    // TODO: handle PE, macho
    return isELF(buf, size, ARCH_32);
}

bool BdUtils::CExecutableFormat::isArch64(const uint8_t *buf, size_t size) {
    // TODO: handle PE, macho
    return isELF(buf, size, ARCH_64);
}

bool BdUtils::CExecutableFormat::isMatchingArchitecture(const char *path) {
    // load
    uint8_t* buf = nullptr;
    size_t size;
    int res = CFile::toBuffer(path,&buf,&size);
    if (res != 0) {
        // error occurred reading the file
        return false;
    }

    // check arch
    bool b = isMatchingArchitecture(buf, size);
    CMemory::free(buf);
    return b;
}

bool BdUtils::CExecutableFormat::isMatchingArchitecture(const uint8_t *buf, size_t size) {
    int sizeOfPtr = sizeof(void*);
    if (sizeOfPtr == 4) {
        // 32bit
        return isArch32(buf, size);
    }

    // 64bit
    return isArch64(buf, size);
}

