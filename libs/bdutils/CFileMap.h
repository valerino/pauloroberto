//
// Created by jrf/ht on 21/03/19.
//

#ifndef RCSLINUX_CFILEMAP_H
#define RCSLINUX_CFILEMAP_H
#ifdef __cplusplus
extern "C" {
#include <fmem.h>
}
#endif

#include "CFile.h"

namespace BdUtils {
    /**
     * @class CFileMap
     * @brief implements portable memory mapped files
     */
    class CFileMap : public CFile {
    public:
        CFileMap();

        /**
         * open a memory mapped file
         * @param flags fopen() style flags (i.e. "wb", ...)
         * @return 0 on success, or errno
         */
        int open(const char* flags) override;

        /**
         * get underlying memory buffer
         * @param m on successful return, the buffer
         * @param s on successful return, buffer size
         */
        void mem(uint8_t** m, size_t* s);

        /**
         * release resources
         */
        ~CFileMap() final;

        /**
         * test code
         * @return 0 on success
         */
        static int test();

    private:
        fmem _fm = {};
    };
}
#endif //RCSLINUX_CFILEMAP_H
