//
// Created by jrf/ht on 19/02/19.
//

#include "CRNG.h"
#include <errno.h>
#include <random>
#include <mbedtls/ctr_drbg.h>
#include <mbedtls/entropy.h>

uint32_t BdUtils::CRNG::nextRand(int min, int max, bool lock) {
    if (lock) {
        _lock.lock();
    }

    // generate random
    std::uniform_int_distribution<uint32_t >gen(min, max);
    uint32_t rnd = gen(*_rng);

    if (lock) {
        _lock.unlock();
    }
    return rnd;
}

int BdUtils::CRNG::randBytes(uint8_t *bytes, size_t size, bool lock) {
    if (!bytes || !size) {
        return EINVAL;
    }

    if (lock) {
        _lock.lock();
    }

    // generate random
    int res = mbedtls_ctr_drbg_random(&_ctx, bytes, size);

    if (lock) {
        _lock.unlock();
    }
    return res;
}

BdUtils::CRNG::CRNG() {
    // initialize contextes and seed the rng
    mbedtls_entropy_init(&_entropy);
    mbedtls_ctr_drbg_init(&_ctx);
    mbedtls_ctr_drbg_seed(&_ctx, mbedtls_entropy_func, &_entropy, nullptr, 0);

    // this is used to easily have random ints
    _rng = new std::mt19937(_rd());
}

BdUtils::CRNG::~CRNG() {
    // free contextes
    mbedtls_ctr_drbg_free(&_ctx);
    mbedtls_entropy_free(&_entropy);
    delete _rng;
}

mbedtls_ctr_drbg_context *BdUtils::CRNG::context() {
    return &_ctx;
}
