//
// Created by jrf/ht on 18/02/19.
//

#include "CDynLoad.h"
#include "CUser.h"
#include "CFile.h"
#include "CMemory.h"
#include "CLog.h"
#include "CThread.h"
#include "CFileMap.h"
#include <errno.h>

bool BdUtils::CDynLoad::_dbgEnabled = false;

void BdUtils::CDynLoad::enableDebugging(bool enable) {
#ifndef _NDEBUG
    _dbgEnabled = enable;
#endif
}

int BdUtils::CDynLoad::load(const char *path, void **h) {
    if (!path || !h) {
        return EINVAL;
    }
    void *d = dlopen(path, RTLD_NOW);
    if (!d) {
        // failed
        char *err = dlerror();
        DBGLOGE(err);
        return ENOENT;
    }
    *h = d;
    return 0;
}

int BdUtils::CDynLoad::load(uint8_t *buf, size_t size, void **h) {
    if (!buf || !size || !h) {
        return EINVAL;
    }
    *h = nullptr;

    // get a temporary filename
    char *t;
    int res = CFile::temporaryFilePath(&t);
    if (res != 0) {
        return res;
    }

    LibraryHandle *hh = nullptr;
    do {
        // create file
        res = CFile::fromBuffer(t, buf, size);
        if (res != 0) {
            break;
        }

        // load
        res = load(t, &hh);
        if (res != 0) {
            break;
        }

        // delete temporary file
        if (!_dbgEnabled) {
           CFile::remove(t);
        }

        // and return the handle
        *h = hh;
    } while (0);
    CMemory::free(t);

    return res;
}

int BdUtils::CDynLoad::unload(void *h) {
    if (!h) {
        return EINVAL;
    }
    int res = dlclose(h);
    return res;
}

int BdUtils::CDynLoad::getSymbol(void *h, const char *sym, void **func) {
    if (!h || !sym || !func) {
        return EINVAL;
    }
    *func = nullptr;

    // get function pointer
    void *f = dlsym(h, sym);
    if (!f) {
        return ENOENT;
    }
    *func = f;
    return 0;
}

int BdUtils::CDynLoad::test() {
#ifndef NDEBUG
    void *hp;
    void *func;
    uint8_t *libBuf = nullptr;
    size_t libBufSize = 0;
    int res = CFile::toBuffer("./plg/libmodtest.so", &libBuf, &libBufSize);
    if (res != 0) {
        return res;
    }
    res = CDynLoad::load(libBuf, libBufSize, &hp);
    if (res != 0) {
        return res;
    }

    // get the plugin instance
    res = CDynLoad::getSymbol(hp, "instance", &func);

    CDynLoad::unload(hp);
    return res;
#else
    return 0;
#endif
}
