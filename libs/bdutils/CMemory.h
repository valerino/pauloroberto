//
// Created by jrf/ht on 13/02/19.
//

#ifndef RCSLINUX_CMEMORY_H
#define RCSLINUX_CMEMORY_H

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <stdint.h>

/**
 * BdUtils groups backdoor utilities framework
 */
namespace BdUtils {
    /**
     * @brief to avoid checking for null when calling std::free()
     */
    #define SAFE_FREE(_x_) if(_x_) { std::free(_x_); }

    /**
     * @class CMemory
     * @brief memory management utilities
     */
    class CMemory {
    public:
        /**
         * allocate memory
         * @param size size to allocate
         * @return memory to be freed with @ref CMemory::free(), or NULL on out of memory/wrong parameters
         */
        static void* alloc(size_t size);

        /**
         * free memory allocated with @ref CMemory::alloc()
         * @note it's ok to pass null, to avoid checking
         * @param ptr the memory
         */
        static void free(void* ptr);

        /**
         * duplicates a buffer
         * @param ptr the buffer to duplicate
         * @param size size of the buffer to duplicate
         * @return memory to be freed with @ref CMemory::free(), or NULL on out of memory/wrong parameters
         */
        static void* dup(void* ptr, size_t size);

        /**
         * reallocate memory allocated with alloc()
         * @param ptr the old memory block
         * @param newSize size of the new memory block
         * @note if ptr is null, this behaves the same as @ref CMemory::alloc()
         * @return the new memory pointer (old is no more valid) to be freed with @ref CMemory::free, or NULL (old is still valid)
         */
        static void* realloc(void *ptr, size_t newSize);

        /**
         * allocate padded memory
         * @param size size to allocate
         * @param outSize on successful return, the allocated size
         * @param pkcs7 if true, pkcs#7 padding is used (either, pads with 0)
         * @return memory to be freed with @ref CMemory::free(), or NULL on out of memory
         */
        static void* allocPadded(size_t size, size_t *outSize, bool pkcs7 = false);

        /**
         * calculate padded size for the given size
         * @param size the size
         * @param padByte optional, if not null on succesful return is the byte used as pad
         * @param pkcs7 if true, pkcs#7 padding is used
         * @return the padded size
         */
        static size_t sizeToPaddedSize(size_t size, uint8_t *padByte = nullptr, bool pkcs7 = false);
    };
}

#endif //RCSLINUX_CMEMORY_H
