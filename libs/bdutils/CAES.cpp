//
// Created by jrf/ht on 13/02/19.
//

#include "CAES.h"
#include <CMemory.h>
#include <errno.h>
int BdUtils::CAES::encryptCbc(uint8_t *key, int keyBits, int paddingMode, uint8_t *iv, void *in, size_t size,
                              uint8_t **out, size_t *outSize) {
    try {
        CAES c = CAES(AES_MODE_ENCRYPT,key, keyBits, paddingMode, iv);
        int res = c.update(in ,size,out,outSize);
        return res;
    }
    catch (...) {
        return EINVAL;
    }
}

int BdUtils::CAES::decryptCbc(uint8_t *key, int keyBits, int paddingMode, uint8_t *iv, void *in, size_t size,
                              uint8_t **out, size_t *outSize) {
    try {
        CAES c = CAES(AES_MODE_DECRYPT,key, keyBits, paddingMode, iv);
        int res = c.update(in,size,out,outSize);
        return res;
    }
    catch (...) {
        return EINVAL;
    }
}

BdUtils::CAES::CAES(int mode, uint8_t *key, int keyBits, int paddingMode, uint8_t *iv) {
    if ( (mode != AES_MODE_ENCRYPT && mode != AES_MODE_DECRYPT) || !key ) {
        throw;
    }
    if (keyBits != 128 && keyBits != 192 && keyBits != 256) {
        throw;
    }

    // initialize
    mbedtls_aes_init(&_ctx);
    if (iv != nullptr) {
        memcpy(_iv,iv,sizeof(_iv));
    }
    _mode = mode;
    _paddingMode = paddingMode;
    if (_mode == AES_MODE_ENCRYPT) {
        mbedtls_aes_setkey_enc(&_ctx, key, keyBits);
    }
    else {
        mbedtls_aes_setkey_dec(&_ctx, key, keyBits);
    }
}

BdUtils::CAES::~CAES() {
    mbedtls_aes_free(&_ctx);
}

int BdUtils::CAES::update(void *in, size_t size, uint8_t **out, size_t *outSize) {
    if (!in || !size || !out || !outSize) {
        return EINVAL;
    }
    if (_ctr) {
        // need to call the CBC constructor!
        return ECANCELED;
    }

    uint8_t* o;
    size_t allocSize;
    if (*out != nullptr) {
        // use provided
        o = *out;
        allocSize = *outSize;
    }
    else {
        // allocate output buffer
        // always allocate +1 to account for a 0 terminator in case the decompressed buffer is a string, to allow string functions to work flawlessy
        *out = nullptr;
        *outSize = 0;
        if (_paddingMode == AES_NO_PADDING) {
            allocSize = size;
            o = (uint8_t*)CMemory::alloc(size + 1);
        }
        else {
            // use either 0 or pkcs7 padding
            o = (uint8_t*)CMemory::allocPadded(size + 1, &allocSize, _paddingMode == AES_PKCS7_PADDING);
            if (!o) {
                return ENOMEM;
            }
        }
    }

    // copy in to out
    if (o != in) {
        memcpy(o,in,size);
    }
    size_t inSize = size;
    if (_paddingMode != AES_NO_PADDING) {
        inSize = allocSize;
    }

    // process
    int res = mbedtls_aes_crypt_cbc(&_ctx, _mode, inSize, _iv, o, o);
    if (res != 0) {
        if (*out == nullptr) {
            // buffer was allocated
            CMemory::free(o);
        }
        return res;
    }
    if (_paddingMode == AES_PKCS7_PADDING && _mode == AES_MODE_DECRYPT) {
        // return correct outsize
        *outSize = size - o[size-1];
    }
    else {
        *outSize = allocSize;
    }

    // done
    *out = o;
    return 0;
}

int BdUtils::CAES::test() {
#ifndef NDEBUG
    uint8_t evKey[] = "WfClq6HxbSaOuJGaWfClq6HxbSaOuJGa";
    char in[] = "blablablablablablublibloblablablablabla!!";
    char inUnencrypted[] = "blablablablablablublibloblablablablabla!!";
    uint8_t iv[] = "a4Clq6HxbSaOuJGa";

    // encrypt running ctr, simulating chunked encryption
    CAES ctx = CAES(evKey, 256, iv);
    int len = strlen(in) + 1;
    int offset = 0;
    char* p = in;
    int chunkSize=10;
    int remaining = len;
    while (1) {
        bool last=false;
        if (chunkSize > remaining) {
            chunkSize = remaining;
            last=true;
        }

        ctx.updateCtr(p,chunkSize,(uint8_t**)&p);
        offset+=chunkSize;
        remaining-=chunkSize;
        p+=chunkSize;
        if(last) {
            break;
        }
    }

    // decrypt as a whole
    uint8_t* outDec = nullptr;
    CAES::decryptCtr(evKey, 256, iv, in, len, &outDec);
    int res = -1;
    if (memcmp(outDec, inUnencrypted, len) == 0) {
        res = 0;
    }
    CMemory::free(outDec);

    return res;
#else
    return 0;
#endif
}

BdUtils::CAES::CAES(uint8_t *key, int keyBits, uint8_t *nonce) {
    if ( !key ) {
        throw;
    }
    if (keyBits != 128 && keyBits != 192 && keyBits != 256) {
        throw;
    }

    // initialize ctr
    _ctr = true;
    mbedtls_aes_init(&_ctx);
    if (nonce != nullptr) {
        memcpy(_iv,nonce,sizeof(_iv));
    }

    // for ctr, we always use setkey_enc() !
    mbedtls_aes_setkey_enc(&_ctx, key, keyBits);
}

int BdUtils::CAES::encryptCtr(uint8_t *key, int keyBits, uint8_t *nonce, void *in, size_t size, uint8_t **out) {
    try {
        CAES c = CAES(key, keyBits, nonce);
        int res = c.updateCtr(in ,size,out);
        return res;
    }
    catch (...) {
        return EINVAL;
    }
}

int BdUtils::CAES::decryptCtr(uint8_t *key, int keyBits, uint8_t *nonce, void *in, size_t size, uint8_t **out) {
    return encryptCtr(key, keyBits, nonce, in, size, out);
}

int BdUtils::CAES::updateCtr(void *in, size_t size, uint8_t **out) {
    if (!in || !size || !out) {
        return EINVAL;
    }
    if (!_ctr) {
        // need to call the CTR constructor!
        return ECANCELED;
    }

    uint8_t* o;
    if (*out != nullptr) {
        // use provided
        o = *out;
    }
    else {
        // allocate output buffer
        *out = nullptr;
        o = (uint8_t*)CMemory::alloc(size);
        if (!o) {
            return ENOMEM;
        }

        // copy input to output, to be encrypted
        memcpy(o,in,size);
    }

    // process
    int res = mbedtls_aes_crypt_ctr(&_ctx, size, &_ctrOffset,_iv, _ctrStreamBlock, o, o);
    if (res != 0) {
        if (*out == nullptr) {
            // buffer was allocated
            CMemory::free(o);
        }
        return res;
    }

    // done
    *out = o;
    return 0;
}
