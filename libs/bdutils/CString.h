//
// Created by jrf/ht on 13/02/19.
//

#ifndef RCSLINUX_CSTRING_H
#define RCSLINUX_CSTRING_H

#include <stdint.h>
#include <stddef.h>

/**
 * BdUtils groups backdoor utilities framework
 */
namespace BdUtils {
    /**
     * @class CString
     * @brief implements generic string utilities
     * @note: paths are treated unix stile (with /), check if it's ok (should be) also on windows
     * @todo: for macos/ios, handle differences with posix in fromErrno() (uses gnu strerror_r())
     */
    class CString {
    public:
        /**
         * duplicates string
         * @param s the string
         * @return the new string, to be freed with CMemory::free(), or null on out of memory/wrong parameters
         */
        static char* dup(const char* s);

        /**
         * join 2 paths with a separator
         * @param path1 first part of the path. on successful return, path2 is appended
         * @param path1Size size of path1 buffer
         * @param path2 2nd part of the path
         * @return 0 on success, or errno
         */
        static int joinPath(char *path1, size_t path1Size, const char *path2);

        /**
         * join 2 paths with a separator, dynamically allocating the result
         * @param path1 first part of the path
         * @param path2 2nd part of the path
         * @param out on successful return the joined path, to be freed with CMemory::free()
         * @return 0 on success, or errno
         */
        static int joinPath(const char* path1, const char* path2, char** out);

        /**
         * strip last slash from string (if it ends with slash)
         * @param s the string
         */
        static void stripLastSlash(char *s);

        /**
         * strip last character from string, if matches
         * @param s the string
         * @param c the character to strip if found
         */
        static void stripLastChar(char *s, char c);

        /**
         * get meaningful error string from errno value
         * @param errnoVal errno
         * @param err on successful return, the error string
         * @param size size of err buffer
         * @note this version is thread safe (calls strerror_r())
         * @return 0 on success, or errno
         */
        static int fromErrno(int errnoVal, char *err, size_t size);

        /**
         * get meaningful error string from errno value
         * @param errnoVal errno
         * @note this version is not thread safe (calls strerror())
         * @return 0 on success, or errno
         */
        static const char* fromErrno(int errnoVal);

        /**
         * convert byte buffer to hexstring (0xaa,0xbb, 0xcc, 0xdd --> "aabbccdd")
         * @param in the byte buffer
         * @param inSize size of the byte buffer
         * @param hexStr on successful return, the hex string
         * @param size size of the hexStr buffer (must be at least inSize*2 + 1)
         * @return 0 on success, or errno
         */
        static int toHexString(const uint8_t *in, size_t inSize, char *hexStr, size_t size);

        /**
         * convert byte buffer to hexstring (0xaa,0xbb, 0xcc, 0xdd --> "aabbccdd")
         * @param in the byte buffer
         * @param inSize size of the byte buffer
         * @param hexStr on successful return, the allocated hex string to be freed with CMemory::free()
         * @return 0 on success, or errno
         */
        static int toHexString(const uint8_t *in, size_t inSize, char **hexStr);

        /**
         * convert hexstring to byte buffer ("aabbccdd" --> 0xaa,0xbb, 0xcc, 0xdd)
         * @param hexStr the hex string
         * @param out on successful return, the byte buffer
         * @param outSize size of the out buffer (must be at least strlen(hexStr) / 2)
         * @return 0 on success, or errno
         */
        static int fromHexString(const char *hexStr, uint8_t *out, size_t outSize);

        /**
         * convert hexstring to byte buffer ("aabbccdd" --> 0xaa,0xbb, 0xcc, 0xdd)
         * @param hexStr the hex string
         * @param out on successful return, the allocated byte buffer to be freed with CMemory::free()
         * @param outSize optional, on successful return size of the byte buffer
         * @return 0 on success, or errno
         */
        static int fromHexString(const char *hexStr, uint8_t **out, size_t *outSize = nullptr);

        /**
         * convert a string from utf8 to (sort-of) sized windows utf16 string
         * @param utf8Str the source utf8 string
         * @param utf16Str if points to null on input, on successful return it's the utf16 string to be freed with @ref CMemory::free()\n
         *  either it points to a provided buffer for the output string and the value pointed by size must be its size in bytes
         * @param size if contains 0 on input, on successful return it's the utf16 string size IN BYTES, including the double-0 terminator\n
         *  either it's the size of the input buffer, and on output it's the size of the converted string copied to the buffer pointed by utf16str
         * @note to copy the resulting string, use memcpy(dst, utf16str, dst, size), not strcpy()!!!
         * @note the allocated utf16 buffer is guaranteed to be zero-terminated
         * @note the utf16 string output has the following format:
         * ~~~
         * uint32_t size        # size of the following buffer in bytes, including the double-0 terminator
         * char16_t string[]
         * double-0 terminator
         * ~~~
         * @return 0 on success, or errno
         */
        static int toUtf16(const char *utf8Str, char16_t **utf16Str, size_t *size);

        /**
         * convert a string from windows utf16(sort-of) to utf8
         * @param utf16Str the source utf16 string
         * @param utf8Str on successful return, the utf8 string to be freed with @ref CMemory::free()
         * @param utf16Size optional, on return is the size in bytes of the utf16 buffer (including the double-0 terminator)
         * @note the input utf16 string must match the format as specified in @ref CString::toUtf16()
         * @return 0 on success, or errno
         */
        static int fromUtf16(const char16_t *utf16Str, char **utf8Str, size_t* utf16Size=nullptr);

        /**
         * expands environment variables in a string
         * @param str the input string, which may contain environment variables
         * @param out on successful return, the string with expanded environment variables, to be freed with @ref BdUtils::CMemory::free()
         * @note special environment variables are: %USERPROFILE% (home directory), $DIR$ (current working directory)\n
         *  either, the function tries to expand any ${something} found
         * @return 0 on success, or errno
         */
        static int expandEnvVars(const char* str, char**out);

        /**
         * get base path from full path (a/b/c -> a/b)
         * @param path the full path
         * @param out on successful return, the base path to be freed with @ref CMemory::free()
         * @note the returned path DOESN'T have slash in the end
         * @return 0 on success, or errno
         */
        static int basePath(const char* path, char** out);

        /**
         * get filename/last part from full path (a/b/c -> c)
         * @param path the full path
         * @return pointer to the last part of the path (may be the same as path if there's no separators)
         */
        static const char* filename(const char* path);

        /**
         * generates a random (hex) string
         * @param rndBytes number of random bytes to generate
         * @param str on successful return, filled with the random hex string
         * @param size size of the str buffer, must be at least (rndBytes * 2) + 1
         * @return 0 on success, or errno
         */
        static int randomString(int rndBytes, char* str, int size);

        /**
         * test code
         * @return 0 on success
         */
        static int test();
    private:
        static int expandEnvVarsInternal(char **src, char **out, int *allocSize, int *consumed);
    };
}

#endif //RCSLINUX_CSTRING_H
