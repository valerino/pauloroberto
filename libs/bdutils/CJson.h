//
// Created by jrf/ht on 13/02/19.
//

#ifndef RCSLINUX_CJSON_H
#define RCSLINUX_CJSON_H
#include <ArduinoJson.h>

/**
 * BdUtils groups backdoor utilities framework
 */
namespace BdUtils {
    /**
     * @class CJson
     * @brief json encoding/decoding utilities, based on ArduinoJson
     */
    class CJson {
    public:
        /**
         * loads json from string
         * @param txt a json string
         * @throws on invalid input
         */
        CJson(const char* txt);

        /**
         * initializes an empty json
         */
        CJson();

        virtual ~CJson();

        /**
         * loads json from file
         * @param path path to the file
         * @throws on invalid input
         */
        static CJson* loadFile(const char *path);

        /**
         * loads json from an existing object
         * @param js an existing JsonObject (makes a deep copy)
         * @throws on out of memory
         */
        static CJson* fromJsonObject(JsonObject& js);

        /**
         * serializes json to file
         * @param path path to the file
         * @param prettyPrint true to indent the output
         * @return 0 on success, or errno
         */
        int write(const char *path, bool prettyPrint=false);

        /**
         * serializes json to string
         * @param prettyPrint true to indent the output
         * @return the string, to be freed with CMemory::free()
         */
        char *toString(bool prettyPrint=false);

        /**
         * serializes json to string
         * @param js a node
         * @param prettyPrint true to indent the output
         * @return the string, to be freed with CMemory::free()
         */
        static char * toString(JsonObject& js,  bool prettyPrint=false);

        /**
         * get the underlying node
         * @note an instance of CJson refers to a single node, so the root node is simply a reference to the underlying JsonObject of THIS node
         * @return reference to the node
         */
        JsonObject& node();

        /**
         * shortcut to get a string from a node
         * @param n the node
         * @param name the value name
         * @param out if not null, returns a pointer to the underlying buffer stored in the json directly with no allocation
         * @return pointer to the string, to be freed with CMemory::free() (see notes), or null on error
         * @note if out is provided, the returned buffer MUST NOT be freed and it's the same as the pointer provided by out
         */
        static char * getStringValue(JsonObject &n, const char *name, const char** out=nullptr);

        /**
         * shortcut to get a string from a node
         * @param name the value name
         * @param out if not null, returns a pointer to the underlying buffer stored in the json directly with no allocation
         * @return pointer to the string, to be freed with CMemory::free() (see notes), or null on error
         * @note if out is provided, the returned buffer MUST NOT be freed and it's the same as the pointer provided by out
         */
        char * getStringValue(const char *name, const char** out=nullptr);

        /**
         * shortcut to get a node from a node
         * @param n the node
         * @param name the value name
         * @return reference to the node, or JsonObject::invalid()
         */
        static JsonObject& getNodeValue(JsonObject& n, const char* name);

        /**
         * shortcut to get a node from a node
         * @param name the value name
         * @return reference to the node, or JsonObject::invalid()
         */
        JsonObject& getNodeValue(const char* name);

        /**
         * shortcut to get an array from a node
         * @param n the node
         * @param name the value name
         * @return reference to the array, or JsonObject::invalid()
         */
        static JsonArray& getArrayValue(JsonObject& n, const char* name);

        /**
         * shortcut to get an array from a node
         * @param name the value name
         * @return reference to the array, or JsonObject::invalid()
         */
        JsonArray& getArrayValue(const char* name);

        /**
         * check if the json is valid by attempting to parse it
         * @param json the json string
         * @return true if valid
         */
        static bool isValid(const char* json);

        /**
         * returns the number of this node key/value pairs
         * @return int
         */
        int size();

        /**
         * returns the memory occupied by the internal JsonBuffer
         * @return int
         */
        int memorySize();

        /**
         * test code
         * @return
         */
        static int test();
    private:
        DynamicJsonBuffer _jsBuf = {0};
        JsonVariant _node;
    };
}

#endif //RCSLINUX_CJSON_H
