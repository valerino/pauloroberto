//
// Created by jrf/ht on 21/03/19.
//

#include "CFileMap.h"
#include <errno.h>
#include <string.h>

BdUtils::CFileMap::CFileMap() {
    fmem_init(&_fm);
}

int BdUtils::CFileMap::open(const char *flags) {
    if (!flags) {
        return EINVAL;
    }

    // open memory backed file
    FILE* f = fmem_open(&_fm, flags);
    if (!f) {
        return errno;
    }

    // set the underlying CFile file ptr
    setFilePtr(f);
    return 0;
}

void BdUtils::CFileMap::mem(uint8_t **m, size_t *s) {
    if(m && s) {
        // return buffer and size
        fmem_mem(&_fm, reinterpret_cast<void **>(m), s);
    }
}

BdUtils::CFileMap::~CFileMap() {
    // free memory, destructor will close the underlying file
    fmem_term(&_fm);
}

int BdUtils::CFileMap::test() {
#ifndef NDEBUG
    CFileMap fm;
    fm.open("w+b");
    char buf[] = "blablabla part 1 and";
    char buf2[] = " bliblibli part 2";
    fm.write((uint8_t *)buf, strlen(buf));
    fm.write((uint8_t *)buf2, strlen(buf2));
    fm.close();
    char* testBuf;
    size_t s;
    fm.mem((uint8_t**)&testBuf,&s);
    if (strcmp(testBuf,"blablabla part 1 and bliblibli part 2") == 0) {
        return 0;
    }
    return 1;
#endif
}
