//
// Created by jrf/ht on 20/02/19.
//

#include "CSHABase.h"
#include "CMemory.h"
#include "CFile.h"
#include "CString.h"
#include <errno.h>

using namespace BdUtilsPrivate;
using namespace BdUtils;

BdUtilsPrivate::CSHABase::CSHABase(void* ctx, SHA_UPDATE_FUNC updateFunc, SHA_FINISH_FUNC finishFunc, int shaSize) {
    _hashSize = shaSize;
    _ctx = ctx;
    _finishFunc = finishFunc;
    _updateFunc = updateFunc;
}

BdUtilsPrivate::CSHABase::~CSHABase() {
}

int BdUtilsPrivate::CSHABase::digest(uint8_t *buf, size_t size) {
    if (!buf) {
        return EINVAL;
    }
    if (size < _hashSize) {
        return EOVERFLOW;
    }

    // call the alloc version
    uint8_t* b;
    int res = digest(&b);
    if (res != 0) {
        return res;
    }
    memcpy(buf,b,_hashSize);
    CMemory::free(b);
    return 0;
}

int BdUtilsPrivate::CSHABase::digest(uint8_t **buf, size_t *size) {
    if (!buf) {
        return EINVAL;
    }
    *buf = nullptr;
    if (size) {
        *size = 0;
    }

    // allocate memory
    uint8_t* b = (uint8_t*)CMemory::alloc(_hashSize);
    if (!b) {
        return ENOMEM;
    }

    // calculate hash
    int res = _finishFunc(_ctx, b);
    *buf = b;
    if (size) {
        *size = _hashSize;
    }
    return res;
}

int BdUtilsPrivate::CSHABase::digestString(char *hexStr, size_t size) {
    if (!hexStr) {
        return EINVAL;
    }
    if (size < (_hashSize * 2) + 1) {
        return EOVERFLOW;
    }

    // call the alloc version
    char* s;
    int res = digestString(&s);
    if (res != 0) {
        return res;
    }

    strcpy(hexStr,s);
    CMemory::free(s);
    return 0;
}

int BdUtilsPrivate::CSHABase::digestString(char **hexStr) {
    if (!hexStr) {
        return EINVAL;
    }

    // calculate hash
    uint8_t* b;
    int res = digest(&b);
    if (res != 0) {
        return res;
    }

    // to hex string
    char* s;
    res = CString::toHexString(b, _hashSize, &s);
    CMemory::free(b);
    if (res != 0) {
        return res;
    }
    *hexStr = s;
    return 0;
}

int BdUtilsPrivate::CSHABase::update(const uint8_t *buf, size_t size) {
    if (!buf || !size) {
        return EINVAL;
    }
    int res = _updateFunc(_ctx, buf, size);
    return res;
}

/**
 * hashFile() callback
 * @param f
 * @param buf
 * @param size
 * @param offset
 * @param chunkNum
 * @param totalChunks
 * @param chunkSize
 * @param context
 * @see BdUtils::CFile::walkFile()
 * @return
 */
static int hashFileChunk(BdUtils::CFile* f, uint8_t* buf, size_t size, size_t offset, int chunkNum, int totalChunks, size_t chunkSize, void* context) {
    BdUtilsPrivate::CSHABase* b = (BdUtilsPrivate::CSHABase*)context;
    int res = b->update(buf, size);
    return res;
}

int BdUtilsPrivate::CSHABase::hashFile(const char *path, uint8_t *hash, size_t hashSize, CSHABase* impl) {
    if (!path || !hash) {
        return EINVAL;
    }

    // process file
    int res=0;
    do {
        // walk in chunksize steps
        res = CFile::walkFile(path, hashFileChunk, false, impl);
        if (res != 0) {
            break;
        }

        // hash
        res = impl->digest(hash, hashSize);
    } while (0);

    return res;
}

int BdUtilsPrivate::CSHABase::hashBuffer(const uint8_t *buf, size_t size, uint8_t *hash, size_t hashSize, CSHABase* impl) {
    if (!buf || !size || !hash) {
        return EINVAL;
    }

    int res=0;
    do {
        res = impl->update(buf, size);
        if (res != 0) {
            break;
        }

        // hash
        res = impl->digest(hash, hashSize);
    } while (0);
    return res;
}
