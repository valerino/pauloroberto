//
// Created by jrf/ht on 20/02/19.
//

#include "CDevice.h"
#include "obfuscate.h"
#include <errno.h>

int BdUtils::CDevice::uniqueId(uint8_t *id, size_t size) {
    if (!id || size < DEVICE_ID_SIZE) {
        return EINVAL;
    }
    // TODO: linux only implementation as now ...
    // read /etc/machine-id and hash it
    int res = CSHA256::hashFile(vxENCRYPT("/etc/machine-id"), id, size);
    return res;
}
